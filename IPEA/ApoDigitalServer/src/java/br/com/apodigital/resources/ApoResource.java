package br.com.apodigital.resources;

import br.com.apodigital.dao.ApoDao;
import br.com.apodigital.dao.AtributoDao;
import br.com.apodigital.dao.CategoriaDao;
import br.com.apodigital.dao.ComodoDao;
import br.com.apodigital.dao.ConceitoDao;
import br.com.apodigital.dao.ConnectionDao;
import br.com.apodigital.dao.MoradorDao;
import br.com.apodigital.dao.PerguntaDao;
import br.com.apodigital.dao.QualificadorDao;
import br.com.apodigital.dao.QuestionarioDao;
import br.com.apodigital.dao.RespostaDao;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import com.google.gson.Gson;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.ApoPacote;
import br.com.apodigital.model.Morador;
import br.com.apodigital.model.PacoteGambiarra;
import br.com.apodigital.model.Resposta;
import br.com.apodigital.model.RespostaPacote;
import br.com.apodigital.model.StatusResposta;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author pedro
 */
@Path("/apo")
public class ApoResource
{

   @GET
   @Path("/listarTodos")
   @Produces("application/json")
   public String listarTodos()
   {
      ArrayList<APO> apos = null;
      ApoDao apoDao = new ApoDao();
      Connection conn = null;
      System.out.println("Executando o web service de busca de avaliações...");
      try
      {
         conn = abrirConexao();
         apoDao.setConn(conn);
         apos = apoDao.buscarTodos();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      return apos.toString();
   }

   @GET
   @Path("/carregar/{id}")
   @Produces("application/json")
   public ApoPacote getApoCompleta(@PathParam("id") int id)
   {
      ApoPacote pacote = new ApoPacote();
      ApoDao apoDao = new ApoDao();
      QuestionarioDao questionarioDao = new QuestionarioDao();
      CategoriaDao categoriaDao = new CategoriaDao();
      PerguntaDao perguntaDao = new PerguntaDao();
      QualificadorDao qualificadorDao = new QualificadorDao();
      ConceitoDao conceitoDao = new ConceitoDao();
      ComodoDao comodoDao = new ComodoDao();
      AtributoDao atributoDao = new AtributoDao();
      Connection conn = null;

      APO apo = new APO();
      apo.setId(id);

      try
      {
         conn = abrirConexao();
         apoDao.setConn(conn);
         questionarioDao.setConn(conn);
         categoriaDao.setConn(conn);
         perguntaDao.setConn(conn);
         qualificadorDao.setConn(conn);
         conceitoDao.setConn(conn);
         comodoDao.setConn(conn);
         atributoDao.setConn(conn);

         pacote.setApo(apoDao.buscarPorId(id));
         pacote.setQuestionarios(questionarioDao.buscarPorApo(apo));
         pacote.setCategorias(categoriaDao.buscarPorApo(apo));
         pacote.setQuestionariosCategorias(categoriaDao.buscarAuxiliares(apo));
         pacote.setPerguntas(perguntaDao.buscarPorApo(apo));
         pacote.setCategoriasPerguntas(perguntaDao.buscarCategoriasPerguntas(apo));
         pacote.setPerguntasQualificadores(perguntaDao.buscarPerguntasQualificadores(apo));
         pacote.setPerguntasConceitos(perguntaDao.buscarPerguntasConceitos(apo));
         pacote.setPerguntasComodos(perguntaDao.buscarPerguntasComodos(apo));
         pacote.setPerguntasAtributos(perguntaDao.buscarPerguntasAtributos(apo));
         pacote.setQualificadores(qualificadorDao.buscarPorApo(apo));
         pacote.setConceitos(conceitoDao.buscarPorApo(apo));
         pacote.setComodos(comodoDao.buscarPorApo(apo));
         pacote.setAtributos(atributoDao.buscarPorApo(apo));

         pacote.setConceitosQualificadores(conceitoDao.buscarConceitoQualificador(apo));

         System.out.println("Busca Completa da APO: " + id);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         if (conn != null)
         {
            fecharConexao(conn);
         }
      }

      return pacote;
   }

   @GET
   @Path("/enviar/{respostas}")
   @Produces("application/json")
   public String getRespostas(@PathParam("respostas") String respostas)
   {
      Gson gson = new Gson();
      RespostaPacote rp = gson.fromJson(respostas, RespostaPacote.class);
      System.out.println("Número de respostas: " + rp.getRespostas().size());
      System.out.println("Número de moradores: " + rp.getMoradores().size());
      return "ok";
   }

   @POST
   @Path("/enviar_respostas")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces("application/json")
   public String receberRespostas(String respostaPacote)
   {
      StatusResposta sr = new StatusResposta();

      respostaPacote = respostaPacote.replace("\\", "");
      respostaPacote = respostaPacote.substring(5, respostaPacote.length() - 2);
      RespostaPacote rp = new RespostaPacote();
      rp = new Gson().fromJson(respostaPacote, RespostaPacote.class);
      System.out.println("Número de respostas: " + rp.getRespostas().size());
      System.out.println("Número de moradores: " + rp.getMoradores().size());

      Map<Integer, Integer> mapaMorador = new HashMap<Integer, Integer>();

      /*
       * Operações no banco de dados
       */
      MoradorDao moradorDao = new MoradorDao();
      RespostaDao respostaDao = new RespostaDao();
      Connection conn = null;
      try
      {
         conn = abrirConexao();
         moradorDao.setConn(conn);
         respostaDao.setConn(conn);
         
         /*
          * Salvando cada morador e povoando mapa com id do app android e
          * id gerado pela aplicação servidor
          */
         for(Morador m : rp.getMoradores())
         {
            int idGerado = moradorDao.inserir(m);
            mapaMorador.put(m.getId(), idGerado);
         }
         
         /*
          * Salvando respostas
          */
         for(Resposta r : rp.getRespostas())
         {
            /*
             * Resposta de um morador, então checa qual o id gerado
             * pelo banco de dados através do mapa. Caso o id seja 0
             * significa que foi uma resposta dada por um pesquisador,
             * portanto o valor 0 se mantém
             */
            if(r.getMorador().getId() > 0)
            {
               r.getMorador().setId(mapaMorador.get(r.getMorador().getId()));
            }
            respostaDao.salvar(r);
         }
         
         /*
          * Se chegou até aqui, então tudo ok com as inserções no banco de dados
          */
         sr.setOk(true);
      }
      catch(Exception e)
      {
         e.printStackTrace();
         abortarTransacao(conn);
         
         /*
          * Exceção foi pega, então deu pau
          */
         sr.setOk(false);
      }
      finally
      {
         fecharConexao(conn);
      }

      /*
       * Envia status da operação para o client Android
       */
      return new Gson().toJson(sr);
   }

   @POST
   @Path("/enviarRespostas")
   @Consumes(MediaType.APPLICATION_JSON)
   public Response capturarRespostas(RespostaPacote rp)
   {
      String result = "Resposta chegou";
      System.out.println("Número de respostas: " + rp.getRespostas().size());
      System.out.println("Número de moradores: " + rp.getMoradores().size());
      return Response.status(201).entity(result).build();
   }

   private Connection abrirConexao() throws SQLException
   {
      ConnectionDao connFactory = new ConnectionDao();
      Connection conn = connFactory.getConnection();
      conn.setAutoCommit(false);
      return conn;
   }

   private void fecharConexao(Connection conn)
   {
      try
      {
         conn.commit();
         conn.close();
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
   }

   private void abortarTransacao(Connection conn)
   {
      try
      {
         conn.rollback();
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
   }
}
