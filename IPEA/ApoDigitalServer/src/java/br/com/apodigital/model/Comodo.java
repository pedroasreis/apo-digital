package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro
 */
@XmlRootElement
public class Comodo
{

   private int id;
   private String nome;
   private Integer tipo;
   /*
    * Tipos de cômodos
    */
   public static final int EDIFICIO = 1;
   public static final int USO_COMUM = 2;
   public static final int UNIDADE = 3;

   public Comodo()
   {
   }

   @Override
   public boolean equals(Object obj)
   {
      return (nome.equals(((Comodo) obj).getNome()));
   }

   /**
    * @return the id
    */
   public int getId()
   {
      return id;
   }

   /**
    * @param id the id to set
    */
   public void setId(int id)
   {
      this.id = id;
   }

   /**
    * @return the nome
    */
   public String getNome()
   {
      return nome;
   }

   /**
    * @param nome the nome to set
    */
   public void setNome(String nome)
   {
      this.nome = nome;
   }

   /**
    * @return the tipo
    */
   public Integer getTipo()
   {
      return tipo;
   }

   /**
    * @param tipo the tipo to set
    */
   public void setTipo(Integer tipo)
   {
      this.tipo = tipo;
   }
}
