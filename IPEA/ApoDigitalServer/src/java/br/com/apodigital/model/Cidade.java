package br.com.apodigital.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro
 */
@XmlRootElement
public class Cidade implements Serializable {
    
    private int id;
    private String nome;

    @Override
    public boolean equals(Object obj) {
        return(id == ((Cidade)obj).getId());
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
