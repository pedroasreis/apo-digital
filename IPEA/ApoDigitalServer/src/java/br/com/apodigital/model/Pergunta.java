package br.com.apodigital.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class Pergunta implements Serializable {

    private int id;
    private String texto;
    private String comentario;
    private int tipo;
    private int ordem;
    private int escalaDeCores;

    public static final int TEXTO = 1;
    public static final int MULTIPLA_ESCOLHA = 2;
    public static final int ESCOLHA_UNICA = 3;
    public static final int INTERVALO_NUMERICO = 4;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     * @return the tipo
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the comentario
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * @param comentario the comentario to set
     */
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

   /**
    * @return the ordem
    */
   public int getOrdem()
   {
      return ordem;
   }

   /**
    * @param ordem the ordem to set
    */
   public void setOrdem(int ordem)
   {
      this.ordem = ordem;
   }

   /**
    * @return the escalaDeCores
    */
   public int getEscalaDeCores()
   {
      return escalaDeCores;
   }

   /**
    * @param escalaDeCores the escalaDeCores to set
    */
   public void setEscalaDeCores(int escalaDeCores)
   {
      this.escalaDeCores = escalaDeCores;
   }

}
