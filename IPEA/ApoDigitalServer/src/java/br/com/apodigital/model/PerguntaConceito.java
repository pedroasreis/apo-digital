package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class PerguntaConceito
{
   
   private int perguntaId;
   private int conceitoId;

   /**
    * @return the perguntaId
    */
   public int getPerguntaId()
   {
      return perguntaId;
   }

   /**
    * @param perguntaId the perguntaId to set
    */
   public void setPerguntaId(int perguntaId)
   {
      this.perguntaId = perguntaId;
   }

   /**
    * @return the conceitoId
    */
   public int getConceitoId()
   {
      return conceitoId;
   }

   /**
    * @param conceitoId the conceitoId to set
    */
   public void setConceitoId(int conceitoId)
   {
      this.conceitoId = conceitoId;
   }
   
}
