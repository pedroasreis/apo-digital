package br.com.apodigital.model;

import com.google.gson.JsonObject;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro
 */
@XmlRootElement
public class APO implements Serializable
{

   private int id;
   private String nome;
   private String texto;
   private String cidade;
   private String estado;

   public APO()
   {
   }

   @Override
   public String toString()
   {
      JsonObject jo = new JsonObject();
      jo.addProperty("id", id);
      jo.addProperty("nome", nome);
      jo.addProperty("texto", texto);
      jo.addProperty("cidade", cidade);
      jo.addProperty("estado", estado);
      return jo.toString();
   }

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getNome()
   {
      return nome;
   }

   public void setNome(String nome)
   {
      this.nome = nome;
   }

   public String getTexto()
   {
      return texto;
   }

   public void setTexto(String texto)
   {
      this.texto = texto;
   }

   /**
    * @return the cidade
    */
   public String getCidade()
   {
      return cidade;
   }

   /**
    * @param cidade the cidade to set
    */
   public void setCidade(String cidade)
   {
      this.cidade = cidade;
   }

   /**
    * @return the estado
    */
   public String getEstado()
   {
      return estado;
   }

   /**
    * @param estado the estado to set
    */
   public void setEstado(String estado)
   {
      this.estado = estado;
   }
}
