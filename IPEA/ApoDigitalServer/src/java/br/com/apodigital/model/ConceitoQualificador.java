package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class ConceitoQualificador
{
   
   private int conceitoId;
   private int perguntaId;
   private int qualificadorId;

   /**
    * @return the conceitoId
    */
   public int getConceitoId()
   {
      return conceitoId;
   }

   /**
    * @param conceitoId the conceitoId to set
    */
   public void setConceitoId(int conceitoId)
   {
      this.conceitoId = conceitoId;
   }

   /**
    * @return the perguntaId
    */
   public int getPerguntaId()
   {
      return perguntaId;
   }

   /**
    * @param perguntaId the perguntaId to set
    */
   public void setPerguntaId(int perguntaId)
   {
      this.perguntaId = perguntaId;
   }

   /**
    * @return the qualificadorId
    */
   public int getQualificadorId()
   {
      return qualificadorId;
   }

   /**
    * @param qualificadorId the qualificadorId to set
    */
   public void setQualificadorId(int qualificadorId)
   {
      this.qualificadorId = qualificadorId;
   }
   
}
