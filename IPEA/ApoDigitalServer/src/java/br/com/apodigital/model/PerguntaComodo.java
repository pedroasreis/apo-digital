package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class PerguntaComodo
{
   
   private int perguntaId;
   private int comodoId;

   /**
    * @return the perguntaId
    */
   public int getPerguntaId()
   {
      return perguntaId;
   }

   /**
    * @param perguntaId the perguntaId to set
    */
   public void setPerguntaId(int perguntaId)
   {
      this.perguntaId = perguntaId;
   }

   /**
    * @return the comodoId
    */
   public int getComodoId()
   {
      return comodoId;
   }

   /**
    * @param comodoId the comodoId to set
    */
   public void setComodoId(int comodoId)
   {
      this.comodoId = comodoId;
   }
   
}
