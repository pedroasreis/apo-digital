package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author pedro
 * Classe responsável por informar a aplicação android que as resposta enviadas
 * foram inseridas com sucesso
 */
@XmlRootElement
public class StatusResposta
{
   
   private boolean ok;

   /**
    * @return the erro
    */
   public boolean isOk()
   {
      return ok;
   }

   /**
    * @param erro the erro to set
    */
   public void setOk(boolean ok)
   {
      this.ok = ok;
   }
   
}
