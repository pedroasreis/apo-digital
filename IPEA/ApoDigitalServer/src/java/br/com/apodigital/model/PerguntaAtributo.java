package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class PerguntaAtributo
{
   
   private int perguntaId;
   private int atributoId;

   /**
    * @return the perguntaId
    */
   public int getPerguntaId()
   {
      return perguntaId;
   }

   /**
    * @param perguntaId the perguntaId to set
    */
   public void setPerguntaId(int perguntaId)
   {
      this.perguntaId = perguntaId;
   }

   /**
    * @return the atributoId
    */
   public int getAtributoId()
   {
      return atributoId;
   }

   /**
    * @param atributoId the atributoId to set
    */
   public void setAtributoId(int atributoId)
   {
      this.atributoId = atributoId;
   }
   
}
