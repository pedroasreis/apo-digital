package br.com.apodigital.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class PacoteGambiarra implements Serializable
{
   
   private RespostaPacote respostaPacote;

   /**
    * @return the respostaPacote
    */
   public RespostaPacote getRespostaPacote()
   {
      return respostaPacote;
   }

   /**
    * @param respostaPacote the respostaPacote to set
    */
   public void setRespostaPacote(RespostaPacote respostaPacote)
   {
      this.respostaPacote = respostaPacote;
   }
   
}
