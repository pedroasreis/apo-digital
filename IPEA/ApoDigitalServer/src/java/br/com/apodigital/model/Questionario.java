package br.com.apodigital.model;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class Questionario {

    private int id;
    private String nome;
    private String texto;
    private int respondente; //'1' para pesquisador e '2' para morador
    private ArrayList<Categoria> categorias;
    private int numCategorias;
    
    public final static int PESQUISADOR = 1;
    public final static int MORADOR = 2;

    public Questionario(){
        categorias = new ArrayList<Categoria>();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     * @return the perguntas
     */
    public ArrayList<Categoria> getCategorias() {
        return categorias;
    }

    /**
     * @param perguntas the perguntas to set
     */
    public void setCategorias(ArrayList<Categoria> categorias) {
        this.categorias = categorias;
    }

    /**
     * @return the numPerguntas
     */
    public int getNumCategorias() {
        return numCategorias;
    }

    /**
     * @param numPerguntas the numPerguntas to set
     */
    public void setNumCategorias(int numCategorias) {
        this.numCategorias = numCategorias;
    }

    /**
     * @return the respondente
     */
    public int getRespondente() {
        return respondente;
    }

    /**
     * @param respondente the respondente to set
     */
    public void setRespondente(int respondente) {
        this.respondente = respondente;
    }

}
