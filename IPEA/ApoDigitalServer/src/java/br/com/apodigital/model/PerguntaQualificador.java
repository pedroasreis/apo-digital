package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class PerguntaQualificador
{
   
   private int perguntaId;
   private int qualificadorId;

   /**
    * @return the perguntaId
    */
   public int getPerguntaId()
   {
      return perguntaId;
   }

   /**
    * @param perguntaId the perguntaId to set
    */
   public void setPerguntaId(int perguntaId)
   {
      this.perguntaId = perguntaId;
   }

   /**
    * @return the qualificadorId
    */
   public int getQualificadorId()
   {
      return qualificadorId;
   }

   /**
    * @param qualificadorId the qualificadorId to set
    */
   public void setQualificadorId(int qualificadorId)
   {
      this.qualificadorId = qualificadorId;
   }
   
}
