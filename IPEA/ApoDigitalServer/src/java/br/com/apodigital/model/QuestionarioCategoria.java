package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@XmlRootElement
public class QuestionarioCategoria
{
   
   private int questionarioId;
   private int categoriaId;

   /**
    * @return the questionarioId
    */
   public int getQuestionarioId()
   {
      return questionarioId;
   }

   /**
    * @param questionarioId the questionarioId to set
    */
   public void setQuestionarioId(int questionarioId)
   {
      this.questionarioId = questionarioId;
   }

   /**
    * @return the categoriaId
    */
   public int getCategoriaId()
   {
      return categoriaId;
   }

   /**
    * @param categoriaId the categoriaId to set
    */
   public void setCategoriaId(int categoriaId)
   {
      this.categoriaId = categoriaId;
   }
   
}
