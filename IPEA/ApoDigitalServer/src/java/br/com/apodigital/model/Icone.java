package br.com.apodigital.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro
 */
@XmlRootElement
public class Icone {
    
    private int id;
    private String titulo;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
}
