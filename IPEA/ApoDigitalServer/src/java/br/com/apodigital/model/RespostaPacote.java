package br.com.apodigital.model;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RespostaPacote implements Serializable
{

   private ArrayList<Resposta> respostas;
   private ArrayList<Morador> moradores;
   
   private int codigo;

   public ArrayList<Resposta> getRespostas()
   {
      return respostas;
   }

   public void setRespostas(ArrayList<Resposta> respostas)
   {
      this.respostas = respostas;
   }

   public ArrayList<Morador> getMoradores()
   {
      return moradores;
   }

   public void setMoradores(ArrayList<Morador> moradores)
   {
      this.moradores = moradores;
   }

   /**
    * @return the codigo
    */
   public int getCodigo()
   {
      return codigo;
   }

   /**
    * @param codigo the codigo to set
    */
   public void setCodigo(int codigo)
   {
      this.codigo = codigo;
   }
}
