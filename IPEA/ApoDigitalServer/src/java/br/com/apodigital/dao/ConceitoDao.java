package br.com.apodigital.dao;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Conceito;
import br.com.apodigital.model.ConceitoQualificador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class ConceitoDao extends DaoGeral
{
   
   public ArrayList<Conceito> buscarPorApo(APO apo) throws SQLException
   {
      ArrayList<Conceito> cs = new ArrayList<Conceito>();
      String sql = "select c.id, c.nome, c.ordem from conceito c, conceito_pergunta pcs, "
              + "pergunta_categoria pc, categoria_questionario cq, questionario_avaliacao qa "
              + "where c.id = pcs.conceito_id and pcs.pergunta_id = pc.pergunta_id and "
              + "pc.categoria_id = cq.categoria_id and cq.questionario_id = qa.questionario_id and "
              + "qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         cs.add(preencher(rs));
      }
      rs.close();
      pst.close();
      return cs;
   }
   
   public ArrayList<ConceitoQualificador> buscarConceitoQualificador(APO apo) throws SQLException
   {
      ArrayList<ConceitoQualificador> cq = new ArrayList<ConceitoQualificador>();
      String sql = "select distinct cq.conceito_id, cq.pergunta_id, cq.qualificador_id from conceito_qualificador cq, "
              + "pergunta_categoria pc, categoria_questionario cqt, questionario_avaliacao qa where "
              + "qa.avaliacao_id = ? and qa.questionario_id = cqt.questionario_id and cqt.categoria_id = "
              + "pc.categoria_id and pc.pergunta_id = cq.pergunta_id";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         cq.add(preencherConceitoQualificador(rs));
      }
      rs.close();
      pst.close();
      return cq;
   }
   
   private Conceito preencher(ResultSet rs) throws SQLException
   {
      Conceito c = new Conceito();
      c.setId(rs.getInt(1));
      c.setNome(rs.getString(2));
      c.setOrdem(rs.getInt(3));
      return c;
   }
   
   private ConceitoQualificador preencherConceitoQualificador(ResultSet rs) throws SQLException
   {
      ConceitoQualificador cq = new ConceitoQualificador();
      cq.setConceitoId(rs.getInt(1));
      cq.setPerguntaId(rs.getInt(2));
      cq.setQualificadorId(rs.getInt(3));
      return cq;
   }
   
}
