package br.com.apodigital.dao;

import br.com.apodigital.model.APO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class ApoDao extends DaoGeral
{
   
   public ArrayList<APO> buscarTodos() throws SQLException
   {
      ArrayList<APO> apos = new ArrayList<APO>();
      String sql = "select * from avaliacao;";
      PreparedStatement pst = conn.prepareStatement(sql);
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         APO apo = preencher(rs);
         apos.add(apo);
      }
      return apos;
   }
   
   public APO buscarPorId(int id) throws SQLException
   {
      APO apo = null;
      String sql = "select id, nome, texto, cidade, estado from avaliacao where "
              + "id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, id);
      ResultSet rs = pst.executeQuery();
      if(rs.next())
      {
         apo = preencher(rs);
      }
      rs.close();
      pst.close();
      return apo;
   }
   
   private APO preencher(ResultSet rs) throws SQLException
   {
      APO apo = new APO();
      apo.setId(rs.getInt("id"));
      apo.setNome(rs.getString("nome"));
      apo.setTexto(rs.getString("texto"));
      apo.setCidade(rs.getString("cidade"));
      apo.setEstado(rs.getString("estado"));
      return apo;
   }
   
}
