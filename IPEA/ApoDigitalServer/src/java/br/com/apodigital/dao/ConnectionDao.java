package br.com.apodigital.dao;

/**
 *
 * @author pedro
 */
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionDao
{

   public Connection getConnection()
   {
      try
      {
         InitialContext initialContext = new InitialContext();
         DataSource dataSource = (DataSource) initialContext.lookup("apo");
         return dataSource.getConnection();
      }
      catch (NamingException ex)
      {
         Logger.getLogger(ConnectionDao.class.getName()).log(Level.SEVERE, null, ex);
      }
      catch (SQLException ex)
      {
         Logger.getLogger(ConnectionDao.class.getName()).log(Level.SEVERE, null, ex);
      }
      return null;
   }
}
