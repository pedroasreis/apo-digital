package br.com.apodigital.dao;

import br.com.apodigital.model.Morador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author pedro
 */
public class MoradorDao
{
   
   private Connection conn;
    
    public int inserir(Morador m) throws SQLException{
        int id = 0;
        String sql = "insert into morador values(null, ?, ?, ?, null, ?, null, null, null, null, null, null)";
        PreparedStatement pst = getConn().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        pst.setInt(1, m.getApo().getId());
        pst.setString(2, m.getBloco());
        pst.setString(3, m.getNumApartamento());
        pst.setString(4, m.getTempoResposta());
        pst.executeUpdate();
        /*
        Recuperando o id gerado para esse registro
        */
        ResultSet rs = pst.getGeneratedKeys();
        if(rs.next()){
            id = rs.getInt(1);
        }
        rs.close();
        pst.close();
        return id;
    }

   /**
    * @return the conn
    */
   public Connection getConn()
   {
      return conn;
   }

   /**
    * @param conn the conn to set
    */
   public void setConn(Connection conn)
   {
      this.conn = conn;
   }
   
}
