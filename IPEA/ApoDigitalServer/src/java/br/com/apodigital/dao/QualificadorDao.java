package br.com.apodigital.dao;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Qualificador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class QualificadorDao extends DaoGeral
{
   
   public ArrayList<Qualificador> buscarPorApo(APO apo) throws SQLException
   {
      ArrayList<Qualificador> qs = new ArrayList<Qualificador>();
      String sql = "select q.id, q.texto, q.inicio, q.fim from qualificador q, "
              + "pergunta_qualificador pq, pergunta_categoria pc, categoria_questionario cq, "
              + "questionario_avaliacao qa where q.id = pq.qualificador_id and pq.pergunta_id = "
              + "pc.pergunta_id and pc.categoria_id = cq.categoria_id and cq.questionario_id = "
              + "qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         qs.add(preencher(rs));
      }
      rs.close();
      pst.close();
      return qs;
   }
   
   private Qualificador preencher(ResultSet rs) throws SQLException
   {
      Qualificador q = new Qualificador();
      q.setId(rs.getInt(1));
      q.setTexto(rs.getString(2));
      q.setInicio(rs.getInt(3));
      q.setFim(rs.getInt(4));
      return q;
   }
   
}
