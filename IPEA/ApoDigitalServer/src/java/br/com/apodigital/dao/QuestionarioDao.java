package br.com.apodigital.dao;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Questionario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class QuestionarioDao extends DaoGeral
{
   
   public ArrayList<Questionario> buscarPorApo(APO apo) throws SQLException
   {
      ArrayList<Questionario> qs = new ArrayList<Questionario>();
      String sql = "select q.id, q.nome, q.texto, q.respondente from questionario q, "
              + "questionario_avaliacao qa where qa.questionario_id = q.id and "
              + "qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next()){
         qs.add(preencher(rs));
      }
      rs.close();
      pst.close();
      return qs;
   }
   
   private Questionario preencher(ResultSet rs) throws SQLException
   {
      Questionario q = new Questionario();
      q.setId(rs.getInt(1));
      q.setNome(rs.getString(2));
      q.setTexto(rs.getString(3));
      q.setRespondente(rs.getInt(4));
      return q;
   }
   
}
