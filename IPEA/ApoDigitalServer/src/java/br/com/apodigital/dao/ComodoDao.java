package br.com.apodigital.dao;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Comodo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class ComodoDao extends DaoGeral
{
   
   public ArrayList<Comodo> buscarPorApo(APO apo) throws SQLException
   {
      ArrayList<Comodo> cs = new ArrayList<Comodo>();
      String sql = "select c.id, c.nome, c.tipo from comodo c, pergunta_comodo pcs, pergunta_categoria pc, "
              + "categoria_questionario cq, questionario_avaliacao qa where c.id = pcs.comodo_id and "
              + "pcs.pergunta_id = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         cs.add(preencher(rs));
      }
      rs.close();
      pst.close();
      return cs;
   }
   
   private Comodo preencher(ResultSet rs) throws SQLException
   {
      Comodo c = new Comodo();
      c.setId(rs.getInt(1));
      c.setNome(rs.getString(2));
      c.setTipo(rs.getInt(3));
      return c;
   }
   
}
