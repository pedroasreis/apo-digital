package br.com.apodigital.dao;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.CategoriaPergunta;
import br.com.apodigital.model.Pergunta;
import br.com.apodigital.model.PerguntaAtributo;
import br.com.apodigital.model.PerguntaComodo;
import br.com.apodigital.model.PerguntaConceito;
import br.com.apodigital.model.PerguntaQualificador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class PerguntaDao extends DaoGeral
{
   
   public ArrayList<Pergunta> buscarPorApo(APO apo) throws SQLException
   {
      ArrayList<Pergunta> ps = new ArrayList<Pergunta>();
      String sql = "select p.id, p.texto, p.comentario, p.tipo, p.icone_id, p.ordem from pergunta p, "
              + "pergunta_categoria pc, categoria_questionario cq, "
              + "questionario_avaliacao qa where p.id = pc.pergunta_id and "
              + "pc.categoria_id = cq.categoria_id and cq.questionario_id = qa.questionario_id and "
              + "qa.avaliacao_id = ? order by p.id";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         ps.add(preencher(rs));
      }
      rs.close();
      pst.close();
      return ps;
   }
   
   public ArrayList<CategoriaPergunta> buscarCategoriasPerguntas(APO apo) throws SQLException
   {
      ArrayList<CategoriaPergunta> cps = new ArrayList<CategoriaPergunta>();
      String sql = "select pc.categoria_id, pc.pergunta_id from pergunta_categoria pc, "
              + "categoria_questionario cq, questionario_avaliacao qa where "
              + "pc.categoria_id = cq.categoria_id and cq.questionario_id = "
              + "qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         CategoriaPergunta cp = new CategoriaPergunta();
         cp.setCategoriaId(rs.getInt(1));
         cp.setPerguntaId(rs.getInt(2));
         cps.add(cp);
      }
      rs.close();
      pst.close();
      return cps;
   }
   
   public ArrayList<PerguntaQualificador> buscarPerguntasQualificadores(APO apo) throws SQLException
   {
      ArrayList<PerguntaQualificador> pqs = new ArrayList<PerguntaQualificador>();
      String sql = "select pq.pergunta_id, pq.qualificador_id from pergunta_qualificador pq, "
              + "pergunta_categoria pc, categoria_questionario cq, questionario_avaliacao qa where "
              + "pq.pergunta_id = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next()){
         PerguntaQualificador pq = new PerguntaQualificador();
         pq.setPerguntaId(rs.getInt(1));
         pq.setQualificadorId(rs.getInt(2));
         pqs.add(pq);
      }
      rs.close();
      pst.close();
      return pqs;
   }
   
   public ArrayList<PerguntaConceito> buscarPerguntasConceitos(APO apo) throws SQLException
   {
      ArrayList<PerguntaConceito> pcs = new ArrayList<PerguntaConceito>();
      String sql = "select pcs.pergunta_id, pcs.conceito_id from conceito_pergunta pcs, "
              + "pergunta_categoria pc, categoria_questionario cq, questionario_avaliacao qa "
              + "where pcs.pergunta_id = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         PerguntaConceito pc = new PerguntaConceito();
         pc.setPerguntaId(rs.getInt(1));
         pc.setConceitoId(rs.getInt(2));
         pcs.add(pc);
      }
      rs.close();
      pst.close();
      return pcs;
   }
   
   public ArrayList<PerguntaComodo> buscarPerguntasComodos(APO apo) throws SQLException
   {
      ArrayList<PerguntaComodo> pcs = new ArrayList<PerguntaComodo>();
      String sql = "select pcs.pergunta_id, pcs.comodo_id from pergunta_comodo pcs, "
              + "pergunta_categoria pc, categoria_questionario cq, questionario_avaliacao qa "
              + "where pcs.pergunta_id = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         PerguntaComodo pc = new PerguntaComodo();
         pc.setPerguntaId(rs.getInt(1));
         pc.setComodoId(rs.getInt(2));
         pcs.add(pc);
      }
      rs.close();
      pst.close();
      return pcs;
   }
   
   public ArrayList<PerguntaAtributo> buscarPerguntasAtributos(APO apo) throws SQLException
   {
      ArrayList<PerguntaAtributo> pas = new ArrayList<PerguntaAtributo>();
      String sql = "select pa.id_pergunta, pa.id_atributo from pergunta_atributo pa, "
              + "pergunta_categoria pc, categoria_questionario cq, questionario_avaliacao qa "
              + "where pa.id_pergunta = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         PerguntaAtributo pa = new PerguntaAtributo();
         pa.setPerguntaId(rs.getInt(1));
         pa.setAtributoId(rs.getInt(2));
         pas.add(pa);
      }
      rs.close();
      pst.close();
      return pas;
   }
   
   private Pergunta preencher(ResultSet rs) throws SQLException
   {
      Pergunta p = new Pergunta();
      p.setId(rs.getInt(1));
      p.setTexto(rs.getString(2));
      p.setComentario(rs.getString(3));
      p.setTipo(rs.getInt(4));
      p.setEscalaDeCores(rs.getInt(5));
      p.setOrdem(rs.getInt(6));
      return p;
   }
   
}
