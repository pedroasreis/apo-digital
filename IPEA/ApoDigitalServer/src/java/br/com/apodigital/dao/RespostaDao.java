package br.com.apodigital.dao;

import br.com.apodigital.model.Resposta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author pedro
 */
public class RespostaDao
{

   private Connection conn;

   public void salvar(Resposta r) throws SQLException
   {
      String sql = "insert into resposta values(null, ?, ?, ?, ?, ?, ?, ?, ?)";
      PreparedStatement pst = getConn().prepareStatement(sql);
      pst.setInt(1, r.getMorador().getId());
      pst.setInt(2, r.getPergunta().getId());
      pst.setString(3, r.getTexto());
      pst.setInt(4, r.getApo().getId());
      pst.setInt(5, r.getComodo().getId());
      pst.setInt(6, r.getConceito().getId());
      pst.setInt(7, 0); // valor para a instância de um cômodo, esse atributo atualmente está depreciado na aplicação
      pst.setInt(8, r.getAtributo().getId());
      pst.executeUpdate();
      pst.close();
   }

   /**
    * @return the conn
    */
   public Connection getConn()
   {
      return conn;
   }

   /**
    * @param conn the conn to set
    */
   public void setConn(Connection conn)
   {
      this.conn = conn;
   }
}
