package br.com.apodigital.dao;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Categoria;
import br.com.apodigital.model.QuestionarioCategoria;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class CategoriaDao extends DaoGeral
{
   
   public ArrayList<Categoria> buscarPorApo(APO apo) throws SQLException
   {
      ArrayList<Categoria> cs = new ArrayList<Categoria>();
      String sql = "select c.id, c.nome, c.texto from categoria c, categoria_questionario cq, "
              + "questionario_avaliacao qa where c.id = cq.categoria_id and cq.questionario_id = "
              + "qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         cs.add(preencher(rs));
      }
      rs.close();
      pst.close();
      return cs;
   }
   
   public ArrayList<QuestionarioCategoria> buscarAuxiliares(APO apo) throws SQLException
   {
      ArrayList<QuestionarioCategoria> qcs = new ArrayList<QuestionarioCategoria>();
      String sql = "select cq.questionario_id, cq.categoria_id from categoria_questionario cq, "
              + "questionario_avaliacao qa where cq.questionario_id = qa.questionario_id and "
              + "qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         QuestionarioCategoria qc = new QuestionarioCategoria();
         qc.setQuestionarioId(rs.getInt(1));
         qc.setCategoriaId(rs.getInt(2));
         qcs.add(qc);
      }
      rs.close();
      pst.close();
      return qcs;
   }
   
   public Categoria preencher(ResultSet rs) throws SQLException
   {
      Categoria c = new Categoria();
      c.setId(rs.getInt(1));
      c.setNome(rs.getString(2));
      c.setTexto(rs.getString(3));
      return c;
   }
   
}
