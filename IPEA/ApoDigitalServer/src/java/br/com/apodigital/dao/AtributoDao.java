package br.com.apodigital.dao;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Atributo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class AtributoDao extends DaoGeral
{
   
   public ArrayList<Atributo> buscarPorApo(APO apo) throws SQLException
   {
      ArrayList<Atributo> as = new ArrayList<Atributo>();
      String sql = "select a.id, a.nome from atributo a, pergunta_atributo pa, pergunta_categoria pc, "
              + "categoria_questionario cq, questionario_avaliacao qa where a.id = pa.id_atributo and "
              + "pa.id_pergunta = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while(rs.next())
      {
         as.add(preencher(rs));
      }
      rs.close();
      pst.close();
      return as;
   }
   
   private Atributo preencher(ResultSet rs) throws SQLException
   {
      Atributo a = new Atributo();
      a.setId(rs.getInt(1));
      a.setNome(rs.getString(2));
      return a;
   }
   
}
