package br.com.apodigital.dao;

import java.sql.Connection;

/**
 *
 * @author pedro
 */
public class DaoGeral
{
   
   protected Connection conn;

   /**
    * @return the conn
    */
   public Connection getConn()
   {
      return conn;
   }

   /**
    * @param conn the conn to set
    */
   public void setConn(Connection conn)
   {
      this.conn = conn;
   }
   
}
