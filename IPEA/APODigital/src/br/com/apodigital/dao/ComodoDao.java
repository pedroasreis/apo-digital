package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.Comodo;
import br.com.apodigital.model.Pergunta;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class ComodoDao extends DaoGeral {
	
	public ComodoDao(Context context) {
		super(context);
	}

	public static final String CREATE_COMODO = "create table comodo(" +
			"id int, nome varchar(100), tipo int, primary key(id));";
	
	public void insertComodo(Comodo c){
		ContentValues cv = new ContentValues();
		cv.put("id", c.getId());
		cv.put("nome", c.getNome());
		cv.put("tipo", c.getTipo());
		
		db.insert("comodo", null, cv);
	}
	
	public ArrayList<Comodo> buscarPorPergunta(Pergunta p){
		ArrayList<Comodo> comodos = new ArrayList<Comodo>();
		String query = "select c.id, c.nome, c.tipo from comodo c, pergunta_comodo pc " +
				"where c.id = pc.comodo_id and pc.pergunta_id = ?";
		
		String[] params = {p.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			comodos.add(preencherComodo(cursor));
		}
		
		cursor.close();
		return comodos;
	}
	
	private Comodo preencherComodo(Cursor cursor){
		Comodo c = new Comodo();
		c.setId(cursor.getInt(0));
		c.setNome(cursor.getString(1));
		c.setTipo(cursor.getInt(2));
		return c;
	}

}
