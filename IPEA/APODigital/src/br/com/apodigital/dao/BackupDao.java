package br.com.apodigital.dao;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import android.content.Context;
import android.os.Environment;

public class BackupDao {
	private String appName = "APO Digital";
	private String packageName = "br.com.apodigital";

	public boolean backup() {
		boolean rc = false;

		boolean writeable = isSDCardWriteable();
		if (writeable) {
			File file = new File(Environment.getDataDirectory() + "/data/"
					+ packageName + "/databases/"
					+ "apo.db");

			File fileBackupDir = new File(
					Environment.getExternalStorageDirectory(), appName
							+ "/backup");
			if (!fileBackupDir.exists()) {
				fileBackupDir.mkdirs();
			}

			if (file.exists()) {
				File fileBackup = new File(fileBackupDir,
						"apo.db");
				try {
					fileBackup.createNewFile();
					FileUtils.copyFile(file, fileBackup);
					rc = true;
				} catch (IOException ioException) {
					ioException.printStackTrace();
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
		}

		return rc;
	}

	private boolean isSDCardWriteable() {
		boolean rc = false;

		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			rc = true;
		}

		return rc;
	}

	public void MyDatabaseTools(final Context context, final String appName) {
		this.appName = appName;
		packageName = context.getPackageName();
	}
}