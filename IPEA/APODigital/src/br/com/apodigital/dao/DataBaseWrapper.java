package br.com.apodigital.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseWrapper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "apo.db";
	private static final int DATABASE_VERSION = 8;

	public DataBaseWrapper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		/*
		 * Criando a concatenação de strings que representa o script de criação de
		 * todas as tabelas da base de dados
		 */
		String createCmd = ApoDao.CREATE_APO + " " + TecnicaDao.CREATE_TECNICA + " " +
		 TecnicaDao.CREATE_APO_TECNICA + " " + CategoriaDao.CREATE_CATEGORIA + " " +
				CategoriaDao.CREATE_TECNICA_CATEGORIA + " " + PerguntaDao.CREATE_PERGUNTA + " " + PerguntaDao.CREATE_PERGUNTA_CATEGORIA + " " + 
		 QualificadorDao.CREATE_QUALIFICADOR + " " + ConceitoDao.CREATE_CONCEITO + " " +
				ComodoDao.CREATE_COMODO + " " + AtributoDao.CREATE_ATRIBUTO + " " + 
		 PerguntaDao.CREATE_PERGUNTA_ATRIBUTO + " " + PerguntaDao.CREATE_CONCEITO_PERGUNTA + " " +
				PerguntaDao.CREATE_PERGUNTA_QUALIFICADOR + " " + PerguntaDao.CREATE_PERGUNTA_COMODO + " " + 
		 MoradorDao.CREATE_MORADOR + " " + RespostaDao.CREATE_RESPOSTA;
		
		Log.i("cmd", createCmd);
		
		db.execSQL(ApoDao.CREATE_APO);
		db.execSQL(TecnicaDao.CREATE_TECNICA);
		db.execSQL(TecnicaDao.CREATE_APO_TECNICA);
		db.execSQL(CategoriaDao.CREATE_CATEGORIA);
		db.execSQL(CategoriaDao.CREATE_TECNICA_CATEGORIA);
		db.execSQL(PerguntaDao.CREATE_PERGUNTA);
		db.execSQL(PerguntaDao.CREATE_PERGUNTA_CATEGORIA);
		db.execSQL(QualificadorDao.CREATE_QUALIFICADOR);
		db.execSQL(ConceitoDao.CREATE_CONCEITO);
		db.execSQL(ComodoDao.CREATE_COMODO);
		db.execSQL(AtributoDao.CREATE_ATRIBUTO);
		db.execSQL(PerguntaDao.CREATE_PERGUNTA_QUALIFICADOR);
		db.execSQL(PerguntaDao.CREATE_CONCEITO_PERGUNTA);
		db.execSQL(PerguntaDao.CREATE_PERGUNTA_COMODO);
		db.execSQL(PerguntaDao.CREATE_PERGUNTA_ATRIBUTO);
		db.execSQL(MoradorDao.CREATE_MORADOR);
		db.execSQL(RespostaDao.CREATE_RESPOSTA);
		db.execSQL(ConceitoDao.CREATE_CONCEITO_QUALIFICADOR);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion < 5){
			db.execSQL("alter table morador add sincronizado int;");
			db.execSQL("alter table resposta add sincronizado int;");
		}
		if(oldVersion < 6){
			db.execSQL("update morador set sincronizado = 0 where sincronizado is null");
		}
		if(oldVersion < 7){
			db.execSQL("update resposta set sincronizado = 0 where sincronizado is null;");
		}
		db.execSQL("update morador set sincronizado = 0;");
	}

}
