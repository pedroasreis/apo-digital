package br.com.apodigital.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import br.com.apodigital.model.APO;

public class ApoDao extends DaoGeral {

	/*
	 * Constantes que representam o nome da tabela e
	 * suas colunas
	 */
	public static final String APO = "apo";
	public static final String ID = "id";
	public static final String NOME = "nome";
	public static final String TEXTO = "texto";
	public static final String CIDADE = "cidade";
	public static final String ESTADO = "estado";
	
	/*
	 * Constante para o array de colunas necessário para consultas no SQLite
	 */
	private static final String[] APO_COLUMNS = {ID, NOME, TEXTO, CIDADE, ESTADO}; 
	
	/*
	 * Create table
	 */
	public static final String CREATE_APO = "create table " + APO + " (id int, nome varchar(100), " +
			"texto varchar(300), cidade varchar(100), estado varchar(30), primary key(id));";
	
	public ApoDao(Context context) {
		super(context);
	}
	
	public void inserir(APO apo){
		ContentValues cv = new ContentValues();
		cv.put(ID, apo.getId());
		cv.put(NOME, apo.getNome());
		cv.put(TEXTO, apo.getTexto());
		cv.put(CIDADE, apo.getCidade());
		cv.put(ESTADO, apo.getEstado());
		
		db.insert("apo", null, cv);
	}
	
	public APO buscarPorId(int id){
		Cursor cursor = db.query(APO, APO_COLUMNS, ID + " = " + id, null, null, null, null);
		cursor.moveToFirst();
		APO apo = preencher(cursor);
		cursor.close();
		return apo;
	}
	
	public ArrayList<APO> buscarTodos(){
		ArrayList<APO> apos = new ArrayList<APO>();
		Cursor cursor = db.query(APO, APO_COLUMNS, "", null, null, null, null);
		while(cursor.moveToNext()){
			apos.add(preencher(cursor));
		}
		cursor.close();
		return apos;
	}
	
	private APO preencher(Cursor cursor){
		APO apo = new APO();
		apo.setId(cursor.getInt(0));
		apo.setNome(cursor.getString(1));
		apo.setTexto(cursor.getString(2));
		apo.setCidade(cursor.getString(3));
		apo.setEstado(cursor.getString(4));
		return apo;
	}

}










