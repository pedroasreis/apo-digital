package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.Atributo;
import br.com.apodigital.model.Conceito;
import br.com.apodigital.model.Pergunta;
import br.com.apodigital.model.Qualificador;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class QualificadorDao extends DaoGeral {
	
	public QualificadorDao(Context context) {
		super(context);
	}

	public static final String CREATE_QUALIFICADOR = "create table qualificador(" +
			"id int, texto varchar(255), inicio int, fim int, primary key(id));";
	
	public void insertQualificador(Qualificador q){
		ContentValues cv = new ContentValues();
		cv.put("id", q.getId());
		cv.put("texto", q.getTexto());
		cv.put("inicio", q.getInicio());
		cv.put("fim", q.getFim());
		
		db.insert("qualificador", null, cv);
	}
	
	public ArrayList<Qualificador> buscarPorPergunta(Pergunta p){
		ArrayList<Qualificador> qualificadores = new ArrayList<Qualificador>();
		String query = "select q.id, q.texto, q.inicio, q.fim from qualificador q, " +
				"pergunta_qualificador pq where q.id = pq.qualificador_id and " +
				"pq.pergunta_id = ?";
		
		String[] params = {p.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			qualificadores.add(preencherQualificador(cursor));
		}
		
		cursor.close();
		return qualificadores;
	}
	
	public ArrayList<Qualificador> buscarPorConceitoPergunta(Conceito c, Pergunta p){
		ArrayList<Qualificador> qualificadores = new ArrayList<Qualificador>();
		String query = "select q.id, q.texto, q.inicio, q.fim from qualificador q, " +
				"conceito_qualificador cq where q.id = cq.qualificador_id and " +
				"cq.pergunta_id = ? and cq.conceito_id = ?";
		
		String[] params = {p.getId() + "", c.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			qualificadores.add(preencherQualificador(cursor));
		}
		
		cursor.close();
		return qualificadores;
	}
	
	public ArrayList<Qualificador> buscarPorAtributoPergunta(Atributo a, Pergunta p){
		ArrayList<Qualificador> qualificadores = new ArrayList<Qualificador>();
		String query = "select q.id, q.texto, q.inicio, q.fim from qualificador q, " +
				"conceito_qualificador cq where q.id = cq.qualificador_id and " +
				"cq.pergunta_id = ? and cq.conceito_id = ?";
		
		String[] params = {p.getId() + "", a.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			qualificadores.add(preencherQualificador(cursor));
		}
		
		cursor.close();
		return qualificadores;
	}
	
	private Qualificador preencherQualificador(Cursor cursor){
		Qualificador q = new Qualificador();
		q.setId(cursor.getInt(0));
		q.setTexto(cursor.getString(1));
		q.setInicio(cursor.getInt(2));
		q.setFim(cursor.getInt(3));
		return q;
	}

}
