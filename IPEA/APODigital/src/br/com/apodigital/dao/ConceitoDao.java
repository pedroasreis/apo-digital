package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.Conceito;
import br.com.apodigital.model.ConceitoQualificador;
import br.com.apodigital.model.Pergunta;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class ConceitoDao extends DaoGeral {
	
	public ConceitoDao(Context context) {
		super(context);
	}

	public static final String CREATE_CONCEITO = "create table conceito(" +
			"id int, nome varchar(100), ordem int, primary key(id));";
	
	public static final String CREATE_CONCEITO_QUALIFICADOR = "create table conceito_qualificador(" +
			"conceito_id int, pergunta_id int, qualificador_id int, " +
			"primary key(conceito_id, pergunta_id, qualificador_id));";
	
	public void insertConceito(Conceito c){
		ContentValues cv = new ContentValues();
		cv.put("id", c.getId());
		cv.put("nome", c.getNome());
		cv.put("ordem", c.getOrdem());
		
		db.insert("conceito", null, cv);
	}
	
	public void insertConceitoQualificador(ConceitoQualificador cq){
		ContentValues cv = new ContentValues();
		cv.put("conceito_id", cq.getConceitoId());
		cv.put("pergunta_id", cq.getPerguntaId());
		cv.put("qualificador_id", cq.getQualificadorId());
		
		db.insert("conceito_qualificador", null, cv);
	}
	
	public ArrayList<Conceito> buscarPorPergunta(Pergunta p){
		ArrayList<Conceito> conceitos = new ArrayList<Conceito>();
		String query = "select c.id, c.nome, c.ordem from conceito c, conceito_pergunta cp " +
				"where c.id = cp.conceito_id and cp.pergunta_id = ?";
		
		String[] params = {p.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			conceitos.add(preencherConceito(cursor));
		}
		
		cursor.close();
		return conceitos;
	}
	
	private Conceito preencherConceito(Cursor cursor){
		Conceito c = new Conceito();
		c.setId(cursor.getInt(0));
		c.setNome(cursor.getString(1));
		c.setOrdem(cursor.getInt(2));
		return c;
	}

}
