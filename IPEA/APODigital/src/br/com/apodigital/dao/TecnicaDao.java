package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Questionario;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class TecnicaDao extends DaoGeral {
	
	public TecnicaDao(Context context) {
		super(context);
	}

	public static final String TECNICA = "tecnica";
	public static final String NOME = "nome";
	public static final String RESPONDENTE = "respondente";
	
	private static final String[] TECNICA_COLUMNS = {"id", "nome", "respondente"};
	
	public static final String CREATE_TECNICA = "create table " + TECNICA + "(id int, " +
			"nome varchar(100), respondente int, primary key(id));";
	
	public static final String CREATE_APO_TECNICA = "create table apo_tecnica(apo_id int, " +
			"tecnica_id int, primary key(apo_id, tecnica_id));";
	
	public void insertTecnica(Questionario q){
		ContentValues cv = new ContentValues();
		cv.put("id", q.getId());
		cv.put("nome", q.getNome());
		cv.put("respondente", q.getRespondente());
		
		db.insert("tecnica", null, cv);
	}
	
	public void insertApoTecnica(int apoId, int tecnicaId){
		ContentValues cv = new ContentValues();
		cv.put("apo_id", apoId);
		cv.put("tecnica_id", tecnicaId);
		
		db.insert("apo_tecnica", null, cv);
	}
	
	public ArrayList<Questionario> buscarPorApoRespondente(APO apo, int respondente){
		ArrayList<Questionario> questionarios = new ArrayList<Questionario>();
		String query = "select t.id, t.nome, t.respondente from tecnica t, apo_tecnica at where " +
				"t.id = at.tecnica_id and at.apo_id = ? and t.respondente = ?";
		
		//String cmd = "select * from tecnica;";
		
		String[] paramentros = {apo.getId() + "", respondente + ""};
		
		//String[] params = {};
		
		Cursor cursor = db.rawQuery(query, paramentros);
		//Cursor cursor = db.rawQuery(cmd, params);
		
		while(cursor.moveToNext()){
			questionarios.add(preencher(cursor));
		}
		
		cursor.close();
		return questionarios;
	}
	
	private Questionario preencher(Cursor c){
		Questionario q = new Questionario();
		q.setId(c.getInt(0));
		q.setNome(c.getString(1));
		q.setRespondente(c.getInt(2));
		return q;
	}

}
