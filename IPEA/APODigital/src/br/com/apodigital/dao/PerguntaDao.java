package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.Categoria;
import br.com.apodigital.model.Pergunta;
import br.com.apodigital.model.CategoriaPergunta;
import br.com.apodigital.model.PerguntaAtributo;
import br.com.apodigital.model.PerguntaComodo;
import br.com.apodigital.model.PerguntaConceito;
import br.com.apodigital.model.PerguntaQualificador;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class PerguntaDao extends DaoGeral {
	
	public PerguntaDao(Context context) {
		super(context);
	}

	public static final String CREATE_PERGUNTA = "create table pergunta(id int, texto varchar(1500), " +
			"tipo int, ordem int, escala_cores int, primary key(id));";
	
	public static final String CREATE_PERGUNTA_CATEGORIA = "create table pergunta_categoria(" +
			"pergunta_id int, categoria_id int, primary key(pergunta_id, categoria_id));";
	
	public static final String CREATE_PERGUNTA_QUALIFICADOR = "create table pergunta_qualificador(" +
			"pergunta_id int, qualificador_id int, primary key(pergunta_id, qualificador_id));";
	
	public static final String CREATE_CONCEITO_PERGUNTA = "create table conceito_pergunta(" +
			"conceito_id int, pergunta_id int, primary key(conceito_id, pergunta_id));";
	
	public static final String CREATE_PERGUNTA_COMODO = "create table pergunta_comodo(" +
			"pergunta_id int, comodo_id int, primary key(pergunta_id, comodo_id));";
	
	public static final String CREATE_PERGUNTA_ATRIBUTO = "create table pergunta_atributo(" +
			"pergunta_id int, atributo_id int, primary key(pergunta_id, atributo_id));";
	
	public void insertPergunta(Pergunta p){
		ContentValues cv = new ContentValues();
		cv.put("id", p.getId());
		cv.put("texto", p.getTexto());
		cv.put("tipo", p.getTipo());
		cv.put("ordem", p.getOrdem());
		cv.put("escala_cores", p.getEscalaDeCores());
		
		db.insert("pergunta", null, cv);
	}
	
	public void insertPerguntaCategoria(CategoriaPergunta cp){
		ContentValues cv = new ContentValues();
		cv.put("pergunta_id", cp.getPerguntaId());
		cv.put("categoria_id", cp.getCategoriaId());
		
		db.insert("pergunta_categoria", null, cv);
	}
	
	public void insertPerguntaQualificador(PerguntaQualificador pq){
		ContentValues cv = new ContentValues();
		cv.put("pergunta_id", pq.getPerguntaId());
		cv.put("qualificador_id", pq.getQualificadorId());
		
		db.insert("pergunta_qualificador", null, cv);
	}
	
	public void insertPerguntaConceito(PerguntaConceito pc){
		ContentValues cv = new ContentValues();
		cv.put("conceito_id", pc.getConceitoId());
		cv.put("pergunta_id", pc.getPerguntaId());
		
		db.insert("conceito_pergunta", null, cv);
	}
	
	public void insertPerguntaComodo(PerguntaComodo pc){
		ContentValues cv = new ContentValues();
		cv.put("pergunta_id", pc.getPerguntaId());
		cv.put("comodo_id", pc.getComodoId());
		
		db.insert("pergunta_comodo", null, cv);
	}
	
	public void insertPerguntaAtributo(PerguntaAtributo pa){
		ContentValues cv = new ContentValues();
		cv.put("pergunta_id", pa.getPerguntaId());
		cv.put("atributo_id", pa.getAtributoId());
		
		db.insert("pergunta_atributo", null, cv);
	}
	
	/*
	 * Busca perguntas por categoria
	 */
	public ArrayList<Pergunta> buscarPorCategoria(Categoria c){
		ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
		
		String query = "select p.id, p.texto, p.tipo, p.ordem, p.escala_cores from pergunta p, " +
				"pergunta_categoria pc where p.id = pc.pergunta_id and pc.categoria_id = ? " +
				"order by p.ordem";
		
		String[] params = {c.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			perguntas.add(preencherPergunta(cursor));
		}
		
		cursor.close();
		return perguntas;
	}
	
	public Pergunta preencherPergunta(Cursor cursor){
		Pergunta p = new Pergunta();
		p.setId(cursor.getInt(0));
		p.setTexto(cursor.getString(1));
		p.setTipo(cursor.getInt(2));
		p.setOrdem(cursor.getInt(3));
		p.setEscalaDeCores(cursor.getInt(4));
		return p;
	}

}
