package br.com.apodigital.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DaoGeral {
	
	private DataBaseWrapper dbWrapper;
	protected SQLiteDatabase db;
	
	public DaoGeral(Context context){
		dbWrapper = new DataBaseWrapper(context);
	}
	
	public void open(){
		db = dbWrapper.getWritableDatabase();
	}
	
	public void close(){
		db.close();
	}

}
