package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Conceito;
import br.com.apodigital.model.Morador;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.widget.AdapterView.OnItemClickListener;

public class MoradorDao extends DaoGeral {
	
	public MoradorDao(Context context) {
		super(context);
	}

	public static final String CREATE_MORADOR = "create table morador(id int auto_increment, " +
			"num_apartamento varchar(10), bloco varchar(10), apo_id int, tempo_resposta varchar(30), " +
			"sincronizado int, primary key(id));";
	
	public int insertMorador(Morador m){
		int id = 0;
		
		ContentValues cv = new ContentValues();
		cv.put("num_apartamento", m.getNumApartamento());
		cv.put("bloco", m.getBloco());
		cv.put("apo_id", m.getApo().getId());
		cv.put("tempo_resposta", "");
		cv.put("sincronizado", 0);
		
		id = (int) db.insert("morador", null, cv);
		
		return id;
	}
	
	public void editMorador(Morador m){
		ContentValues cv = new ContentValues();
		cv.put("tempo_resposta", m.getTempoResposta());
		cv.put("num_apartamento", m.getNumApartamento());
		db.update("morador", cv, "rowid = " + m.getId(), null);
	}
	
	public void sincronizarMorador(Morador m){
		ContentValues cv = new ContentValues();
		cv.put("sincronizado", 1);
		db.update("morador", cv, "rowid = " + m.getId(), null);
	}
	
	public void sincronizarMoradorPorApo(APO apo){
		ContentValues cv = new ContentValues();
		cv.put("sincronizado", 1);
		db.update("morador", cv, "apo_id = " + apo.getId(), null);
	}
	
	public void deSincronizarMorador(){
		ContentValues cv = new ContentValues();
		cv.put("sincronizado", 0);
		db.update("morador", cv, "apo_id = " + 21, null);
	}
	
	public ArrayList<Morador> buscarPorApo(APO apo){
		ArrayList<Morador> moradores = new ArrayList<Morador>();
		
		String query = "select rowid, num_apartamento, bloco, apo_id, tempo_resposta, sincronizado from " +
				"morador where apo_id = ?";
		
		String[] params = {apo.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			moradores.add(preencherMorador(cursor));
		}
		
		cursor.close();
		return moradores;
	}
	
	public ArrayList<Morador> buscarPorApoSinc(APO apo, int sincronizado){
		ArrayList<Morador> moradores = new ArrayList<Morador>();
		
		String query = "select rowid, num_apartamento, bloco, apo_id, tempo_resposta, sincronizado from " +
				"morador where apo_id = ? and sincronizado = ?";
		
		String[] params = {apo.getId() + "", sincronizado + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			moradores.add(preencherMorador(cursor));
		}
		
		cursor.close();
		return moradores;
	}
	
	public Morador buscarPorId(int id){
		Morador m = new Morador();
		String query = "select rowid, num_apartamento, bloco, apo_id, tempo_resposta, sincronizado from " +
				"morador where rowid = ?";
		String[] params = {id + ""};
		Cursor cursor = db.rawQuery(query, params);
		if(cursor.moveToNext()){
			m = preencherMorador(cursor);
		}
		cursor.close();
		return m;
	}
	
	private Morador preencherMorador(Cursor cursor){
		Morador m = new Morador();
		m.setId(cursor.getInt(0));
		m.setNumApartamento(cursor.getString(1));
		m.setBloco(cursor.getString(2));
		m.getApo().setId(cursor.getInt(3));
		m.setTempoResposta(cursor.getString(4));
		m.setSincronizado(cursor.getInt(5));
		return m;
	}

}
