package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.Categoria;
import br.com.apodigital.model.Questionario;
import br.com.apodigital.model.QuestionarioCategoria;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class CategoriaDao extends DaoGeral {
	
	public CategoriaDao(Context context) {
		super(context);
	}

	public static final String CATEGORIA = "categoria";
	public static final String ID = "id";
	public static final String NOME = "nome";
	
	public static final String CREATE_CATEGORIA = "create table " + CATEGORIA + "(" +
			"id int, nome varchar(100), primary key(id));";
	
	public static final String CREATE_TECNICA_CATEGORIA = "create table tecnica_categoria(" +
			"tecnica_id int, categoria_id int, primary key(tecnica_id, categoria_id));";
	
	public void insertCategoria(Categoria c){
		ContentValues cv = new ContentValues();
		cv.put("id", c.getId());
		cv.put("nome", c.getNome());
		
		db.insert("categoria", null, cv);
	}
	
	public void insertTecnicaCategoria(QuestionarioCategoria qc){
		ContentValues cv = new ContentValues();
		cv.put("tecnica_id", qc.getQuestionarioId());
		cv.put("categoria_id", qc.getCategoriaId());
		
		db.insert("tecnica_categoria", null, cv);
	}
	
	public ArrayList<Categoria> buscarPorTecnica(Questionario tecnica){
		ArrayList<Categoria> categorias = new ArrayList<Categoria>();
		String query = "select c.id, c.nome from categoria c, tecnica_categoria tc where " +
				"c.id = tc.categoria_id and tc.tecnica_id = ?";
		
		String[] params = {tecnica.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		
		while(cursor.moveToNext()){
			categorias.add(preencherCategoria(cursor));
		}
		
		cursor.close();
		return categorias;
	}
	
	private Categoria preencherCategoria(Cursor cursor){
		Categoria c = new Categoria();
		c.setId(cursor.getInt(0));
		c.setNome(cursor.getString(1));
		return c;
	}

}
