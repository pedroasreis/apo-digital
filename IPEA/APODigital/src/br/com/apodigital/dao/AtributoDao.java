package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.Atributo;
import br.com.apodigital.model.Pergunta;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class AtributoDao extends DaoGeral{
	
	public AtributoDao(Context context) {
		super(context);
	}

	public static final String CREATE_ATRIBUTO = "create table atributo(id int, " +
			"nome varchar(100), primary key(id));";
	
	public void insertAtributo(Atributo a){
		ContentValues cv = new ContentValues();
		cv.put("id", a.getId());
		cv.put("nome", a.getNome());
		
		db.insert("atributo", null, cv);
	}
	
	public ArrayList<Atributo> buscarPorPergunta(Pergunta p){
		ArrayList<Atributo> atributos = new ArrayList<Atributo>();
		String query = "select a.id, a.nome from atributo a, pergunta_atributo pa " +
				"where a.id = pa.atributo_id and pa.pergunta_id = ?";
		
		String[] params = {p.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			atributos.add(preencherAtributo(cursor));
		}
		
		cursor.close();
		return atributos;
	}
	
	private Atributo preencherAtributo(Cursor cursor){
		Atributo a = new Atributo();
		a.setId(cursor.getInt(0));
		a.setNome(cursor.getString(1));
		return a;
	}

}
