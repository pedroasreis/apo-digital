package br.com.apodigital.dao;

import java.util.ArrayList;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Morador;
import br.com.apodigital.model.Pergunta;
import br.com.apodigital.model.Resposta;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class RespostaDao extends DaoGeral {
	
	public RespostaDao(Context context) {
		super(context);
	}

	public static final String CREATE_RESPOSTA = "create table resposta(id int auto_increment, " +
			"morador_id int, pergunta_id int, texto varchar(255), apo_id int, " +
			"comodo_id int, conceito_id int, atributo_id int, sincronizado int, primary key(id));";
	
	public void insertResposta(Resposta r){
		ContentValues cv = new ContentValues();
		cv.put("morador_id", r.getMorador().getId());
		cv.put("pergunta_id", r.getPergunta().getId());
		cv.put("texto", r.getTexto());
		cv.put("apo_id", r.getApo().getId());
		cv.put("comodo_id", r.getComodo().getId());
		cv.put("conceito_id", r.getConceito().getId());
		cv.put("atributo_id", r.getAtributo().getId());
		cv.put("sincronizado", 0); // sempre que uma resposta é inserida no banco, é evidente q a mesma não estará sincronizada
		
		db.insert("resposta", null, cv);
	}
	
	public void sincronizarResposta(Resposta r){
		ContentValues cv = new ContentValues();
		cv.put("sincronizado", 1);
		db.update("resposta", cv, "rowid = " + r.getId(), null);
	}
	
	public void sincronizarRespostaPorApo(APO apo){
		ContentValues cv = new ContentValues();
		cv.put("sincronizado", 1);
		db.update("resposta", cv, "apo_id = " + apo.getId(), null);
	}
	
	public void deSincronizarResposta(){
		ContentValues cv = new ContentValues();
		cv.put("sincronizado", 0);
		db.update("resposta", cv, "apo_id = " + 21, null);
	}
	
	/*
	 * Busca apenas as respostas não sincronizadas
	 */
	public ArrayList<Resposta> buscarRespostasPorApo(APO apo){
		ArrayList<Resposta> respostas = new ArrayList<Resposta>();
		
		String query = "select rowid, morador_id, pergunta_id, texto, apo_id, comodo_id, conceito_id, " +
				"atributo_id, sincronizado from resposta where apo_id = ? and sincronizado = 0";
		
		String[] params = {apo.getId() + ""};
		
		Cursor cursor = db.rawQuery(query, params);
		while(cursor.moveToNext()){
			respostas.add(preencherReposta(cursor));
		}
		
		cursor.close();
		return respostas;
	}
	
	private Resposta preencherReposta(Cursor cursor){
		Resposta r = new Resposta();
		r.setId(cursor.getInt(0));
		
		r.setMorador(new Morador());
		r.getMorador().setId(cursor.getInt(1));
		
		r.setPergunta(new Pergunta());
		r.getPergunta().setId(cursor.getInt(2));
		
		r.setTexto(cursor.getString(3));
		r.getApo().setId(cursor.getInt(4));
		r.getComodo().setId(cursor.getInt(5));
		r.getConceito().setId(cursor.getInt(6));
		r.getAtributo().setId(cursor.getInt(7));
		r.setSincronizado(cursor.getInt(8));
		return r;
	}

}
