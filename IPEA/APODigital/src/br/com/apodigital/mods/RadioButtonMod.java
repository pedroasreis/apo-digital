package br.com.apodigital.mods;

import br.com.apodigital.model.Atributo;
import br.com.apodigital.model.Conceito;
import br.com.apodigital.model.Pergunta;
import android.content.Context;
import android.widget.RadioButton;

public class RadioButtonMod extends RadioButton {
	
	private Pergunta pergunta;
	private Atributo atributo;
	private Conceito conceito;

	public RadioButtonMod(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public Pergunta getPergunta() {
		return pergunta;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}

	public Atributo getAtributo() {
		return atributo;
	}

	public void setAtributo(Atributo atributo) {
		this.atributo = atributo;
	}

	public Conceito getConceito() {
		return conceito;
	}

	public void setConceito(Conceito conceito) {
		this.conceito = conceito;
	}

}
