package br.com.apodigital.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.apodigital.model.Questionario;

import br.com.apodigital.R;

public class TecnicaAdapter extends ArrayAdapter<Questionario> {

    Context mContext;
    int layoutResourceId;
    Questionario data[] = null;

    public TecnicaAdapter(Context mContext, int layoutResourceId, Questionario[] data) {

        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        Questionario q = data[position];

        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(R.id.tecnica_nome);
        ImageView tecnicaFinalizada = (ImageView) convertView.findViewById(R.id.tecnica_finalizada);
        
        if(q.isCompleto()){
        	tecnicaFinalizada.setImageResource(R.drawable.ic_action_accept);
        }
        
        textViewItem.setText(q.getNome());
        textViewItem.setTag(q.getId());

        return convertView;
    }

}
