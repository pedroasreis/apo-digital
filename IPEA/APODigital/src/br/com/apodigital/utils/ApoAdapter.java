package br.com.apodigital.utils;

import br.com.apodigital.R;

import br.com.apodigital.model.APO;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ApoAdapter extends ArrayAdapter<APO> {

    Context mContext;
    int layoutResourceId;
    APO data[] = null;

    public ApoAdapter(Context mContext, int layoutResourceId, APO[] data) {

        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = LayoutInflater.from(mContext);;
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        APO apo = data[position];

        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(R.id.apo_nome_list);
        textViewItem.setText(apo.getNome());
        textViewItem.setTag(apo.getId());

        return convertView;
    }

}