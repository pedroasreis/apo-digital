package br.com.apodigital.utils;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import android.content.Context;
import android.net.SSLCertificateSocketFactory;

public class VolleyHelper
{
	private static final String TAG = "EventMeVolley";
	private static final int CACHE_VM_HEAP_PROPORTION = 4;
	
	private static RequestQueue mQueue;
	private static ImageLoader mImageLoader;
	private static int cacheSize;

	protected VolleyHelper() { }

	public static void init(Context context)
	{
		cacheSize = calcMemoryCacheSize();
		final HttpStack stack = new HurlStack(null, SSLCertificateSocketFactory.getDefault(0, null));
		mQueue = Volley.newRequestQueue(context, stack);
		//mImageLoader = new ImageLoader(mQueue, new BitmapLRUImageCache(cacheSize));
	}
	
	private static int calcMemoryCacheSize()
	{
		// how many total mega bytes of heap the app is allowed to use
		int maxMemory = (int) Runtime.getRuntime().maxMemory() / (1024 * 1024);
		// size of memory allocated to cache
		int bestCacheSize = maxMemory / CACHE_VM_HEAP_PROPORTION;
		return bestCacheSize;
	}

	public static RequestQueue getRequestQueue() {
		return mQueue;
	}

	public static ImageLoader getImageLoader() {
		return mImageLoader;
	}
	
	public static int getCacheSize() {
		return cacheSize;
	}

}
