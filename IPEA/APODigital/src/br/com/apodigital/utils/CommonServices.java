package br.com.apodigital.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

/*
 * Classe que agrupa serviços simples usados em toda a aplicação
 */
public class CommonServices extends Activity {
	
	public static double screenDeviceSize(DisplayMetrics dm){
		double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
	    double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
	    double screenInches = Math.sqrt(x + y);
	    screenInches = (double)Math.round(screenInches * 10) / 10;
	    return screenInches;
	}
	
	public static float convertPixelsToDp(float px, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;
	}
	
	public static float convertDpToPixel(float dp, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * (metrics.densityDpi / 160f);
	    return px;
	}
	
	/*public static String getUrlService(String property){
		String url = "";
		Properties prop = new Properties();
		InputStream input = null;
		
		try{
			input = new FileInputStream(Constantes.DIR_PROPERTIES);
			prop.load(input);
			url = prop.getProperty(property);
		}
		catch(IOException ioe){
			ioe.printStackTrace();
		}
		finally{
			if(input != null){
				try{
					input.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		return url;
	}*/

}
