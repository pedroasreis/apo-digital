package br.com.apodigital.utils;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.apodigital.R;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.Morador;

public class MoradorAdapter extends ArrayAdapter<Morador> {

    Context mContext;
    int layoutResourceId;
    ArrayList<Morador> data = null;

    public MoradorAdapter(Context mContext, int layoutResourceId, ArrayList<Morador> data) {

        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        Morador morador = data.get(position);

        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView moradorAp = (TextView) convertView.findViewById(R.id.morador_ap);
        moradorAp.setText(morador.getNumApartamento());
        
        TextView moradorBloco = (TextView) convertView.findViewById(R.id.morador_bloco);
        moradorBloco.setText(morador.getId() + "");
        
        TextView moradorTempoResposta = (TextView) convertView.findViewById(R.id.morador_tempo_resposta);
        moradorTempoResposta.setText(morador.getTempoResposta());

        return convertView;
    }

}