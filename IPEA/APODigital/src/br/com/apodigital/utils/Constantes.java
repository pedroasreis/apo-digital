package br.com.apodigital.utils;

import java.util.ArrayList;

import br.com.apodigital.model.Morador;
import br.com.apodigital.model.Pergunta;
import br.com.apodigital.model.Questionario;

public class Constantes {
	
	public static final String SERVER = "http://192.168.1.144:8080/";
	//public static final String SERVER = "http://54.186.162.82:8080/";
	public static final String LISTAR_APOS =  SERVER + "ApoDigitalServer/apo/listarTodos";
	public static final String APO_COMPLETA = SERVER + "ApoDigitalServer/apo/carregar";
	public static final String ENVIAR_REPOSTAS =  SERVER + "ApoDigitalServer/apo/enviar_respostas";
	
	/*
	 * Técnica escolhida
	 */
	public static Questionario tecnicasGlobal;
	
	/*
	 * Morador respondente
	 */
	public static Morador morador;
	
	/*
	 * Flag que indica se o questionário já foi respondido
	 */
	public static boolean novo = true;
	
	public static ArrayList<Pergunta> perguntasUnidade;
	
	public static boolean back; // controle de uso do botão "back" do Android
	
	/*
	 * Medição de tempo
	 */
	public static long startTime;
	public static long finishTime;
	public static long elapsedTime;
	
	public static int minutos;
	public static int seg;
	public static long segundos;
	
}
