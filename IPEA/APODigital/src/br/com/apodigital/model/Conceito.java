package br.com.apodigital.model;

import java.io.Serializable;
import java.util.ArrayList;

import br.com.apodigital.mods.CheckBoxMod;
import br.com.apodigital.mods.RadioButtonMod;

/**
 *
 * @author pedro
 */
public class Conceito implements Serializable {

    private int id;
    private String nome;
    private int ordem;
    
    private transient ArrayList<Qualificador> qualificadores;
    
    private transient ArrayList<RadioButtonMod> radios;
    public transient boolean[] qualificadoresMarcados;
    
    private transient ArrayList<Atributo> atributos;
    
    public Conceito(){
        
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString(){
        return nome;
    }

    /**
     * @return the ordem
     */
    public int getOrdem() {
        return ordem;
    }

    /**
     * @param ordem the ordem to set
     */
    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

	public ArrayList<RadioButtonMod> getRadios() {
		return radios;
	}

	public void setRadios(ArrayList<RadioButtonMod> radios) {
		this.radios = radios;
	}

	public ArrayList<Atributo> getAtributos() {
		return atributos;
	}

	public void setAtributos(ArrayList<Atributo> atributos) {
		this.atributos = atributos;
	}

	public ArrayList<Qualificador> getQualificadores() {
		return qualificadores;
	}

	public void setQualificadores(ArrayList<Qualificador> qualificadores) {
		this.qualificadores = qualificadores;
	}

}
