/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.apodigital.model;

import java.io.Serializable;
import java.util.ArrayList;

import android.widget.EditText;
import br.com.apodigital.mods.CheckBoxMod;
import br.com.apodigital.mods.RadioButtonMod;

/**
 *
 * @author Pedro
 */
public class Atributo implements Serializable {
    
    private int id;
    private String nome;
    
    /*
    Um atributo pode ter uma lista de respostas ou uma única resposta dependendo
    do tipo da pergunta com a qual o atributo está associado
    */
    private String resposta;
    private ArrayList<String> respostas;
    
    private Integer idQualificador;
    private ArrayList<String> idQualificadores;
    
    private transient ArrayList<Qualificador> qualificadores;
    
    private transient ArrayList<RadioButtonMod> radios;
	private transient ArrayList<CheckBoxMod> checks;
	private transient EditText campoResposta;
	public transient boolean[] qualificadoresMarcados;
    
    public Atributo(){
        respostas = new ArrayList<String>();
        idQualificadores = new ArrayList<String>();
    }
    
    public Atributo(int id, String nome){
        this.id = id;
        this.nome = nome;
    }

    @Override
    public boolean equals(Object obj) {
        Atributo a = (Atributo) obj;
        if(id == a.getId()){
            return true;
        }
        
        return false;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the resposta
     */
    public String getResposta() {
        return resposta;
    }

    /**
     * @param resposta the resposta to set
     */
    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    /**
     * @return the respostas
     */
    public ArrayList<String> getRespostas() {
        return respostas;
    }

    /**
     * @param respostas the respostas to set
     */
    public void setRespostas(ArrayList<String> respostas) {
        this.respostas = respostas;
    }

    /**
     * @return the idQualificador
     */
    public Integer getIdQualificador() {
        return idQualificador;
    }

    /**
     * @param idQualificador the idQualificador to set
     */
    public void setIdQualificador(Integer idQualificador) {
        this.idQualificador = idQualificador;
    }

    /**
     * @return the idQualificadores
     */
    public ArrayList<String> getIdQualificadores() {
        return idQualificadores;
    }

    /**
     * @param idQualificadores the idQualificadores to set
     */
    public void setIdQualificadores(ArrayList<String> idQualificadores) {
        this.idQualificadores = idQualificadores;
    }

	public ArrayList<RadioButtonMod> getRadios() {
		return radios;
	}

	public void setRadios(ArrayList<RadioButtonMod> radios) {
		this.radios = radios;
	}

	public ArrayList<CheckBoxMod> getChecks() {
		return checks;
	}

	public void setChecks(ArrayList<CheckBoxMod> checks) {
		this.checks = checks;
	}

	public EditText getCampoResposta() {
		return campoResposta;
	}

	public void setCampoResposta(EditText campoResposta) {
		this.campoResposta = campoResposta;
	}

	public ArrayList<Qualificador> getQualificadores() {
		return qualificadores;
	}

	public void setQualificadores(ArrayList<Qualificador> qualificadores) {
		this.qualificadores = qualificadores;
	}
    
}
