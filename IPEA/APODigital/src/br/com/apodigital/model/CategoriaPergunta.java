package br.com.apodigital.model;

/**
 *
 * @author pedro
 */
public class CategoriaPergunta
{
   
   private int categoriaId;
   private int perguntaId;

   /**
    * @return the categoriaId
    */
   public int getCategoriaId()
   {
      return categoriaId;
   }

   /**
    * @param categoriaId the categoriaId to set
    */
   public void setCategoriaId(int categoriaId)
   {
      this.categoriaId = categoriaId;
   }

   /**
    * @return the perguntaId
    */
   public int getPerguntaId()
   {
      return perguntaId;
   }

   /**
    * @param perguntaId the perguntaId to set
    */
   public void setPerguntaId(int perguntaId)
   {
      this.perguntaId = perguntaId;
   }
   
}
