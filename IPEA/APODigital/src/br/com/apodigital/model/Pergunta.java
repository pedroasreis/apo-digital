package br.com.apodigital.model;

import java.io.Serializable;
import java.util.ArrayList;

import android.widget.CheckBox;
import android.widget.EditText;
import br.com.apodigital.mods.CheckBoxMod;
import br.com.apodigital.mods.RadioButtonMod;

/**
 * 
 * @author pedro
 */
public class Pergunta implements Serializable {

	private int id;
	private String texto;
	private String comentario;
	private int tipo;
	private int ordem;
	private int escalaDeCores;
	
	private ArrayList<Qualificador> qualificadores;
	private ArrayList<Conceito> conceitos;
	private ArrayList<Comodo> comodos;
	private ArrayList<Atributo> atributos;
	
	private transient ArrayList<RadioButtonMod> radios;
	private transient ArrayList<CheckBoxMod> checks;
	private transient EditText campoResposta;
	public transient boolean[] qualificadoresMarcados;

	public static final int TEXTO = 1;
	public static final int MULTIPLA_ESCOLHA = 2;
	public static final int ESCOLHA_UNICA = 3;
	public static final int INTERVALO_NUMERICO = 4;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the texto
	 */
	public String getTexto() {
		return texto;
	}

	/**
	 * @param texto
	 * the texto to set
	 */
	public void setTexto(String texto) {
		this.texto = texto;
	}

	/**
	 * @return the tipo
	 */
	public int getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 * the tipo to set
	 */
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario
	 *            the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the ordem
	 */
	public int getOrdem() {
		return ordem;
	}

	/**
	 * @param ordem
	 *            the ordem to set
	 */
	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
	
	public ArrayList<Qualificador> getQualificadores() {
		return qualificadores;
	}

	public void setQualificadores(ArrayList<Qualificador> qualificadores) {
		this.qualificadores = qualificadores;
	}

	public ArrayList<Conceito> getConceitos() {
		return conceitos;
	}

	public void setConceitos(ArrayList<Conceito> conceitos) {
		this.conceitos = conceitos;
	}

	public ArrayList<Comodo> getComodos() {
		return comodos;
	}

	public void setComodos(ArrayList<Comodo> comodos) {
		this.comodos = comodos;
	}

	public ArrayList<Atributo> getAtributos() {
		return atributos;
	}

	public void setAtributos(ArrayList<Atributo> atributos) {
		this.atributos = atributos;
	}
	
	public ArrayList<RadioButtonMod> getRadios() {
		return radios;
	}

	public void setRadios(ArrayList<RadioButtonMod> radios) {
		this.radios = radios;
	}

	public ArrayList<CheckBoxMod> getChecks() {
		return checks;
	}

	public void setChecks(ArrayList<CheckBoxMod> checks) {
		this.checks = checks;
	}

	public EditText getCampoResposta() {
		return campoResposta;
	}

	public void setCampoResposta(EditText campoResposta) {
		this.campoResposta = campoResposta;
	}

	public int getEscalaDeCores() {
		return escalaDeCores;
	}

	public void setEscalaDeCores(int escalaDeCores) {
		this.escalaDeCores = escalaDeCores;
	}

}
