package br.com.apodigital.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author Pedro
 */
public class APO implements Serializable {

	private int id;
	private String nome;
	private String texto;
	private String cidade;
	private String estado;
	private ArrayList<Questionario> tecnicas;

	private transient ArrayList<Morador> moradores;

	public APO() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}

	/**
	 * @param cidade
	 *            the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public ArrayList<Questionario> getTecnicas() {
		return tecnicas;
	}

	public void setTecnicas(ArrayList<Questionario> tecnicas) {
		this.tecnicas = tecnicas;
	}

	public ArrayList<Morador> getMoradores() {
		return moradores;
	}

	public void setMoradores(ArrayList<Morador> moradores) {
		this.moradores = moradores;
	}

}
