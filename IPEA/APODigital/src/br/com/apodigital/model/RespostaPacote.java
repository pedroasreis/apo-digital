package br.com.apodigital.model;

import java.io.Serializable;
import java.util.ArrayList;

public class RespostaPacote implements Serializable {

	private ArrayList<Resposta> respostas;
	private ArrayList<Morador> moradores;
	
	private int codigo; // controlador que evita que uma transferência seja efetuada múltiplas vezes

	public ArrayList<Resposta> getRespostas() {
		return respostas;
	}

	public void setRespostas(ArrayList<Resposta> respostas) {
		this.respostas = respostas;
	}

	public ArrayList<Morador> getMoradores() {
		return moradores;
	}

	public void setMoradores(ArrayList<Morador> moradores) {
		this.moradores = moradores;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
