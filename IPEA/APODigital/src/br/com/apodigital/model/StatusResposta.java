package br.com.apodigital.model;

import java.io.Serializable;

public class StatusResposta implements Serializable {

	private boolean ok;

	/**
	 * @return the erro
	 */
	public boolean isOk() {
		return ok;
	}

	/**
	 * @param erro
	 *            the erro to set
	 */
	public void setOk(boolean ok) {
		this.ok = ok;
	}

}
