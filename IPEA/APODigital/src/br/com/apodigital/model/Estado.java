package br.com.apodigital.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class Estado implements Serializable {
    
    private int id;
    private String nome;
    private String uf;
    private ArrayList<Cidade> cidades;
    
    public Estado(){
        cidades = new ArrayList<Cidade>();
    }

    @Override
    public boolean equals(Object obj) {
        return (id == ((Estado)obj).getId());
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the uf
     */
    public String getUf() {
        return uf;
    }

    /**
     * @param uf the uf to set
     */
    public void setUf(String uf) {
        this.uf = uf;
    }

    /**
     * @return the cidades
     */
    public ArrayList<Cidade> getCidades() {
        return cidades;
    }

    /**
     * @param cidades the cidades to set
     */
    public void setCidades(ArrayList<Cidade> cidades) {
        this.cidades = cidades;
    }
    
}
