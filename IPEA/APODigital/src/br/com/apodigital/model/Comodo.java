package br.com.apodigital.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author Pedro
 */
public class Comodo implements Serializable {

	private int id;
	private String nome;
	private Integer tipo;
	
	private transient ArrayList<Conceito> conceitos;
	private transient ArrayList<Pergunta> perguntas;

	/*
	 * Tipos de cômodos
	 */
	public static final int EDIFICIO = 1;
	public static final int USO_COMUM = 2;
	public static final int UNIDADE = 3;

	public Comodo() {

	}

	@Override
	public boolean equals(Object obj) {
		return (nome.equals(((Comodo) obj).getId()));
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the tipo
	 */
	public Integer getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public ArrayList<Conceito> getConceitos() {
		return conceitos;
	}

	public void setConceitos(ArrayList<Conceito> conceitos) {
		this.conceitos = conceitos;
	}

	public ArrayList<Pergunta> getPerguntas() {
		return perguntas;
	}

	public void setPerguntas(ArrayList<Pergunta> perguntas) {
		this.perguntas = perguntas;
	}

}
