package br.com.apodigital.model;

import java.util.ArrayList;

/**
 * Classe que agrupará todos os arrays de objetos necessários para a migração de
 * uma APO para um cliente Android
 * 
 * @author pedro
 */
public class ApoPacote {

	private APO apo;
	private ArrayList<Questionario> questionarios;
	private ArrayList<Categoria> categorias;
	private ArrayList<QuestionarioCategoria> questionariosCategorias;
	private ArrayList<Pergunta> perguntas;
	private ArrayList<CategoriaPergunta> categoriasPerguntas;
	private ArrayList<Qualificador> qualificadores;
	private ArrayList<PerguntaQualificador> perguntasQualificadores;
	private ArrayList<Conceito> conceitos;
	private ArrayList<PerguntaConceito> perguntasConceitos;
	private ArrayList<Comodo> comodos;
	private ArrayList<PerguntaComodo> perguntasComodos;
	private ArrayList<Atributo> atributos;
	private ArrayList<PerguntaAtributo> perguntasAtributos;
	
	private ArrayList<ConceitoQualificador> conceitosQualificadores;

	/**
	 * @return the questionarios
	 */
	public ArrayList<Questionario> getQuestionarios() {
		return questionarios;
	}

	/**
	 * @param questionarios
	 *            the questionarios to set
	 */
	public void setQuestionarios(ArrayList<Questionario> questionarios) {
		this.questionarios = questionarios;
	}

	/**
	 * @return the categorias
	 */
	public ArrayList<Categoria> getCategorias() {
		return categorias;
	}

	/**
	 * @param categorias
	 *            the categorias to set
	 */
	public void setCategorias(ArrayList<Categoria> categorias) {
		this.categorias = categorias;
	}

	/**
	 * @return the questionariosCategorias
	 */
	public ArrayList<QuestionarioCategoria> getQuestionariosCategorias() {
		return questionariosCategorias;
	}

	/**
	 * @param questionariosCategorias
	 *            the questionariosCategorias to set
	 */
	public void setQuestionariosCategorias(
			ArrayList<QuestionarioCategoria> questionariosCategorias) {
		this.questionariosCategorias = questionariosCategorias;
	}

	/**
	 * @return the perguntas
	 */
	public ArrayList<Pergunta> getPerguntas() {
		return perguntas;
	}

	/**
	 * @param perguntas
	 *            the perguntas to set
	 */
	public void setPerguntas(ArrayList<Pergunta> perguntas) {
		this.perguntas = perguntas;
	}

	/**
	 * @return the categoriasPerguntas
	 */
	public ArrayList<CategoriaPergunta> getCategoriasPerguntas() {
		return categoriasPerguntas;
	}

	/**
	 * @param categoriasPerguntas
	 *            the categoriasPerguntas to set
	 */
	public void setCategoriasPerguntas(
			ArrayList<CategoriaPergunta> categoriasPerguntas) {
		this.categoriasPerguntas = categoriasPerguntas;
	}

	/**
	 * @return the qualificadores
	 */
	public ArrayList<Qualificador> getQualificadores() {
		return qualificadores;
	}

	/**
	 * @param qualificadores
	 *            the qualificadores to set
	 */
	public void setQualificadores(ArrayList<Qualificador> qualificadores) {
		this.qualificadores = qualificadores;
	}

	/**
	 * @return the perguntasQualificadores
	 */
	public ArrayList<PerguntaQualificador> getPerguntasQualificadores() {
		return perguntasQualificadores;
	}

	/**
	 * @param perguntasQualificadores
	 *            the perguntasQualificadores to set
	 */
	public void setPerguntasQualificadores(
			ArrayList<PerguntaQualificador> perguntasQualificadores) {
		this.perguntasQualificadores = perguntasQualificadores;
	}

	/**
	 * @return the conceitos
	 */
	public ArrayList<Conceito> getConceitos() {
		return conceitos;
	}

	/**
	 * @param conceitos
	 *            the conceitos to set
	 */
	public void setConceitos(ArrayList<Conceito> conceitos) {
		this.conceitos = conceitos;
	}

	/**
	 * @return the perguntasConceitos
	 */
	public ArrayList<PerguntaConceito> getPerguntasConceitos() {
		return perguntasConceitos;
	}

	/**
	 * @param perguntasConceitos
	 *            the perguntasConceitos to set
	 */
	public void setPerguntasConceitos(
			ArrayList<PerguntaConceito> perguntasConceitos) {
		this.perguntasConceitos = perguntasConceitos;
	}

	/**
	 * @return the comodos
	 */
	public ArrayList<Comodo> getComodos() {
		return comodos;
	}

	/**
	 * @param comodos
	 *            the comodos to set
	 */
	public void setComodos(ArrayList<Comodo> comodos) {
		this.comodos = comodos;
	}

	/**
	 * @return the perguntasComodos
	 */
	public ArrayList<PerguntaComodo> getPerguntasComodos() {
		return perguntasComodos;
	}

	/**
	 * @param perguntasComodos
	 *            the perguntasComodos to set
	 */
	public void setPerguntasComodos(ArrayList<PerguntaComodo> perguntasComodos) {
		this.perguntasComodos = perguntasComodos;
	}

	/**
	 * @return the atributos
	 */
	public ArrayList<Atributo> getAtributos() {
		return atributos;
	}

	/**
	 * @param atributos
	 *            the atributos to set
	 */
	public void setAtributos(ArrayList<Atributo> atributos) {
		this.atributos = atributos;
	}

	/**
	 * @return the perguntasAtributos
	 */
	public ArrayList<PerguntaAtributo> getPerguntasAtributos() {
		return perguntasAtributos;
	}

	/**
	 * @param perguntasAtributos
	 *            the perguntasAtributos to set
	 */
	public void setPerguntasAtributos(
			ArrayList<PerguntaAtributo> perguntasAtributos) {
		this.perguntasAtributos = perguntasAtributos;
	}

	/**
	 * @return the apo
	 */
	public APO getApo() {
		return apo;
	}

	/**
	 * @param apo
	 *            the apo to set
	 */
	public void setApo(APO apo) {
		this.apo = apo;
	}

	public ArrayList<ConceitoQualificador> getConceitosQualificadores() {
		return conceitosQualificadores;
	}

	public void setConceitosQualificadores(
			ArrayList<ConceitoQualificador> conceitosQualificadores) {
		this.conceitosQualificadores = conceitosQualificadores;
	}

}
