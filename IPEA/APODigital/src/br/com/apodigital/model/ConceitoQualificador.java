package br.com.apodigital.model;

import java.io.Serializable;

public class ConceitoQualificador implements Serializable {
	
	private int conceitoId;
	private int perguntaId;
	private int qualificadorId;
	
	public int getConceitoId() {
		return conceitoId;
	}
	
	public void setConceitoId(int conceitoId) {
		this.conceitoId = conceitoId;
	}
	
	public int getPerguntaId() {
		return perguntaId;
	}
	
	public void setPerguntaId(int perguntaId) {
		this.perguntaId = perguntaId;
	}
	
	public int getQualificadorId() {
		return qualificadorId;
	}
	
	public void setQualificadorId(int qualificadorId) {
		this.qualificadorId = qualificadorId;
	}

}
