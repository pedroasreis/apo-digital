package br.com.apodigital.activities;

import br.com.apodigital.dao.MoradorDao;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.Morador;
import br.com.apodigital.utils.Constantes;

import br.com.apodigital.R;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MoradorActivity extends RootActivity {
	
	private EditText numero;
	private EditText bloco;
	private Button salvar;
	
	private APO apo;
	private int respondente;
	
	private Morador morador;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_morador);
        
        /*
         * Recuperando parâmetros da activity anterior
         */
        Bundle b = getIntent().getExtras();
		apo = (APO) b.getSerializable("apo");
		respondente = b.getInt("respondente");
		morador = new Morador();
		
		final ActionBar actionBar = getActionBar();
		actionBar.setTitle(apo.getNome());
        
        numero = (EditText) findViewById(R.id.numero);
        bloco = (EditText) findViewById(R.id.bloco);
        
        salvar = (Button) findViewById(R.id.save_morador);
        
        /*
         * Definindo listener que salva o morador no banco de dados
         */
        salvar.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				MoradorDao dao = new MoradorDao(getApplicationContext());
				dao.open();
				morador.setNumApartamento(numero.getText().toString());
				morador.setBloco(bloco.getText().toString());
				morador.setApo(apo);
				
				morador.setId(dao.insertMorador(morador));
				dao.close();
				
				Constantes.morador = morador;
				Toast.makeText(getApplicationContext(), "ID: " + morador.getId(), Toast.LENGTH_LONG).show();
				
				/*
				 * Chama próxima tela passando os argumentos recebidos
				 * pela activity anterior: APO e respondente
				 */
				Intent i = new Intent(MoradorActivity.this, TecnicasActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("apo", apo);
				b.putInt("respondente", respondente);
				i.putExtras(b);
				
				startActivity(i);
				finish();
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_morador, menu);
        return true;
    }
}
