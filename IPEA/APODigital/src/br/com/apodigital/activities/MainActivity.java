package br.com.apodigital.activities;

import java.util.ArrayList;
import java.util.List;

import br.com.apodigital.dao.ApoDao;
import br.com.apodigital.dao.AtributoDao;
import br.com.apodigital.dao.CategoriaDao;
import br.com.apodigital.dao.ComodoDao;
import br.com.apodigital.dao.ConceitoDao;
import br.com.apodigital.dao.PerguntaDao;
import br.com.apodigital.dao.QualificadorDao;
import br.com.apodigital.dao.TecnicaDao;
import br.com.apodigital.migration.ApoClient;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.ApoPacote;
import br.com.apodigital.model.Atributo;
import br.com.apodigital.model.Categoria;
import br.com.apodigital.model.CategoriaPergunta;
import br.com.apodigital.model.Comodo;
import br.com.apodigital.model.Conceito;
import br.com.apodigital.model.ConceitoQualificador;
import br.com.apodigital.model.Pergunta;
import br.com.apodigital.model.PerguntaAtributo;
import br.com.apodigital.model.PerguntaComodo;
import br.com.apodigital.model.PerguntaConceito;
import br.com.apodigital.model.PerguntaQualificador;
import br.com.apodigital.model.Qualificador;
import br.com.apodigital.model.Questionario;
import br.com.apodigital.model.QuestionarioCategoria;
import br.com.apodigital.mods.RadioButtonMod;
import br.com.apodigital.utils.CommonServices;
import br.com.apodigital.utils.Constantes;

import br.com.apodigital.R;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends RootActivity {

	private List<APO> apos;
	
	private ProgressDialog mProgress;
	private Handler mHandler = new Handler();
	
	private int mProgressStatus = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/*
		 * Inicializando database
		 */
		ApoDao apoDao = new ApoDao(getApplicationContext());
		apoDao.open();
		apoDao.close();
		
		setListenerButton();
		
		new BuscaDeApos().execute();
	}
	
	private void setListenerButton(){
		Button b = (Button) findViewById(R.id.botaoCarregarApo);
		b.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroupApos);
				int apoId = rg.getCheckedRadioButtonId();
				Integer[] params = {apoId};
				new BuscaApoEscolhida().execute(params);
				
				mProgress = new ProgressDialog(v.getContext());
				mProgress.setCancelable(false);
				mProgress.setMessage("Fazendo o download da avaliação...");
				mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				mProgress.setProgress(0);
				mProgress.setMax(100);
				mProgress.show();
				
				/*
				 * Ativando spinner indicativo de tarefa em andamento
				 */
				new Thread(new Runnable() {
		            public void run() {
		                while (mProgressStatus < 100) {
		                    // Update the progress bar
		                    mHandler.post(new Runnable() {
		                        public void run() {
		                            mProgress.setProgress(mProgressStatus);
		                        }
		                    });
		                }
		                
		                if(mProgressStatus >= 100){
		                	mProgress.dismiss();
		                }
		            }
		        }).start();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private class BuscaDeApos extends AsyncTask<Void, Void, List<APO>> {

		@Override
		protected List<APO> doInBackground(Void... params) {
			ApoClient apoClient = new ApoClient();
			apos = null;
			try {
				apos = apoClient.getListaApos();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			return apos;
		}

		@Override
		protected void onPostExecute(List<APO> result) {
			String msg = "";
			if (result == null) {
				msg = "Erro na conexão com o servidor";
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
			} 
			else {
				/*
				 * Na pós execução os radio buttons serão criados
				 */
				RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroupApos);
				for(APO a : result){
					RadioButtonMod rb = new RadioButtonMod(getApplicationContext());
					rb.setId(a.getId());
					rb.setText(a.getNome() + " - " + a.getId());
					rb.setTextColor(Color.BLACK);
					Pergunta p = new Pergunta();
					p.setId(a.getId());
					rb.setPergunta(p);
					rg.addView(rb);
					
					rb.setOnClickListener(new OnClickListener() {
						
						public void onClick(View v) {
							RadioButtonMod este = (RadioButtonMod) v;
							Toast.makeText(getApplicationContext(), este.getPergunta().getId() + "",
									Toast.LENGTH_SHORT).show();
						}
					});
				}
			}
		}
	}
	
	private class BuscaApoEscolhida extends AsyncTask<Integer, Void, ApoPacote> {

		@Override
		protected ApoPacote doInBackground(Integer... params) {
			ApoPacote ap = null;
			//String res = "NADA";
			ApoClient ac = new ApoClient();
			try {
				ap = ac.getApoCompleta(params[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return ap;
		}

		@Override
		protected void onPostExecute(ApoPacote result) {
			String res = "Deu pau";
			if(result != null){
				res = "Tudo ok";
				/*
				 * Inserindo informações recebidas no banco de dados.
				 * Começando pela APO escolhida
				 */
				ApoDao apoDao = new ApoDao(getApplicationContext());
				apoDao.open();
				apoDao.inserir(result.getApo());
				apoDao.close();
				
				/*
				 * Inserindo as técnicas da APO e suas associações
				 * com a APO
				 */
				TecnicaDao tecnicaDao = new TecnicaDao(getApplicationContext());
				tecnicaDao.open();
				for(Questionario q : result.getQuestionarios()){
					tecnicaDao.insertTecnica(q);
					tecnicaDao.insertApoTecnica(result.getApo().getId(), q.getId());
				}
				tecnicaDao.close();
				
				/*
				 * Categorias e relações com as técnicas
				 */
				CategoriaDao categoriaDao = new CategoriaDao(getApplicationContext());
				categoriaDao.open();
				for(Categoria c : result.getCategorias()){
					categoriaDao.insertCategoria(c);
				}
				for(QuestionarioCategoria qc : result.getQuestionariosCategorias()){
					categoriaDao.insertTecnicaCategoria(qc);
				}
				categoriaDao.close();
				
				/*
				 * Perguntas e seus relacionamentos com demais entidades
				 */
				PerguntaDao perguntaDao = new PerguntaDao(getApplicationContext());
				perguntaDao.open();
				for(Pergunta p : result.getPerguntas()){
					perguntaDao.insertPergunta(p);
				}
				for(CategoriaPergunta cp : result.getCategoriasPerguntas()){
					perguntaDao.insertPerguntaCategoria(cp);
				}
				for(PerguntaQualificador pq : result.getPerguntasQualificadores()){
					perguntaDao.insertPerguntaQualificador(pq);
				}
				for(PerguntaConceito pc : result.getPerguntasConceitos()){
					perguntaDao.insertPerguntaConceito(pc);
				}
				for(PerguntaComodo pc : result.getPerguntasComodos()){
					perguntaDao.insertPerguntaComodo(pc);
				}
				for(PerguntaAtributo pa : result.getPerguntasAtributos()){
					perguntaDao.insertPerguntaAtributo(pa);
				}
				perguntaDao.close();
				
				/*
				 * Qualificadores
				 */
				QualificadorDao qualificadorDao = new QualificadorDao(getApplicationContext());
				qualificadorDao.open();
				for(Qualificador q : result.getQualificadores()){
					qualificadorDao.insertQualificador(q);
				}
				qualificadorDao.close();
				
				/*
				 * Conceitos e seus derivados
				 */
				ConceitoDao conceitoDao = new ConceitoDao(getApplicationContext());
				conceitoDao.open();
				for(Conceito c : result.getConceitos()){
					conceitoDao.insertConceito(c);
				}
				for(ConceitoQualificador cq : result.getConceitosQualificadores()){
					conceitoDao.insertConceitoQualificador(cq);
				}
				conceitoDao.close();
				
				/*
				 * Cômodos
				 */
				ComodoDao comodoDao = new ComodoDao(getApplicationContext());
				comodoDao.open();
				for(Comodo c : result.getComodos()){
					comodoDao.insertComodo(c);
				}
				comodoDao.close();
				
				/*
				 * Atributos
				 */
				AtributoDao atributoDao = new AtributoDao(getApplicationContext());
				atributoDao.open();
				for(Atributo a : result.getAtributos()){
					atributoDao.insertAtributo(a);
				}
				atributoDao.close();
			}
			//Log.i("FIM CARREGAMENTO", "APO " + result.getApo().getNome() + " carregada...");
			mProgressStatus = 100;
			Toast.makeText(getApplicationContext(), res, Toast.LENGTH_LONG).show();
		}
	}

}
