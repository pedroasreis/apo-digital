package br.com.apodigital.activities;

import java.lang.reflect.Method;
import java.util.ArrayList;

import br.com.apodigital.dao.AtributoDao;
import br.com.apodigital.dao.CategoriaDao;
import br.com.apodigital.dao.ComodoDao;
import br.com.apodigital.dao.ConceitoDao;
import br.com.apodigital.dao.MoradorDao;
import br.com.apodigital.dao.PerguntaDao;
import br.com.apodigital.dao.QualificadorDao;
import br.com.apodigital.dao.RespostaDao;
import br.com.apodigital.dao.TecnicaDao;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.Atributo;
import br.com.apodigital.model.Categoria;
import br.com.apodigital.model.Comodo;
import br.com.apodigital.model.Conceito;
import br.com.apodigital.model.Pergunta;
import br.com.apodigital.model.Qualificador;
import br.com.apodigital.model.Questionario;
import br.com.apodigital.model.Resposta;
import br.com.apodigital.mods.CheckBoxMod;
import br.com.apodigital.mods.RadioButtonMod;
import br.com.apodigital.mods.ScrollViewMod;
import br.com.apodigital.utils.CommonServices;
import br.com.apodigital.utils.Constantes;
import br.com.apodigital.utils.DialogHandler;

import br.com.apodigital.R;
import br.com.apodigital.R.id;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;
import br.com.apodigital.R.string;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

public class TesteActivity extends FragmentActivity implements
		ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	private APO apo;
	private Questionario tecnica;
	private int respondente;

	private boolean[] abasVisitadas;
	private boolean back = false;

	/*
	 * Array de fragmentos que representam as abas de categorias
	 */
	private ArrayList<Fragment> abas;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_teste);

		abas = new ArrayList<Fragment>();

		/*
		 * Recuperando argumentos da activity anterior
		 */
		Bundle b = getIntent().getExtras();
		apo = (APO) b.getSerializable("apo");
		respondente = b.getInt("respondente");
		// tecnica = (Questionario) b.getSerializable("tecnica");
		tecnica = Constantes.tecnicasGlobal;

		/*
		 * Buscando categorias da técnica escolhida
		 */

		CategoriaDao categoriaDao = new CategoriaDao(getApplicationContext());
		categoriaDao.open();
		tecnica.setCategorias(categoriaDao.buscarPorTecnica(tecnica));
		categoriaDao.close();

		/*
		 * array que indica se determinada já foi visitada. Caso tenha sido
		 * visitada, não há necessidade de criá-la novamente
		 */
		abasVisitadas = new boolean[tecnica.getCategorias().size()];

		/*
		 * Para cada categoria, criar sua aba correspondente
		 */
		int j = 0;
		for (Categoria c : tecnica.getCategorias()) {
			abas.add(new DummySectionFragment());
			abasVisitadas[j] = false;
			j++;
		}

		carregarPerguntas();

		// Create the adapter that will return a fragment for each of the
		// primary sections
		// of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setTitle(apo.getNome() + " (Técnica: " + tecnica.getNome()
				+ ")");

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab.
		// We can also use ActionBar.Tab#select() to do this if we have a
		// reference to the
		// Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter.
			// Also specify this Activity object, which implements the
			// TabListener interface, as the
			// listener for when this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}

		forceTabs();
		Constantes.startTime = System.currentTimeMillis();
	}

	private void forceTabs() {
		try {
			final ActionBar actionBar = getActionBar();
			final Method setHasEmbeddedTabsMethod = actionBar.getClass()
					.getDeclaredMethod("setHasEmbeddedTabs", boolean.class);
			setHasEmbeddedTabsMethod.setAccessible(true);
			setHasEmbeddedTabsMethod.invoke(actionBar, false);
		} catch (final Exception e) {
			// Handle issues as needed: log, warn user, fallback etc
			// This error is safe to ignore, standard tabs will appear.
			e.printStackTrace();
		}
	}

	private void carregarPerguntas() {
		/*
		 * Criando DAOs e suas conexões
		 */
		PerguntaDao perguntaDao = new PerguntaDao(getApplicationContext());
		perguntaDao.open();

		QualificadorDao qualificadorDao = new QualificadorDao(
				getApplicationContext());
		qualificadorDao.open();

		ConceitoDao conceitoDao = new ConceitoDao(getApplicationContext());
		conceitoDao.open();

		ComodoDao comodoDao = new ComodoDao(getApplicationContext());
		comodoDao.open();

		AtributoDao atributoDao = new AtributoDao(getApplicationContext());
		atributoDao.open();

		/*
		 * Iniciando array de perguntas associadas a cômodos Unidade
		 */
		Constantes.perguntasUnidade = new ArrayList<Pergunta>();

		/*
		 * Buscando perguntas e seus componentes para cada categoria
		 */
		for (Categoria c : tecnica.getCategorias()) {
			c.setPerguntas(perguntaDao.buscarPorCategoria(c));

			for (Pergunta p : c.getPerguntas()) {
				p.setQualificadores(qualificadorDao.buscarPorPergunta(p));
				p.setConceitos(conceitoDao.buscarPorPergunta(p));
				p.setComodos(comodoDao.buscarPorPergunta(p));
				p.setAtributos(atributoDao.buscarPorPergunta(p));

				/*
				 * Para cada pergunta instancia-se seus atributos necessários
				 */
				p.setRadios(new ArrayList<RadioButtonMod>());
				p.setChecks(new ArrayList<CheckBoxMod>());
				p.setCampoResposta(new EditText(getApplicationContext()));
				p.qualificadoresMarcados = new boolean[p.getQualificadores()
						.size()];

				/*
				 * Para cada atributo da pergunta instanciar os componentes
				 * necessários
				 */
				for (Atributo a : p.getAtributos()) {
					a.setRadios(new ArrayList<RadioButtonMod>());
					a.setChecks(new ArrayList<CheckBoxMod>());
					a.qualificadoresMarcados = new boolean[p
							.getQualificadores().size()];
				}

				/*
				 * Associação da pergunta apenas com conceitos
				 */
				if (p.getComodos().isEmpty() && !p.getConceitos().isEmpty()
						&& p.getAtributos().isEmpty()) {
					for (Conceito conceito : p.getConceitos()) {
						conceito.setQualificadores(qualificadorDao
								.buscarPorConceitoPergunta(conceito, p));

						conceito.setRadios(new ArrayList<RadioButtonMod>());
						conceito.qualificadoresMarcados = new boolean[conceito
								.getQualificadores().size()];
					}
				}

				/*
				 * Caso de associação de uma pergunta com cômodos e conceitos.
				 * Deve-se criar uma lista de conceitos em cada cômodo da
				 * pergunta, não apenas copiar a referência, mas sim criar novos
				 * objetos
				 */
				if (!p.getComodos().isEmpty() && !p.getConceitos().isEmpty()
						&& p.getAtributos().isEmpty()) {
					/*
					 * Apenas carregando os qualificadores para cada conceito
					 */
					for (Conceito conceito : p.getConceitos()) {
						conceito.setQualificadores(qualificadorDao
								.buscarPorConceitoPergunta(conceito, p));
					}

					for (Comodo cm : p.getComodos()) {
						cm.setConceitos(new ArrayList<Conceito>());

						for (Conceito conceito : p.getConceitos()) {
							Conceito novoConceito = new Conceito();
							novoConceito.setId(conceito.getId());
							novoConceito.setNome(conceito.getNome());
							novoConceito.setOrdem(conceito.getOrdem());
							novoConceito
									.setRadios(new ArrayList<RadioButtonMod>());
							novoConceito.setQualificadores(conceito
									.getQualificadores());
							novoConceito.qualificadoresMarcados = new boolean[p
									.getQualificadores().size()];
							cm.getConceitos().add(novoConceito);
						}
					}

					/*
					 * Adicionando a pergunta na lista de perguntas
					 * correspondentes
					 */
					Constantes.perguntasUnidade.add(p);
				}

				/*
				 * Caso em que a pergunta está associada apenas com conceitos e
				 * atributos. Nesse caso, para cada conceito cria uma lista com
				 * os atributos da pergunta
				 */
				if (p.getComodos().isEmpty() && !p.getConceitos().isEmpty()
						&& !p.getAtributos().isEmpty()) {
					/*
					 * Buscando qualificadores de cada atributo
					 */
					for (Atributo a : p.getAtributos()) {
						a.setQualificadores(qualificadorDao
								.buscarPorAtributoPergunta(a, p));
					}

					for (Conceito con : p.getConceitos()) {
						con.setAtributos(new ArrayList<Atributo>());

						for (Atributo a : p.getAtributos()) {
							Atributo novoAtributo = new Atributo();
							novoAtributo.setId(a.getId());
							novoAtributo.setNome(a.getNome());
							novoAtributo.setQualificadores(a.getQualificadores());
							novoAtributo.setRadios(new ArrayList<RadioButtonMod>());
							novoAtributo.setChecks(new ArrayList<CheckBoxMod>());
							novoAtributo.qualificadoresMarcados = new boolean[p.getQualificadores().size()];
							con.getAtributos().add(novoAtributo);
						}
					}
				}
			}
		}

		/*
		 * Fechando as conexões de todos os DAOs
		 */
		perguntaDao.close();
		qualificadorDao.close();
		conceitoDao.close();
		comodoDao.close();
		atributoDao.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_teste, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_save:
			DialogHandler appdialog = new DialogHandler();
			boolean dlg = appdialog.Confirm(TesteActivity.this, "Confirmação",
					"Deseja realmente finalizar esta técnica?", "Cancelar",
					"OK", aproc(), bproc());
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
	    new AlertDialog.Builder(this)
	           .setMessage("Você tem certeza que deseja sair? Suas respostas serão perdidas!")
	           .setCancelable(false)
	           .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                    TesteActivity.this.finish();
	               }
	           })
	           .setNegativeButton("Não", null)
	           .show();
	}

	public Runnable aproc() {
		return new Runnable() {
			public void run() {
				salvarRespostas();
				Constantes.finishTime = System.currentTimeMillis();
				Constantes.elapsedTime = Constantes.finishTime - Constantes.startTime;
				Constantes.segundos = Constantes.elapsedTime / 1000;
				Constantes.minutos = (int)(Constantes.segundos / 60);
				Constantes.seg = (int)(Constantes.segundos % 60);
				String tempoResposta = Constantes.minutos + "min e " + Constantes.seg + "s";
				Toast.makeText(getApplicationContext(),
						"Respostas inseridas com sucesso em " + tempoResposta, Toast.LENGTH_LONG)
						.show();
				Constantes.morador.setTempoResposta(tempoResposta);
				
				/*
				 * Atualizando registro do morador com o tempo gasto para responder a técnica
				 */
				MoradorDao moradorDao = new MoradorDao(getApplicationContext());
				moradorDao.open();
				moradorDao.editMorador(Constantes.morador);
				moradorDao.close();
				
				Intent i = new Intent(TesteActivity.this,
						RespondenteActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("apo", apo);
				b.putInt("respondente", respondente);
				i.putExtras(b);

				startActivity(i);
				finish();
			}
		};
	}

	public Runnable bproc() {
		return new Runnable() {
			public void run() {
				Toast.makeText(getApplicationContext(), "Continuando APO...",
						Toast.LENGTH_LONG).show();
			}
		};
	}

	public Runnable voltar() {
		return new Runnable() {
			public void run() {
				Constantes.back = true;
			}
		};
	}

	public Runnable cancelar() {
		return new Runnable() {
			public void run() {
				Constantes.back = false;
			}
		};
	}

	/*
	 * Método que salva as respostas obtidas no banco de dados
	 */
	private void salvarRespostas() {
		/*
		 * Abrindo conexão com a base de dados
		 */
		RespostaDao respostaDao = new RespostaDao(getApplicationContext());
		respostaDao.open();
		for (Categoria c : tecnica.getCategorias()) {
			for (Pergunta p : c.getPerguntas()) {
				/*
				 * Perguntas sem nenhuma associação
				 */
				if (p.getConceitos().isEmpty() && p.getComodos().isEmpty()
						&& p.getAtributos().isEmpty()) {
					Resposta r = null;
					if (p.getTipo() == Pergunta.TEXTO) {
						r = new Resposta();
						r.setApo(apo);
						r.setAtributo(new Atributo()); // significa ausência
														// dessa entidade, pois
														// id = 0
						r.setComodo(new Comodo());
						r.setConceito(new Conceito());
						r.setMorador(Constantes.morador);
						r.setPergunta(p);
						r.setTexto(p.getCampoResposta().getText().toString());
					} else if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
						r = prepararEscolhaUnica(p.getRadios(), new Conceito(),
								new Comodo(), new Atributo(), p);
					} else if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
						r = prepararMultiplaEscolha(p.getChecks(),
								new Conceito(), new Comodo(), new Atributo(), p);
					}

					respostaDao.insertResposta(r);
				}

				/*
				 * Perguntas associadas apenas com atributos
				 */
				else if (p.getConceitos().isEmpty() && p.getComodos().isEmpty()
						&& !p.getAtributos().isEmpty()) {
					for (Atributo a : p.getAtributos()) {
						Resposta r = null;
						if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
							r = prepararEscolhaUnica(a.getRadios(),
									new Conceito(), new Comodo(), a, p);
						} 
						else if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
							r = prepararMultiplaEscolha(a.getChecks(),
									new Conceito(), new Comodo(), a, p);
						}

						respostaDao.insertResposta(r);
					}
				}

				/*
				 * Perguntas associadas apenas com conceitos
				 */
				else if (!p.getConceitos().isEmpty()
						&& p.getComodos().isEmpty()
						&& p.getAtributos().isEmpty()) {
					for (Conceito con : p.getConceitos()) {
						Resposta r = null;
						if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
							r = prepararEscolhaUnica(con.getRadios(), con,
									new Comodo(), new Atributo(), p);
						}
						respostaDao.insertResposta(r);
					}
				}

				/*
				 * Perguntas associadas com cômodos e conceitos
				 */
				else if (!p.getConceitos().isEmpty()
						&& !p.getComodos().isEmpty()
						&& p.getAtributos().isEmpty()) {
					for (Comodo cm : p.getComodos()) {
						for (Conceito con : cm.getConceitos()) {
							Resposta r = null;
							if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
								r = prepararEscolhaUnica(con.getRadios(), con,
										cm, new Atributo(), p);
								respostaDao.insertResposta(r);
							}
						}
					}
				}

				/*
				 * Perguntas associadas com conceitos e atributos
				 */
				else if (!p.getConceitos().isEmpty()
						&& p.getComodos().isEmpty()
						&& !p.getAtributos().isEmpty()) {
					for (Conceito con : p.getConceitos()) {
						for (Atributo a : con.getAtributos()) {
							Resposta r = null;
							if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
								r = prepararEscolhaUnica(a.getRadios(), con,
										new Comodo(), a, p);
							}
							else if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
								r = prepararMultiplaEscolha(a.getChecks(),
										con, new Comodo(), a, p);
							}
							respostaDao.insertResposta(r);
						}
					}
				}
			}
		}

		/*
		 * Fechando a conexão com o banco de dados
		 */
		respostaDao.close();
	}

	private Resposta prepararEscolhaUnica(ArrayList<RadioButtonMod> radios,
			Conceito conceito, Comodo comodo, Atributo atributo,
			Pergunta pergunta) {
		Resposta r = new Resposta();
		r.setApo(apo);
		r.setAtributo(atributo); // significa ausência dessa entidade, pois id =
									// 0
		r.setComodo(comodo);
		r.setConceito(conceito);
		r.setMorador(Constantes.morador);
		r.setPergunta(pergunta);

		for (RadioButtonMod rb : radios) {
			if (rb.isChecked()) {
				r.setTexto(rb.getTag() + "");
				break;
			}
		}
		return r;
	}

	private Resposta prepararMultiplaEscolha(ArrayList<CheckBoxMod> checks,
			Conceito conceito, Comodo comodo, Atributo atributo,
			Pergunta pergunta) {
		Resposta r = new Resposta();
		r.setApo(apo);
		r.setAtributo(atributo); // significa ausência dessa entidade, pois id =
									// 0
		r.setComodo(comodo);
		r.setConceito(conceito);
		r.setMorador(Constantes.morador);
		r.setPergunta(pergunta);
		r.setTexto("");

		for (CheckBoxMod cb : checks) {
			if (cb.isChecked()) {
				r.setTexto(r.getTexto() + cb.getTag() + ";");
			}
		}
		return r;
	}

	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.

		mViewPager.setCurrentItem(tab.getPosition());
	}

	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the primary sections of the app.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			// Fragment fragment = new DummySectionFragment();
			Fragment fragment = abas.get(i);
			Bundle args = new Bundle();
			// args.putSerializable("categoria",
			// tecnica.getCategorias().get(i));
			args.putInt("indiceCategoria", i);
			fragment.setArguments(args);

			if (!abasVisitadas[i]) {
				abasVisitadas[i] = true;
			}
			return fragment;
		}

		@Override
		public int getCount() {
			return tecnica.getCategorias().size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return tecnica.getCategorias().get(position).getNome();
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {

		private Categoria c;
		private int index;
		private boolean visited = false;
		private ScrollViewMod root;

		private int[] colors = { Color.parseColor("#0a8d1a"),
				Color.parseColor("#6ee55c"), Color.parseColor("#f9ef14"),
				Color.parseColor("#fdb710"), Color.parseColor("#ff3f0e") };

		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
			setRetainInstance(true);
			Log.i("DUMMY FRAGMENT", "instanciando fragmento");
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			if (savedInstanceState != null) {
				index = savedInstanceState.getInt("indiceCategoria");
				root = (ScrollViewMod) savedInstanceState
						.getSerializable("view");
				Log.i("DUMMY FRAGMENT", "Recuperando view já criada...");
			}
		}

		@Override
		public void onSaveInstanceState(Bundle outState) {
			super.onSaveInstanceState(outState);
			outState.putInt("indiceCategoria", index);
			outState.putSerializable("view", root);
		}

		@Override
		public void onDestroyView() {
			super.onDestroyView();
			if (root != null) {
				ViewGroup parentViewGroup = (ViewGroup) root.getParent();
				if (parentViewGroup != null) {
					parentViewGroup.removeAllViews();
				}
			}
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			if (root == null) {
				/*
				 * Recuperando a referência da categoria deste fragmento
				 */
				Bundle args = getArguments();
				index = args.getInt("indiceCategoria");

				c = Constantes.tecnicasGlobal.getCategorias().get(index);

				Log.i("DUMMY FRAGMENT",
						"criando view do fragmento com índice: " + index);
				// Toast.makeText(getActivity(), "Criando view...",
				// Toast.LENGTH_SHORT).show();
				/*
				 * Cálculo do tamanho da tela do dispositivo
				 */
				DisplayMetrics dm = new DisplayMetrics();
				getActivity().getWindowManager().getDefaultDisplay()
						.getMetrics(dm);

				double tamanhoTela = CommonServices.screenDeviceSize(dm);

				/*
				 * Fator multiplicador do tamanho e distância dos elementos.
				 * Para telas abaixo de 5 polegadas o valor é 1, acima disso o
				 * valor é 2
				 */
				boolean screen10 = false;
				if (tamanhoTela > 5) {
					screen10 = true;
				}

				/*
				 * Criando os componentes principais do fragmento
				 */
				root = new ScrollViewMod(getActivity());

				LinearLayout ll = new LinearLayout(getActivity());
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				ll.setGravity(Gravity.CENTER);
				ll.setOrientation(LinearLayout.VERTICAL);
				ll.setLayoutParams(params);

				root.addView(ll);

				/*
				 * Verificando tamanho do texto
				 */
				int textSize = 15;
				if (screen10) {
					textSize = 20;
				}

				/*
				 * Atributos de Layout das tabelas e linhas
				 */
				TableLayout.LayoutParams tlp = new TableLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tlp.setMargins(0, 20, 0, 0);

				TableRow.LayoutParams tlpRow = new TableRow.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tlpRow.setMargins(0, 0, 0, 20);

				int marginLeft = 10;

				TableRow.LayoutParams tlpColAtributos = new TableRow.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tlpColAtributos.setMargins(10, 0, 0, 0);
				tlpColAtributos.gravity = Gravity.CENTER;

				TableRow.LayoutParams tlpCol = new TableRow.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				tlpCol.setMargins(10, 0, 0, 0);
				tlpCol.gravity = Gravity.CENTER;

				/*
				 * Criando as margens dos elementos
				 */
				LinearLayout.LayoutParams marginsTextoPergunta = new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				marginsTextoPergunta.setMargins(0, 60, 0, 0);

				for (Pergunta p : c.getPerguntas()) {
					/*
					 * Independente do tipo da pergunta, seu texto deve ser
					 * exibido
					 */
					TextView perguntaTexto = new TextView(getActivity());
					perguntaTexto.setText(p.getTexto());
					perguntaTexto.setTypeface(null, Typeface.BOLD);
					// perguntaTexto.setGravity(Gravity.CENTER);
					perguntaTexto.setLayoutParams(marginsTextoPergunta);
					ll.addView(perguntaTexto);

					perguntaTexto.setTextSize(TypedValue.COMPLEX_UNIT_SP,
							textSize);

					/*
					 * Tipo de pergunta sem relação com conceitos, cômodos ou
					 * atributos
					 */
					if (p.getConceitos().isEmpty() && p.getComodos().isEmpty()
							&& p.getAtributos().isEmpty()) {
						/*
						 * Primeiramente cria-se um TableLayout
						 */
						TableLayout tl = new TableLayout(getActivity());
						tl.setLayoutParams(tlp);
						tl.setBackgroundColor(Color.parseColor("#CCFFFF"));

						/*
						 * Acrescentando tabela ao LinearLayout
						 */
						ll.addView(tl);

						/*
						 * Por se tratar de uma pergunta simples, apenas uma
						 * linha deve ser criada e adicionada à tabela no caso
						 * de pergunta do tipo texto livre, caso contrário a
						 * linha criada servirá para a listagem das opções de
						 * resposta e outra linha deverá ser criada para a
						 * listagem dos componentes de entrada de dados
						 */
						TableRow tr = new TableRow(getActivity());
						tr.setLayoutParams(tlpRow);
						// tr.setBackgroundColor(Color.BLACK);
						tl.addView(tr);

						/*
						 * Verificação do tipo da pergunta. Por enquanto o
						 * sistema não terá suporte para perguntas do tipo
						 * intervalo numérico
						 */
						if (p.getTipo() == Pergunta.TEXTO) {
							p.setCampoResposta(new EditText(getActivity()));
							p.getCampoResposta().setLayoutParams(tlpCol);
							p.getCampoResposta().setGravity(Gravity.CENTER);
							p.getCampoResposta().setEms(50);
							tr.addView(p.getCampoResposta());
						}
						/*
						 * Pergunta na qual sua resposta deve ser escolhida a
						 * partir de um conjunto pré-definido
						 */
						else {
							boolean finished = false;
							boolean rollbackQ = false;
							boolean rollbackE = false;
							int contQ = 1, contE = 1;
							while (!finished) {
								/*
								 * Linha para os qualificadores
								 */
								tr = new TableRow(getActivity());
								tr.setLayoutParams(tlpRow);
								tl.addView(tr);

								/*
								 * Linha para as entradas de respostas
								 */
								TableRow trComp = new TableRow(getActivity());
								trComp.setLayoutParams(tlpRow);
								tl.addView(trComp);

								/*
								 * Criando linha com no máximo 6 qualificadores
								 */
								while (contQ % 7 != 0 || rollbackQ) {
									if (contQ <= p.getQualificadores().size()) {
										TextView tv = new TextView(
												getActivity());
										tv.setText(p.getQualificadores()
												.get(contQ - 1).getTexto());
										tv.setLayoutParams(tlpColAtributos);
										tv.setWidth(160);
										// tv.setBackgroundColor(Color.WHITE);
										// tv.setGravity(Gravity.CENTER);
										tv.setTextSize(
												TypedValue.COMPLEX_UNIT_SP,
												textSize);
										tr.addView(tv);
										Log.i("CR. RB", "criando rb " + contQ);
									} else {
										finished = true;
										break;
									}
									rollbackQ = false;
									contQ++;
								}
								rollbackQ = true;
								int contadorCor = 0;

								/*
								 * Criando linha com no máximo 6 componentes de
								 * entrada de resposta
								 */
								while (contE % 7 != 0 || rollbackE) {
									if (contE <= p.getQualificadores().size()) {
										if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
											CheckBoxMod cb = new CheckBoxMod(
													getActivity());
											cb.setLayoutParams(tlpCol);
											cb.setGravity(Gravity.CENTER);
											cb.setTag(p.getQualificadores()
													.get(contE - 1).getTexto());
											cb.setPergunta(p);
											cb.setWidth(160);

											cb.setOnClickListener(new OnClickListener() {

												public void onClick(View v) {
													String tag = (String) v
															.getTag();
													CheckBoxMod cbm = (CheckBoxMod) v;
													int i = 0;
													for (Qualificador q : cbm
															.getPergunta()
															.getQualificadores()) {
														if (tag.equals(q
																.getTexto())) {
															cbm.getPergunta().qualificadoresMarcados[i] = cbm
																	.isActivated();
														}
														i++;
													}
												}
											});

											trComp.addView(cb);
											p.getChecks().add(cb);
										} else if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {

											RadioButtonMod rb = new RadioButtonMod(
													getActivity());
											rb.setLayoutParams(tlpCol);
											rb.setGravity(Gravity.CENTER);
											rb.setTag(p.getQualificadores()
													.get(contE - 1).getTexto());
											if (p.getEscalaDeCores() == 1) {
												rb.setBackgroundColor(colors[contadorCor]);
												contadorCor++;
											}
											rb.setPergunta(p);
											rb.setWidth(160);

											rb.setOnClickListener(new OnClickListener() {

												public void onClick(View v) {
													RadioButtonMod mod = (RadioButtonMod) v;
													String tag = (String) mod
															.getTag();
													int i = 0;
													for (Qualificador q : mod
															.getPergunta()
															.getQualificadores()) {
														if (tag.equals(q
																.getTexto())) {
															mod.getPergunta().qualificadoresMarcados[i] = true;
														} else {
															mod.getPergunta()
																	.getRadios()
																	.get(i)
																	.setChecked(
																			false);
															mod.getPergunta().qualificadoresMarcados[i] = false;
														}
														i++;
													}
												}
											});
											p.getRadios().add(rb);
											trComp.addView(rb);
											// marcaRadioButton(p);
										}
									} else {
										break;
									}
									rollbackE = false;
									contE++;
								}
								rollbackE = true;
							}

						}
					}

					/*
					 * Listagem das perguntas onde há associação apenas com
					 * atributos
					 */
					else if (p.getConceitos().isEmpty()
							&& p.getComodos().isEmpty()
							&& !p.getAtributos().isEmpty()) {
						constroiAtributo(ll, p, tlp, tlpCol, tlpColAtributos,
								tlpRow, textSize);
					}

					/*
					 * Listagem das perguntas onde há associação apenas com
					 * conceitos
					 */
					else if (!p.getConceitos().isEmpty()
							&& p.getComodos().isEmpty()
							&& p.getAtributos().isEmpty()) {
						constroiConceito(ll, p, tlp, tlpCol, tlpColAtributos,
								tlpRow, textSize);
					}

					/*
					 * Listagem das perguntas associadas à cômodos e conceitos
					 */
					else if (!p.getConceitos().isEmpty()
							&& !p.getComodos().isEmpty()
							&& p.getAtributos().isEmpty()) {
						constroiComodoConceito(ll, p, tlp, tlpCol,
								tlpColAtributos, tlpRow, textSize);
					}

					/*
					 * Listagem das perguntas associadas à conceitos e atributos
					 */
					else if (!p.getConceitos().isEmpty()
							&& !p.getAtributos().isEmpty()
							&& p.getComodos().isEmpty()) {
						constroiConceitoAtributo(ll, p, tlp, tlpCol,
								tlpColAtributos, tlpRow, textSize);
					}
				}
			}

			return root;
		}

		/*
		 * Método que recebe uma view container e uma pergunta que está
		 * associada apenas à atributos, além de layouts já criados para
		 * associação com as view criadas pelo método.
		 */
		private void constroiAtributo(LinearLayout container, Pergunta p,
				TableLayout.LayoutParams tbl, TableRow.LayoutParams tbrl,
				TableRow.LayoutParams tbrlQualificadores,
				TableRow.LayoutParams tbrlRow, int textSize) {
			/*
			 * Criação de layouts específicos
			 */
			TableRow.LayoutParams layoutArtificial = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutArtificial.setMargins(0, 0, 0, 0);

			TableRow.LayoutParams layoutAtributos = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutAtributos.setMargins(0, 0, 0, 10);
			layoutAtributos.gravity = Gravity.CENTER;

			/*
			 * view com o texto da pergunta já é criada no método principal,
			 * esta função apenas criará as view dos textos de atributos e
			 * qualificadores e dos componentes de entrada de respostas. Ao
			 * contrário do caso em que não há associação de uma pergunta com
			 * outras entidades além de qualificadores, os qualificadores não
			 * serão quebrados em mais de uma linha.
			 */
			TableLayout tl = new TableLayout(getActivity());
			tl.setLayoutParams(tbl);
			tl.setBackgroundColor(Color.parseColor("#CCFFFF"));
			container.addView(tl);

			/*
			 * Primeiro TableRow para a listagem de qualificadores
			 */
			TableRow tbrQualificadores = new TableRow(getActivity());
			tbrQualificadores.setLayoutParams(tbrlQualificadores);
			tl.addView(tbrQualificadores);

			/*
			 * Primeiramente cria-se um TextView vazio para preencher a coluna
			 * de atributos que não aparece na linha de qualificadores
			 */
			TextView artificial = new TextView(getActivity());
			artificial.setText("");
			artificial.setLayoutParams(layoutArtificial);
			tbrQualificadores.addView(artificial);

			/*
			 * Iteração na lista de qualificadores da pergunta para criação das
			 * views dos qualificadores
			 */
			for (Qualificador q : p.getQualificadores()) {
				TextView tv = new TextView(getActivity());
				tv.setText(q.getTexto());
				tv.setLayoutParams(tbrlQualificadores);
				tv.setWidth(160);
				tv.setGravity(Gravity.CENTER);
				tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
				tbrQualificadores.addView(tv);
			}

			/*
			 * Para cada atributo uma linha será criada, onde na primeira coluna
			 * será exibido o nome do atributo e nas demais o componente de
			 * entrada de resposta para cada qualificador
			 */
			for (Atributo a : p.getAtributos()) {
				/*
				 * Linha é criada na tabela para cada atributo
				 */
				TableRow trComp = new TableRow(getActivity());
				trComp.setLayoutParams(tbrlRow);
				tl.addView(trComp);

				/*
				 * Texto do atributo
				 */
				TextView tv = new TextView(getActivity());
				tv.setText(a.getNome());
				tv.setLayoutParams(layoutAtributos);
				tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
				tv.setWidth(160);
				trComp.addView(tv);

				/*
				 * Para cada qualificador criar os componentes para entrada de
				 * resposta adequado ao tipo da pergunta
				 */
				int contadorCor = 0;
				for (Qualificador q : p.getQualificadores()) {
					if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
						CheckBoxMod cb = new CheckBoxMod(getActivity());
						cb.setLayoutParams(tbrl);
						cb.setGravity(Gravity.CENTER);
						cb.setTag(q.getTexto());
						cb.setWidth(160);
						cb.setPergunta(p);
						cb.setAtributo(a);

						cb.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								String tag = (String) v.getTag();
								CheckBoxMod cbm = (CheckBoxMod) v;
								int i = 0;
								for (Qualificador q : cbm.getPergunta()
										.getQualificadores()) {
									if (tag.equals(q.getTexto())) {
										cbm.getAtributo().qualificadoresMarcados[i] = cbm
												.isActivated();
									}
									i++;
								}
							}
						});

						trComp.addView(cb);
						a.getChecks().add(cb);
					}
					else if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {

						RadioButtonMod rb = new RadioButtonMod(getActivity());
						rb.setLayoutParams(tbrl);
						rb.setGravity(Gravity.CENTER);
						rb.setTag(q.getTexto());
						if (p.getEscalaDeCores() == 1) {
							rb.setBackgroundColor(colors[contadorCor]);
							contadorCor++;
						}
						rb.setWidth(160);
						rb.setPergunta(p);
						rb.setAtributo(a);

						rb.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								RadioButtonMod mod = (RadioButtonMod) v;
								String tag = (String) mod.getTag();
								int i = 0;
								for (Qualificador q : mod.getPergunta()
										.getQualificadores()) {
									if (tag.equals(q.getTexto())) {
										mod.getAtributo().qualificadoresMarcados[i] = true;
									} else {
										mod.getAtributo().getRadios().get(i)
												.setChecked(false);
										mod.getAtributo().qualificadoresMarcados[i] = false;
									}
									i++;
								}
							}
						});
						a.getRadios().add(rb);
						trComp.addView(rb);
					}
				}
			}
		}

		/*
		 * Método que recebe uma view container e uma pergunta que está
		 * associada apenas à conceitos, além de layouts já criados para
		 * associação com as view criadas pelo método.
		 */
		private void constroiConceito(LinearLayout container, Pergunta p,
				TableLayout.LayoutParams tbl, TableRow.LayoutParams tbrl,
				TableRow.LayoutParams tbrlQualificadores,
				TableRow.LayoutParams tbrlRow, int textSize) {
			/*
			 * Criação de layouts específicos
			 */
			TableRow.LayoutParams layoutArtificial = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutArtificial.setMargins(0, 0, 0, 0);

			TableRow.LayoutParams layoutAtributos = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutAtributos.setMargins(0, 0, 0, 10);
			layoutAtributos.gravity = Gravity.CENTER;

			/*
			 * view com o texto da pergunta já é criada no método principal,
			 * esta função apenas criará as view dos textos de atributos e
			 * qualificadores e dos componentes de entrada de respostas. Ao
			 * contrário do caso em que não há associação de uma pergunta com
			 * outras entidades além de qualificadores, os qualificadores não
			 * serão quebrados em mais de uma linha.
			 */
			TableLayout tl = new TableLayout(getActivity());
			tl.setLayoutParams(tbl);
			tl.setBackgroundColor(Color.parseColor("#CCFFFF"));
			container.addView(tl);

			/*
			 * Para cada conceito uma linha será criada, onde na primeira coluna
			 * será exibido o nome do atributo e nas demais o componente de
			 * entrada de resposta para cada qualificador
			 */
			for (Conceito c : p.getConceitos()) {
				/*
				 * Linha é criada na tabela para cada atributo
				 */
				TableRow trComp = new TableRow(getActivity());
				trComp.setLayoutParams(tbrlRow);
				tl.addView(trComp);

				/*
				 * Texto do conceito
				 */
				TextView tv = new TextView(getActivity());
				tv.setText(c.getNome());
				tv.setLayoutParams(layoutAtributos);
				tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
				tv.setWidth(160);
				trComp.addView(tv);

				/*
				 * Para cada qualificador criar os componentes para entrada de
				 * resposta adequado ao tipo da pergunta
				 */
				int contadorCor = 0;
				for (Qualificador q : c.getQualificadores()) {
					if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {

						RadioButtonMod rb = new RadioButtonMod(getActivity());
						rb.setLayoutParams(tbrl);
						// rb.setGravity(Gravity.CENTER);
						rb.setTag(q.getTexto());
						rb.setText(q.getTexto());
						if (p.getEscalaDeCores() == 1) {
							rb.setBackgroundColor(colors[contadorCor]);
							contadorCor++;
						}
						rb.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
						rb.setWidth(160);
						rb.setPergunta(p);
						rb.setConceito(c);

						rb.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {
								RadioButtonMod mod = (RadioButtonMod) v;
								String tag = (String) mod.getTag();
								int i = 0;
								for (Qualificador q : mod.getConceito()
										.getQualificadores()) {
									if (tag.equals(q.getTexto())) {
										mod.getConceito().qualificadoresMarcados[i] = true;
									} else {
										mod.getConceito().getRadios().get(i)
												.setChecked(false);
										mod.getConceito().qualificadoresMarcados[i] = false;
									}
									i++;
								}
							}
						});
						c.getRadios().add(rb);
						trComp.addView(rb);
					}
				}
			}
		}

		/*
		 * Método que recebe uma view container e uma pergunta que está
		 * associada apenas à cômodos e conceitos, além de layouts já criados
		 * para associação com as view criadas pelo método.
		 */
		private void constroiComodoConceito(LinearLayout container, Pergunta p,
				TableLayout.LayoutParams tbl, TableRow.LayoutParams tbrl,
				TableRow.LayoutParams tbrlQualificadores,
				TableRow.LayoutParams tbrlRow, int textSize) {
			/*
			 * Criação de layouts específicos
			 */
			TableRow.LayoutParams layoutArtificial = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutArtificial.setMargins(0, 0, 0, 0);

			tbrl = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutArtificial.setMargins(20, 0, 0, 0);
			layoutArtificial.gravity = Gravity.CENTER;

			TableRow.LayoutParams layoutAtributos = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutAtributos.setMargins(0, 0, 0, 10);
			layoutAtributos.gravity = Gravity.CENTER;

			LinearLayout.LayoutParams layoutComodo = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutComodo.setMargins(5, 30, 0, 0);

			/*
			 * Instanciando arrays de componentes de entrada de resposta de cada
			 * conceito de cada cômodo
			 */
			for (Comodo cm : p.getComodos()) {
				for (Conceito c : cm.getConceitos()) {
					c.setRadios(new ArrayList<RadioButtonMod>());
					c.qualificadoresMarcados = new boolean[p
							.getQualificadores().size()];
				}
			}

			/*
			 * Itera-se na lista de cômodos de cada pergunta
			 */
			for (Comodo cm : p.getComodos()) {
				/*
				 * Texto do cômodo é criado no container passado como parâmetro
				 */
				TextView nomeComodo = new TextView(getActivity());
				nomeComodo.setText(cm.getNome());
				nomeComodo.setLayoutParams(layoutComodo);
				nomeComodo.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
				nomeComodo.setTextColor(Color.GRAY);
				container.addView(nomeComodo);

				/*
				 * TableLayout do cômodo
				 */
				TableLayout tl = new TableLayout(getActivity());
				tl.setLayoutParams(tbl);
				tl.setBackgroundColor(Color.parseColor("#CCFFFF"));
				container.addView(tl);

				/*
				 * Primeiro TableRow para a listagem de qualificadores
				 */
				/*
				 * TableRow tbrQualificadores = new TableRow(getActivity());
				 * tbrQualificadores.setLayoutParams(tbrlQualificadores);
				 * tl.addView(tbrQualificadores);
				 */

				/*
				 * Primeiramente cria-se um TextView vazio para preencher a
				 * coluna de atributos que não aparece na linha de
				 * qualificadores
				 */
				/*
				 * TextView artificial = new TextView(getActivity());
				 * artificial.setText("");
				 * artificial.setLayoutParams(layoutArtificial);
				 * tbrQualificadores.addView(artificial);
				 */

				/*
				 * Iteração na lista de qualificadores da pergunta para criação
				 * das views dos qualificadores
				 */
				/*
				 * for (Qualificador q : p.getQualificadores()) { TextView tv =
				 * new TextView(getActivity()); tv.setText(q.getTexto());
				 * tv.setLayoutParams(tbrlQualificadores); tv.setWidth(160);
				 * tv.setGravity(Gravity.CENTER);
				 * tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
				 * tbrQualificadores.addView(tv); }
				 */

				/*
				 * Criação das views com os textos dos conceitos e componentes
				 * de entrada de respostas. Apenas será suportado perguntas do
				 * tipo Escolha Única
				 */
				for (Conceito c : cm.getConceitos()) {
					/*
					 * Linha é criada na tabela para cada atributo
					 */
					TableRow trComp = new TableRow(getActivity());
					trComp.setLayoutParams(tbrlRow);
					// trComp.setBackgroundColor(Color.GREEN);
					tl.addView(trComp);
					c.setRadios(new ArrayList<RadioButtonMod>());

					/*
					 * Texto do conceito
					 */
					TextView tv = new TextView(getActivity());
					tv.setText(c.getNome());
					tv.setLayoutParams(layoutAtributos);
					tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
					tv.setWidth(160);
					trComp.addView(tv);

					int index = 0;
					for (Qualificador q : c.getQualificadores()) {
						if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {

							RadioButtonMod rb = new RadioButtonMod(
									getActivity());
							rb.setLayoutParams(tbrl);
							// rb.setGravity(Gravity.CENTER);
							rb.setTag(q.getTexto());
							rb.setPergunta(p);
							rb.setText(q.getTexto());
							if (p.getEscalaDeCores() == 1) {
								rb.setBackgroundColor(colors[index]);
							}
							index++;
							rb.setWidth(160);
							rb.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
							rb.setConceito(c);

							rb.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									RadioButtonMod mod = (RadioButtonMod) v;
									String tag = (String) mod.getTag();
									int i = 0;
									for (Qualificador q : mod.getConceito()
											.getQualificadores()) {
										if (tag.equals(q.getTexto())) {
											mod.getConceito().qualificadoresMarcados[i] = true;
										} else {
											mod.getConceito().getRadios()
													.get(i).setChecked(false);
											mod.getConceito().qualificadoresMarcados[i] = false;
										}
										i++;
									}
								}
							});
							c.getRadios().add(rb);
							trComp.addView(rb);
						}
					}
				}
			}
		}

		/*
		 * Método que recebe uma view container e uma pergunta que está
		 * associada apenas à conceitos e atributos, além de layouts já criados
		 * para associação com as view criadas pelo método.
		 */
		private void constroiConceitoAtributo(LinearLayout container,
				Pergunta p, TableLayout.LayoutParams tbl,
				TableRow.LayoutParams tbrl,
				TableRow.LayoutParams tbrlQualificadores,
				TableRow.LayoutParams tbrlRow, int textSize) {
			/*
			 * Criação de layouts específicos
			 */
			TableRow.LayoutParams layoutArtificial = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutArtificial.setMargins(0, 0, 0, 0);

			TableRow.LayoutParams layoutAtributos = new TableRow.LayoutParams(
					android.widget.TableRow.LayoutParams.WRAP_CONTENT,
					android.widget.TableRow.LayoutParams.WRAP_CONTENT);
			layoutAtributos.setMargins(0, 0, 0, 10);
			layoutAtributos.gravity = Gravity.CENTER;

			LinearLayout.LayoutParams layoutConceito = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutConceito.setMargins(5, 30, 0, 0);

			/*
			 * Itera-se na lista de cômodos de cada pergunta
			 */
			for (Conceito c : p.getConceitos()) {
				/*
				 * Texto do cômodo é criado no container passado como parâmetro
				 */
				TextView nomeConceito = new TextView(getActivity());
				nomeConceito.setText(c.getNome());
				nomeConceito.setLayoutParams(layoutConceito);
				nomeConceito.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
				nomeConceito.setTextColor(Color.GRAY);
				container.addView(nomeConceito);

				/*
				 * TableLayout do cômodo
				 */
				TableLayout tl = new TableLayout(getActivity());
				tl.setLayoutParams(tbl);
				tl.setBackgroundColor(Color.parseColor("#CCFFFF"));
				container.addView(tl);

				/*
				 * Criação das views com os textos dos conceitos e componentes
				 * de entrada de respostas. Apenas será suportado perguntas do
				 * tipo Escolha Única
				 */
				for (Atributo a : c.getAtributos()) {
					/*
					 * Linha é criada na tabela para cada atributo
					 */
					TableRow trComp = new TableRow(getActivity());
					trComp.setLayoutParams(tbrlRow);
					tl.addView(trComp);
					// a.setRadios(new ArrayList<RadioButtonMod>());

					/*
					 * Texto do atributo
					 */
					TextView tv = new TextView(getActivity());
					tv.setText(a.getNome());
					tv.setLayoutParams(layoutAtributos);
					tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
					tv.setWidth(160);
					trComp.addView(tv);

					int contadorCor = 0;
					for (Qualificador q : a.getQualificadores()) {
						if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {

							RadioButtonMod rb = new RadioButtonMod(
									getActivity());
							rb.setLayoutParams(tbrl);
							// rb.setGravity(Gravity.CENTER);
							rb.setText(q.getTexto());
							rb.setTag(q.getTexto());
							if (p.getEscalaDeCores() == 1) {
								rb.setBackgroundColor(colors[contadorCor]);
								contadorCor++;
							}
							rb.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
							rb.setWidth(160);
							rb.setPergunta(p);
							rb.setAtributo(a);

							rb.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									RadioButtonMod mod = (RadioButtonMod) v;
									String tag = (String) mod.getTag();
									int i = 0;
									for (Qualificador q : mod.getAtributo()
											.getQualificadores()) {
										if (tag.equals(q.getTexto())) {
											mod.getAtributo().qualificadoresMarcados[i] = true;
										} else {
											mod.getAtributo().getRadios()
													.get(i).setChecked(false);
											mod.getAtributo().qualificadoresMarcados[i] = false;
										}
										i++;
									}
								}
							});
							a.getRadios().add(rb);
							trComp.addView(rb);
						}
						else if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
							CheckBoxMod cb = new CheckBoxMod(getActivity());
							cb.setLayoutParams(tbrl);
							//cb.setGravity(Gravity.CENTER);
							cb.setTag(q.getTexto());
							cb.setText(q.getTexto());
							cb.setWidth(160);
							cb.setPergunta(p);
							cb.setAtributo(a);

							cb.setOnClickListener(new OnClickListener() {

								public void onClick(View v) {
									String tag = (String) v.getTag();
									CheckBoxMod cbm = (CheckBoxMod) v;
									int i = 0;
									for (Qualificador q : cbm.getPergunta()
											.getQualificadores()) {
										if (tag.equals(q.getTexto())) {
											cbm.getAtributo().qualificadoresMarcados[i] = cbm
													.isActivated();
										}
										i++;
									}
								}
							});

							trComp.addView(cb);
							a.getChecks().add(cb);
						}
					}
				}
			}
		}

		private void marcaCheckBox(Pergunta p) {
			int i = 0;
			for (Qualificador q : p.getQualificadores()) {
				if (p.qualificadoresMarcados[i]) {
					p.getChecks().get(i).setChecked(true);
				}
				i++;
			}
		}

		private void marcaRadioButton(Pergunta p) {
			int i = 0;
			for (Qualificador q : p.getQualificadores()) {
				if (p.qualificadoresMarcados[i]) {
					p.getRadios().get(i).setChecked(true);
				}
				i++;
			}
		}
	}
}
