package br.com.apodigital.activities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import br.com.apodigital.R;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;
import br.com.apodigital.dao.ApoDao;
import br.com.apodigital.dao.BackupDao;
import br.com.apodigital.dao.MoradorDao;
import br.com.apodigital.dao.RespostaDao;
import br.com.apodigital.migration.ApoClient;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.Morador;
import br.com.apodigital.model.Resposta;
import br.com.apodigital.model.RespostaPacote;
import br.com.apodigital.model.StatusResposta;
import br.com.apodigital.utils.ApoAdapter;
import br.com.apodigital.utils.ApplicationController;
import br.com.apodigital.utils.Constantes;
import br.com.apodigital.utils.DialogHandler;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class TransferirRespostasActivity extends Activity {

	private ArrayList<APO> apos;
	private APO[] aposVector;
	private ListView listApos;

	private APO apoEscolhida;

	private RespostaPacote resPac;

	private Map<String, String> params = new HashMap<String, String>();

	private boolean bloqueado; // indica se transferência está bloqueada ou não

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transferir_respostas);

		bloqueado = false;
		
		/*RespostaDao respDao = new RespostaDao(getApplicationContext());
		respDao.open();
		respDao.deSincronizarResposta();
		respDao.close();
		
		MoradorDao moradorDao = new MoradorDao(getApplicationContext());
		moradorDao.open();
		moradorDao.deSincronizarMorador();
		moradorDao.close();*/

		ApoDao apoDao = new ApoDao(getApplicationContext());
		apoDao.open();
		apos = apoDao.buscarTodos();
		apoDao.close();

		Button backup = (Button) findViewById(R.id.backup);
		backup.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				BackupDao backupDao = new BackupDao();
				backupDao.backup();
			}
		});

		aposVector = new APO[apos.size()];
		int i = 0;
		for (APO apo : apos) {
			aposVector[i] = apo;
			i++;
		}

		ApoAdapter apoAdapter = new ApoAdapter(
				TransferirRespostasActivity.this, R.layout.apo_list_layout,
				aposVector);
		ListView lv = (ListView) findViewById(R.id.list_apos_respostas);
		lv.setAdapter(apoAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (!bloqueado) {
					Log.i("JSON-POST", "REALIZANDO REQUISIÇÃO POST...");
					bloqueado = true; // bloqueia novas transferências
					DialogHandler appdialog = new DialogHandler();
					apoEscolhida = aposVector[position];

					/*
					 * Buscando as respostas da APO escolhida
					 */
					RespostaDao respostaDao = new RespostaDao(
							getApplicationContext());
					respostaDao.open();
					ArrayList<Resposta> respostas = respostaDao
							.buscarRespostasPorApo(apoEscolhida);
					respostaDao.close();

					/*
					 * Buscando moradores
					 */
					MoradorDao moradorDao = new MoradorDao(getApplicationContext());
					moradorDao.open();
					ArrayList<Morador> moradores = moradorDao.buscarPorApoSinc(
							apoEscolhida, 0);
					moradorDao.close();

					RespostaPacote rp = new RespostaPacote();
					rp.setRespostas(respostas);
					rp.setMoradores(moradores);

					resPac = rp;

					/*
					 * ApoClient apoClient = new ApoClient(); String res =
					 * apoClient.enviarRespostasGet(rp);
					 */
					// Toast.makeText(getApplicationContext(), res,
					// Toast.LENGTH_LONG).show();
					Gson gson = new Gson();
					saveJson(gson.toJson(rp));

					/*
					 * BackupDao backupDao = new BackupDao();
					 * backupDao.backup();
					 */

					/*
					 * JsonPost with Volley
					 */
					params.put("", gson.toJson(rp));
					JsonObjectRequest req = new JsonObjectRequest(
							Constantes.ENVIAR_REPOSTAS, new JSONObject(params),
							new Response.Listener<JSONObject>() {
								public void onResponse(JSONObject response) {
									try {
										StatusResposta sr = new Gson()
												.fromJson(response.toString(),
														StatusResposta.class);
										String msg;
										if (sr.isOk()) {
											msg = "Transferência de respostas realizada com sucesso.";

											/*
											 * Marcando todas as respostas e
											 * moradores enviados como
											 * sincronizados
											 */
											MoradorDao moradorDao = new MoradorDao(
													getApplicationContext());
											moradorDao.open();
											/*for (Morador m : resPac
													.getMoradores()) {
												moradorDao
														.sincronizarMorador(m);
											}*/
											moradorDao.sincronizarMoradorPorApo(apoEscolhida);
											moradorDao.close();

											RespostaDao respostaDao = new RespostaDao(
													getApplicationContext());
											respostaDao.open();
											/*for (Resposta r : resPac
													.getRespostas()) {
												respostaDao
														.sincronizarResposta(r);
											}*/
											respostaDao.sincronizarRespostaPorApo(apoEscolhida);
											respostaDao.close();
										}
										else {
											msg = "Falha na transferência de respostas...";
										}
										Toast.makeText(getApplicationContext(),
												msg, Toast.LENGTH_LONG).show();
									}
									catch (Exception e) {
										e.printStackTrace();
									}
									finally{
										bloqueado = false;
									}
								}
							}, new Response.ErrorListener() {

								public void onErrorResponse(VolleyError error) {
									VolleyLog.e("Error: ", error.getMessage());
									Toast.makeText(
											getApplicationContext(),
											"Erro na conexão com o servidor...",
											Toast.LENGTH_LONG).show();
									
									bloqueado = false;
								}

							});

					ApplicationController.getInstance().cancelPendingRequests("ENVIAR_RESPOSTAS");
					ApplicationController.getInstance().addToRequestQueue(req, "ENVIAR_RESPOSTAS");
				}
			}

			private void saveJson(String json) {
				File file = null;
				FileWriter fw = null;
				BufferedWriter bw = null;
				try {
					file = new File("/sdcard/respostas.txt");
					if (!file.exists()) {
						file.createNewFile();
					}

					fw = new FileWriter(file.getAbsoluteFile());
					bw = new BufferedWriter(fw);
					bw.write(json);
				} catch (IOException ioe) {
					ioe.printStackTrace();
				} finally {
					if (bw != null) {
						try {
							bw.close();
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}
					}
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_transferir_respostas, menu);
		return true;
	}
}
