package br.com.apodigital.activities;

import java.util.ArrayList;

import br.com.apodigital.R;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class RootActivity extends Activity {
	
	private static final String TAG = RootActivity.class.getName();
    private static ArrayList<Activity> activities = new ArrayList<Activity>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activities.add(this);
    }

    public void destroy(){
    	activities.remove(this);
    }
    
}
