package br.com.apodigital.activities;

import java.util.ArrayList;

import br.com.apodigital.R;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;
import br.com.apodigital.dao.ApoDao;
import br.com.apodigital.dao.MoradorDao;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.Morador;
import br.com.apodigital.utils.ApoAdapter;
import br.com.apodigital.utils.MoradorAdapter;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ListarMoradoresActivity extends Activity {
	
	private ListView lv;
	private ArrayList<APO> apos;
	private APO[] aposVector;
	
	private AlertDialog alertDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_moradores);
        
        ApoDao apoDao = new ApoDao(getApplicationContext());
        apoDao.open();
        apos = apoDao.buscarTodos();
        apoDao.close();
        
        /*
         * Para cada APO buscar seus moradores
         */
        MoradorDao moradorDao = new MoradorDao(getApplicationContext());
        moradorDao.open();
        for(APO apo : apos){
        	apo.setMoradores(moradorDao.buscarPorApo(apo));
        }
        
        moradorDao.close();
        
        /*
         * Passar cada avaliação para o vetor de avaliações
         */
        aposVector = new APO[apos.size()];
        int i = 0;
        for(APO apo : apos){
        	aposVector[i] = apo;
        	i++;
        }
        
        ApoAdapter apoAdapter = new ApoAdapter(ListarMoradoresActivity.this,
				R.layout.apo_list_layout, aposVector);
		ListView lv = (ListView) findViewById(R.id.list_apos_moradores);
		lv.setAdapter(apoAdapter);
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showDialog(apos.get(position));
			}
		});
    }
    
    public void showDialog(APO apoEscolhida){
        //View view = getLayoutInflater().inflate(R.layout.morador_list, null); 

        ListView list = (ListView) new ListView(ListarMoradoresActivity.this);
        MoradorAdapter adapter= new MoradorAdapter(ListarMoradoresActivity.this, R.layout.morador_list,
        		apoEscolhida.getMoradores());

        list.setAdapter(adapter);
        
        alertDialog = new AlertDialog.Builder(ListarMoradoresActivity.this)
        .setView(list)
        .setTitle("Moradores - " + apoEscolhida.getNome() + " (nº ap - bloco - tempo de avaliação)")
        .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_listar_moradores, menu);
        return true;
    }
}
