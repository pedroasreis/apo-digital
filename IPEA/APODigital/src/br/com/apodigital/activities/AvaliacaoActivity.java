package br.com.apodigital.activities;

import java.util.ArrayList;

import br.com.apodigital.dao.TecnicaDao;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.Questionario;

import br.com.apodigital.R;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.Menu;
import android.widget.TabHost;

public class AvaliacaoActivity extends FragmentActivity {
	
	private APO apo;
	private int respondente;
	
	private ArrayList<Questionario> tecnicas;
	
	private FragmentTabHost tabHost;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliacao);
        
        Bundle b = getIntent().getExtras();
		apo = (APO) b.getSerializable("apo");
		respondente = b.getInt("respondente");
		
		/*
		 * Buscando técnicas da avaliacão
		 */
		TecnicaDao tecnicaDao = new TecnicaDao(getApplicationContext());
		tecnicaDao.open();
		apo.setTecnicas(tecnicaDao.buscarPorApoRespondente(apo, respondente));
		tecnicaDao.close();
		
		/*
		 * Preparando as abas
		 */
		tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		tabHost.setup(AvaliacaoActivity.this, getSupportFragmentManager(), android.R.id.tabcontent);
		
		for(Questionario q : apo.getTecnicas()){
			Bundle bnd = new Bundle();
			bnd.putSerializable("tecnica", q);
			
			tabHost.addTab(tabHost.newTabSpec(q.getNome()).setIndicator(q.getNome(), null), 
					FragmentTab.class, bnd);
		}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_avaliacao, menu);
        return true;
    }
}
