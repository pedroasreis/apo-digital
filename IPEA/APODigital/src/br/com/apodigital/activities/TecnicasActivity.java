package br.com.apodigital.activities;

import br.com.apodigital.dao.TecnicaDao;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.Questionario;
import br.com.apodigital.utils.ApoAdapter;
import br.com.apodigital.utils.Constantes;
import br.com.apodigital.utils.DialogHandler;
import br.com.apodigital.utils.TecnicaAdapter;

import br.com.apodigital.R;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class TecnicasActivity extends RootActivity {
	
	private APO apo;
	private int respondente;
	
	private Questionario[] tecnicas;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tecnicas);
        
        /*
		 * Recuperando argumentos da activity anterior
		 */
		Bundle b = getIntent().getExtras();
		apo = (APO) b.getSerializable("apo");
		respondente = b.getInt("respondente");
		
		/*
		 * Definindo título da activity
		 */
		final ActionBar actionBar = getActionBar();
		actionBar.setTitle(apo.getNome());
		
		/*
		 * Buscando técnicas da avaliacão
		 */
		TecnicaDao tecnicaDao = new TecnicaDao(getApplicationContext());
		tecnicaDao.open();
		apo.setTecnicas(tecnicaDao.buscarPorApoRespondente(apo, respondente));
		tecnicaDao.close();
		tecnicas = new Questionario[apo.getTecnicas().size()];
		
		int i = 0;
		for(Questionario q : apo.getTecnicas()){
			if(i % 2 == 0){
				q.setCompleto(true);
			}
			tecnicas[i] = q;
			i++;
		}
		
		/*
		 * Povoando ListView das técnicas
		 */
		ListView lv = (ListView) findViewById(R.id.list_tecnicas);
		TecnicaAdapter tecnicaAdapter = new TecnicaAdapter(TecnicasActivity.this,
				R.layout.tecnicas_list, tecnicas);
		lv.setAdapter(tecnicaAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
			
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				DialogHandler appdialog = new DialogHandler();
				Questionario qEscolhido = tecnicas[position];
				
				/*
				 * Chamando próxima activity
				 */
				Intent i = new Intent(TecnicasActivity.this, TesteActivity.class);
				Bundle bnd = new Bundle();
				bnd.putSerializable("apo", apo);
				bnd.putSerializable("tecnica", qEscolhido);
				bnd.putInt("respondente", respondente);
				/*
				 * Técnica escolhida vai para memória com escopo de aplicação
				 */
				Constantes.tecnicasGlobal = qEscolhido;
				i.putExtras(bnd);
				startActivity(i);
			}
			
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_tecnicas, menu);
        return true;
    }
}
