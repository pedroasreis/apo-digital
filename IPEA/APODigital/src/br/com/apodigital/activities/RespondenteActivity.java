package br.com.apodigital.activities;

import br.com.apodigital.model.APO;
import br.com.apodigital.model.Morador;
import br.com.apodigital.model.Questionario;
import br.com.apodigital.utils.Constantes;

import br.com.apodigital.R;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class RespondenteActivity extends RootActivity {

	private APO apo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_respondente);

		Bundle b = getIntent().getExtras();
		apo = (APO) b.getSerializable("apo");
		
		final ActionBar actionBar = getActionBar();
		actionBar.setTitle(apo.getNome());

		Button pesquisador = (Button) findViewById(R.id.pesquisador_button);
		Button morador = (Button) findViewById(R.id.morador_button);

		/*
		 * Definição dos listeners dos botões
		 */
		pesquisador.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				/*
				 * Não há morador na sessão do pesquisador, portanto, seu id na tabela será 0
				 */
				Constantes.morador = new Morador();
				listener(Questionario.PESQUISADOR);
			}
		});

		morador.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				listener(Questionario.MORADOR);
			}
		});
	}

	public void listener(int respondente) {
		Intent i = null;
		if(respondente == Questionario.MORADOR){
			i = new Intent(RespondenteActivity.this, MoradorActivity.class);
		}
		else{
			i = new Intent(RespondenteActivity.this, TecnicasActivity.class);
		}
		Bundle b = new Bundle();
		b.putSerializable("apo", apo);
		b.putInt("respondente", respondente);
		i.putExtras(b);
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_respondente, menu);
		return true;
	}

}
