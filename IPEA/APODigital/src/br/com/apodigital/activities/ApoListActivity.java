package br.com.apodigital.activities;

import java.util.ArrayList;

import br.com.apodigital.dao.ApoDao;
import br.com.apodigital.model.APO;
import br.com.apodigital.utils.ApoAdapter;
import br.com.apodigital.utils.DialogHandler;

import br.com.apodigital.R;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;

public class ApoListActivity extends RootActivity {

	private APO[] apos;
	private APO apoEscolhida;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apo_list);

		ApoDao apoDao = new ApoDao(getApplicationContext());
		apoDao.open();
		ArrayList<APO> avaliacoes = apoDao.buscarTodos();
		apoDao.close();

		apos = new APO[avaliacoes.size()];
		int i = 0;
		for (APO a : avaliacoes) {
			apos[i] = a;
			i++;
		}

		ApoAdapter apoAdapter = new ApoAdapter(ApoListActivity.this,
				R.layout.apo_list_layout, apos);
		ListView lv = (ListView) findViewById(R.id.list_apos);
		lv.setAdapter(apoAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				DialogHandler appdialog = new DialogHandler();
				apoEscolhida = apos[position];
				boolean dlg = appdialog.Confirm(ApoListActivity.this,
						"Confirmação", "Deseja realmente escolher essa APO?",
						"Cancelar", "OK", aproc(), bproc());
			}
		});
		/*LinearLayout ll = (LinearLayout) findViewById(R.id.apos_container);
		ll.addView(lv);*/
	}

	public Runnable aproc() {
		return new Runnable() {
			public void run() {
				Intent i = new Intent(ApoListActivity.this, RespondenteActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("apo", apoEscolhida);
				i.putExtras(b);
				startActivity(i);
			}
		};
	}

	public Runnable bproc() {
		return new Runnable() {
			public void run() {
				Toast.makeText(getApplicationContext(), "Escolha uma APO",
						Toast.LENGTH_LONG).show();
			}
		};
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_apo_list, menu);
		return true;
	}
}
