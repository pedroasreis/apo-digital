package br.com.apodigital.activities;

import br.com.apodigital.R;
import br.com.apodigital.R.layout;
import br.com.apodigital.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.Toast;

public class HomeActivity extends RootActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_activity);
		// setContentView(R.layout.perguntas_layout);

		Button responder = (Button) findViewById(R.id.responder_apo);
		Button carregar = (Button) findViewById(R.id.carregar_apo);
		Button enviar = (Button) findViewById(R.id.enviar_respostas);
		Button moradores = (Button) findViewById(R.id.verificar_moradores);

		responder.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i = new Intent(HomeActivity.this, ApoListActivity.class);
				startActivity(i);
			}
		});

		carregar.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i = new Intent(HomeActivity.this, MainActivity.class);
				startActivity(i);
			}
		});
		
		enviar.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i = new Intent(HomeActivity.this, TransferirRespostasActivity.class);
				startActivity(i);
			}
		});
		
		moradores.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i = new Intent(HomeActivity.this, ListarMoradoresActivity.class);
				startActivity(i);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_home_acitivity, menu);
		return true;
	}
}
