package br.com.apodigital.migration;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import br.com.apodigital.model.APO;
import br.com.apodigital.model.ApoPacote;
import br.com.apodigital.model.RespostaPacote;
import br.com.apodigital.utils.CommonServices;
import br.com.apodigital.utils.Constantes;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

public class ApoClient {
	
	public String enviarRespostas(RespostaPacote rp){
		Gson gson = new Gson();
		String[] resposta = new WebServiceClient().post(Constantes.ENVIAR_REPOSTAS, gson.toJson(rp));
		return resposta[0];
	}
	
	public String enviarRespostasGet(RespostaPacote rp){
		Gson gson = new Gson();
		String respostas = gson.toJson(rp);
		String[] resposta = new WebServiceClient().get(Constantes.ENVIAR_REPOSTAS + "/" + respostas);
		return resposta[0];
	}

	public List<APO> getListaApos() throws Exception {
		String[] resposta = new WebServiceClient().get(Constantes.LISTAR_APOS);
		if (resposta[0].equals("200")) {
			Gson gson = new Gson();
			List<APO> apos = new ArrayList<APO>();
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();

			for (int i = 0; i < array.size(); i++) {
				apos.add(gson.fromJson(array.get(i), APO.class));
			}
			return apos;
		} else {
			throw new Exception(resposta[1]);
		}
	}
	
	public ApoPacote getApoCompleta(int id) throws Exception {
		String[] resposta = new WebServiceClient().get(Constantes.APO_COMPLETA + "/" + id);

		if (!resposta[0].equals("0")) {
			Log.d("RETORNO APO", resposta[1]);
			Gson gson = new Gson();
			ApoPacote ap = gson.fromJson(resposta[1], ApoPacote.class);
			return ap;
			//return resposta[1];
		} else {
			throw new Exception(resposta[1]);
		}
	}

}
