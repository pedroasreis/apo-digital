/* inser��o de super-atividades */
insert into super_atividade values(12, 'Conviver'); /*12*/
insert into super_atividade values(13, 'Cuidar do Corpo'); /*13*/
insert into super_atividade values(14, 'Higiene Pessoal'); /*14*/
insert into super_atividade values(15, 'Cuidar da Casa'); /*15*/
insert into super_atividade values(16, 'Repousar'); /*16*/
insert into super_atividade values(17, 'Estocar'); /*17*/
insert into super_atividade values(18, 'Armazenar'); /*18*/

/* inser��o de atividades */
insert into atividade values(1, 'Ver TV', 12);
insert into atividade values(2, 'Falar ao Telefone', 12);
insert into atividade values(3, 'Fazer Refei��es Coletivamente', 12);
insert into atividade values(4, 'Brincar com os filhos', 12);

insert into atividade values(5, 'Alimenta��o r�pida', 13);
insert into atividade values(6, 'Alimenta��o prolongada', 13);
insert into atividade values(7, 'Praticar exerc�cios f�sicos', 13);

insert into atividade values(8, 'Escovar os dentes', 14);
insert into atividade values(9, 'Banhar-se', 14);
insert into atividade values(10, 'Lavar rosto e m�os', 14);
insert into atividade values(11, 'Pentear-se', 14);
insert into atividade values(12, 'Vestir-se', 14);

insert into atividade values(13, 'Passar roupas', 15);
insert into atividade values(14, 'Lavar roupas na m�quina', 15);
insert into atividade values(15, 'Lavar roupas no tanque', 15);
insert into atividade values(16, 'Preparar alimentos', 15);
insert into atividade values(17, 'Lavar alimentos', 15);
insert into atividade values(18, 'Lavar e secar utens�lios', 15);

insert into atividade values(19, 'Dormir', 16);
insert into atividade values(20, 'Descansar', 16);
insert into atividade values(21, 'Alojar h�spedes', 16);

insert into atividade values(22, 'Livros e material escolar', 17);
insert into atividade values(23, 'Objetos pessoais diversos', 17);
insert into atividade values(24, 'Roupas e cal�ados', 17);
insert into atividade values(25, 'Roupas de cama, mesa e banho', 17);

insert into atividade values(26, 'Alimentos frescos (frutas)', 18);
insert into atividade values(27, 'Alimentos secos', 18);
insert into atividade values(28, 'Fotos e documentos', 18);
insert into atividade values(29, 'Material de limpeza', 18);