-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-04-02 09:51:08
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for apo
CREATE DATABASE IF NOT EXISTS `apo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `apo`;


-- Dumping structure for table apo.administrador
CREATE TABLE IF NOT EXISTS `administrador` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.avaliacao
CREATE TABLE IF NOT EXISTS `avaliacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `id_edificio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_edificio` (`id_edificio`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `texto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.categoria_questionario
CREATE TABLE IF NOT EXISTS `categoria_questionario` (
  `categoria_id` int(10) unsigned NOT NULL,
  `questionario_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`categoria_id`,`questionario_id`),
  KEY `questionario_id` (`questionario_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.comodo
CREATE TABLE IF NOT EXISTS `comodo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `icone_id` int(10) unsigned DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.conceito
CREATE TABLE IF NOT EXISTS `conceito` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.conceito_categoria
CREATE TABLE IF NOT EXISTS `conceito_categoria` (
  `conceito_id` int(10) unsigned NOT NULL,
  `categoria_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`conceito_id`,`categoria_id`),
  KEY `categoria_id` (`categoria_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.conceito_pergunta
CREATE TABLE IF NOT EXISTS `conceito_pergunta` (
  `conceito_id` int(10) unsigned NOT NULL,
  `pergunta_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`conceito_id`,`pergunta_id`),
  KEY `pergunta_id` (`pergunta_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.edificio
CREATE TABLE IF NOT EXISTS `edificio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.icone
CREATE TABLE IF NOT EXISTS `icone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.morador
CREATE TABLE IF NOT EXISTS `morador` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `edificio_id` int(10) unsigned NOT NULL,
  `perfil_familiar` varchar(45) DEFAULT NULL,
  `num_apartamento` int(10) unsigned DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `grau_escolaridade` varchar(45) DEFAULT NULL,
  `posicao_grupo_familiar` varchar(20) DEFAULT NULL,
  `num_pessoas_apartamento` int(10) unsigned DEFAULT NULL,
  `renda_familiar` double DEFAULT NULL,
  `situacao_ap` varchar(20) DEFAULT NULL,
  `ano_mudanca_ap` int(11) DEFAULT NULL,
  `trabalhadores_ap` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `edificio_id` (`edificio_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.pergunta
CREATE TABLE IF NOT EXISTS `pergunta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` varchar(255) DEFAULT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `icone_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.pergunta_categoria
CREATE TABLE IF NOT EXISTS `pergunta_categoria` (
  `pergunta_id` int(10) unsigned NOT NULL,
  `categoria_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pergunta_id`,`categoria_id`),
  KEY `categoria_id` (`categoria_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.pergunta_comodo
CREATE TABLE IF NOT EXISTS `pergunta_comodo` (
  `pergunta_id` int(10) unsigned NOT NULL,
  `comodo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pergunta_id`,`comodo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.pergunta_qualificador
CREATE TABLE IF NOT EXISTS `pergunta_qualificador` (
  `pergunta_id` int(10) unsigned NOT NULL,
  `qualificador_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pergunta_id`,`qualificador_id`),
  KEY `qualificador_id` (`qualificador_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.qualificador
CREATE TABLE IF NOT EXISTS `qualificador` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` varchar(255) DEFAULT NULL,
  `inicio` int(10) unsigned DEFAULT NULL,
  `fim` int(10) unsigned DEFAULT NULL,
  `icone_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.questionario
CREATE TABLE IF NOT EXISTS `questionario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `respondente` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.questionario_avaliacao
CREATE TABLE IF NOT EXISTS `questionario_avaliacao` (
  `questionario_id` int(10) unsigned NOT NULL,
  `avaliacao_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`questionario_id`,`avaliacao_id`),
  KEY `avaliacao_id` (`avaliacao_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.resposta
CREATE TABLE IF NOT EXISTS `resposta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `morador_id` int(10) unsigned NOT NULL,
  `pergunta_id` int(10) unsigned NOT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `apo_id` int(11) DEFAULT NULL,
  `ambiente_id` int(11) DEFAULT NULL,
  `conceito_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `morador_id` (`morador_id`),
  KEY `pergunta_id` (`pergunta_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table apo.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
