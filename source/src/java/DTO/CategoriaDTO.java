package DTO;

import Model.Categoria;
import Model.PerguntaAgrupada;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class CategoriaDTO
{
   
   private Categoria categoria;
   private ArrayList<PerguntaAgrupada> perguntas;

   @Override
   public boolean equals(Object obj)
   {
      return (categoria.getId() == ((CategoriaDTO)obj).getCategoria().getId());
   }

   /**
    * @return the categoria
    */
   public Categoria getCategoria()
   {
      return categoria;
   }

   /**
    * @param categoria the categoria to set
    */
   public void setCategoria(Categoria categoria)
   {
      this.categoria = categoria;
   }

   /**
    * @return the perguntas
    */
   public ArrayList<PerguntaAgrupada> getPerguntas()
   {
      return perguntas;
   }

   /**
    * @param perguntas the perguntas to set
    */
   public void setPerguntas(ArrayList<PerguntaAgrupada> perguntas)
   {
      this.perguntas = perguntas;
   }
   
}
