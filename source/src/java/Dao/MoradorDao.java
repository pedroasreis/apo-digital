/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.APO;
import Model.Morador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class MoradorDao {
    
    private Connection conn;
    
    public int inserir(Morador m) throws SQLException{
        int id = 0;
        String sql = "insert into morador values(null, ?, ?, ?, null, null, null, null, null, null, null, null)";
        PreparedStatement pst = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        pst.setInt(1, m.getApo().getId());
        pst.setString(2, m.getBloco());
        pst.setString(3, m.getNumApartamento());
        pst.executeUpdate();
        /*
        Recuperando o id gerado para esse registro
        */
        ResultSet rs = pst.getGeneratedKeys();
        if(rs.next()){
            id = rs.getInt(1);
        }
        rs.close();
        pst.close();
        return id;
    }
    
    public ArrayList<Morador> buscarTodos() throws SQLException{
        ArrayList<Morador> moradores = new ArrayList<Morador>();
        String sql = "select * from morador";
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while(rs.next()){
            Morador m = new Morador();
            preencher(m, rs);
            moradores.add(m);
        }
        
        st.close();
        rs.close();
        return moradores;
    }
    
    public ArrayList<Morador> buscarPorApo(APO apo) throws SQLException{
        ArrayList<Morador> moradores = new ArrayList<Morador>();
        String sql = "select * from morador where edificio_id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, apo.getId());
        
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            Morador m = new Morador();
            preencher(m, rs);
            moradores.add(m);
        }
        
        rs.close();
        pst.close();
        return moradores;
    }
    
    public int quantidadeMoradoresApo(APO apo) throws SQLException{
        int quantidade = 0;
        String sql = "select count(id) from morador where edificio_id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, apo.getId());
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
            quantidade = rs.getInt(1);
        }
        rs.close();
        pst.close();
        return quantidade;
    }
    
    private void preencher(Morador m, ResultSet rs) throws SQLException {
        m.setId(rs.getInt("id"));
        m.setEdificioId(rs.getInt("edificio_id"));
        m.setNumApartamento(rs.getString("num_apartamento"));
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
}
