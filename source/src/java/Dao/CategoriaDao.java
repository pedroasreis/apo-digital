/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.APO;
import Model.Categoria;
import Model.Pergunta;
import Model.Questionario;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class CategoriaDao implements Serializable
{

   private Connection conn;

   public void salvar(Categoria c, Questionario q) throws SQLException
   {
      int id = 0;
      String sql = "insert into categoria values(null, ?, ?)";
      PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      ps.setString(1, c.getNome());
      ps.setString(2, c.getTexto());
      ps.executeUpdate();
      ResultSet rs = ps.getGeneratedKeys();
      if (rs.next())
      {
         id = rs.getInt(1);
         c.setId(id);
      }
      // inserir pergunta_categoria
      String sql2 = "insert into categoria_questionario values(?, ?)";
      //System.out.println("Cid: " + c.getId() + " | Qid: " + q.getId());
      PreparedStatement ps2 = conn.prepareStatement(sql2);
      ps2.setInt(1, id);
      ps2.setInt(2, q.getId());
      ps2.executeUpdate();
      //ps.close();
   }

   public void editar(Categoria c) throws SQLException
   {
      String sql = "update categoria set nome = ?, texto = ? where id = ?";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, c.getNome());
      ps.setString(2, c.getTexto());
      ps.setInt(3, c.getId());
      ps.executeUpdate();
      //ps.close();
   }

   public void excluir(Categoria c) throws SQLException
   {
      String sql = "delete from categoria where id = ?";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setInt(1, c.getId());
      ps.executeUpdate();
      //ps.close();
   }

   public ArrayList<Categoria> buscarTodos() throws SQLException
   {
      String sql = "select id, nome, texto from categoria order by id";
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(sql);
      PerguntaDao pDao = new PerguntaDao();
      pDao.setConn(conn);
      ArrayList<Categoria> cs = new ArrayList<Categoria>();
      while (rs.next())
      {
         Categoria c = new Categoria();
         preencherCategoria(c, rs);
         c.setPerguntas(pDao.buscarPorCategoria(c.getId()));
         cs.add(c);
      }
      //rs.close();
      //st.close();
      return cs;
   }

   public ArrayList<Categoria> buscarPorQuestionario(Questionario q) throws SQLException
   {
      String sql = "select categoria.id, categoria.nome, categoria.texto from categoria, questionario, categoria_questionario "
              + "where questionario.id = ? and categoria.id = categoria_id "
              + "and questionario.id = questionario_id order by categoria.id";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setInt(1, q.getId());
      ResultSet rs = ps.executeQuery();
      PerguntaDao pDao = new PerguntaDao();
      pDao.setConn(conn);
      ArrayList<Categoria> cs = new ArrayList<Categoria>();
      while (rs.next())
      {
         Categoria c = new Categoria();
         preencherCategoria(c, rs);
         c.setPerguntas(pDao.buscarPorCategoria(c.getId()));
         cs.add(c);
      }
      //rs.close();
      //st.close();
      return cs;
   }

   public Categoria buscarPorPergunta(Pergunta p) throws SQLException
   {
      Categoria c = new Categoria();
      String sql = "select c.id, c.nome, c.texto from categoria c, pergunta_categoria pc where pc.pergunta_id = ? "
              + "and pc.categoria_id = c.id order by c.id";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, p.getId());
      ResultSet rs = pst.executeQuery();
      if (rs.next())
      {
         preencherCategoria(c, rs);
      }

      rs.close();
      pst.close();
      return c;
   }

   /*
    Método responsável por buscar uma categoria com um nome e APO específicos
    */
   public Categoria buscarPorApoNome(String nome, APO apo, Questionario q) throws SQLException
   {
      Categoria c = new Categoria();
      String sql = "select c.id, c.nome, c.texto from categoria c, categoria_questionario cq, questionario_avaliacao qa "
              + "where c.nome = ? and c.id = cq.categoria_id and cq.questionario_id = qa.questionario_id and "
              + "qa.avaliacao_id = ? and qa.questionario_id = ? order by c.id";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, nome);
      pst.setInt(2, apo.getId());
      pst.setInt(3, q.getId());
      ResultSet rs = pst.executeQuery();
      if (rs.next())
      {
         preencherCategoria(c, rs);
      }
      rs.close();
      pst.close();
      return c;
   }

   private void preencherCategoria(Categoria c, ResultSet rs) throws SQLException
   {
      c.setId(rs.getInt("id"));
      c.setNome(rs.getString("nome"));
      c.setTexto(rs.getString("texto"));
   }

   /**
    * @return the conn
    */
   public Connection getConn()
   {
      return conn;
   }

   /**
    * @param conn the conn to set
    */
   public void setConn(Connection conn)
   {
      this.conn = conn;
   }
}
