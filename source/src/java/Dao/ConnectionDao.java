package Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionDao {

    /*public Connection getConnection() {
        try {
            InitialContext initialContext = new InitialContext();
            DataSource dataSource = (DataSource) initialContext.lookup("apo");
            return dataSource.getConnection();
        } catch (NamingException ex) {
            Logger.getLogger(ConnectionDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }*/
    
    public Connection getConnection() throws SQLException{
        String url = "jdbc:mysql://localhost:3306/apo";
        String username = "root";
        String password = "root";
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        }
        catch(ClassNotFoundException e){System.out.println("ClassNotFoundException na classe de conexao com o banco de dados");}
        catch(InstantiationException e){System.out.println("InstantiationException na classe de conexao com o banco de dados");}
        catch(IllegalAccessException e){System.out.println("IllegalAccessException na classe de conexao com o banco de dados");}
        Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }
    
}
