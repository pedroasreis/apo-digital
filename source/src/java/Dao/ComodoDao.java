/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Comodo;
import Model.Pergunta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Alterar nome da tabela "comodo" para "ambiente" e acrescentar coluna "tipo" que diferencia um espaço privado de um
 * espaço comum
 * 
 */
public class ComodoDao 
{
    
    private Connection conn;
    
    public void salvar(Comodo c) throws SQLException
    {
        String sql = "insert into comodo values(null, ?, null, ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, c.getNome());
        pst.setInt(2, c.getTipo());
        pst.executeUpdate();
    }
    
    public void associarComodoPergunta(Pergunta p, Comodo c) throws SQLException
    {
        String sql = "insert into pergunta_comodo values(?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, p.getId());
        pst.setInt(2, c.getId());
        pst.executeUpdate();
    }
    
    public void desassociarComodoPergunta(Pergunta p) throws SQLException
    {
        String sql = "delete from pergunta_comodo where pergunta_id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, p.getId());
        pst.executeUpdate();
    }
    
    public void editar(Comodo c) throws SQLException
    {
        String sql = "update comodo set nome = ?, icone_id = ?, tipo = ? where id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, c.getNome());
        pst.setInt(2, c.getIcone().getId());
        pst.setInt(3, c.getTipo());
        pst.setInt(4, c.getId());
        pst.executeUpdate();
    }
    
    public void excluir(Comodo c) throws SQLException
    {
        String sql = "delete from comodo where id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, c.getId());
        pst.executeUpdate();
    }
    
    public ArrayList<Comodo> buscarPorPergunta(Pergunta p) throws SQLException
    {
        String sql = "select c.id, c.nome, c.tipo from comodo c, pergunta p, pergunta_comodo pc where comodo_id = c.id and pergunta_id = p.id "
                + "and p.id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, p.getId());
        ResultSet rs = pst.executeQuery();
        
        ArrayList<Comodo> comodos = new ArrayList<Comodo>();
        while(rs.next())
        {
            Comodo c = new Comodo();
            preencherComodo(rs, c);
            comodos.add(c);
        }
        return comodos;
    }
    
    public Comodo buscarPorId(int id) throws SQLException
    {
        String sql = "select c.id, c.nome, c.tipo from comodo c where id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, id);
        ResultSet rs = pst.executeQuery();
        
        Comodo c = new Comodo();
        while(rs.next())
        {
            preencherComodo(rs, c);
        }
        return c;
    }
    
    public Comodo buscarUnidadePorInstancia(Comodo comodo) throws SQLException
    {
        String sql = "select c.id, c.nome, c.tipo from comodo c, planta p where p.id = ? and p.id_comodo = c.id";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, comodo.getId());
        ResultSet rs = pst.executeQuery();
        
        Comodo c = new Comodo();
        while(rs.next())
        {
            preencherComodo(rs, c);
        }
        rs.close();
        pst.close();
        return c;
    }
    
    public Comodo buscarPorNome(String nome) throws SQLException {
        Comodo c = new Comodo();
        String sql = "select c.id, c.nome, c.tipo from comodo c where nome = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, nome);
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
            preencherComodo(rs, c);
        }
        rs.close();
        pst.close();
        return c;
    }
    
    /*
     * select distinct id, nome from comodo where id 
     * not in (select distinct c.id from comodo c, pergunta p, pergunta_comodo 
     * where c.id = comodo_id and p.id = pergunta_id and p.id = ?) 
     */
    public ArrayList<Comodo> buscarPorNaoPergunta(Pergunta p) throws SQLException
    {
        String sql = "select distinct id, nome, tipo from comodo where id "
                + "not in (select distinct c.id from comodo c, pergunta p, pergunta_comodo "
                + "where c.id = comodo_id and p.id = pergunta_id and p.id = ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, p.getId());
        ResultSet rs = pst.executeQuery();
        
        ArrayList<Comodo> comodos = new ArrayList<Comodo>();
        while(rs.next())
        {
            Comodo c = new Comodo();
            preencherComodo(rs, c);
            comodos.add(c);
        }
        return comodos;
    }
    
    public ArrayList<Comodo> buscarTodos() throws SQLException
    {
        String sql = "select id, nome, tipo from comodo";
        PreparedStatement pst = conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        
        ArrayList<Comodo> comodos = new ArrayList<Comodo>();
        while(rs.next())
        {
            Comodo c = new Comodo();
            preencherComodo(rs, c);
            comodos.add(c);
        }
        return comodos;
    }
    
    public ArrayList<Comodo> buscarPorTipo(int tipo) throws SQLException{
        ArrayList<Comodo> comodos = new ArrayList<Comodo>();
        String sql = "select * from comodo where tipo = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, tipo);
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            Comodo c = new Comodo();
            preencherComodo(rs, c);
            comodos.add(c);
        }
        
        return comodos;
    }
    
    private void preencherComodo(ResultSet rs, Comodo c) throws SQLException {
        c.setId(rs.getInt("id"));
        c.setNome(rs.getString("nome"));
        c.setTipo(rs.getInt("tipo"));
    }
    
    /**
     * @return the conn
     */
    public Connection getConn() 
    {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) 
    {
        this.conn = conn;
    }

}
