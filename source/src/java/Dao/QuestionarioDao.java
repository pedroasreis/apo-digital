/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.APO;
import Model.Categoria;
import Model.Questionario;
import java.sql.Connection;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class QuestionarioDao implements Serializable {

    private Connection conn;

    public int salvar(Questionario q) throws SQLException {
        int id = 0;
        String sql = "insert into questionario values(null, ?, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, q.getNome());
        ps.setString(2, q.getTexto());
        ps.setInt(3, q.getRespondente());
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(rs.next()){
            id = rs.getInt(1);
        }
        rs.close();
        ps.close();
        return id;
    }
    
    public void salvarQuestionarioAvaliacao(Questionario q, APO a) throws SQLException {
        String sql = "insert into questionario_avaliacao values(?, ?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, q.getId());
        ps.setInt(2, a.getId());
        ps.executeUpdate();
    }

    public void editar(Questionario q) throws SQLException {
        String sql = "update questionario set nome = ?, texto = ?, respondente = ? where id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, q.getNome());
        ps.setString(2, q.getTexto());
        ps.setInt(3, q.getRespondente());
        ps.setInt(4, q.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public void excluir(Questionario q) throws SQLException {
        String sql = "delete from questionario where id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, q.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public void excluirQuestionarioAvaliacao(APO a) throws SQLException {
        String sql = "delete from questionario_avaliacao where avaliacao_id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, a.getId());
        ps.executeUpdate();
    }

    public Questionario buscarPorId(int id) throws SQLException {
        String sql = "select id, nome, texto, respondente from questionario where id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        Questionario q = new Questionario();
        if (rs.next()) {
            preencherQuestionario(q, rs);
            //q.setId(id);
        } else {
            q = null;
        }
        //rs.close();
        //ps.close();
        return q;
    }
    
    public Questionario buscarPorNomeApo(String nome, APO apo) throws SQLException{
        Questionario q = new Questionario();
        String sql = "select q.id, q.nome, q.texto, q.respondente from questionario q, questionario_avaliacao qa where "
                + "q.id = qa.questionario_id and q.nome = ? and qa.avaliacao_id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, nome);
        pst.setInt(2, apo.getId());
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
            preencherQuestionario(q, rs);
        }
        rs.close();
        pst.close();
        return q;
    }
    
    public Questionario buscarPorCategoria(Categoria c) throws SQLException{
        Questionario q = new Questionario();
        String sql = "select q.id, q.nome, q.texto, q.respondente from questionario q, categoria_questionario cq where "
                + "cq.categoria_id = ? and cq.questionario_id = q.id";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, c.getId());
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
            preencherQuestionario(q, rs);
        }
        
        rs.close();
        pst.close();
        return q;
    }

    public ArrayList<Questionario> buscarTodos() throws SQLException {
        String sql = "select id, nome, texto, respondente from questionario";
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);

        ArrayList<Questionario> qs = new ArrayList<Questionario>();
        while (rs.next()) {
            Questionario q = new Questionario();
            preencherQuestionario(q, rs);
            qs.add(q);
        }
        //rs.close();
        //st.close();
        return qs;
    }

    public ArrayList<Questionario> buscarPorApo(int idApo) throws SQLException {
        ArrayList<Questionario> questionarios = new ArrayList<Questionario>();

        String sql = "select q.id, q.nome, q.texto, q.respondente from questionario q, questionario_avaliacao "
                + "where q.id = questionario_id and avaliacao_id = ?";
        PreparedStatement st = conn.prepareStatement(sql);
        st.setInt(1, idApo);
        ResultSet rs = st.executeQuery();

        while (rs.next()) {
            Questionario q = new Questionario();
            preencherQuestionario(q, rs);
            questionarios.add(q);
        }

        return questionarios;
    }
    
    public ArrayList<Questionario> buscarPorNaoApo(int idApo) throws SQLException {
        ArrayList<Questionario> questionarios = new ArrayList<Questionario>();

        String sql = "select distinct id, nome, texto, respondente from questionario where id " 
                    + "not in (select distinct q.id from questionario q, questionario_avaliacao qa where q.id = qa.questionario_id and qa.avaliacao_id = ?)";
        PreparedStatement st = conn.prepareStatement(sql);
        st.setInt(1, idApo);
        ResultSet rs = st.executeQuery();

        while (rs.next()) {
            Questionario q = new Questionario();
            preencherQuestionario(q, rs);
            questionarios.add(q);
        }

        rs.close();
        st.close();
        return questionarios;
    }
    
    public ArrayList<Questionario> buscarPorRespondente(int respondente) throws SQLException{
        ArrayList<Questionario> questionarios = new ArrayList<Questionario>();
        String sql = "select * from questionario where respondente = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, respondente);
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            Questionario q = new Questionario();
            preencherQuestionario(q, rs);
            questionarios.add(q);
        }
        
        rs.close();
        pst.close();
        return questionarios;
    }
    
    /*
    Método que busca todas as técnicas de uma APO e de um determinado respondente
    */
    public ArrayList<Questionario> buscarPorApoRespondente(APO apo, int respondente) throws SQLException{
        ArrayList<Questionario> questionarios = new ArrayList<Questionario>();
        String sql = "select * from questionario q, questionario_avaliacao qa where qa.avaliacao_id = ? and "
                + "q.respondente = ? and q.id = qa.questionario_id";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, apo.getId());
        pst.setInt(2, respondente);
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            Questionario q = new Questionario();
            preencherQuestionario(q, rs);
            questionarios.add(q);
        }
        rs.close();
        pst.close();
        return questionarios;
    }

    public int contarCategorias(int id) throws SQLException {
        int quantidade = 0;
        String sql = "select count(*) as quantidade from categoria, questionario, categoria_questionario "
                + "where questionario.id = ? and questionario.id = questionario_id and categoria.id = categoria_id;";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            quantidade = rs.getInt(1);
        }
        return quantidade;
    }

    private void preencherQuestionario(Questionario q, ResultSet rs) throws SQLException {
        q.setId(rs.getInt("id"));
        q.setNome(rs.getString("nome"));
        q.setTexto(rs.getString("texto"));
        q.setRespondente(rs.getInt("respondente"));
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }
}
