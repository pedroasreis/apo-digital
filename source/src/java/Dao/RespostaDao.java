/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.APO;
import Model.Atributo;
import Model.Comodo;
import Model.Conceito;
import Model.Morador;
import Model.Pergunta;
import Model.Resposta;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class RespostaDao implements Serializable {

    private Connection conn;

    public void salvar(Resposta r) throws SQLException {
        String sql = "insert into resposta values(null, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, r.getMorador().getId());
        pst.setInt(2, r.getPergunta().getId());
        pst.setString(3, r.getTexto());
        pst.setInt(4, r.getApo().getId());
        pst.setInt(5, r.getComodo().getId());
        pst.setInt(6, r.getConceito().getId());
        pst.setInt(7, 0); // valor para a instância de um cômodo, esse atributo atualmente está depreciado na aplicação
        pst.setInt(8, r.getAtributo().getId());
        pst.executeUpdate();
        pst.close();
    }

    public void editar(Resposta r) throws SQLException {
        String sql = "update resposta set texto = ? where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setString(1, r.getTexto());
        ps.setInt(2, r.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public ArrayList<Resposta> buscarPorPergunta(Pergunta p) throws SQLException {
        ArrayList<Resposta> resp = new ArrayList<Resposta>();
        String sql = "select resposta.id, morador_id, pergunta_id, resposta.texto, apo_id from resposta, pergunta where pergunta_id = ? "
                + "and pergunta_id = pergunta.id";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Resposta r = new Resposta();
            preencherResposta(r, rs);
            resp.add(r);
        }
        return resp;
        //ps.close();
    }
    
    public ArrayList<Resposta> buscaCompleta(Pergunta p, APO apo) throws SQLException {
        ArrayList<Resposta> resp = new ArrayList<Resposta>();
        String sql = "select r.id, r.morador_id, r.pergunta_id, r.texto, r.apo_id "
                + "from resposta r, pergunta p where p.id = ? "
                + "and r.pergunta_id = p.id and r.conceito_id = ? and r.ambiente_id = ? and r.atributo_id = ? "
                + "and apo_id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ps.setInt(2, p.getConceito().getId());
        ps.setInt(3, p.getComodo().getId());
        ps.setInt(4, p.getAtributo().getId());
        ps.setInt(5, apo.getId());
        
        
        
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Resposta r = new Resposta();
            preencherResposta(r, rs);
            resp.add(r);
        }
        return resp;
        //ps.close();
    }
    
    /*public ArrayList<Resposta> buscarPorPergunta(Pergunta p) throws SQLException {
        ArrayList<Resposta> resp = new ArrayList<Resposta>();
        String sql = "select resposta.id, morador_id, pergunta_id, resposta.texto, apo_id from resposta, pergunta where pergunta_id = ? "
                + "and pergunta_id = pergunta.id";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Resposta r = new Resposta();
            preencherResposta(r, rs);
            resp.add(r);
        }
        return resp;
        //ps.close();
    }*/
    
    public ArrayList<Resposta> buscarPorPerguntaApo(Pergunta p, APO a) throws SQLException {
        ArrayList<Resposta> resp = new ArrayList<Resposta>();
        String sql = "select resposta.id, morador_id, pergunta_id, resposta.texto, apo_id from resposta, pergunta where pergunta_id = ? "
                + "and apo_id = ? and pergunta_id = pergunta.id";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ps.setInt(2, a.getId());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Resposta r = new Resposta();
            preencherResposta(r, rs);
            resp.add(r);
        }
        return resp;
        //ps.close();
    }
    
    public ArrayList<Resposta> buscarPorPerguntaMorador(Pergunta p, Morador m) throws SQLException {
        ArrayList<Resposta> resp = new ArrayList<Resposta>();
        String sql = "select resposta.id, morador_id, pergunta_id, resposta.texto, apo_id from resposta, pergunta where pergunta_id = ? "
                + "and morador_id = ? and pergunta_id = pergunta.id";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ps.setInt(2, m.getId());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Resposta r = new Resposta();
            preencherResposta(r, rs);
            resp.add(r);
        }
        return resp;
        //ps.close();
    }
    
    public Resposta buscaCompleta(Morador m, Pergunta p, int c, int a, int cm) throws SQLException{
       Resposta resposta = new Resposta();
       String sql = "select * from resposta where morador_id = ? and pergunta_id = ? and "
               + "conceito_id = ? and atributo_id = ? and comodo_id = ?";
       PreparedStatement pst = conn.prepareStatement(sql);
       pst.setInt(1, m.getId());
       pst.setInt(2, p.getId());
       pst.setInt(3, c);
       pst.setInt(4, a);
       pst.setInt(5, cm);
       
       ResultSet rs = pst.executeQuery();
       if(rs.next()){
          preencherResposta(resposta, rs);
       }
       
       rs.close();
       pst.close();
       return resposta;
    }
    
    public ArrayList<Resposta> buscarPorMorador(Morador m) throws SQLException{
        ArrayList<Resposta> respostas = new ArrayList<Resposta>();
        String sql = "select * from resposta where morador_id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, m.getId());
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            Resposta r = new Resposta();
            r.setId(rs.getInt(1));
            
            r.setMorador(new Morador());
            r.getMorador().setId(rs.getInt(2));
            
            r.setPergunta(new Pergunta());
            r.getPergunta().setId(rs.getInt(3));
            
            r.setTexto(rs.getString(4));
            r.getApo().setId(rs.getInt(5));
            r.getComodo().setId(rs.getInt(6));
            
            r.getConceito().setId(rs.getInt(7));
            r.getComodoInstancia().setId(rs.getInt(8));
            r.getAtributo().setId(rs.getInt(9));
            
            respostas.add(r);            
        }
        
        return respostas;
    }

    public void excluir(Resposta r) throws SQLException {
        String sql = "delete from resposta where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, r.getId());
        ps.executeUpdate();
        //ps.close();
    }

    private void preencherResposta(Resposta r, ResultSet rs) throws SQLException {
        r.setId(rs.getInt("id"));
        r.setTexto(rs.getString("texto"));
        r.setPergunta(new Pergunta());
        r.getPergunta().setId(rs.getInt("pergunta_id"));
        r.getApo().setId(rs.getInt("apo_id"));
    }
    
    /*
     * Consultas específicas
     */
    
    public ArrayList<String> aluguelSatisfacaoBairroAnteriorBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 826 and morador_id in "
               + "(select morador_id from resposta where texto = 'alugada' and pergunta_id = 821 and "
               + "apo_id = 21);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> aluguelSatisfacaoBairroAtualBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 824 and morador_id in "
               + "(select morador_id from resposta where texto = 'alugada' and pergunta_id = 821 and "
               + "apo_id = 21);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> aluguelSatisfacaoBairroAnteriorSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1716 and morador_id in "
               + "(select morador_id from resposta where texto = 'alugada' and pergunta_id = 1711 and "
               + "apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> aluguelSatisfacaoBairroAtualSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1714 and morador_id in "
               + "(select morador_id from resposta where texto = 'alugada' and pergunta_id = 1711 and "
               + "apo_id = 27);";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> satisfeitosLocalizacaoInclusaoBairroAnteriorBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 860 "
               + "and conceito_id = 134 and morador_id in "
               + "(select morador_id from resposta where (texto = 'Muito próxima' or texto = 'Próxima') "
               + "and pergunta_id = 835 and conceito_id = 98 and apo_id = 21)";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> satisfeitosLocalizacaoInclusaoBairroAnteriorSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1718 "
               + "and conceito_id = 134 and morador_id in "
               + "(select morador_id from resposta where (texto = 'Muito próxima' or texto = 'Próxima') "
               + "and pergunta_id = 1727 and conceito_id = 98 and apo_id = 27)";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> satisfeitosLocalizacaoInclusaoBairroAtualBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 859 "
               + "and conceito_id = 134 and morador_id in "
               + "(select morador_id from resposta where (texto = 'Muito próxima' or texto = 'Próxima') "
               + "and pergunta_id = 835 and conceito_id = 98 and apo_id = 21)";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> satisfeitosLocalizacaoInclusaoBairroAtualSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1717 "
               + "and conceito_id = 134 and morador_id in "
               + "(select morador_id from resposta where (texto = 'Muito próxima' or texto = 'Próxima') "
               + "and pergunta_id = 1727 and conceito_id = 98 and apo_id = 27)";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> rendaSentimentoAnteriorBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 826 and morador_id in "
               + "(select morador_id from resposta where texto = '1 a 3 salários mínimos (724 a 2172)' "
               + "and pergunta_id = 818 and apo_id = 21);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> rendaSentimentoAtualBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 824 and morador_id in "
               + "(select morador_id from resposta where texto = '1 a 3 salários mínimos (724 a 2172)' "
               + "and pergunta_id = 818 and apo_id = 21);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> rendaSentimentoAnteriorSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1716 and morador_id in "
               + "(select morador_id from resposta where texto = '1 a 3 salários mínimos (724 a 2172)' "
               + "and pergunta_id = 1708 and apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> rendaSentimentoAtualSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1714 and morador_id in "
               + "(select morador_id from resposta where texto = '1 a 3 salários mínimos (724 a 2172)' "
               + "and pergunta_id = 1708 and apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> posicaoFemininaBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 815 and morador_id in "
               + "(select morador_id from resposta where texto = 'F' "
               + "and pergunta_id = 812 and apo_id = 21);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> posicaoMasculinaBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 815 and morador_id in "
               + "(select morador_id from resposta where texto = 'M' "
               + "and pergunta_id = 812 and apo_id = 21);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> posicaoFemininaSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1705 and morador_id in "
               + "(select morador_id from resposta where texto = 'F' "
               + "and pergunta_id = 1702 and apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> posicaoMasculinaSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1705 and morador_id in "
               + "(select morador_id from resposta where texto = 'M' "
               + "and pergunta_id = 1702 and apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> reformaPrivacidadeVizinhos() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1727 and conceito_id = 109 "
               + "and morador_id in "
               + "(select morador_id from resposta where texto = 'Sim' "
               + "and pergunta_id = 1730 and apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> reformaPrivacidadeMoradores() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1727 and conceito_id = 110 "
               + "and morador_id in "
               + "(select morador_id from resposta where texto = 'Sim' "
               + "and pergunta_id = 1730 and apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> reformaAparencia() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1727 and conceito_id = 102 "
               + "and morador_id in "
               + "(select morador_id from resposta where texto = 'Sim' "
               + "and pergunta_id = 1730 and apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> reformaNivelConvivencia() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1721 and atributo_id = 115 "
               + "and morador_id in "
               + "(select morador_id from resposta where texto = 'Sim' "
               + "and pergunta_id = 1730 and apo_id = 27);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> faixaEtariaInsatisfeitosEquipamentosColetivosBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 813 "
               + "and morador_id in "
               + "(select morador_id from resposta where (texto = 'Ruim' or texto = 'Péssima') "
               + "and pergunta_id = 829 and atributo_id = 102 and apo_id = 21);";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> faixaEtariaInsatisfeitosEquipamentosColetivosSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1703 "
               + "and morador_id in "
               + "(select morador_id from resposta where (texto = 'Ruim' or texto = 'Péssima') "
               + "and pergunta_id = 1721 and atributo_id = 117 and apo_id = 27);";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> maiorDistanciaProximidadeEquipamentos() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1727 and conceito_id = 99 "
               + "and morador_id in "
               + "(select morador_id from resposta where (texto like '%Maiores distâncias até equipamentos públicos%') "
               + "and pergunta_id = 1720 and apo_id = 27);";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> satTransColMeioTransMaisUtilizadoSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1749 "
               + "and morador_id in "
               + "(select morador_id from resposta where (texto = 'Totalmente Satisfatório' or texto = 'Satisfatório') "
               + "and pergunta_id = 1713 and atributo_id = 93 and apo_id = 27);";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> satTransColMeioTransMaisUtilizadoBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 857 "
               + "and morador_id in "
               + "(select morador_id from resposta where (texto = 'Totalmente Satisfatório' or texto = 'Satisfatório') "
               + "and pergunta_id = 823 and atributo_id = 93 and apo_id = 21);";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> reformaAparenciaBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 835 and conceito_id = 102 "
               + "and morador_id in "
               + "(select morador_id from resposta where (texto = 'Sim') "
               + "and pergunta_id = 838 and apo_id = 21);";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> reformaNivelConvivenciaBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 829 and atributo_id = 101 "
               + "and morador_id in "
               + "(select morador_id from resposta where (texto = 'Sim') "
               + "and pergunta_id = 838 and apo_id = 21);";
       System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> satisfeitosLocalizacaoTrabalhoSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1748 and morador_id in "
               + "(select morador_id from resposta where (texto = 'Muito próxima' or texto = 'Próxima')"
               + "and pergunta_id = 1727 and conceito_id = 100 and apo_id = 27)";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> satisfeitosLocalizacaoTrabalhoBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 856 and morador_id in "
               + "(select morador_id from resposta where (texto = 'Muito próxima' or texto = 'Próxima')"
               + "and pergunta_id = 835 and conceito_id = 100 and apo_id = 21)";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> bairroAnteriorBaltimore() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 822";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }
    
    public ArrayList<String> bairroAnteriorSucupira() throws SQLException
    {
       ArrayList<String> respostas = new ArrayList<String>();
       String query = "select texto from resposta where pergunta_id = 1712";
       //System.out.println(query);
       Statement st = conn.createStatement();
       ResultSet rs = st.executeQuery(query);
       while(rs.next()){
          respostas.add(rs.getString(1));
       }
       rs.close();
       st.close();
       return respostas;
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }
}
