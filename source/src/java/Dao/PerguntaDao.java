/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.APO;
import Model.Categoria;
import Model.Comodo;
import Model.Morador;
import Model.Pergunta;
import Model.Questionario;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class PerguntaDao implements Serializable
{

   private Connection conn;

   public int salvar(Pergunta p, Categoria c) throws SQLException
   {
      // inserir pergunta
      int id = 0;
      String sql = "insert into pergunta values(null, ?, ?, ?, ?, ?)";
      PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      ps.setString(1, p.getTexto());
      ps.setString(2, p.getComentario());
      ps.setInt(3, p.getTipo());
      ps.setInt(4, p.getEscalaDeCores());
      ps.setInt(5, p.getOrdem());
      ps.executeUpdate();
      ResultSet rs = ps.getGeneratedKeys();
      if (rs.next())
      {
         id = rs.getInt(1);
      }
      // inserir pergunta_categoria
      String sql2 = "insert into pergunta_categoria values(?, ?)";
      PreparedStatement ps2 = conn.prepareStatement(sql2);
      ps2.setInt(1, id);
      ps2.setInt(2, c.getId());
      ps2.executeUpdate();
      rs.close();
      return id;
   }

   public void editar(Pergunta p) throws SQLException
   {
      String sql = "update pergunta set texto = ?, tipo = ?, comentario = ?, icone_id = ?, "
              + "ordem = ? where id = ?";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, p.getTexto());
      ps.setInt(2, p.getTipo());
      ps.setString(3, p.getComentario());
      ps.setInt(4, p.getEscalaDeCores());
      ps.setInt(5, p.getOrdem());
      ps.setInt(6, p.getId());
      ps.executeUpdate();
      //ps.close();
   }

   public void excluir(Pergunta p) throws SQLException
   {
      String sql = "delete from pergunta where id = ?";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setInt(1, p.getId());
      ps.executeUpdate();
      //ps.close();
   }

   public Pergunta buscarPorId(int id) throws SQLException
   {
      String sql = "select id, texto, tipo, comentario from pergunta where id = ?";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setInt(1, id);
      ResultSet rs = ps.executeQuery();
      Pergunta p = new Pergunta();
      if (rs.next())
      {
         preencherPergunta(p, rs);
      }
      else
      {
         p = null;
      }
      rs.close();
      ps.close();
      return p;
   }

   public ArrayList<Pergunta> buscarPorAPO(APO apo) throws SQLException
   {
      ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
      String sql = "select p.id, p.texto, p.tipo, p.comentario, p.icone_id, p.ordem from pergunta p, pergunta_categoria pc, categoria_questionario cq, "
              + "questionario_avaliacao qa where p.id = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ? order by p.ordem";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         perguntas.add(p);
      }

      return perguntas;
   }

   public ArrayList<Pergunta> buscarPorApoETipo(APO apo, int tipo) throws SQLException
   {
      ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
      String sql = "select p.id, p.texto, p.tipo, p.comentario, p.icone_id, p.ordem from pergunta p, pergunta_categoria pc, categoria_questionario cq, "
              + "questionario_avaliacao qa where p.id = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ? and p.tipo = ? order by pc.categoria_id";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      pst.setInt(2, tipo);
      ResultSet rs = pst.executeQuery();
      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         perguntas.add(p);
      }

      return perguntas;
   }

   /*
    Busca todas as perguntas quantitativas do sistema, isto é, perguntas do tipo
    2 e 3
    */
   public ArrayList<Pergunta> buscarPorApoQuantitativas(APO apo) throws SQLException
   {
      ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
      String sql = "select p.id, p.texto, p.tipo, p.comentario, p.icone_id, p.ordem from pergunta p, pergunta_categoria pc, categoria_questionario cq, "
              + "questionario_avaliacao qa where p.id = pc.pergunta_id and pc.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ? and (p.tipo = 2 or p.tipo = 3)  order by pc.categoria_id";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      ResultSet rs = pst.executeQuery();
      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         perguntas.add(p);
      }

      return perguntas;
   }

   public ArrayList<Pergunta> buscarPorTipoMorador(int tipo, Morador m) throws SQLException
   {
      ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
      String sql = "select p.id, p.texto, p.tipo, p.comentario, p.icone_id, p.ordem from pergunta p, resposta r where p.tipo = ? "
              + "and r.pergunta_id = p.id and r.morador_id = ? order by p.ordem";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setInt(1, tipo);
      ps.setInt(2, m.getId());
      ResultSet rs = ps.executeQuery();

      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         perguntas.add(p);
      }

      rs.close();
      ps.close();
      return perguntas;
   }

   public ArrayList<Pergunta> buscarPorCategoria(int id) throws SQLException
   {
      String sql = "select pergunta.id, pergunta.texto, pergunta.tipo, pergunta.comentario, pergunta.icone_id, pergunta.ordem from pergunta, "
              + "categoria, pergunta_categoria "
              + "where categoria.id = ? and categoria.id = pergunta_categoria.categoria_id "
              + "and pergunta.id = pergunta_categoria.pergunta_id order by pergunta.id";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setInt(1, id);
      ResultSet rs = ps.executeQuery();

      ArrayList<Pergunta> prs = new ArrayList<Pergunta>();
      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         prs.add(p);
      }
      //rs.close();
      //ps.close();
      return prs;
   }

   public ArrayList<Pergunta> buscarTodos() throws SQLException
   {
      String sql = "select id, texto, tipo, comentario from pergunta order by ordem";
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(sql);

      ArrayList<Pergunta> prs = new ArrayList<Pergunta>();
      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         prs.add(p);
      }
      //rs.close();
      //st.close();
      return prs;
   }

   public ArrayList<Pergunta> buscarPorComodo(Comodo c) throws SQLException
   {
      ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
      String sql = "select p.id, p.texto, p.tipo, p.comentario, p.icone_id, p.ordem from pergunta p, pergunta_comodo pc where p.id = pc.pergunta_id and "
              + "pc.comodo_id = ? order by p.ordem";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, c.getId());
      ResultSet rs = pst.executeQuery();
      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         perguntas.add(p);
      }
      rs.close();
      pst.close();
      return perguntas;
   }

   public ArrayList<Pergunta> buscarPorComodoApo(Comodo c, APO apo) throws SQLException
   {
      ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
      String sql = "select distinct p.id, p.texto, p.tipo, p.comentario, p.icone_id, p.ordem from pergunta p, pergunta_comodo pc, pergunta_categoria pct, "
              + "categoria_questionario cq, "
              + "questionario_avaliacao qa "
              + "where p.id = pc.pergunta_id and "
              + "pc.comodo_id = ? and p.id = pct.pergunta_id and pct.categoria_id = cq.categoria_id and "
              + "cq.questionario_id = qa.questionario_id and qa.avaliacao_id = ? order by p.ordem";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, c.getId());
      pst.setInt(2, apo.getId());
      ResultSet rs = pst.executeQuery();
      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         perguntas.add(p);
      }
      rs.close();
      pst.close();
      return perguntas;
   }

   /*
    Método que retorna uma lista de perguntas pertencentes a uma APO, Técnica e
    de um tipo especificado
    */
   public ArrayList<Pergunta> buscarPorApoTecnicaTipo(APO apo, Questionario tecnica, int tipo) throws SQLException
   {
      ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
      String sql = "select distinct p.id, p.texto, p.tipo, p.comentario, p.icone_id, p.icone_id, p.ordem from pergunta p, pergunta_categoria pc, "
              + "categoria_questionario cq, questionario_avaliacao qa where qa.avaliacao_id = ? and qa.questionario_id = cq.questionario_id and "
              + "cq.questionario_id = ? and cq.categoria_id = pc.categoria_id and pc.pergunta_id = p.id and p.tipo = ?";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setInt(1, apo.getId());
      pst.setInt(2, tecnica.getId());
      pst.setInt(3, tipo);
      ResultSet rs = pst.executeQuery();
      while (rs.next())
      {
         Pergunta p = new Pergunta();
         preencherPergunta(p, rs);
         perguntas.add(p);
      }
      rs.close();
      pst.close();
      return perguntas;
   }

   private void preencherPergunta(Pergunta p, ResultSet rs) throws SQLException
   {
      p.setId(rs.getInt("id"));
      p.setTexto(rs.getString("texto"));
      p.setTipo(rs.getInt("tipo"));
      p.setComentario(rs.getString("comentario"));
      p.setEscalaDeCores(rs.getInt("icone_id"));
      p.setOrdem(rs.getInt("ordem"));
   }

   /**
    * @return the conn
    */
   public Connection getConn()
   {
      return conn;
   }

   /**
    * @param conn the conn to set
    */
   public void setConn(Connection conn)
   {
      this.conn = conn;
   }
}
