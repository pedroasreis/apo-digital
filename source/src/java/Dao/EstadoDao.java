package Dao;

import Model.Cidade;
import Model.Estado;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class EstadoDao implements Serializable {
    
    private Connection conn;
    
    public ArrayList<Estado> buscarTodos() throws SQLException{
        ArrayList<Estado> estados = new ArrayList<Estado>();
        String sql = "select * from estado";
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            Estado e = new Estado();
            e.setId(rs.getInt(1));
            e.setNome(rs.getString(2));
            e.setUf(rs.getString(3));
            estados.add(e);
        }
        
        rs.close();
        st.close();
        return estados;
    }
    
    public ArrayList<Cidade> buscarCidadesPorEstado(Estado e) throws SQLException{
        ArrayList<Cidade> cidades = new ArrayList<Cidade>();
        String sql = "select * from cidade where estado = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, e.getId());
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            Cidade c = new Cidade();
            c.setId(rs.getInt(1));
            c.setNome(rs.getString(2));
            cidades.add(c);
        }
        
        rs.close();
        pst.close();
        return cidades;
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
}
