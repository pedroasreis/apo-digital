/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Icone;
import Model.Pergunta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class IconeDao 
{
    
    private Connection conn;
    
    public ArrayList<Icone> buscarTodos() throws SQLException
    {
        ArrayList<Icone> icones = new ArrayList<Icone>();
        String sql = "select id, titulo from icone";
        
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while(rs.next())
        {
            Icone ic = new Icone();
            preencherIcone(ic, rs);
            icones.add(ic);
        }
                
        return icones;
    }
    
    public ArrayList<Icone> buscarPorPergunta(Pergunta p) throws SQLException
    {
        ArrayList<Icone> icones = new ArrayList<Icone>();
        String sql = "select id, titulo from icone i, pergunta p, pergunta_icone where i.id = icone_id and p.id = pergunta_id "
                + "and p.id = ?";
        
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, p.getId());
        ResultSet rs = pst.executeQuery();
        
        while(rs.next())
        {
            Icone ic = new Icone();
            preencherIcone(ic, rs);
            icones.add(ic);
        }
                
        return icones;
    }
    
    public void associarPerguntaIcone(Pergunta p, Icone ic) throws SQLException
    {
        String sql = "update pergunta set icone_id = ? where id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, ic.getId());
        pst.setInt(2, p.getId());
        pst.executeUpdate();
        System.out.println("PergId: " + p.getId() + " - IcId: " + ic.getId());
    }
    
    private void preencherIcone(Icone ic, ResultSet rs) throws SQLException
    {
        ic.setId(rs.getInt("id"));
        ic.setTitulo(rs.getString("titulo"));
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
}
