/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Model.Pergunta;
import Model.Qualificador;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class QualificadorDao implements Serializable {

    private Connection conn;

    public void salvar(Qualificador q, Pergunta p) throws SQLException{
        int id = 0;
        String sql = "insert into qualificador values(null, ?, ?, ?, ?)";
        PreparedStatement ps = getConn().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, q.getTexto());
        ps.setInt(2, q.getInicio());
        ps.setInt(3, q.getFim());
        ps.setInt(4, q.getIconeId());
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(rs.next()){
            id = rs.getInt(1);
        }
        // inserir pergunta_categoria
        String sql2 = "insert into pergunta_qualificador values(?, ?)";
        PreparedStatement ps2 = getConn().prepareStatement(sql2);
        ps2.setInt(1, p.getId());
        ps2.setInt(2, id);
        ps2.executeUpdate();
        //ps.close();
    }

    public void editar(Qualificador q) throws SQLException{
        String sql = "update categoria set texto = ?, inicio = ?, fim = ?, icone_id = ? where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setString(1, q.getTexto());
        ps.setInt(2, q.getInicio());
        ps.setInt(3, q.getIconeId());
        ps.setInt(4, q.getFim());
        ps.executeUpdate();
        //ps.close();
    }

    public void excluir(Qualificador q) throws SQLException{
        String sql = "delete from qualificador where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, q.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public void excluirPerguntaQualificador(Pergunta p) throws SQLException{
        String sql = "delete from pergunta_qualificador where pergunta_id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public ArrayList<Qualificador> buscarTodos() throws SQLException{
        String sql = "select id, texto, inicio, fim from qualificador";
        Statement st = getConn().createStatement();
        ResultSet rs = st.executeQuery(sql);
        ArrayList<Qualificador> qs = new ArrayList<Qualificador>();
        while(rs.next()){
            Qualificador q = new Qualificador();
            preencherQualificador(q, rs);
            qs.add(q);
        }
        //rs.close();
        //st.close();
        return qs;
    }

    public ArrayList<Qualificador> buscarPorPergunta(Pergunta p) throws SQLException{
        String sql = "select qualificador.id, qualificador.texto, inicio, fim from qualificador, pergunta, pergunta_qualificador "
                + "where pergunta_id = ? and pergunta_id = pergunta.id and qualificador_id = qualificador.id";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ResultSet rs = ps.executeQuery();
        ArrayList<Qualificador> qs = new ArrayList<Qualificador>();
        while(rs.next()){
            Qualificador q = new Qualificador();
            preencherQualificador(q, rs);
            qs.add(q);
        }
        //rs.close();
        //st.close();
        return qs;
    }
    
    public ArrayList<Qualificador> buscarPorAtributoOuConceito(int perguntaId, int entidadeId) throws SQLException
    {
       ArrayList<Qualificador> qualificadores = new ArrayList<Qualificador>();
       String sql = "select q.id, q.texto, q.inicio, q.fim from qualificador q, conceito_qualificador cq where "
               + "cq.pergunta_id = ? and cq.conceito_id = ? and cq.qualificador_id = q.id";
       PreparedStatement pst = conn.prepareStatement(sql);
       pst.setInt(1, perguntaId);
       pst.setInt(2, entidadeId);
       ResultSet rs = pst.executeQuery();
       while(rs.next()){
          Qualificador q = new Qualificador();
          preencherQualificador(q, rs);
          qualificadores.add(q);
       }
       rs.close();
       pst.close();
       return qualificadores;
    }

    private void preencherQualificador(Qualificador q, ResultSet rs) throws SQLException {
        q.setId(rs.getInt("id"));
        q.setTexto(rs.getString("texto"));
        q.setInicio(rs.getInt("inicio"));
        q.setFim(rs.getInt("fim"));
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }

}
