/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Atributo;
import Model.Pergunta;
import Model.PerguntaAtributo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class AtributoDao {
    
    private Connection conn;
    
    public void inserir(Atributo atributo) throws SQLException{
        String sql = "insert into atributo values(?, ?);";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, atributo.getId());
        pst.setString(2, atributo.getNome());
        
        pst.executeUpdate();
        pst.close();
    }
    
    public void editar(Atributo atributo) throws SQLException
    {
        String sql = "update atributo set nome = ? where id = ?;";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, atributo.getNome());
        pst.setInt(2, atributo.getId());
        
        pst.executeUpdate();
        pst.close();
    }
    
    public void deletar(int id) throws SQLException{
        String sql = "delete from atributo where id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, id);
        
        pst.executeUpdate();
        pst.close();
    }
    
    public ArrayList<Atributo> buscarTodos() throws SQLException{
        ArrayList<Atributo> atributos = new ArrayList<Atributo>();
        String sql = "select * from atributo order by id;";
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        
        while(rs.next()){
            Atributo a = new Atributo(rs.getInt(1), rs.getString(2));
            Atributo a1 = new Atributo();
            a1.setId(rs.getInt(1));
            a1.setNome(rs.getString(2));
            atributos.add(a);
        }
        
        rs.close();
        st.close();
        return atributos;
    }
    
    public Atributo buscarPorId(int id) throws SQLException{
        Atributo atributo = new Atributo();
        String sql = "select * from atributo where id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, id);
        
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
            atributo.setId(rs.getInt("id"));
            atributo.setNome(rs.getString("nome"));
        }
        
        return atributo;
    }
    
    public void inserirPerguntaAtributo(PerguntaAtributo perguntaAtributo) throws SQLException{
        String sql = "insert into pergunta_atributo values(?, ?);";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, perguntaAtributo.getPergunta().getId());
        pst.setInt(2, perguntaAtributo.getAtributo().getId());
        pst.executeUpdate();
        pst.close();
    }
    
    public ArrayList<Atributo> buscarPorPergunta(Pergunta p) throws SQLException{
        ArrayList<Atributo> atributos = new ArrayList<Atributo>();
        String sql = "select a.id, a.nome from atributo a, pergunta_atributo pa where "
                + "pa.id_pergunta = ? and pa.id_atributo = a.id order by a.id";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, p.getId());
        
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            Atributo a = new Atributo(rs.getInt("id"), rs.getString("nome"));
            atributos.add(a);
        }
        
        rs.close();
        pst.close();
        return atributos;
    }
    
    public void desassociarPerguntaAtributo(Pergunta pergunta) throws SQLException {
        String sql = "delete from pergunta_atributo where id_pergunta = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, pergunta.getId());
        pst.executeUpdate();
        pst.close();
    }
    
    public void associarComodoPergunta(Pergunta pergunta, Atributo atributo) throws SQLException {
        String sql = "insert into pergunta_atributo values(?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, pergunta.getId());
        pst.setInt(2, atributo.getId());
        pst.executeUpdate();
        
        pst.close();
    }
    
    public ArrayList<Atributo> buscarPorNaoPergunta(Pergunta pergunta) throws SQLException{
        ArrayList<Atributo> atributos = new ArrayList<Atributo>();
        String sql = "select a.id, a.nome from atributo a, pergunta_atributo pa where "
                + "a.id = pa.id_atributo and pa.id_pergunta != ? order by a.id";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, pergunta.getId());
        
        ResultSet rs = pst.executeQuery();
        while(rs.next()){
            Atributo a = new Atributo(rs.getInt("id"), rs.getString("nome"));
            atributos.add(a);
        }
        
        rs.close();
        pst.close();
        return atributos;
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
}
