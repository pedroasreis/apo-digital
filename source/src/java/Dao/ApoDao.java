/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.APO;
import Model.Morador;
import Model.Questionario;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pedro
 */
public class ApoDao implements Serializable {

    private Connection conn;

    public void salvar(APO a) throws SQLException{
        String sql = "insert into avaliacao values(null, ?, ?, 1, ?, ?, ?, ?)";
        PreparedStatement ps = getConn().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, a.getNome());
        ps.setString(2, a.getTexto());
        ps.setString(3, a.getCidade());
        ps.setString(4, a.getEstado());
        ps.setInt(5, a.getIdCidade());
        ps.setInt(6, a.getIdEstado());
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(rs.next()){
            a.setId(rs.getInt(1));
        }
        rs.close();
        ps.close();
    }

    public void salvarQuestionarioAvaliacao(Questionario q, APO a) throws SQLException{
        // inserir conceito_pergunta
        String sql2 = "insert into questionario_avaliacao values(?, ?)";
        PreparedStatement ps2 = getConn().prepareStatement(sql2);
        ps2.setInt(1, q.getId());
        ps2.setInt(2, a.getId());
        ps2.executeUpdate();
        //ps.close();
    }

    public void editar(APO a) throws SQLException{
        String sql = "update avaliacao set nome = ?, texto = ?, cidade = ?, estado = ?, id_cidade = ?, id_estado = ? where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setString(1, a.getNome());
        ps.setString(2, a.getTexto());
        ps.setString(3, a.getCidade());
        ps.setString(4, a.getEstado());
        ps.setInt(5, a.getIdCidade());
        ps.setInt(6, a.getIdEstado());
        ps.setInt(7, a.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public void excluir(APO a) throws SQLException{
        String sql = "delete from avaliacao where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, a.getId());
        ps.executeUpdate();
        //ps.close();
    }
    
    public APO buscarPorMorador(Morador m) throws SQLException{
        APO apo = new APO();
        String sql = "select * from avaliacao where id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, m.getEdificioId());
        
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
            preencherAPO(apo, rs);
        }
        
        pst.close();
        rs.close();
        return apo;
    }
    
    public APO buscarPorId(int id) throws SQLException{
       APO apo = new APO();
        String sql = "select * from avaliacao where id = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, id);
        
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
            preencherAPO(apo, rs);
        }
        
        pst.close();
        rs.close();
        return apo;
    }

    public List<APO> buscarTodos() throws SQLException{
        String sql = "select * from avaliacao";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        List<APO> apos = new ArrayList<APO>();
        while(rs.next()){
            APO a = new APO();
            preencherAPO(a, rs);
            apos.add(a);
        }
        return apos;
    }

    private void preencherAPO(APO a, ResultSet rs) throws SQLException {
        a.setId(rs.getInt("id"));
        a.setNome(rs.getString("nome"));
        a.setTexto(rs.getString("texto"));
        a.setCidade(rs.getString("cidade"));
        a.setEstado(rs.getString("estado"));
        a.setIdCidade(rs.getInt("id_cidade"));
        a.setIdEstado(rs.getInt("id_estado"));
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
}
