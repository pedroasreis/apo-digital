package Dao;

import Model.ComodoPlanta;
import Model.ModeloTopologico;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Pedro
 */
public class ModeloTopologicoDao implements Serializable {
    
    private Connection conn;
    
    public int inserirModeloTopologico(ModeloTopologico mt) throws SQLException{
        int id = 0;
        String sql = "insert into modelo_topologico values(null, ?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        preencherStatementModeloTopologico(pst, mt);
        pst.executeUpdate();
        ResultSet rs = pst.getGeneratedKeys();
        if(rs.next()){
            id = rs.getInt(1);
        }
        
        rs.close();
        pst.close();
        return id;
    }
    
    private void preencherStatementModeloTopologico(PreparedStatement pst, ModeloTopologico mt) 
            throws SQLException{
        pst.setInt(1, mt.getApo().getId());
        pst.setString(2, mt.getDescricao());
    }
    
    public int inserirComodoTopologico(ComodoPlanta cp) throws SQLException{
        int id = 0;
        String sql = "insert into comodo_topologico values(null, ?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        preencherStatementeComodoPlanta(pst, cp);
        pst.executeUpdate();
        ResultSet rs = pst.getGeneratedKeys();
        if(rs.next()){
            id = rs.getInt(1);
        }
        
        rs.close();
        pst.close();
        return id;
    }
    
    private void preencherStatementeComodoPlanta(PreparedStatement pst, ComodoPlanta cp) throws SQLException{
        pst.setInt(1, cp.getComodo().getId());
        pst.setInt(2, cp.getModeloTopologico().getId());
    }
    
    public void inserirConexaoTopologica(ComodoPlanta cp1, ComodoPlanta cp2) throws SQLException{
        String sql = "insert into conexao_topologica values(?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setInt(1, cp1.getIdReal());
        pst.setInt(2, cp2.getIdReal());
        pst.executeUpdate();
        pst.close();
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
}
