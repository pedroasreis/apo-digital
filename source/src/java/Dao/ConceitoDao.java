/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Model.Categoria;
import Model.Conceito;
import Model.Pergunta;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedro
 */
public class ConceitoDao implements Serializable {

    private Connection conn;

    public void salvar(Conceito c) throws SQLException{
        String sql = "insert into conceito values(null, ?, ?)";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setString(1, c.getNome());
        ps.setInt(2, c.getOrdem());
        ps.executeUpdate();
    }

    public void salvarConceitoPergunta(Conceito c, Pergunta p) throws SQLException{
        // inserir conceito_pergunta
        String sql2 = "insert into conceito_pergunta values(?, ?)";
        PreparedStatement ps2 = getConn().prepareStatement(sql2);
        ps2.setInt(1, c.getId());
        ps2.setInt(2, p.getId());
        ps2.executeUpdate();
        //ps.close();
    }

    public void salvarConceitoCategoria(Conceito c, Categoria ct) throws SQLException{
        String sql2 = "insert into conceito_categoria values(?, ?)";
        PreparedStatement ps2 = getConn().prepareStatement(sql2);
        ps2.setInt(1, c.getId());
        ps2.setInt(2, ct.getId());
        ps2.executeUpdate();
        //ps.close();
    }

    public void editar(Conceito c) throws SQLException{
        String sql = "update conceito set nome = ?, ordem = ? where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setString(1, c.getNome());
        ps.setInt(2, c.getOrdem());
        ps.setInt(3, c.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public void excluir(Conceito c) throws SQLException{
        String sql = "delete from conceito where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, c.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public void excluirPorCategoria(Categoria cat) throws SQLException{
        String sql = "delete from conceito_categoria where categoria_id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, cat.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public void excluirPorPergunta(Pergunta p) throws SQLException{
        String sql = "delete from conceito_pergunta where pergunta_id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ps.executeUpdate();
        //ps.close();
    }

    public List<Conceito> buscarTodos() throws SQLException{
        String sql = "select id, nome, ordem from conceito order by id";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        List<Conceito> cs = new ArrayList<Conceito>();
        while(rs.next()){
            Conceito c = new Conceito();
            preencherConceito(c, rs);
            cs.add(c);
        }
        return cs;
    }

    public List<Conceito> buscarPorCategoria(Categoria cat) throws SQLException{
        String sql = "select id, nome, ordem from conceito, conceito_categoria "
                + "where categoria_id = ? and conceito_id = conceito.id order by id";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, cat.getId());
        ResultSet rs = ps.executeQuery();
        List<Conceito> cs = new ArrayList<Conceito>();
        while(rs.next()){
            Conceito c = new Conceito();
            preencherConceito(c, rs);
            cs.add(c);
        }
        return cs;
    }
    
    public Conceito buscarPorId(int id) throws SQLException{
        String sql = "select id, nome, ordem from conceito where id = ?";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Conceito c = new Conceito();
        while(rs.next()){
            preencherConceito(c, rs);
        }
        return c;
    }

    public ArrayList<Conceito> buscarPorPergunta(Pergunta p) throws SQLException{
        String sql = "select id, nome, ordem from conceito, conceito_pergunta "
                + "where pergunta_id = ? and conceito_id = conceito.id order by id";
        PreparedStatement ps = getConn().prepareStatement(sql);
        ps.setInt(1, p.getId());
        ResultSet rs = ps.executeQuery();
        ArrayList<Conceito> cs = new ArrayList<Conceito>();
        while(rs.next()){
            Conceito c = new Conceito();
            preencherConceito(c, rs);
            cs.add(c);
        }
        return cs;
    }

    private void preencherConceito(Conceito c, ResultSet rs) throws SQLException{
        c.setId(rs.getInt("id"));
        c.setNome(rs.getString("nome"));
        c.setOrdem(rs.getInt("ordem"));
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }

}
