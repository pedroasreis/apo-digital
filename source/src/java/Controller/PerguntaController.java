/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.AtributoDao;
import Dao.CategoriaDao;
import Dao.ComodoDao;
import Dao.ConceitoDao;
import Dao.ConnectionDao;
import Dao.IconeDao;
import Dao.PerguntaDao;
import Dao.QualificadorDao;
import Dao.RespostaDao;
import Model.Atributo;
import Model.Categoria;
import Model.Comodo;
import Model.Conceito;
import Model.Icone;
import Model.Pergunta;
import Model.Qualificador;
import Model.Resposta;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.model.DualListModel;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author pedro
 */

@ManagedBean (name = "PerguntaController")
@ViewScoped
public class PerguntaController implements Serializable
{

    private Pergunta pergunta;
    private ArrayList<Categoria> categorias;
    private Categoria categoria;
    private Connection conn;
    private ConnectionDao connFactory;
    private PerguntaDao dao;
    private CategoriaDao cDao;
    private ConceitoDao conDao;
    private QualificadorDao qDao;
    private RespostaDao rDao;
    private ComodoDao comDao;
    private String idTabela;
    private Conceito conceito;
    private List<String> qualificadores;
    private List<Conceito> conceitos;
    private List<Conceito> conceitosSelecionadosCategoria;
    private List<Conceito> conceitosSelecionadosPergunta;
    private ArrayList<Comodo> comodos;
    private ArrayList<Comodo> cs;
    private Comodo comodo;
    private Conceito conceitosCategoria[];
    private Conceito conceitosPergunta[];
    private Conceito selecionadosConceitos[];
    private Conceito conceitosSelecionadosPerg[];
    private String opcao;
    private String opcao2;
    private Qualificador qualificador;
    private ArrayList<GraficoRespostas> graficos;
    private PieChartModel pie;
    private TreeMap<String, String> treeComodos;
    private boolean isNumerico;
    
    private Comodo comodoIn;
    private Comodo comodoOut;
    
    private ArrayList<Icone> icones;
    private Icone icone;
    private IconeDao icDao;
    
    private Comodo[] comodosVector;
    private Comodo[] comodosVectorDaPergunta;
    
    private int comodoId;
    private String cId;

    private DualListModel<Conceito> dualCategoria;
    
    private boolean editando;
    private int index = 0;
    
    private ArrayList<Atributo> atributos;
    private Atributo atributoIn;
    private Atributo atributoOut;
    private AtributoDao atributoDao;
    
    private boolean escCores;

    public PerguntaController() throws SQLException
    {
        connFactory = new ConnectionDao();
        pergunta = new Pergunta();
        categoria = new Categoria();
        dao = new PerguntaDao();
        cDao = new CategoriaDao();
        qDao = new QualificadorDao();
        conDao = new ConceitoDao();
        comDao = new ComodoDao();
        rDao = new RespostaDao();
        icDao = new IconeDao();
        qualificadores = new ArrayList<String>();
        comodos = new ArrayList<Comodo>();
        conceitosSelecionadosCategoria = new ArrayList<Conceito>();
        conceitosSelecionadosPergunta = new ArrayList<Conceito>();
        qualificador = new Qualificador();
        conceito = new Conceito();
        comodo = new Comodo();
        comodoIn = new Comodo();
        comodoOut = new Comodo();
        isNumerico = false;
        graficos = new ArrayList<GraficoRespostas>();
        pie = new PieChartModel();
        icone = new Icone();
        atributoDao = new AtributoDao();
        carregarCategorias();
    }
    
    public void odd(){
        System.out.println("oO");
    }

    private void carregarCategorias() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        cDao.setConn(conn);
        qDao.setConn(conn);
        categorias = cDao.buscarPorQuestionario(recuperarSessao().getQuestionarioCategoria());
        Iterator<Categoria> i = categorias.iterator();
        while(i.hasNext())
        {
            Categoria c = i.next();
            Iterator<Pergunta> ip = c.getPerguntas().iterator();
            while(ip.hasNext())
            {
                Pergunta p = ip.next();
                p.setQualificadores(qDao.buscarPorPergunta(p));
                GraficoRespostas g = new GraficoRespostas();
                g.setPergunta(p);
                graficos.add(g);
            }
        }
        conn.commit();
        conn.close();
    }

    public void prepararConceitos() throws SQLException
    {
        System.out.println("Preparando conceitos novamente...");
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        conDao.setConn(conn);
        setConceitos(conDao.buscarTodos());
        setConceitosSelecionadosCategoria(conDao.buscarPorCategoria(categoria));
        Iterator<Conceito> it = conceitosSelecionadosCategoria.iterator();
        while(it.hasNext())
        {
            Conceito c = it.next();
            Iterator<Conceito> i = conceitos.iterator();
            while(i.hasNext()){
                Conceito cn = i.next();
                if(cn.getId() == c.getId()){
                    conceitos.remove(cn);
                    break;
                }
            }
        }
        conn.commit();
        conn.close();
    }
    
    public void prepararAtributos() throws SQLException
    {
       conn = connFactory.getConnection();
       conn.setAutoCommit(false);
       atributoDao.setConn(conn);
       
       atributos = atributoDao.buscarTodos();
       pergunta.setAtributos(atributoDao.buscarPorPergunta(pergunta));
       
       System.out.println("Tamanho do array com todos os atributos: " + atributos.size());
       System.out.println("Tamanho do array apenas com os atributos associados à pergunta escolhida: " + pergunta.getAtributos().size());
       
       Iterator<Atributo> it = pergunta.getAtributos().iterator();
       while(it.hasNext()){
           Atributo a = it.next();
           atributos.remove(a);
       }
       
       conn.commit();
       conn.close();
    }
    
    public void prepararComodos() throws SQLException
    {
        System.out.println("Preparando cômodos...");
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        comDao.setConn(conn);
        
        comodos = comDao.buscarPorNaoPergunta(pergunta);
        pergunta.setComodos(comDao.buscarPorPergunta(pergunta));
        
        int size = comodos.size() + pergunta.getComodos().size();
        
        comodosVector = new Comodo[size];
        comodosVectorDaPergunta = new Comodo[size];
        
        int i = 0;
        Iterator<Comodo> it = comodos.iterator();
        while(it.hasNext())
        {
            Comodo c = it.next();
            comodosVector[i] = c;
            i++;
        }
        
        i = 0;
        it = pergunta.getComodos().iterator();
        while(it.hasNext())
        {
            Comodo c = it.next();
            comodosVectorDaPergunta[i] = c;
            i++;
        }
        
        /*treeComodos = new TreeMap<String, String>();
        System.out.println("...Preparando cômodos...");
        Iterator<Comodo> it = comodos.iterator();
        while(it.hasNext())
        {
            Comodo c = it.next();
            treeComodos.put(c.getNome(), "" + c.getId());
            System.out.println(c.getNome());
        }*/
        conn.commit();
        conn.close();
    }
    
    public void associarComodo() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        comDao.setConn(conn);
        
        comDao.desassociarComodoPergunta(pergunta); // será essa a melhor estratégia?
        
        Iterator<Comodo> it = pergunta.getComodos().iterator();
        while(it.hasNext())
        {
            Comodo c = it.next();
            comDao.associarComodoPergunta(pergunta, c);
        }
        
        conn.commit();
        conn.close();
    }
    
    public void associarAtributo() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        atributoDao.setConn(conn);
        
        atributoDao.desassociarPerguntaAtributo(pergunta);
        
        Iterator<Atributo> it = pergunta.getAtributos().iterator();
        while(it.hasNext())
        {
            Atributo a = it.next();
            atributoDao.associarComodoPergunta(pergunta, a);
        }
        
        conn.commit();
        conn.close();
    }
    
    public void associarIcone() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        icDao.setConn(conn);

        System.out.println("Associando ícone...");
        icDao.associarPerguntaIcone(pergunta, icone);
        
        conn.commit();
        conn.close();
    }

    public void prepararConceitosPergunta() throws SQLException
    {
        //prepararConceitos();
        System.out.println("Preparando conceitos...");
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        conDao.setConn(conn);
        conceitosSelecionadosCategoria = conDao.buscarTodos();
        setConceitosSelecionadosPergunta(conDao.buscarPorPergunta(pergunta));
        Iterator<Conceito> it = conceitosSelecionadosPergunta.iterator();
        while(it.hasNext())
        {
            Conceito c = it.next();
            Iterator<Conceito> i = conceitosSelecionadosCategoria.iterator();
            while(i.hasNext()){
                Conceito cn = i.next();
                if(cn.getId() == c.getId()){
                    conceitosSelecionadosCategoria.remove(cn);
                    break;
                }
            }
        }
        conn.commit();
        conn.close();
    }

    public void criarCategoria() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        cDao.setConn(conn);
        cDao.salvar(categoria, recuperarSessao().getQuestionarioCategoria());
        conn.commit();
        conn.close();
        categoria = new Categoria();
        categorias = new ArrayList<Categoria>();
        carregarCategorias();
    }

    public void editarCategoria() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        cDao.setConn(conn);
        cDao.editar(categoria);
        conn.commit();
        conn.close();
        categoria = new Categoria();
        categorias = new ArrayList<Categoria>();
        carregarCategorias();
    }

    public void prepararEdicao()
    {
        if(pergunta.getTipo() == Pergunta.INTERVALO_NUMERICO)
        {
            qualificador = pergunta.getQualificadores().get(0);
        }
        else if(pergunta.getTipo() == Pergunta.ESCOLHA_UNICA || pergunta.getTipo() == Pergunta.MULTIPLA_ESCOLHA)
        {
            qualificadores = new ArrayList<String>();
            Iterator<Qualificador> i = pergunta.getQualificadores().iterator();
            while(i.hasNext())
            {
                Qualificador q = i.next();
                qualificadores.add(q.getTexto());
            }
        }
        
        if(pergunta.getEscalaDeCores() == 1){
           escCores = true;
        }
        else{
           escCores = false;
        }
    }

    public void editar() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);
        qDao.setConn(conn);
        qDao.excluirPerguntaQualificador(pergunta);
        Iterator<Qualificador> it = pergunta.getQualificadores().iterator();
        while (it.hasNext())
        {
            Qualificador q = it.next();
            qDao.excluir(q);
        }
        pergunta.setQualificadores(new ArrayList<Qualificador>());
        if (pergunta.getTipo() == Pergunta.INTERVALO_NUMERICO)
        {
            qualificador.setTexto("");
            pergunta.getQualificadores().add(qualificador);
        }
        else if (pergunta.getTipo() == Pergunta.MULTIPLA_ESCOLHA || pergunta.getTipo() == Pergunta.ESCOLHA_UNICA)
        {
            Iterator<String> i = qualificadores.iterator();
            while (i.hasNext())
            {
                String aux = i.next();
                Qualificador qual = new Qualificador();
                qual.setTexto(aux);
                pergunta.getQualificadores().add(qual);
            }
        }
        if (!(pergunta.getTipo() == Pergunta.TEXTO))
        {
            Iterator<Qualificador> i = pergunta.getQualificadores().iterator();
            while (i.hasNext())
            {
                Qualificador q = i.next();
                qDao.salvar(q, pergunta);
            }
        }
        if(escCores){
           pergunta.setEscalaDeCores(1);
        }
        else{
           pergunta.setEscalaDeCores(0);
        }
        dao.editar(pergunta);
        conn.commit();
        conn.close();
        pergunta = new Pergunta();
        categorias = new ArrayList<Categoria>();
        qualificadores = new ArrayList<String>();
        qualificador = new Qualificador();
        carregarCategorias();
    }

    public void criarPergunta() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);
        qDao.setConn(conn);
        if(escCores){
           pergunta.setEscalaDeCores(1);
        }
        int id = dao.salvar(pergunta, categoria);
        pergunta.setId(id);
        if (pergunta.getTipo() == Pergunta.INTERVALO_NUMERICO)
        {
            qualificador.setTexto("");
            pergunta.getQualificadores().add(qualificador);
        }
        else if (pergunta.getTipo() == Pergunta.MULTIPLA_ESCOLHA || pergunta.getTipo() == Pergunta.ESCOLHA_UNICA)
        {
            Iterator<String> i = qualificadores.iterator();
            while (i.hasNext())
            {
                String aux = i.next();
                Qualificador qual = new Qualificador();
                qual.setTexto(aux);
                pergunta.getQualificadores().add(qual);
            }
        }
        if (!(pergunta.getTipo() == Pergunta.TEXTO))
        {
            Iterator<Qualificador> i = pergunta.getQualificadores().iterator();
            while (i.hasNext())
            {
                Qualificador q = i.next();
                qDao.salvar(q, pergunta);
            }
        }
        conn.commit();
        conn.close();
        categoria = new Categoria();
        pergunta = new Pergunta();
        categorias = new ArrayList<Categoria>();
        qualificadores = new ArrayList<String>();
        qualificador = new Qualificador();
        escCores = false;
        carregarCategorias();
    }

    public void excluirPergunta() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);
        dao.excluir(pergunta);
        conn.commit();
        conn.close();
        pergunta = new Pergunta();
        categorias = new ArrayList<Categoria>();
        carregarCategorias();
    }

    public void addOpcao()
    {
        qualificadores.add(opcao);
        opcao = "";
    }
    
    public void editOpcao()
    {
        qualificadores.remove(index);
        qualificadores.add(index, opcao);
        editando = false;
        opcao = "";
    }
    
    public void changeToEdit()
    {
        System.out.println("changeToEdit()");
        editando = true;
        index = 0;
        /*
         * Encontrando indice da string
         */
        Iterator<String> it = qualificadores.iterator();
        while(it.hasNext())
        {
            String op = it.next();
            if(op == opcao) // a ideia é realmente verificar se realmente se trata do mesmo objeto
            {
                System.out.println("Index: " + index);
                break;
            }
            index++;
        }
    }

    private SessionController recuperarSessao()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        return (SessionController) session.getAttribute("SessionController");
    }

    public void adicionarConceitoCategoria() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        conDao.setConn(conn);
        conDao.excluirPorCategoria(categoria);
        Iterator<Conceito> it = conceitosSelecionadosCategoria.iterator();
        while (it.hasNext())
        {
            Conceito c = it.next();
            conDao.salvarConceitoCategoria(c, categoria);
        }
        conn.commit();
        conn.close();
    }
    
    public void prepararIcones() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        icDao.setConn(conn);
        
        System.out.println("Buscando icones");
        icones = icDao.buscarTodos();
        
        conn.commit();
        conn.close();
    }

    public void adicionarConceitoPergunta() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        conDao.setConn(conn);
        conDao.excluirPorPergunta(pergunta);
        Iterator<Conceito> it = conceitosSelecionadosPergunta.iterator();
        while (it.hasNext())
        {
            Conceito c = it.next();
            conDao.salvarConceitoPergunta(c, pergunta);
        }
        conn.commit();
        conn.close();
    }

    public void toTarget()
    {
        for (int i = 0; i < conceitosCategoria.length; i++)
        {
            conceitosSelecionadosCategoria.add(conceitosCategoria[i]);
            conceitos.remove(conceitosCategoria[i]);
        }
    }
    
    public void toSource()
    {
       for (int i = 0; i < selecionadosConceitos.length; i++)
        {
            conceitos.add(selecionadosConceitos[i]);
            conceitosSelecionadosCategoria.remove(selecionadosConceitos[i]);
        }
    }
    
    public void toTargetAtributo()
    {
        pergunta.getAtributos().add(atributoIn);
        
        int i;
        for(i = 0; i < atributos.size(); i++)
        {
            if(atributos.get(i).getId() == atributoIn.getId())
            {
                break;
            }
        }
        atributos.remove(i);
    }
    
    public void toSourceAtributo()
    {
        atributos.add(atributoOut);
        
        int i;
        for(i = 0; i < pergunta.getAtributos().size(); i++)
        {
            if(pergunta.getAtributos().get(i).getId() == atributoOut.getId())
            {
                break;
            }
        }
        pergunta.getAtributos().remove(i);
    }
    
    public void toTargetComodo()
    {
        pergunta.getComodos().add(comodoIn);
        System.out.println("Comodo selecionado: " + comodoIn.getNome());
        
        int i;
        for(i = 0; i < comodos.size(); i++)
        {
            System.out.println("Comodo: " + comodos.get(i).getNome());
            System.out.println("Comodo selecionado: " + comodoIn.getNome());
            if(comodos.get(i).getId() == comodoIn.getId())
            {
                break;
            }
        }
        comodos.remove(i);
    }
    
    public void toSourceComodo()
    {
        comodos.add(comodoOut);
        
        int i;
        for(i = 0; i < pergunta.getComodos().size(); i++)
        {
            if(pergunta.getComodos().get(i).getId() == comodoOut.getId())
            {
                break;
            }
        }
        pergunta.getComodos().remove(i);
    }

    public void toTargetPergunta()
    {
        for (int i = 0; i < conceitosPergunta.length; i++)
        {
            conceitosSelecionadosPergunta.add(conceitosPergunta[i]);
            conceitosSelecionadosCategoria.remove(conceitosPergunta[i]);
        }
    }

    public void toSourcePergunta()
    {
       for (int i = 0; i < conceitosSelecionadosPerg.length; i++)
        {
            conceitosSelecionadosCategoria.add(conceitosSelecionadosPerg[i]);
            conceitosSelecionadosPergunta.remove(conceitosSelecionadosPerg[i]);
        }
    }

    public void deleteOpcao()
    {
        qualificadores.remove(opcao);
    }

    public int handler()
    {
        return pergunta.getTipo();
    }

    public void resetCategoria()
    {
        categoria = new Categoria();
    }

    public void resetPergunta()
    {
        pergunta = new Pergunta();
        qualificadores = new ArrayList<String>();
        qualificador = new Qualificador();
    }

    /**
     * @return the graficos
     */
    public ArrayList<GraficoRespostas> getGrafico() {
        return graficos;
    }

    /**
     * @param graficos the graficos to set
     */
    public void setGrafico(ArrayList<GraficoRespostas> grafico) {
        this.graficos = grafico;
    }

    /**
     * @return the pie
     */
    public PieChartModel getPie() {
        Iterator<GraficoRespostas> it = graficos.iterator();
        while(it.hasNext()){
            GraficoRespostas g = it.next();
            if(g.getPergunta().getId() == pergunta.getId()){
                pie = g.getPieModel();
                break;
            }
        }
        return pie;
    }

    /**
     * @param pie the pie to set
     */
    public void setPie(PieChartModel pie) {
        this.pie = pie;
    }

    /**
     * @return the comodos
     */
    public ArrayList<Comodo> getComodos() {
        return comodos;
    }

    /**
     * @param comodos the comodos to set
     */
    public void setComodos(ArrayList<Comodo> comodos) {
        this.comodos = comodos;
    }

    /**
     * @return the comodo
     */
    public Comodo getComodo() {
        return comodo;
    }

    /**
     * @param comodo the comodo to set
     */
    public void setComodo(Comodo comodo) {
        this.comodo = comodo;
    }

    /**
     * @return the comodoId
     */
    public int getComodoId() {
        return comodoId;
    }

    /**
     * @param comodoId the comodoId to set
     */
    public void setComodoId(int comodoId) {
        this.comodoId = comodoId;
    }

    /**
     * @return the treeComodos
     */
    public TreeMap<String, String> getTreeComodos() {
        return treeComodos;
    }

    /**
     * @param treeComodos the treeComodos to set
     */
    public void setTreeComodos(TreeMap<String, String> treeComodos) {
        this.treeComodos = treeComodos;
    }

    /**
     * @return the cId
     */
    public String getcId() {
        return cId;
    }

    /**
     * @param cId the cId to set
     */
    public void setcId(String cId) {
        this.cId = cId;
    }

    /**
     * @return the cs
     */
    public ArrayList<Comodo> getCs() {
        return cs;
    }

    /**
     * @param cs the cs to set
     */
    public void setCs(ArrayList<Comodo> cs) {
        this.cs = cs;
    }

    /**
     * @return the icones
     */
    public ArrayList<Icone> getIcones() {
        return icones;
    }

    /**
     * @param icones the icones to set
     */
    public void setIcones(ArrayList<Icone> icones) {
        this.icones = icones;
    }

    /**
     * @return the icone
     */
    public Icone getIcone() {
        return icone;
    }

    /**
     * @param icone the icone to set
     */
    public void setIcone(Icone icone) {
        this.icone = icone;
    }

    /**
     * @return the comodosVector
     */
    public Comodo[] getComodosVector() {
        return comodosVector;
    }

    /**
     * @param comodosVector the comodosVector to set
     */
    public void setComodosVector(Comodo[] comodosVector) {
        this.comodosVector = comodosVector;
    }

    /**
     * @return the comodosVectorDaPergunta
     */
    public Comodo[] getComodosVectorDaPergunta() {
        return comodosVectorDaPergunta;
    }

    /**
     * @param comodosVectorDaPergunta the comodosVectorDaPergunta to set
     */
    public void setComodosVectorDaPergunta(Comodo[] comodosVectorDaPergunta) {
        this.comodosVectorDaPergunta = comodosVectorDaPergunta;
    }

    /**
     * @return the comodoIn
     */
    public Comodo getComodoIn() {
        return comodoIn;
    }

    /**
     * @param comodoIn the comodoIn to set
     */
    public void setComodoIn(Comodo comodoIn) {
        this.comodoIn = comodoIn;
    }

    /**
     * @return the comodoOut
     */
    public Comodo getComodoOut() {
        return comodoOut;
    }

    /**
     * @param comodoOut the comodoOut to set
     */
    public void setComodoOut(Comodo comodoOut) {
        this.comodoOut = comodoOut;
    }

    /**
     * @return the editando
     */
    public boolean isEditando() {
        return editando;
    }

    /**
     * @param editando the editando to set
     */
    public void setEditando(boolean editando) {
        this.editando = editando;
    }

    /**
     * @return the atributos
     */
    public ArrayList<Atributo> getAtributos() {
        return atributos;
    }

    /**
     * @param atributos the atributos to set
     */
    public void setAtributos(ArrayList<Atributo> atributos) {
        this.atributos = atributos;
    }

    /**
     * @return the atributoIn
     */
    public Atributo getAtributoIn() {
        return atributoIn;
    }

    /**
     * @param atributoIn the atributoIn to set
     */
    public void setAtributoIn(Atributo atributoIn) {
        this.atributoIn = atributoIn;
    }

    /**
     * @return the atributoOut
     */
    public Atributo getAtributoOut() {
        return atributoOut;
    }

    /**
     * @param atributoOut the atributoOut to set
     */
    public void setAtributoOut(Atributo atributoOut) {
        this.atributoOut = atributoOut;
    }

   /**
    * @return the escCores
    */
   public boolean isEscCores()
   {
      return escCores;
   }

   /**
    * @param escCores the escCores to set
    */
   public void setEscCores(boolean escCores)
   {
      this.escCores = escCores;
   }

    public class GraficoRespostas {

        private Pergunta pergunta;
        private ArrayList<Resposta> respostas;
        private PieChartModel pieModel;
        private Integer quant[];

        public GraficoRespostas(){
            pieModel = new PieChartModel();
            /*pieModel.set("Brand 1", 540);
            pieModel.set("Brand 2", 325);
            pieModel.set("Brand 3", 702);
            pieModel.set("Brand 4", 421);*/
        }

        /**
         * @return the pergunta
         */
        public Pergunta getPergunta() {
            return pergunta;
        }

        /**
         * @param pergunta the pergunta to set
         */
        public void setPergunta(Pergunta pergunta) {
            this.pergunta = pergunta;
        }

        /**
         * @return the pieModel
         */
        public PieChartModel getPieModel() {
            return pieModel;
        }

        /**
         * @param pieModel the pieModel to set
         */
        public void setPieModel(PieChartModel pieModel) {
            this.pieModel = pieModel;
        }

        /**
         * @return the respostas
         */
        public ArrayList<Resposta> getRespostas() {
            return respostas;
        }

        /**
         * @param respostas the respostas to set
         */
        public void setRespostas(ArrayList<Resposta> respostas) {
            this.respostas = respostas;
        }

        /**
         * @return the quant
         */
        public Integer[] getQuant() {
            return quant;
        }

        /**
         * @param quant the quant to set
         */
        public void setQuant(Integer[] quant) {
            this.quant = quant;
        }

    }

    /**
     * @return the pergunta
     */
    public Pergunta getPergunta()
    {
        return pergunta;
    }

    /**
     * @param pergunta the pergunta to set
     */
    public void setPergunta(Pergunta pergunta)
    {
        this.pergunta = pergunta;
    }

    /**
     * @return the categorias
     */
    public ArrayList<Categoria> getCategorias()
    {
        return categorias;
    }

    /**
     * @param categorias the categorias to set
     */
    public void setCategorias(ArrayList<Categoria> categorias)
    {
        this.categorias = categorias;
    }

    /**
     * @return the categoria
     */
    public Categoria getCategoria()
    {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Categoria categoria)
    {
        this.categoria = categoria;
    }

    /**
     * @return the idTabela
     */
    public String getIdTabela()
    {
        return idTabela;
    }

    /**
     * @param idTabela the idTabela to set
     */
    public void setIdTabela(String idTabela)
    {
        this.idTabela = idTabela;
    }

    /**
     * @return the qualificadores
     */
    public List<String> getQualificadores()
    {
        return qualificadores;
    }

    /**
     * @param qualificadores the qualificadores to set
     */
    public void setQualificadores(List<String> qualificadores)
    {
        this.qualificadores = qualificadores;
    }

    /**
     * @return the qualificador
     */
    public Qualificador getQualificador()
    {
        return qualificador;
    }

    /**
     * @param qualificador the qualificador to set
     */
    public void setQualificador(Qualificador qualificador)
    {
        this.qualificador = qualificador;
    }

    /**
     * @return the isNumerico
     */
    public boolean isIsNumerico()
    {
        return isNumerico;
    }

    /**
     * @param isNumerico the isNumerico to set
     */
    public void setIsNumerico(boolean isNumerico)
    {
        this.isNumerico = isNumerico;
    }

    /**
     * @return the opcao
     */
    public String getOpcao()
    {
        return opcao;
    }

    /**
     * @param opcao the opcao to set
     */
    public void setOpcao(String opcao)
    {
        this.opcao = opcao;
    }

    /**
     * @return the opcao2
     */
    public String getOpcao2() {
        return opcao2;
    }

    /**
     * @param opcao2 the opcao2 to set
     */
    public void setOpcao2(String opcao2) {
        this.opcao2 = opcao2;
    }

    /**
     * @return the conceitos
     */
    public List<Conceito> getConceitos() {
        return conceitos;
    }

    /**
     * @param conceitos the conceitos to set
     */
    public void setConceitos(List<Conceito> conceitos) {
        this.conceitos = conceitos;
    }

    /**
     * @return the conceitosCategoria
     */
    public Conceito[] getConceitosCategoria() {
        return conceitosCategoria;
    }

    /**
     * @param conceitosCategoria the conceitosCategoria to set
     */
    public void setConceitosCategoria(Conceito[] conceitosCategoria) {
        this.conceitosCategoria = conceitosCategoria;
    }

    /**
     * @return the dualCategoria
     */
    public DualListModel<Conceito> getDualCategoria() {
        return dualCategoria;
    }

    /**
     * @param dualCategoria the dualCategoria to set
     */
    public void setDualCategoria(DualListModel<Conceito> dualCategoria) {
        this.dualCategoria = dualCategoria;
    }

    /**
     * @return the conceito
     */
    public Conceito getConceito() {
        return conceito;
    }

    /**
     * @param conceito the conceito to set
     */
    public void setConceito(Conceito conceito) {
        this.conceito = conceito;
    }

    /**
     * @return the conceitosSelecionadosCategoria
     */
    public List<Conceito> getConceitosSelecionadosCategoria() {
        return conceitosSelecionadosCategoria;
    }

    /**
     * @param conceitosSelecionadosCategoria the conceitosSelecionadosCategoria to set
     */
    public void setConceitosSelecionadosCategoria(List<Conceito> conceitosSelecionadosCategoria) {
        this.conceitosSelecionadosCategoria = conceitosSelecionadosCategoria;
    }

    /**
     * @return the selecionadosConceitos
     */
    public Conceito[] getSelecionadosConceitos() {
        return selecionadosConceitos;
    }

    /**
     * @param selecionadosConceitos the selecionadosConceitos to set
     */
    public void setSelecionadosConceitos(Conceito[] selecionadosConceitos) {
        this.selecionadosConceitos = selecionadosConceitos;
    }

    /**
     * @return the conceitosPergunta
     */
    public Conceito[] getConceitosPergunta() {
        return conceitosPergunta;
    }

    /**
     * @param conceitosPergunta the conceitosPergunta to set
     */
    public void setConceitosPergunta(Conceito[] conceitosPergunta) {
        this.conceitosPergunta = conceitosPergunta;
    }

    /**
     * @return the conceitosSelecionadosPergunta
     */
    public List<Conceito> getConceitosSelecionadosPergunta() {
        return conceitosSelecionadosPergunta;
    }

    /**
     * @param conceitosSelecionadosPergunta the conceitosSelecionadosPergunta to set
     */
    public void setConceitosSelecionadosPergunta(List<Conceito> conceitosSelecionadosPergunta) {
        this.conceitosSelecionadosPergunta = conceitosSelecionadosPergunta;
    }

    /**
     * @return the conceitosSelecionadosPerg
     */
    public Conceito[] getConceitosSelecionadosPerg() {
        return conceitosSelecionadosPerg;
    }

    /**
     * @param conceitosSelecionadosPerg the conceitosSelecionadosPerg to set
     */
    public void setConceitosSelecionadosPerg(Conceito[] conceitosSelecionadosPerg) {
        this.conceitosSelecionadosPerg = conceitosSelecionadosPerg;
    }
    
    public String getCid() {
        return cId;
    }
    
    public void setCid(String cId) {
        this.cId = cId;
    }

}
