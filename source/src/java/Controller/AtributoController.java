/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.AtributoDao;
import Dao.ConnectionDao;
import Model.Atributo;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Pedro
 */
@ManagedBean (name = "AtributoController")
@ViewScoped
public class AtributoController {
    
    private Connection conn;
    private ConnectionDao connFactory;
    
    private Atributo atributo;
    private ArrayList<Atributo> atributos;
    
    private AtributoDao dao;
    
    public AtributoController(){
        connFactory = new ConnectionDao();
        atributo = new Atributo();
        atributos = null;
        dao = new AtributoDao();
    }
    
    private void abrirConexao() throws SQLException{
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
    }
    
    private void fecharConexao() throws SQLException{
        conn.commit();
        conn.close();
    }
    
    public void carregarTodos() throws SQLException{
        abrirConexao();
        dao.setConn(conn);
        
        atributos = dao.buscarTodos();
        
        fecharConexao();
    }
    
    public void inserir() throws SQLException{
        abrirConexao();
        dao.setConn(conn);
        
        dao.inserir(atributo);
        
        fecharConexao();
    }
    
    public void editar() throws SQLException{
        abrirConexao();
        dao.setConn(conn);
        
        dao.editar(atributo);
        
        fecharConexao();
    }
    
    public void reset(){
        atributo = new Atributo();
    }

    /**
     * @return the atributo
     */
    public Atributo getAtributo() {
        return atributo;
    }

    /**
     * @param atributo the atributo to set
     */
    public void setAtributo(Atributo atributo) {
        this.atributo = atributo;
    }

    /**
     * @return the atributos
     */
    public ArrayList<Atributo> getAtributos() {
        return atributos;
    }

    /**
     * @param atributos the atributos to set
     */
    public void setAtributos(ArrayList<Atributo> atributos) {
        this.atributos = atributos;
    }
    
}
