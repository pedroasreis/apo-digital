package Controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Pedro
 */
@ManagedBean (name = "UpCase")
@ViewScoped
public class UpCase {
    
    private String frase;
    private String resultado;
    
    public UpCase(){
        frase = "";
        resultado = "";
    }
    
    public void aumentar(){
        setResultado(getFrase().toUpperCase());
        frase = "";
    }

    /**
     * @return the frase
     */
    public String getFrase() {
        return frase;
    }

    /**
     * @param frase the frase to set
     */
    public void setFrase(String frase) {
        this.frase = frase;
    }

    /**
     * @return the resultado
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
    
}
