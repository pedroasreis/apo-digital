package Controller;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.UUID;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.mindmap.DefaultMindmapNode;
import org.primefaces.model.mindmap.MindmapNode;

/**
 *
 * @author Pedro
 */
@ManagedBean (name = "MapaController")
public class MapaController implements Serializable {
    
    private MindmapNode root;
    
    private MindmapNode selectedNode;
    
    public MapaController() {
        root = new DefaultMindmapNode("Quarto", "teste", "6e9ebf", false);
        
        MindmapNode ips = new DefaultMindmapNode("Cozinha", "IP Numbers", "6e9ebf", true);
        MindmapNode ns = new DefaultMindmapNode("Sala de Estar", "Namespaces", "6e9ebf", true);
        MindmapNode malware = new DefaultMindmapNode("Copa", "Malicious Software", "6e9ebf", true);
        
        MindmapNode malwareVizinho = new DefaultMindmapNode("Lavabo", "Malicious Software", "6e9ebf", true);
        
        MindmapNode malwareVizinho2 = new DefaultMindmapNode("Lavabo 2", "Malicious Software", "6e9ebf", true);
        
        malware.addNode(malwareVizinho);
        
        malwareVizinho.addNode(malwareVizinho2);
        
        root.addNode(ips);
        root.addNode(ns);
        root.addNode(malware);
        root.addNode(malwareVizinho);
    }

    public MindmapNode getRoot() {
        return root;
    }

    public MindmapNode getSelectedNode() {
        return selectedNode;
    }
    public void setSelectedNode(MindmapNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public void onNodeSelect(SelectEvent event) {
        MindmapNode node = (MindmapNode) event.getObject();
        
        //populate if not already loaded
        /*if(node.getChildren().isEmpty()) {
            Object label = node.getLabel();

            if(label.equals("NS(s)")) {
                for(int i = 0; i < 25; i++) {
                    node.addNode(new DefaultMindmapNode("ns" + i + ".google.com", "Namespace " + i + " of Google", "82c542"));
                }
            }
            else if(label.equals("IPs")) {
                for(int i = 0; i < 18; i++) {
                    node.addNode(new DefaultMindmapNode("1.1.1."  + i, "IP Number: 1.1.1." + i, "fce24f"));
                } 

            }
            else if(label.equals("Malware")) {
                for(int i = 0; i < 18; i++) {
                    String random = UUID.randomUUID().toString();
                    node.addNode(new DefaultMindmapNode("Malware-"  + random, "Malicious Software: " + random, "3399ff", false));
                }
            }
        }*/
        
    }
    
    public void onNodeDblselect(SelectEvent event) {
        this.selectedNode = (MindmapNode) event.getObject();        
    }
    
}
