package Controller;

/**
 * Classe responsável por centralizar todas as singularidades, em questão de nomes,
 * das categorias e técnicas referentes a aplicação de uma APO em ambiente web.
 * Esta classe se faz necessária, visto que a estrutura organizacional das perguntas
 * de uma APO são bem específicas, especificidades essas que o banco de dados não
 * foi projetado para reproduzir.
 * O ideal, entretanto seria a criação de um arquivo de configuração, que deve ser
 * implementado caso exista algum esforço futuro para melhoria do projeto.
 * @author Pedro
 */
public class ConstantesAplicacaoAvaliacao {
    
    /*
    Técnicas
    */
    public static final String QUESTIONARIO = "Questionario";
    
    /*
    Dados Pessoais
    */
    public static final String DADOS_PESSOAIS = "Dados Pessoais";
    
    /*
    Edifício
    */
    public static final String ASPECTOS_GERAIS_EDIFICIO = "Aspectos Gerais";
    public static final String EDIFICIO = "Edificio";
    public static final String USO_COMUM_EDIFICIO = "Uso Comum";
    public static final String CARACTERISTICAS_EDIFICIO = "Caracteristicas";
    
    /*
    Unidade
    */
    public static final String GERAIS_UNIDADE = "Unidade - Geral";
    public static final String ANALISE_USO_UNIDADE = "Analise de Uso";
    public static final String AMBIENTAL_UNIDADE = "Eficiencia Energetica e Sustentabilidade";
    
    /*
    Cômodos
    */
    public static final String SALA_TV_ESTAR = "Sala de TV/Estar";
    public static final String COZINHA = "Cozinha";
    public static final String SALA_JANTAR = "Sala de Jantar";
    public static final String DORMITORIOS = "Dormitorios";
    public static final String BANHEIROS = "Banheiros";
    public static final String AREA_SERVICO = "Area Servico";
    public static final String VARANDA = "Varanda";
    public static final String SALAS = "Salas";
    
}
