/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.ApoDao;
import Dao.AtributoDao;
import Dao.CategoriaDao;
import Dao.ComodoDao;
import Dao.ConceitoDao;
import Dao.ConnectionDao;
import Dao.EstadoDao;
import Dao.MoradorDao;
import Dao.PerguntaDao;
import Dao.QualificadorDao;
import Dao.QuestionarioDao;
import Dao.RespostaDao;
import Model.APO;
import Model.Atributo;
import Model.Categoria;
import Model.Cidade;
import Model.Comodo;
import Model.Conceito;
import Model.Estado;
import Model.Morador;
import Model.Pergunta;
import Model.Qualificador;
import Model.Questionario;
import Model.Resposta;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * @author Classe responsável por oferecer operações necessárias à manipulação
 * de APOs.
 */
@ManagedBean(name = "APOController")
@ViewScoped
public class APOController implements Serializable
{

   private Connection conn;
   private ConnectionDao connFactory;
   private APO apo;
   private List<APO> apos;
   private Questionario questionario;
   private List<Questionario> questionarios;
   private ApoDao apoDao;
   private EstadoDao estadoDao;
   private QuestionarioDao qDao;
   private ArrayList<Estado> estados;
   private CategoriaDao categoriaDao;
   private PerguntaDao perguntaDao;
   private QualificadorDao qualificadorDao;
   private ConceitoDao conceitoDao;
   private ComodoDao comodoDao;
   private AtributoDao atributoDao;
   private Estado estado;
   private int idEstadoEscolhido;
   private int idCidadeEscolhida;

   /*
    Atributos necessários à clonagem de uma APO
    */
   private APO apoClonagem;
   /*
    * Atributos para geração de csv de uma APO
    */
   private APO apoCsv;
   private MoradorDao moradorDao;
   private RespostaDao respostaDao;

   /*
    * Construtor da classe, inicialização dos modelos e objetos do DAO (Data Acess Objects).
    */
   public APOController() throws SQLException
   {
      connFactory = new ConnectionDao();

      apoDao = new ApoDao();
      qDao = new QuestionarioDao();
      estadoDao = new EstadoDao();
      categoriaDao = new CategoriaDao();
      perguntaDao = new PerguntaDao();
      qualificadorDao = new QualificadorDao();
      conceitoDao = new ConceitoDao();
      comodoDao = new ComodoDao();
      atributoDao = new AtributoDao();

      apo = new APO();
      apoClonagem = new APO();
      estados = new ArrayList<Estado>();
      estado = new Estado();

      apoCsv = new APO();
      moradorDao = new MoradorDao();
      respostaDao = new RespostaDao();

      buscarApos();
      buscarEstados();
   }

   private void abrirConexao() throws SQLException
   {
      conn = connFactory.getConnection();
      conn.setAutoCommit(false);
   }

   private void fecharConexao()
   {
      try
      {
         conn.commit();
         conn.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   private void abortarTransacao()
   {
      try
      {
         conn.rollback();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   /*
    Método que prepara uma APO para ser clonada
    */
   public void carregarApoClone()
   {
      try
      {
         abrirConexao();
         apoDao.setConn(conn);
         qDao.setConn(conn);
         categoriaDao.setConn(conn);
         perguntaDao.setConn(conn);
         qualificadorDao.setConn(conn);
         conceitoDao.setConn(conn);
         comodoDao.setConn(conn);
         atributoDao.setConn(conn);

         /*
          Busca técnicas da APO
          */
         apoClonagem.setMetodos(qDao.buscarPorApo(apoClonagem.getId()));

         /*
          Para cada método, buscar suas categorias
          */
         Iterator<Questionario> questionariosIt = apoClonagem.getMetodos().iterator();
         while (questionariosIt.hasNext())
         {
            Questionario q = questionariosIt.next();
            q.setCategorias(categoriaDao.buscarPorQuestionario(q));

            /*
             Para cada categoria, buscar suas perguntas
             */
            Iterator<Categoria> categoriaIt = q.getCategorias().iterator();
            while (categoriaIt.hasNext())
            {
               Categoria c = categoriaIt.next();
               c.setPerguntas(perguntaDao.buscarPorCategoria(c.getId()));

               /*
                Para cada pergunta, buscar seus conceitos, cômodos, atributos e qualificadores
                */
               Iterator<Pergunta> perguntasIterator = c.getPerguntas().iterator();
               while (perguntasIterator.hasNext())
               {
                  Pergunta p = perguntasIterator.next();
                  p.setConceitos(conceitoDao.buscarPorPergunta(p));
                  p.setComodos(comodoDao.buscarPorPergunta(p));
                  p.setAtributos(atributoDao.buscarPorPergunta(p));
                  p.setQualificadores(qualificadorDao.buscarPorPergunta(p));
               }
            }
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }

   public String clonar()
   {
      /*
       Página que a aplicação deve redirecionar o usuário após a clonagem da APO
       */
      String pagina = "";
      try
      {
         abrirConexao();
         apoDao.setConn(conn);
         qDao.setConn(conn);
         categoriaDao.setConn(conn);
         perguntaDao.setConn(conn);
         qualificadorDao.setConn(conn);
         conceitoDao.setConn(conn);
         comodoDao.setConn(conn);
         atributoDao.setConn(conn);

         /*
          Inserir a nova apo no banco de dados
          */
         apoDao.salvar(apoClonagem);

         /*
          Inserção das técnicas
          */
         for (Questionario q : apoClonagem.getMetodos())
         {
            q.setId(qDao.salvar(q));
            /*
             Conexão entre técnicas e APO
             */
            qDao.salvarQuestionarioAvaliacao(q, apoClonagem);

            /*
             Para cada técnica inserção das novas categorias
             */
            for (Categoria c : q.getCategorias())
            {
               categoriaDao.salvar(c, q);

               /*
                Para cada categoria, salvar suas perguntas
                */
               for (Pergunta p : c.getPerguntas())
               {
                  p.setId(perguntaDao.salvar(p, c));

                  /*
                   Para cada pergunta, salvar seus qualificadores
                   */
                  for (Qualificador qf : p.getQualificadores())
                  {
                     qualificadorDao.salvar(qf, p);
                  }

                  /*
                   Associar seus conceitos
                   */
                  for (Conceito con : p.getConceitos())
                  {
                     conceitoDao.salvarConceitoPergunta(con, p);
                  }

                  /*
                   Associar seus atributos
                   */
                  for (Atributo at : p.getAtributos())
                  {
                     atributoDao.associarComodoPergunta(p, at);
                  }

                  /*
                   Associar seus cômodos
                   */
                  for (Comodo cm : p.getComodos())
                  {
                     comodoDao.associarComodoPergunta(p, cm);
                  }
               }
            }
         }

         /*
          Chegou nesse ponto, então tudo ok com a clonagem. Define a página de
          retorno como a página que lista as técnicas de uma APO (no caso a nova APO)
          */
         addMessage("APO clonada com sucesso!");
         pagina = "questionario.xhtml?apo=faces-redirect=true&apo=" + apoClonagem.getId();
      }
      catch (Exception e)
      {
         e.printStackTrace();
         addMessageError("Falha na clonagem da APO");
         abortarTransacao();
      }
      finally
      {
         fecharConexao();
      }
      return pagina;
   }

   /*
    * Método responsável por buscar da base de dados todas as APOs registradas e mantê-las em um
    * array de objetos APO.
    */
   private void buscarApos()
   {
      try
      {
         conn = connFactory.getConnection();
         conn.setAutoCommit(false);
         apoDao.setConn(conn);
         apos = apoDao.buscarTodos();
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
      finally
      {
         try
         {
            conn.commit();
            conn.close();
         }
         catch (SQLException e)
         {
            e.printStackTrace();
         }
      }
   }

   /*
    * Método que busca todos os estados (UF) do Brasil
    */
   private void buscarEstados()
   {
      try
      {
         conn = connFactory.getConnection();
         conn.setAutoCommit(false);
         estadoDao.setConn(conn);
         estados = estadoDao.buscarTodos();
         Iterator<Estado> it = estados.iterator();
         while (it.hasNext())
         {
            Estado e = it.next();
            e.setCidades(estadoDao.buscarCidadesPorEstado(e));
         }

         conn.commit();
         conn.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   public void onEscolhaEstado()
   {
      Estado e = new Estado();
      e.setId(idEstadoEscolhido);
      estado = estados.get(estados.indexOf(e));
   }

   /*
    * Método que verifica qual estado e cidade foram escolhidos pelo usuário e
    * integra tais informações ao objeto APO que será salvo ou editado
    */
   private void analisaLocal()
   {
      apo.setEstado(estado.getNome());
      Cidade c = new Cidade();
      c.setId(idCidadeEscolhida);
      c = estado.getCidades().get(estado.getCidades().indexOf(c));
      apo.setCidade(c.getNome());
      apo.setIdEstado(estado.getId());
      apo.setIdCidade(c.getId());
   }

   /*
    * Método que ajusta as comboboxes de escolha de estado e cidade de uma APO carregada
    */
   public void carregaLocal()
   {
      idEstadoEscolhido = apo.getIdEstado();
      Estado e = new Estado();
      e.setId(idEstadoEscolhido);
      estado = estados.get(estados.indexOf(e));
      idCidadeEscolhida = apo.getIdCidade();
   }

   /*
    * Método que recebe uma string referente a uma mensagem que deseja-se enviar
    * para o cliente no final de sua requisição.
    * Alimenta o jsf com uma nova mensagem que deverá ser mostrada pelo componente
    * p:growl do primefaces
    */
   private void addMessage(String msg1)
   {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg1, ""));
   }

   private void addMessageError(String msg1)
   {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg1, ""));
   }

   /*
    * Método responsável por inserir uma nova APO na base de dados.
    */
   public void inserir()
   {
      try
      {
         conn = connFactory.getConnection();
         conn.setAutoCommit(false);
         apoDao.setConn(conn);
         analisaLocal();
         apoDao.salvar(apo);
         addMessage("APO criada com sucesso!");
      }
      catch (SQLException e)
      {
         e.printStackTrace();
         addMessage("Falha na conexão com o banco de dados");
         rollback(conn);
      }
      finally
      {
         try
         {
            conn.commit();
            conn.close();
         }
         catch (SQLException e)
         {
            e.printStackTrace();
            addMessage("Falha no encerramento da transação");
         }
      }
   }

   private void rollback(Connection connection)
   {
      try
      {
         connection.rollback();
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
   }

   /*
    * Método responsável por editar uma APO qeditarue já esteja na base de dados.
    */
   public void editar() throws SQLException
   {
      conn = connFactory.getConnection();
      conn.setAutoCommit(false);
      apoDao.setConn(conn);
      analisaLocal();
      apoDao.editar(apo);
      conn.commit();
      conn.close();

      apo = new APO();
      addMessage("APO modificada com sucesso!");
   }

   public void remover()
   {
      System.out.println("Removendo apo: " + apo.getId() + " - texto: " + apo.getTexto());
   }

   public void gerenciarApo() throws SQLException
   {
      conn = connFactory.getConnection();
      conn.setAutoCommit(false);

      qDao.setConn(conn);
      apo.setMetodos(qDao.buscarPorApo(apo.getId()));
      questionarios = qDao.buscarTodos();

      Iterator<Questionario> it = questionarios.iterator();
      while (it.hasNext())
      {
         Questionario q = it.next();
         System.out.println("Procedimento metodológico: " + q.getId());
         /*
          * Verificando se o método já faz parte da APO.
          */
         Iterator<Questionario> it2 = apo.getMetodos().iterator();
         while (it2.hasNext())
         {
            Questionario q2 = it2.next();
            if (q.getId() == q2.getId())
            {
               questionarios.remove(q2);
               break;
            }
         }
      }

      conn.commit();
      conn.close();
   }

   public void gerarCsvApo()
   {
      try
      {
         abrirConexao();
         moradorDao.setConn(conn);
         perguntaDao.setConn(conn);
         respostaDao.setConn(conn);
         conceitoDao.setConn(conn);
         atributoDao.setConn(conn);
         comodoDao.setConn(conn);

         /*
          * Buscar todos os moradores da APO
          */
         ArrayList<Morador> moradores = moradorDao.buscarPorApo(apoCsv);

         /*
          * Buscar todas as perguntas do tipo 2, seus conceitos, atributos
          * e cômodos
          */
         ArrayList<Pergunta> perguntas = perguntaDao.buscarPorApoETipo(apoCsv, Pergunta.ESCOLHA_UNICA);
         for (Pergunta p : perguntas)
         {
            p.setConceitos(conceitoDao.buscarPorPergunta(p));
            p.setAtributos(atributoDao.buscarPorPergunta(p));
            p.setComodos(comodoDao.buscarPorPergunta(p));
         }

         /*
          * Para cada pergunta identificar sua configuração e buscar as respostas
          * de cada morador
          */
         for (Morador m : moradores)
         {
            m.setRespostas(new ArrayList<Resposta>());
            for (Pergunta p : perguntas)
            {
               if (p.getConceitos().isEmpty() && p.getAtributos().isEmpty() && p.getComodos().isEmpty())
               {
                  Resposta r = respostaDao.buscaCompleta(m, p, 0, 0, 0);
                  if (r.getId() == 0)
                  {
                     r.setTexto("NULL");
                  }

                  m.getRespostas().add(r);
               }
               else if (p.getConceitos().isEmpty() && !p.getAtributos().isEmpty() && p.getComodos().isEmpty())
               {
                  for (Atributo a : p.getAtributos())
                  {
                     Resposta r = respostaDao.buscaCompleta(m, p, 0, a.getId(), 0);
                     if (r.getId() == 0)
                     {
                        r.setTexto("NULL");
                     }

                     m.getRespostas().add(r);
                  }
               }
               else if (!p.getConceitos().isEmpty() && p.getAtributos().isEmpty() && p.getComodos().isEmpty())
               {
                  for (Conceito c : p.getConceitos())
                  {
                     Resposta r = respostaDao.buscaCompleta(m, p, c.getId(), 0, 0);
                     if (r.getId() == 0)
                     {
                        r.setTexto("NULL");
                     }

                     m.getRespostas().add(r);
                  }
               }
               else if (!p.getConceitos().isEmpty() && p.getAtributos().isEmpty() && !p.getComodos().isEmpty())
               {
                  for (Comodo cm : p.getComodos())
                  {
                     for (Conceito c : p.getConceitos())
                     {
                        Resposta r = respostaDao.buscaCompleta(m, p, c.getId(), 0, cm.getId());
                        if (r.getId() == 0)
                        {
                           r.setTexto("NULL");
                        }

                        m.getRespostas().add(r);
                     }
                  }
               }
               else if (!p.getConceitos().isEmpty() && !p.getAtributos().isEmpty() && p.getComodos().isEmpty())
               {
                  for (Conceito c : p.getConceitos())
                  {
                     for(Atributo a : p.getAtributos())
                     {
                        Resposta r = respostaDao.buscaCompleta(m, p, c.getId(), a.getId(), 0);
                        if (r.getId() == 0)
                        {
                           r.setTexto("NULL");
                        }

                        m.getRespostas().add(r);
                     }
                  }
               }
            }
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   private void gerarArquivoCsv(ArrayList<Morador> moradores){
      
   }

   /*
    * Método que inicializa o objeto do modelo "APO".
    */
   public void resetApo()
   {
      apo = new APO();
   }

   /**
    * @return the apo
    */
   public APO getApo()
   {
      return apo;
   }

   /**
    * @param apo the apo to set
    */
   public void setApo(APO apo)
   {
      this.apo = apo;
   }

   /**
    * @return the apos
    */
   public List<APO> getApos()
   {
      return apos;
   }

   /**
    * @param apos the apos to set
    */
   public void setApos(ArrayList<APO> apos)
   {
      this.apos = apos;
   }

   /**
    * @return the questionario
    */
   public Questionario getQuestionario()
   {
      return questionario;
   }

   /**
    * @param questionario the questionario to set
    */
   public void setQuestionario(Questionario questionario)
   {
      this.questionario = questionario;
   }

   /**
    * @return the questionarios
    */
   public List<Questionario> getQuestionarios()
   {
      return questionarios;
   }

   /**
    * @param questionarios the questionarios to set
    */
   public void setQuestionarios(ArrayList<Questionario> questionarios)
   {
      this.questionarios = questionarios;
   }

   /**
    * @return the estados
    */
   public ArrayList<Estado> getEstados()
   {
      return estados;
   }

   /**
    * @param estados the estados to set
    */
   public void setEstados(ArrayList<Estado> estados)
   {
      this.estados = estados;
   }

   /**
    * @return the idEstadoEscolhido
    */
   public int getIdEstadoEscolhido()
   {
      return idEstadoEscolhido;
   }

   /**
    * @param idEstadoEscolhido the idEstadoEscolhido to set
    */
   public void setIdEstadoEscolhido(int idEstadoEscolhido)
   {
      this.idEstadoEscolhido = idEstadoEscolhido;
   }

   /**
    * @return the estado
    */
   public Estado getEstado()
   {
      return estado;
   }

   /**
    * @param estado the estado to set
    */
   public void setEstado(Estado estado)
   {
      this.estado = estado;
   }

   /**
    * @return the idCidadeEscolhida
    */
   public int getIdCidadeEscolhida()
   {
      return idCidadeEscolhida;
   }

   /**
    * @param idCidadeEscolhida the idCidadeEscolhida to set
    */
   public void setIdCidadeEscolhida(int idCidadeEscolhida)
   {
      this.idCidadeEscolhida = idCidadeEscolhida;
   }

   /**
    * @return the apoClonagem
    */
   public APO getApoClonagem()
   {
      return apoClonagem;
   }

   /**
    * @param apoClonagem the apoClonagem to set
    */
   public void setApoClonagem(APO apoClonagem)
   {
      this.apoClonagem = apoClonagem;
   }

   /**
    * @return the apoCsv
    */
   public APO getApoCsv()
   {
      return apoCsv;
   }

   /**
    * @param apoCsv the apoCsv to set
    */
   public void setApoCsv(APO apoCsv)
   {
      this.apoCsv = apoCsv;
   }
}
