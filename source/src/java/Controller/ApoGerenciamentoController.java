/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.ConnectionDao;
import Dao.QuestionarioDao;
import Model.APO;
import Model.Questionario;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pedro
 */

@ManagedBean(name = "ApoGerenciamentoController")
@ViewScoped
public class ApoGerenciamentoController 
{
    
    private APO apo;
    private Connection conn;
    private QuestionarioDao qDao;
    private ConnectionDao connFactory;
    private ArrayList<Questionario> questionarios;
    private int[] rem;
    
    private Questionario metodoIn;
    private Questionario metodoOut;
    
    public ApoGerenciamentoController() throws SQLException
    {
        SessionController session = recuperarSessao();
        apo = session.getApo();
        qDao = new QuestionarioDao();
        connFactory = new ConnectionDao();
        prepararApo();
    }
    
    private void prepararApo() throws SQLException 
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);

        qDao.setConn(conn);
        apo.setMetodos(qDao.buscarPorApo(apo.getId()));
        setQuestionarios(qDao.buscarPorNaoApo(apo.getId()));
        
        conn.commit();
        conn.close();
    }
    
    public void entrar()
    {
       int i = 0;
       /*
        * Retirando método da lista de disponíveis;
        */
       Iterator<Questionario> it = questionarios.iterator();
       while(it.hasNext())
       {
          Questionario q = it.next();
          if (q.getId() == metodoIn.getId())
          {
              break;
          }
          i++;
       }
       questionarios.remove(i);
       
       apo.getMetodos().add(metodoIn);
    }
    
    public void sair()
    {
        int i = 0;
       /*
        * Retirando método da lista de métodos da APO;
        */
       Iterator<Questionario> it = apo.getMetodos().iterator();
       while(it.hasNext())
       {
          Questionario q = it.next();
          if (q.getId() == metodoOut.getId())
          {
              break;
          }
          i++;
       }
       apo.getMetodos().remove(i);
       
       questionarios.add(metodoOut);
    }
    
    public void salvar() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);

        qDao.setConn(conn);
        
        /*
         * Remover as entradas da tabela 'questionari_avaliacao' com avaliacao_id da APO corrente
         */
        qDao.excluirQuestionarioAvaliacao(apo);
        
        /*
         * Adicionar as novas entradas
         */
        Iterator<Questionario> it = apo.getMetodos().iterator();
        while(it.hasNext())
        {
            Questionario q = it.next();
            qDao.salvarQuestionarioAvaliacao(q, apo);
        }
        
        conn.commit();
        conn.close();
        
        /*
         * Atualizando view
         */
        prepararApo();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Modificações realizadas com sucesso.",
                    null));
        
    }
    
    private SessionController recuperarSessao()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        return (SessionController) session.getAttribute("SessionController");
    }

    /**
     * @return the apo
     */
    public APO getApo() 
    {
        return apo;
    }

    /**
     * @param apo the apo to set
     */
    public void setApo(APO apo) 
    {
        this.apo = apo;
    }

    /**
     * @return the questionarios
     */
    public ArrayList<Questionario> getQuestionarios() 
    {
        return questionarios;
    }

    /**
     * @param questionarios the questionarios to set
     */
    public void setQuestionarios(ArrayList<Questionario> questionarios) 
    {
        this.questionarios = questionarios;
    }

    /**
     * @return the metodoIn
     */
    public Questionario getMetodoIn() 
    {
        return metodoIn;
    }

    /**
     * @param metodoIn the metodoIn to set
     */
    public void setMetodoIn(Questionario metodoIn) 
    {
        this.metodoIn = metodoIn;
    }

    /**
     * @return the metodoOut
     */
    public Questionario getMetodoOut() 
    {
        return metodoOut;
    }

    /**
     * @param metodoOut the metodoOut to set
     */
    public void setMetodoOut(Questionario metodoOut) 
    {
        this.metodoOut = metodoOut;
    }
    
}
