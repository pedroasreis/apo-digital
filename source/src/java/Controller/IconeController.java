/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.IconeDao;
import Dao.ConnectionDao;
import Model.Icone;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Pedro
 */
@ManagedBean(name = "IconeController")
@ViewScoped
public class IconeController {

    private ArrayList<Icone> icones;
    private IconeDao dao;
    private ConnectionDao connectionFactory;
    private Connection conn;

    public IconeController() throws SQLException {
        icones = new ArrayList<Icone>();
        dao = new IconeDao();
        connectionFactory = new ConnectionDao();

        buscarIcones();
    }

    private void buscarIcones() throws SQLException {
        conn = connectionFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);

        icones = dao.buscarTodos();

        conn.commit();
        conn.close();
    }

    public void handleFileUpload(FileUploadEvent event) {
        //FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        System.out.println("Arquivo: " + event.getFile().getFileName());
    }

    /**
     * @return the icones
     */
    public ArrayList<Icone> getIcones() {
        return icones;
    }

    /**
     * @param icones the icones to set
     */
    public void setIcones(ArrayList<Icone> icones) {
        this.icones = icones;
    }
}
