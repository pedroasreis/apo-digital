package Controller;

import DTO.CategoriaDTO;
import Dao.ApoDao;
import Dao.AtributoDao;
import Dao.CategoriaDao;
import Dao.ComodoDao;
import Dao.ConceitoDao;
import Dao.ConnectionDao;
import Dao.MoradorDao;
import Dao.PerguntaDao;
import Dao.QualificadorDao;
import Dao.QuestionarioDao;
import Dao.RespostaDao;
import Model.APO;
import Model.APODataModel;
import Model.Atributo;
import Model.Categoria;
import Model.Comodo;
import Model.Conceito;
import Model.Morador;
import Model.MoradorDataModel;
import Model.Pergunta;
import Model.PerguntaAgrupada;
import Model.Qualificador;
import Model.Questionario;
import Model.Resposta;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

/**
 * @author Pedro Classe responsável por atender as diversas consultas que serão
 * feitas sobre as respostas dadas pelos moradores e pesquisadores.
 */
@ManagedBean(name = "ConsultaController")
@ViewScoped
public class ConsultaController implements Serializable
{

   private ConnectionDao connFactory;
   private Connection conn;
   private RespostaDao respostaDao;
   private QuestionarioDao questionarioDao;
   private CategoriaDao categoriaDao;
   private PerguntaDao perguntaDao;
   private MoradorDao moradorDao;
   private ApoDao apoDao;
   private AtributoDao atributoDao;
   private ConceitoDao conceitoDao;
   private ComodoDao comodoDao;
   private QualificadorDao qualificadorDao;
   private ArrayList<Pergunta> perguntas;
   private List<APO> apos;
   private Morador morador;
   private ArrayList<Morador> moradores;
   private APODataModel apoDataModel;
   private APO apoSelecionada;
   private APO apoOnTheFly;
   private MoradorDataModel moradorDataModel;
   private Morador moradorSelecionado;
   private Morador moradorOnTheFly;
   private StreamedContent moradorCsv;
   private Pergunta pergunta;
   private Pergunta perguntaOnTheFly;
   private int respondenteEscolhido;
   private int idTecnicaEscolhida;
   private int pesquisadorCode = 1;
   private int moradorCode = 2;
   private ArrayList<Questionario> tecnicasPesquisador;
   private ArrayList<Questionario> tecnicasMorador;
   private ArrayList<Questionario> tecnicas;
   private ArrayList<Pergunta> perguntasBackUp;

   /*
    Atributos para os gráficos agrupados
    */
   private ArrayList<Questionario> tecnicasAgrupadas;
   private APO apoAgrupada;
   private int respondenteAgrupado;
   private int tecnicaAgrupadaEscolhida;
   private ArrayList<Pergunta> perguntasAgrupadas;
   private ArrayList<PerguntaAgrupada> conjuntosPerguntas;
   private PerguntaAgrupada conjuntoEscolhido;
   private ArrayList<CategoriaDTO> categorias;

   //private RespostaDataModel respostaDataModel;
   public ConsultaController()
   {
      perguntas = new ArrayList<Pergunta>();
      morador = new Morador();
      moradores = new ArrayList<Morador>();
      apos = new ArrayList<APO>();

      respostaDao = new RespostaDao();
      moradorDao = new MoradorDao();
      apoDao = new ApoDao();
      perguntaDao = new PerguntaDao();
      questionarioDao = new QuestionarioDao();
      categoriaDao = new CategoriaDao();
      connFactory = new ConnectionDao();

      atributoDao = new AtributoDao();
      conceitoDao = new ConceitoDao();
      comodoDao = new ComodoDao();
      qualificadorDao = new QualificadorDao();

      apoSelecionada = new APO();
      apoOnTheFly = new APO();

      moradorSelecionado = new Morador();
      moradorOnTheFly = new Morador();

      pergunta = new Pergunta();

      buscarApos();
      apoDataModel = new APODataModel(apos);

      perguntaOnTheFly = new Pergunta();

      tecnicasPesquisador = new ArrayList<Questionario>();
      tecnicasMorador = new ArrayList<Questionario>();
      buscarQuestionariosPorRespondente();

      tecnicas = new ArrayList<Questionario>();
      perguntasBackUp = new ArrayList<Pergunta>();

      categorias = new ArrayList<CategoriaDTO>();

      /*
       Gráficos agrupados
       */
      tecnicasAgrupadas = new ArrayList<Questionario>();
      apoAgrupada = new APO();
      perguntasAgrupadas = new ArrayList<Pergunta>();
      conjuntosPerguntas = new ArrayList<PerguntaAgrupada>();
      conjuntoEscolhido = new PerguntaAgrupada();
   }

   /*
    * Método que carrega a lista de técnicas de um morador e de um pesquisador
    */
   private void buscarQuestionariosPorRespondente()
   {
      try
      {
         abrirConexao();
         questionarioDao.setConn(conn);
         tecnicasPesquisador = questionarioDao.buscarPorRespondente(Questionario.PESQUISADOR);
         tecnicasMorador = questionarioDao.buscarPorRespondente(Questionario.MORADOR);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }

   public void onApoAgrupadaSelecionada(SelectEvent event)
   {
      apoAgrupada = (APO) event.getObject();
   }

   public void onRespondenteEscolhido()
   {
      try
      {
         abrirConexao();
         questionarioDao.setConn(conn);

         tecnicasAgrupadas = questionarioDao.buscarPorApoRespondente(apoAgrupada, respondenteAgrupado);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }

   /*
    Método que busca todas as perguntas de uma APO e uma técnica especificada
    pelo usuário
    */
   public void onTecnicaAgrupadaEscolhida()
   {
      System.out.println("Gerando conjunto de perguntas da técnica: " + tecnicaAgrupadaEscolhida);
      try
      {
         abrirConexao();
         perguntaDao.setConn(conn);
         qualificadorDao.setConn(conn);
         conceitoDao.setConn(conn);
         comodoDao.setConn(conn);
         atributoDao.setConn(conn);

         Questionario tecnica = new Questionario();
         tecnica.setId(tecnicaAgrupadaEscolhida);
         perguntasAgrupadas = perguntaDao.buscarPorApoTecnicaTipo(apoAgrupada, tecnica, Pergunta.ESCOLHA_UNICA);

         /*
          Para cada pergunta buscar seus qualificadores, conceitos, cômodos e atributos associados.
          O Set de atributos para cada pergunta também será criado nesse laço
          */
         for (Pergunta p : perguntasAgrupadas)
         {
            p.setQualificadores(qualificadorDao.buscarPorPergunta(p));
            for (Qualificador q : p.getQualificadores())
            {
               p.getConjuntoQualificadores().add(q.getTexto());
            }

            p.setConceitos(conceitoDao.buscarPorPergunta(p));
            p.setComodos(comodoDao.buscarPorPergunta(p));
            p.setAtributos(atributoDao.buscarPorPergunta(p));

            /*
             * Buscando qualificadores de conceitos e atributos caso existam
             */
            for (Conceito c : p.getConceitos())
            {
               c.setQualificadores(qualificadorDao.buscarPorAtributoOuConceito(p.getId(), c.getId()));
            }

            for (Atributo a : p.getAtributos())
            {
               a.setQualificadores(qualificadorDao.buscarPorAtributoOuConceito(p.getId(), a.getId()));
            }
         }

         /*
          * Iterando-se na lista de perguntas para verificar as perguntas que serão
          * divididas em outras perguntas. A saber, aquelas perguntas que possuem
          * conceitos e atributos
          */
         ArrayList<Pergunta> novasPerguntas = new ArrayList<Pergunta>();
         Iterator<Pergunta> it = perguntasAgrupadas.iterator();
         while (it.hasNext())
         {
            Pergunta p = it.next();
            /*
             * Pergunta onde as opções de resposta estão associadas aos conceitos
             */
            if (!p.getConceitos().isEmpty() && !p.getConceitos().get(0).getQualificadores().isEmpty())
            {
               /*
                * Percorrer cada conceito criando uma nova pergunta
                */
               for (Conceito c : p.getConceitos())
               {
                  Pergunta novaPergunta = preencherPergunta(p);

                  ArrayList<Conceito> novoConceito = new ArrayList<Conceito>();
                  novoConceito.add(c);
                  novaPergunta.setConceitos(novoConceito);
                  novaPergunta.setAtributos(p.getAtributos());
                  novaPergunta.setQualificadores(c.getQualificadores());

                  novasPerguntas.add(novaPergunta);
               }
               it.remove();
            }
            /*
             * Pergunta onde as opções de resposta estão associadas aos atributos
             */
            else if (!p.getAtributos().isEmpty() && !p.getAtributos().get(0).getQualificadores().isEmpty())
            {
               /*
                * Percorrer cada atributo criando uma nova pergunta
                */
               for (Atributo a : p.getAtributos())
               {
                  Pergunta novaPergunta = preencherPergunta(p);

                  ArrayList<Atributo> novoAtributo = new ArrayList<Atributo>();
                  novoAtributo.add(a);
                  novaPergunta.setAtributos(novoAtributo);
                  novaPergunta.setConceitos(p.getConceitos());
                  novaPergunta.setQualificadores(a.getQualificadores());

                  novasPerguntas.add(novaPergunta);
               }
               it.remove();
            }
         }

         /*
          * Adicionando array de perguntas criado à lista principal
          */
         perguntasAgrupadas.addAll(novasPerguntas);

         /*
          Com a lista de perguntas e suas estruturas carregadas, é hora de chamar
          o método que criará as listas de conjuntos de perguntas com os mesmos
          qualificadores
          */
         gerarConjuntosPerguntasMenor();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         try
         {
            if (!conn.isClosed())
            {
               fecharConexao();
            }
         }
         catch (Exception e)
         {
            e.printStackTrace();
         }
      }
   }

   private Pergunta preencherPergunta(Pergunta p)
   {
      Pergunta novaPergunta = new Pergunta();
      novaPergunta.setId(p.getId());
      novaPergunta.setTexto(p.getTexto());
      novaPergunta.setAlturaGraf(p.getAlturaGraf());
      novaPergunta.setCartesianModel(p.getCartesianModel());
      novaPergunta.setComodos(p.getComodos());
      novaPergunta.setConceitoComodo(p.isConceitoComodo());
      return novaPergunta;
   }

   /*
    Método que gera os conjuntos de perguntas com os mesmos qualificadores
    */
   private void gerarConjuntosPerguntas()
   {
      int count = 1;
      while (!perguntasAgrupadas.isEmpty())
      {
         /*
          Cria o primeiro conjunto de perguntas, acrescentando a primeira pergunta
          da lista 'perguntasAgrupadas' no mesmo e a removendo da lista original
          */
         PerguntaAgrupada pa = new PerguntaAgrupada();
         pa.setLabel("Conjunto #" + count);
         count++;

         Pergunta perguntaAvaliada = perguntasAgrupadas.remove(0); // pergunta base da comparação de conjuntos
         pa.getPerguntas().add(perguntaAvaliada);
         conjuntosPerguntas.add(pa);

         /*
          Itera na lista de perguntas restantes verificando quais perguntas
          possuem o mesmo conjunto de qualificadores que a pergunta comparada
          */
         Iterator<Pergunta> perguntasIterator = perguntasAgrupadas.iterator();
         while (perguntasIterator.hasNext())
         {
            Pergunta p = perguntasIterator.next();

            /*
             Criação de set auxiliares para a comparação
             */
            Set<String> set1 = new HashSet<String>(perguntaAvaliada.getConjuntoQualificadores());
            Set<String> set2 = new HashSet<String>(p.getConjuntoQualificadores());

            set1.removeAll(p.getConjuntoQualificadores());
            set2.removeAll(perguntaAvaliada.getConjuntoQualificadores());

            /*
             Caso o teste abaixo seja verdadeiro, significa que os conjuntos testados são
             idênticos, portanto a pergunta corrente será removida do conjunto e adicionada
             ao atual conjunto de perguntas sendo construído
             */
            if (set1.isEmpty() && set2.isEmpty())
            {
               //System.out.println("CONJUNTOS IGUAIS");
               pa.getPerguntas().add(p);
               perguntasIterator.remove();
            }
         }
      }


      /*
       Percorrendo a lista de conjuntos de perguntas para que cada pergunta possa ser
       'quebrada' em subperguntas com seus respectivos conceitos, cômodos e atributos,
       e ter suas respostas recuperadas da base de dados
       */
      for (PerguntaAgrupada pa : conjuntosPerguntas)
      {
         pa.setPerguntas(buscarRespostasAcumuladas(pa.getPerguntas()));
         pa.setLarguraGrafico(PerguntaAgrupada.ALTURA * pa.getPerguntas().size());

         /*
          Verificando perguntas de cada conjunto
          */
         /*System.out.println("Conjunto: " + pa.getLabel() + " ########################");
          for(Pergunta p : pa.getPerguntas()){
          System.out.println(p.getTexto() + " - " + p.getConceito().getNome() + " - " + p.getComodo().getNome() + " - " + p.getAtributo().getNome());
          }*/
      }
   }

   /*
    Método que gera os conjuntos de perguntas que tenham variações
    */
   private void gerarConjuntosPerguntasMenor() throws SQLException
   {
      int count = 1;
      if (conn == null || conn.isClosed())
      {
         abrirConexao();
      }
      categoriaDao.setConn(conn);
      while (!perguntasAgrupadas.isEmpty())
      {
         /*
          Cria o primeiro conjunto de perguntas, acrescentando a primeira pergunta
          da lista 'perguntasAgrupadas' no mesmo e a removendo da lista original
          */
         PerguntaAgrupada pa = new PerguntaAgrupada();
         count++;

         Pergunta perguntaAvaliada = perguntasAgrupadas.remove(0); // pergunta base da comparação de conjuntos
         perguntaAvaliada.setCategoria(categoriaDao.buscarPorPergunta(perguntaAvaliada));
         
         pa.getPerguntas().add(perguntaAvaliada);
         pa.setLabel(perguntaAvaliada.getTexto());
         pa.setQuantQualificadores(perguntaAvaliada.getQualificadores().size());
         conjuntosPerguntas.add(pa);
      }


      /*
       Percorrendo a lista de conjuntos de perguntas para que cada pergunta possa ser
       'quebrada' em subperguntas com seus respectivos conceitos, cômodos e atributos,
       e ter suas respostas recuperadas da base de dados
       */
      Map<Categoria, ArrayList<PerguntaAgrupada>> mapaCategorias = new HashMap<Categoria, ArrayList<PerguntaAgrupada>>();
      for (PerguntaAgrupada pa : conjuntosPerguntas)
      {
         pa.setPerguntas(buscarRespostasAcumuladas(pa.getPerguntas()));
         pa.setLarguraGrafico(PerguntaAgrupada.ALTURA * pa.getPerguntas().size() + 50);
         //System.out.println("ID analisado: " + pa.getPerguntas().get(0).getCategoria().getId());
         
         Categoria c1 = new Categoria(); c1.setId(1);
         Categoria c2 = new Categoria(); c2.setId(1);
         /*if(c1.equals(c2))
         {
            System.out.println("Categorias iguais...");
         }*/
         
         if(!mapaCategorias.containsKey(pa.getPerguntas().get(0).getCategoria()));
         {
            mapaCategorias.put(pa.getPerguntas().get(0).getCategoria(), new ArrayList<PerguntaAgrupada>());
         }

         /*
          Verificando perguntas de cada conjunto
          */
         /*System.out.println("Conjunto: " + pa.getLabel() + " ########################");
          for(Pergunta p : pa.getPerguntas()){
          System.out.println(p.getTexto() + " - " + p.getConceito().getNome() + " - " + p.getComodo().getNome() + " - " + p.getAtributo().getNome());
          }*/
      }
      
      /*
       * Montando a lista de perguntas para cada Categoria do Map
       */
      for (PerguntaAgrupada pa : conjuntosPerguntas)
      {
         mapaCategorias.get(pa.getPerguntas().get(0).getCategoria()).add(pa);
      }
      
      /*
       * Pegando cada categoria e suas respectivas perguntas
       */
      Iterator it = mapaCategorias.entrySet().iterator();
      while(it.hasNext())
      {
         Map.Entry<Categoria, ArrayList<PerguntaAgrupada>> par = (Map.Entry<Categoria, ArrayList<PerguntaAgrupada>>) it.next();
         CategoriaDTO cDTO = new CategoriaDTO();
         cDTO.setCategoria(par.getKey());
         cDTO.setPerguntas(par.getValue());
         categorias.add(cDTO);
      }
      //System.out.println("Número de categorias: " + categorias.size());
      /*for(CategoriaDTO catDTO : categorias)
      {
         System.out.println("Categoria: " + catDTO.getCategoria().getNome());
      }*/
   }

   /*
    Método que recebe uma lista de perguntas, cria as possíveis combinações dessa pergunta com
    conceitos, cômodos e atributos, e por fim busca as respostas de cada combinação. As combinações
    geradas que possuam alguma resposta serão adicionadas a uma nova lista de perguntas, a qual é o
    retorno do método
    */
   private ArrayList<Pergunta> buscarRespostasAcumuladas(ArrayList<Pergunta> perguntasOriginais)
   {
      ArrayList<Pergunta> perguntasFinais = new ArrayList<Pergunta>();
      int size = 0;
      try
      {
         /*
          Definindo conexão com o banco de dados, mais especificamente com a
          tabela de respostas através do objeto 'respostaDao'
          */
         abrirConexao();
         respostaDao.setConn(conn);

         /*
          Iterando na lista de perguntas
          */
         for (Pergunta p : perguntasOriginais)
         {
            /*
             Grava o tamanho atual da lista de perguntas finais
             */
            size = perguntasFinais.size();

            /*
             Ajustar listas de conceitos, cômodos e atributos. Se uma dessas listas
             for vazia, então inserir um único elemento com id = 0, dessa maneira, quando
             não houver apenas um elemento com id = 0 em uma das listas o laço não irá parar
             devido a uma lista vazia e o id 0 representará a ausência de associação com determinada
             entidade
             */
            if (p.getConceitos().isEmpty())
            {
               p.getConceitos().add(new Conceito());
            }
            if (p.getComodos().isEmpty())
            {
               p.getComodos().add(new Comodo());
            }
            if (p.getAtributos().isEmpty())
            {
               p.getAtributos().add(new Atributo());
            }

            /*
             Para cada conceito, cômodo e atributo, definir uma pergunta com uma
             permutação das 3 entidades e buscar as respostas da combinação resultante
             */
            for (Conceito c : p.getConceitos())
            {
               for (Comodo cm : p.getComodos())
               {
                  for (Atributo a : p.getAtributos())
                  {
                     Pergunta aux = new Pergunta();
                     /*
                      Definindo os atributos necessários da 'nova' pergunta de
                      acordo com a pergunta original
                      */
                     aux.setId(p.getId());
                     aux.setTexto(p.getTexto());
                     aux.setTipo(p.getTipo());
                     aux.setQualificadores(p.getQualificadores());
                     aux.setConceito(c);
                     aux.setComodo(cm);
                     aux.setAtributo(a);
                     aux.setCategoria(p.getCategoria());

                     /*
                      Buscando as respostas da pergunta
                      */
                     aux.setResps(respostaDao.buscaCompleta(aux, apoAgrupada));

                     /*
                      Verifica se a combinação de entidades tem alguma resposta.
                      Caso tenha, então a pergunta será adicionada na lista de
                      perguntas finais.
                      */
                     if (!aux.getResps().isEmpty())
                     {
                        perguntasFinais.add(aux);
                     }
                  }
               }
            }

            /*
             Último passo no processamento de uma pergunta original. Verificar
             se ela gerou novas perguntas, caso não tenha gerado a mesma será adicionada
             à lista final com conceito, cômodo e atributo com id = 0, indicando
             que a pergunta não possui associação com nenhuma dessas entidades
             */
            if (perguntasFinais.size() == size)
            {
               p.setConceito(new Conceito());
               p.setComodo(new Comodo());
               p.setAtributo(new Atributo());
               perguntasFinais.add(p);
            }
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
      return perguntasFinais;
   }

   public void onConjuntoPerguntasEscolhido()
   {
      /*
       Para cada pergunta do conjunto, chamar o método que conta o número de ocorrências
       de cada qualificador
       */
      for (Pergunta p : conjuntoEscolhido.getPerguntas())
      {
         p.setMapaRespostas(contarRespostasAgrupadas(p));
         p.setMapaPercentual(calcularPercentualRespostas(p.getMapaRespostas()));
      }

      /*
       Chamada do método que retorna um objeto preenchido que representa um gráfico
       de barras acumulado
       */
      conjuntoEscolhido.setBarraAgrupada(gerarGraficoAgrupadoPercentual(conjuntoEscolhido.getPerguntas()));
   }

   /*
    Método que recebe uma pergunta e retorna um mapa onde a chave é uma opção de
    resposta e seu valor é o número de ocorrências
    */
   private Map<String, Integer> contarRespostasAgrupadas(Pergunta p)
   {
      Map<String, Integer> mapa = new HashMap<String, Integer>();

      /*
       Primeiramente define as chaves do mapa, que são os textos dos qualificadores
       da pergunta
       */
      for (Qualificador q : p.getQualificadores())
      {
         mapa.put(q.getTexto(), 0);
      }

      /*
       Percorre a lista de respostas da pergunta acessando o contador de cada tipo
       de resposta e aumentando 1 na ocorrência dessa
       */
      Iterator<Resposta> it = p.getResps().iterator();
      while (it.hasNext())
      {
         Resposta r = it.next();
         /*
          Remove qualquer ocorrência de ';' do texto das respostas
          */
         if (r.getTexto() != null || r.getTexto().equals("") || r.getTexto().equals(" "))
         {
            r.setTexto(r.getTexto().replace(";", ""));

            /*
             Teste verificando se há uma ',' no final do texto da resposta
             */
            if (r.getTexto().length() >= 1 && r.getTexto().charAt(r.getTexto().length() - 1) == ',')
            {
               r.setTexto(r.getTexto().replace(",", ""));
            }

            if (mapa.containsKey(r.getTexto()))
            {
               mapa.put(r.getTexto(), mapa.get(r.getTexto()) + 1);
            }
         }
         else
         {
            it.remove();
         }
      }

      return mapa;
   }

   private Map<String, Double> calcularPercentualRespostas(Map<String, Integer> mapa)
   {
      Map<String, Double> mapaPercentual = new HashMap<String, Double>();
      Double total = new Double(0.0);

      /*
       Calculando a quantidade total de respostas dadas
       */
      for (Map.Entry<String, Integer> entrada : mapa.entrySet())
      {
         total += ((double) entrada.getValue());
      }

      /*
       Fazendo o cálculo percentual e criando o mapa com esses valores
       */
      for (Map.Entry<String, Integer> entrada : mapa.entrySet())
      {
         Double res = new Double(0.0);
         res = (((double) entrada.getValue()) * 100.0) / ((double) total);
         //System.out.println("Percentual: " + res);
         mapaPercentual.put(entrada.getKey(), res);
      }

      return mapaPercentual;
   }

   /*
    Método que recebe uma lista de perguntas com os mesmos qualificadores e retorna
    um CartesianChartModel preenchido.
    */
   private CartesianChartModel gerarGraficoAgrupado(ArrayList<Pergunta> perguntas)
   {
      CartesianChartModel grafico = new CartesianChartModel();

      /*
       A lista de qualificadores das perguntas, como todas possuem a mesma lista de opções,
       basta pegar a lista de uma das perguntas
       */
      ArrayList<Qualificador> qualificadores = perguntas.get(0).getQualificadores();

      /*
       Percorrendo a lista de qualificadores e criando um objeto ChartSeries,
       com o objeto criado, a lista de perguntas é percorrida, acessando seu mapa
       que conta as respostas 
       */
      int count = 1;
      for (Qualificador q : qualificadores)
      {
         ChartSeries cs = new ChartSeries(q.getTexto());
         for (Pergunta p : perguntas)
         {
            p.setTextoAgrupado("");
            if (p.getConceito().getId() > 0)
            {
               p.setTextoAgrupado(p.getTextoAgrupado() + " - " + p.getConceito().getNome());
            }
            if (p.getComodo().getId() > 0)
            {
               p.setTextoAgrupado(p.getTextoAgrupado() + " - " + p.getComodo().getNome());
            }
            if (p.getAtributo().getId() > 0)
            {
               p.setTextoAgrupado(p.getTextoAgrupado() + " - " + p.getAtributo().getNome());
            }
            //System.out.println("VALOR NO GRÁFICO: " + p.getMapaRespostas().get(q.getTexto()));
            cs.set(p.getTextoAgrupado(), p.getMapaRespostas().get(q.getTexto()));
            //cs.set(p.getTexto() + " - " + p.getConceito().getNome() + " - " + p.getComodo().getNome() + " - " + p.getAtributo().getNome(), p.getMapaRespostas().get(q.getTexto()));
            count++;
         }

         /*
          adicionando o ChartSeries criado ao CartesianChartModel
          */
         grafico.addSeries(cs);
      }

      return grafico;
   }

   /*
    Método que recebe uma lista de perguntas com os mesmos qualificadores e retorna
    um CartesianChartModel preenchido.
    */
   private CartesianChartModel gerarGraficoAgrupadoPercentual(ArrayList<Pergunta> perguntas)
   {
      CartesianChartModel grafico = new CartesianChartModel();

      /*
       A lista de qualificadores das perguntas, como todas possuem a mesma lista de opções,
       basta pegar a lista de uma das perguntas
       */
      ArrayList<Qualificador> qualificadores = perguntas.get(0).getQualificadores();

      /*
       Percorrendo a lista de qualificadores e criando um objeto ChartSeries,
       com o objeto criado, a lista de perguntas é percorrida, acessando seu mapa
       que conta as respostas 
       */
      int count = 1;
      for (Qualificador q : qualificadores)
      {
         ChartSeries cs = new ChartSeries(q.getTexto());
         for (Pergunta p : perguntas)
         {
            p.setLabel("p #" + count);
            p.setTextoAgrupado("");
            if (p.getConceito().getId() > 0)
            {
               p.setTextoAgrupado(p.getConceito().getNome());
            }
            if (p.getComodo().getId() > 0)
            {
               if (p.getTextoAgrupado().length() > 0)
               {
                  p.setTextoAgrupado(p.getTextoAgrupado() + " - " + p.getComodo().getNome());
               }
               else
               {
                  p.setTextoAgrupado(p.getComodo().getNome());
               }
            }
            if (p.getAtributo().getId() > 0)
            {
               if (p.getTextoAgrupado().length() > 0)
               {
                  //p.setTextoAgrupado(p.getTextoAgrupado() + " - " + p.getAtributo().getNome());
               }
               else
               {
                  p.setTextoAgrupado(p.getAtributo().getNome());
                  
                  /*ArrayList<String> strings = new ArrayList<String>();
                  int round = 1;
                  String resto = p.getAtributo().getNome();
                  while(true)
                  {
                     if(resto.length() > 27)
                     {
                        strings.add(resto.substring(0, 26));
                        resto = resto.substring(26);
                     }
                     else
                     {
                        strings.add(resto);
                        break;
                     }
                  }
                  
                  p.setTextoAgrupado("");
                  String quebra = "";
                  for(String s : strings)
                  {
                     p.setTextoAgrupado(p.getTextoAgrupado() + quebra + s);
                     quebra = "\n";
                  }*/
               }
            }

            p.setTextoAgrupado(p.getTextoAgrupado() + " (" + (p.getResps().size()) + ")");
            //p.setTextoAgrupado(" (" + (p.getResps().size()) + ")");
            cs.set(p.getTextoAgrupado(), p.getMapaPercentual().get(q.getTexto()).doubleValue());
            //cs.setLabel(p.getTexto() + " - " + p.getTextoAgrupado());
            //cs.set(p.getTexto() + " - " + p.getConceito().getNome() + " - " + p.getComodo().getNome() + " - " + p.getAtributo().getNome(), p.getMapaRespostas().get(q.getTexto()));
            count++;
         }

         /*
          adicionando o ChartSeries criado ao CartesianChartModel
          */
         grafico.addSeries(cs);
      }

      return grafico;
   }

   public void atualizarTecnicas()
   {
      if (respondenteEscolhido == pesquisadorCode)
      {
         tecnicas = tecnicasPesquisador;
      }
      else if (respondenteEscolhido == moradorCode)
      {
         tecnicas = tecnicasMorador;
      }
   }

   public void abrirConexao() throws SQLException
   {
      conn = connFactory.getConnection();
      conn.setAutoCommit(false);
   }

   public void fecharConexao()
   {
      try
      {
         conn.commit();
         conn.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   private void buscarApos()
   {
      try
      {
         abrirConexao();
         apoDao.setConn(conn);

         apos = apoDao.buscarTodos();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }

   /*
    Método depreciado que busca todas as APOs, suas perguntas e todas as suas respostas.
    Não há condições para buscar todas essas informações de uma vez.
    */
   private void buscarApos2()
   {
      try
      {
         abrirConexao();
         apoDao.setConn(conn);
         moradorDao.setConn(conn);
         questionarioDao.setConn(conn);
         categoriaDao.setConn(conn);
         perguntaDao.setConn(conn);
         respostaDao.setConn(conn);

         atributoDao.setConn(conn);
         conceitoDao.setConn(conn);
         comodoDao.setConn(conn);

         apos = apoDao.buscarTodos();

         /*
          * Para cada APO busca-se suas técnicas e seu moradores associados
          */
         Iterator<APO> it = apos.iterator();
         while (it.hasNext())
         {
            APO apo = it.next();
            apo.setMoradores(moradorDao.buscarPorApo(apo));
            /*
             * Para cada morador buscar as respostas que o mesmo deu
             */
            Iterator<Morador> it4 = apo.getMoradores().iterator();
            while (it4.hasNext())
            {
               Morador m = it4.next();
               m.setRespostas(respostaDao.buscarPorMorador(m));
               /*
                * Tratando todas as respostas: buscando nome de conceitos, atributos e
                * cômodos
                */
               tratarRespostas(m.getRespostas());
            }

            apo.setMetodos(questionarioDao.buscarPorApo(apo.getId()));
            
            /*
             * Para cada método, buscar categorias associadas
             */
            Iterator<Questionario> it2 = apo.getMetodos().iterator();
            while (it2.hasNext())
            {
               Questionario q = it2.next();
               q.setCategorias(categoriaDao.buscarPorQuestionario(q));
               /*
                * Para cada categoria, busca-se suas perguntas
                */
               Iterator<Categoria> it3 = q.getCategorias().iterator();
               while (it3.hasNext())
               {
                  Categoria c = it3.next();
                  c.setPerguntas(perguntaDao.buscarPorCategoria(c.getId()));
               }
            }
         }

         fecharConexao();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }

   /*
    * Método que seleciona as perguntas de acordo com os filtros definidos pelo
    * usuário
    */
   public void atualizarPerguntas()
   {
      apoOnTheFly.setPerguntas(new ArrayList<Pergunta>());
      Iterator<Pergunta> it = perguntasBackUp.iterator();
      while (it.hasNext())
      {
         Pergunta p = it.next();
         if (p.getCategoria().getQuestionario().getRespondente() == respondenteEscolhido
                 && p.getCategoria().getQuestionario().getId() == idTecnicaEscolhida)
         {
            apoOnTheFly.getPerguntas().add(p);
         }
      }
   }

   /*
    Método executado quando uma APO é selecionada na tabela de gráficos de pizza
    */
   public void onApoPieSelect(SelectEvent event)
   {
      apoOnTheFly = (APO) event.getObject();
      buscarPerguntasAPO();
   }

   /*
    * Método que busca todas as perguntas de uma APO e todas suas
    * respostas
    */
   private void buscarPerguntasAPO()
   {
      try
      {
         abrirConexao();
         perguntaDao.setConn(conn);
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);
         conceitoDao.setConn(conn);
         comodoDao.setConn(conn);
         atributoDao.setConn(conn);
         categoriaDao.setConn(conn);
         questionarioDao.setConn(conn);

         //apoOnTheFly.setPerguntas(perguntaDao.buscarPorApoETipo(apoOnTheFly, Pergunta.ESCOLHA_UNICA));
         apoOnTheFly.setPerguntas(perguntaDao.buscarPorApoQuantitativas(apoOnTheFly));
         //ArrayList<Pergunta> ps = new ArrayList<Pergunta>();
            /*
          * Buscando qualificadores e respostas de cada pergunta do tipo 3.
          * Também buscando atributos, conceitos e cômodos de cada pergunta
          */
         Iterator<Pergunta> it = apoOnTheFly.getPerguntas().iterator();
         //System.out.println("Número de perguntas buscadas: " + apoOnTheFly.getPerguntas().size());
         while (it.hasNext())
         {
            Pergunta p = it.next();
            p.setCategoria(categoriaDao.buscarPorPergunta(p));
            p.getCategoria().setQuestionario(questionarioDao.buscarPorCategoria(p.getCategoria()));

            /*
             * Tratando respondente
             */
            if (p.getCategoria().getQuestionario().getRespondente() == Questionario.MORADOR)
            {
               p.setRespondente("Morador");
            }
            else if (p.getCategoria().getQuestionario().getRespondente() == Questionario.PESQUISADOR)
            {
               p.setRespondente("Pesquisador");
            }
            ArrayList<Pergunta> ps = new ArrayList<Pergunta>();
            if (p.getTipo() == 3 || p.getTipo() == 2)
            {
               /*
                * Array temporário de perguntas, que posteriormente será incluido no array principal,
                * excluindo a pergunta original
                */
               p.setQualificadores(qualificadorDao.buscarPorPergunta(p));
               //p.setResps(respostaDao.buscarPorPerguntaApo(p, apoOnTheFly));
               p.setConceitos(conceitoDao.buscarPorPergunta(p));
               p.setComodos(comodoDao.buscarPorPergunta(p));
               p.setAtributos(atributoDao.buscarPorPergunta(p));

               if (p.getConceitos().isEmpty())
               {
                  p.getConceitos().add(new Conceito());
               }
               if (p.getComodos().isEmpty())
               {
                  p.getComodos().add(new Comodo());
               }
               if (p.getAtributos().isEmpty())
               {
                  p.getAtributos().add(new Atributo());
               }

               /*
                * Todos as estruturas de dados prontas... criar as perguntas a partir
                * dos seus conceitos, cômodos e atributos
                */
               Iterator<Conceito> it2 = p.getConceitos().iterator();
               while (it2.hasNext())
               {
                  Conceito c = it2.next();
                  Iterator<Comodo> it3 = p.getComodos().iterator();
                  while (it3.hasNext())
                  {
                     Comodo cd = it3.next();
                     Iterator<Atributo> it4 = p.getAtributos().iterator();
                     while (it4.hasNext())
                     {
                        Atributo a = it4.next();
                        Pergunta aux = new Pergunta();
                        aux.setId(p.getId());
                        aux.setTexto(p.getTexto());
                        aux.setQualificadores(p.getQualificadores());
                        aux.setConceito(c);
                        aux.setComodo(cd);
                        aux.setAtributo(a);
                        aux.setTipo(p.getTipo());

                        /*
                         * Buscando todas as respostas dessa pergunta referentes à
                         * APO escolhida, o conceito, cômodo e atributo atual
                         */
                        aux.setResps(respostaDao.buscaCompleta(aux, apoOnTheFly));
                        /*
                         Se a pergunta for do tipo 2 (múltipla escolha), então o sistema
                         deverá quebrar cada resposta em todas as suas escolhas e para cada
                         item, acrescenta-lo no array de respostas
                         */
                        if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA)
                        {
                           /*
                            Cria novo array de respostas
                            */
                           ArrayList<Resposta> novasRespostas = new ArrayList<Resposta>();
                           Iterator<Resposta> respostasMultiplas = aux.getResps().iterator();
                           while (respostasMultiplas.hasNext())
                           {
                              Resposta r = respostasMultiplas.next();
                              StringTokenizer stk = new StringTokenizer(r.getTexto(), ";");
                              while (stk.hasMoreElements())
                              {
                                 String s = (String) stk.nextElement();
                                 Resposta rAux = new Resposta();
                                 rAux.setTexto(s);
                                 novasRespostas.add(rAux);
                              }
                           }
                           aux.setResps(novasRespostas);
                        }

                        if (!aux.getResps().isEmpty())
                        {
                           if (p.getTipo() != Pergunta.MULTIPLA_ESCOLHA)
                           {
                              prepararGraficos(aux);
                           }
                           else
                           {
                              prepararGraficosMultiplaEscolha(aux);
                           }
                        }

                        ps.add(aux);

                     }
                  }
               }

               /*
                * Abaixo, realização de busca de respostas por pergunta, conceito,
                * cômodo e atributo
                */
               /*System.out.println("Pergunta: " + p.getTexto());
                System.out.println("Número de respostas encontradas: " + p.getResps().size());
                System.out.println("");*/
               /*
                * Organizando as estruturas de dados internas de cada pergunta
                * para a exibição dos gráficos
                */
               /*if(p.getResps().size() > 0){
                prepararGraficos(p);
                }*/
               p.setPerguntasRelacionadas(ps);
            }
         }
         perguntasBackUp = apoOnTheFly.getPerguntas();
         apoOnTheFly.setPerguntas(new ArrayList<Pergunta>());
         /*
          * Atualizando nova lista de perguntas
          */
         //apoOnTheFly.setPerguntas(ps);

         /*
          * Removendo perguntas sem nenhuma resposta
          */
         ArrayList<Integer> indices = new ArrayList<Integer>();
         for (int i = 0; i < apoOnTheFly.getPerguntas().size(); i++)
         {
            if (apoOnTheFly.getPerguntas().get(i).getResps().size() == 0)
            {
               indices.add(i);
            }
         }

         Iterator<Integer> it3 = indices.iterator();
         while (it3.hasNext())
         {
            apoOnTheFly.getPerguntas().remove(it3.next());
         }
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }

   /*
    * Método que faz a contagem de cada tipo de resposta diferente.
    * Utilizado apenas em perguntas do tipo 3 (escolha única)
    */
   private void prepararGraficos(Pergunta p)
   {
      p.setPieModel(new PieChartModel());
      p.setCategoryModel(new CartesianChartModel());
      ArrayList<ChartSeries> charts = new ArrayList<ChartSeries>();
      int count = 0;

      HashMap<String, Integer> mapa = new HashMap<String, Integer>();
      Iterator<Qualificador> it = p.getQualificadores().iterator();
      //System.out.println("Tipo da pergunta: " + p.getTipo());
      while (it.hasNext())
      {
         Qualificador q = it.next();
         int contador = 0;
         Iterator<Resposta> it2 = p.getResps().iterator();
         while (it2.hasNext())
         {
            Resposta r = it2.next();
            if (p.getId() == 106 || p.getId() == 107)
            {
               r.setTexto(r.getTexto().toUpperCase());
            }
            String token = "";
            if (r.getTexto() == null)
            {
               r.setTexto("");
            }
            if (!r.getTexto().equals(""))
            {
               String delimitador = ",";
               if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA)
               {
                  delimitador = ";";
               }
               StringTokenizer stk = new StringTokenizer(r.getTexto(), delimitador);
               token = (String) stk.nextElement();
            }
            //System.out.println("Texto da resposta: " + token);
            if (token.equals(q.getTexto()))
            {
               contador++;
            }
         }
         //System.out.println("Quantidade de respostas " + q.getTexto() + ": " + contador);
         //System.out.println(q.getTexto() + " -> " + contador);
         if (p.getId() == 136)
         {
            //System.out.println(q.getTexto() + " %%%%%%%%%%%%%% " + contador);
         }
         //System.out.println("");
         p.getPieModel().set(q.getTexto(), contador);

         /*
          * Preparação do gráfico de barra
          */
         charts.add(new ChartSeries(q.getTexto()));
         charts.get(count).set("Respostas", contador);
         p.getCategoryModel().addSeries(charts.get(count));
         count++;
      }
   }

   /*
    Método que prepara o gráfico de barras das perguntas de múltipla escolha  
    */
   private void prepararGraficosMultiplaEscolha(Pergunta p)
   {
      /*
       mapa que armazenará a quantidade de ocorrências de cada resposta.
       Com isso o array de respostas será percorrido apenas uma única vez.
       */
      p.setCartesianModel(new CartesianChartModel());
      HashMap<String, Integer> contador = new HashMap<String, Integer>();
      for (Qualificador q : p.getQualificadores())
      {
         contador.put(q.getTexto(), 0);
      }

      /*
       Contando a ocorrência de cada tipo de resposta
       */
      for (Resposta r : p.getResps())
      {
         Integer i = 0;
         if (contador.containsKey(r.getTexto()))
         {
            contador.put(r.getTexto(), contador.get(r.getTexto()) + 1);
         }
      }

      int total = 1;

      /*
       Buscando o total de moradores que responderam a APO
       */
      try
      {
         abrirConexao();
         moradorDao.setConn(conn);
         total = moradorDao.quantidadeMoradoresApo(apoSelecionada);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }

      /*
       Criando um único chartSeries indicando a quantidade de moradores que marcaram uma
       determinada opção
       */
      ChartSeries quantMoradores = new ChartSeries("Percentual de Moradores");
      int count = 1;
      for (Qualificador q : p.getQualificadores())
      {
         quantMoradores.set(q.getTexto(), ((double) ((double) (contador.get(q.getTexto()) * 100.0) / (double) total)));
         count++;
      }
      p.getCartesianModel().addSeries(quantMoradores);
      p.setAlturaGraf(Pergunta.ALTURA_GRAFICO * p.getQualificadores().size());
      //p.getCartesianModel().addSeries(c2);
   }

   /*
    * Método que trata as respostas dadas por um morador
    */
   private void tratarRespostas(ArrayList<Resposta> respostas) throws Exception
   {
      Iterator<Resposta> it = respostas.iterator();
      while (it.hasNext())
      {
         Resposta r = it.next();
         r.setConceito(conceitoDao.buscarPorId(r.getConceito().getId()));
         r.setAtributo(atributoDao.buscarPorId(r.getAtributo().getId()));

         r.setComodo(comodoDao.buscarPorId(r.getComodo().getId()));
         if (r.getComodoInstancia().getId() > 0)
         {
            r.setComodo(comodoDao.buscarUnidadePorInstancia(r.getComodoInstancia()));
         }

         r.setPergunta(perguntaDao.buscarPorId(r.getPergunta().getId()));
         if (r.getPergunta() != null && r.getPergunta().getId() > 0)
         {
            r.getPergunta().setCategoria(categoriaDao.buscarPorPergunta(r.getPergunta()));
         }

         if (r.getTexto() != null)
         {
            r.setTexto(r.getTexto().replaceAll(",", " "));
         }
      }
   }

   /*
    * Método que exporta arquivo csv com as respostas tratadas de um morador
    */
   public void gerarCsvMorador()
   {
      String nomeArquivo = "";
      try
      {
         /*
          * Criando/Abrindo arquivo e limpando seus dados
          */
         nomeArquivo = moradorOnTheFly.getNumApartamento() + "_respostas.csv";
         FileOutputStream fout = new FileOutputStream(nomeArquivo);
         fout.write((new String()).getBytes());
         fout.close();

         /*
          * Criando as colunas do arquivo
          */
         FileWriter fw = new FileWriter(nomeArquivo, true);
         fw.write("Categoria,Pergunta,Texto,Conceito,Atributo,Cômodo\n");

         /*
          * Criando as linhas do arquivo com cada resposta
          */
         Iterator<Resposta> it = moradorOnTheFly.getRespostas().iterator();
         while (it.hasNext())
         {
            Resposta r = it.next();
            fw.write(r.getPergunta().getCategoria().getNome() + ",");
            fw.write(r.getPergunta().getTexto() + ",");
            fw.write(r.getTexto() + ",");
            fw.write(r.getConceito().getNome() + ",");
            fw.write(r.getAtributo().getNome() + ",");
            fw.write(r.getComodo().getNome() + "\n");
         }

         /*
          * Fechando arquivo...
          */
         fw.close();
         addMessage("Arquivo csv " + nomeArquivo + " gerado com sucesso!");
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      //return nomeArquivo;
   }

   private void addMessage(String msg)
   {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null, new FacesMessage("", msg));
   }

   public void onSelectApo(SelectEvent event)
   {
      apoOnTheFly = (APO) (event.getObject());
      moradorDataModel = new MoradorDataModel(apoOnTheFly.getMoradores());
      addMessage("APO " + apoOnTheFly.getNome() + " selecionada.");
   }

   public void onSelectMorador(SelectEvent event)
   {
      moradorOnTheFly = (Morador) event.getObject();
   }

   private void buscarMoradores()
   {
      try
      {
         abrirConexao();
         moradorDao.setConn(conn);
         perguntaDao.setConn(conn);
         respostaDao.setConn(conn);
         apoDao.setConn(conn);

         moradores = moradorDao.buscarTodos();

         /*
          * Buscando apo a que cada morador faz parte e lista de perguntas do tipo 1
          * que o mesmo tenha respondido
          */
         Iterator<Morador> it = moradores.iterator();
         while (it.hasNext())
         {
            Morador m = it.next();
            m.setApo(apoDao.buscarPorMorador(m));
            m.setPerguntas(perguntaDao.buscarPorTipoMorador(Pergunta.TEXTO, m));

            Iterator<Pergunta> it2 = m.getPerguntas().iterator();
            while (it2.hasNext())
            {
               Pergunta p = it2.next();
               p.setResp(respostaDao.buscarPorPerguntaMorador(p, m).get(0));
            }
         }

         fecharConexao();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   /**
    * @return the perguntas
    */
   public ArrayList<Pergunta> getPerguntas()
   {
      return perguntas;
   }

   /**
    * @param perguntas the perguntas to set
    */
   public void setPerguntas(ArrayList<Pergunta> perguntas)
   {
      this.perguntas = perguntas;
   }

   /**
    * @return the morador
    */
   public Morador getMorador()
   {
      return morador;
   }

   /**
    * @param morador the morador to set
    */
   public void setMorador(Morador morador)
   {
      this.morador = morador;
   }

   /**
    * @return the moradores
    */
   public ArrayList<Morador> getMoradores()
   {
      return moradores;
   }

   /**
    * @param moradores the moradores to set
    */
   public void setMoradores(ArrayList<Morador> moradores)
   {
      this.moradores = moradores;
   }

   /**
    * @return the apos
    */
   public List<APO> getApos()
   {
      return apos;
   }

   /**
    * @param apos the apos to set
    */
   public void setApos(List<APO> apos)
   {
      this.apos = apos;
   }

   /**
    * @return the apoDataModel
    */
   public APODataModel getApoDataModel()
   {
      return apoDataModel;
   }

   /**
    * @param apoDataModel the apoDataModel to set
    */
   public void setApoDataModel(APODataModel apoDataModel)
   {
      this.apoDataModel = apoDataModel;
   }

   /**
    * @return the apoSelecionada
    */
   public APO getApoSelecionada()
   {
      return apoSelecionada;
   }

   /**
    * @param apoSelecionada the apoSelecionada to set
    */
   public void setApoSelecionada(APO apoSelecionada)
   {
      this.apoSelecionada = apoSelecionada;
   }

   /**
    * @return the apoOnTheFly
    */
   public APO getApoOnTheFly()
   {
      return apoOnTheFly;
   }

   /**
    * @param apoOnTheFly the apoOnTheFly to set
    */
   public void setApoOnTheFly(APO apoOnTheFly)
   {
      this.apoOnTheFly = apoOnTheFly;
   }

   /**
    * @return the moradorDataModel
    */
   public MoradorDataModel getMoradorDataModel()
   {
      return moradorDataModel;
   }

   /**
    * @param moradorDataModel the moradorDataModel to set
    */
   public void setMoradorDataModel(MoradorDataModel moradorDataModel)
   {
      this.moradorDataModel = moradorDataModel;
   }

   /**
    * @return the moradorSelecionado
    */
   public Morador getMoradorSelecionado()
   {
      return moradorSelecionado;
   }

   /**
    * @param moradorSelecionado the moradorSelecionado to set
    */
   public void setMoradorSelecionado(Morador moradorSelecionado)
   {
      this.moradorSelecionado = moradorSelecionado;
   }

   /**
    * @return the moradorOnTheFly
    */
   public Morador getMoradorOnTheFly()
   {
      return moradorOnTheFly;
   }

   /**
    * @param moradorOnTheFly the moradorOnTheFly to set
    */
   public void setMoradorOnTheFly(Morador moradorOnTheFly)
   {
      this.moradorOnTheFly = moradorOnTheFly;
   }

   /**
    * @return the moradorCsv
    */
   public StreamedContent getMoradorCsv()
   {
      /*String nomeArquivo = gerarCsvMorador();
       InputStream stream = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getResourceAsStream(nomeArquivo);
       moradorCsv = new DefaultStreamedContent(stream);*/
      return moradorCsv;
   }

   /**
    * @param moradorCsv the moradorCsv to set
    */
   public void setMoradorCsv(StreamedContent moradorCsv)
   {
      this.moradorCsv = moradorCsv;
   }

   /**
    * @return the pergunta
    */
   public Pergunta getPergunta()
   {
      return pergunta;
   }

   /**
    * @param pergunta the pergunta to set
    */
   public void setPergunta(Pergunta pergunta)
   {
      this.pergunta = pergunta;
   }

   /**
    * @return the perguntaOnTheFly
    */
   public Pergunta getPerguntaOnTheFly()
   {
      return perguntaOnTheFly;
   }

   /**
    * @param perguntaOnTheFly the perguntaOnTheFly to set
    */
   public void setPerguntaOnTheFly(Pergunta perguntaOnTheFly)
   {
      this.perguntaOnTheFly = perguntaOnTheFly;
   }

   /**
    * @return the respondenteEscolhido
    */
   public int getRespondenteEscolhido()
   {
      return respondenteEscolhido;
   }

   /**
    * @param respondenteEscolhido the respondenteEscolhido to set
    */
   public void setRespondenteEscolhido(int respondenteEscolhido)
   {
      this.respondenteEscolhido = respondenteEscolhido;
   }

   /**
    * @return the idTecnicaEscolhida
    */
   public int getIdTecnicaEscolhida()
   {
      return idTecnicaEscolhida;
   }

   /**
    * @param idTecnicaEscolhida the idTecnicaEscolhida to set
    */
   public void setIdTecnicaEscolhida(int idTecnicaEscolhida)
   {
      this.idTecnicaEscolhida = idTecnicaEscolhida;
   }

   /**
    * @return the tecnicasPesquisador
    */
   public ArrayList<Questionario> getTecnicasPesquisador()
   {
      return tecnicasPesquisador;
   }

   /**
    * @param tecnicasPesquisador the tecnicasPesquisador to set
    */
   public void setTecnicasPesquisador(ArrayList<Questionario> tecnicasPesquisador)
   {
      this.tecnicasPesquisador = tecnicasPesquisador;
   }

   /**
    * @return the tecnicasMorador
    */
   public ArrayList<Questionario> getTecnicasMorador()
   {
      return tecnicasMorador;
   }

   /**
    * @param tecnicasMorador the tecnicasMorador to set
    */
   public void setTecnicasMorador(ArrayList<Questionario> tecnicasMorador)
   {
      this.tecnicasMorador = tecnicasMorador;
   }

   /**
    * @return the tecnicas
    */
   public ArrayList<Questionario> getTecnicas()
   {
      return tecnicas;
   }

   /**
    * @param tecnicas the tecnicas to set
    */
   public void setTecnicas(ArrayList<Questionario> tecnicas)
   {
      this.tecnicas = tecnicas;
   }

   /**
    * @return the perguntasBackUp
    */
   public ArrayList<Pergunta> getPerguntasBackUp()
   {
      return perguntasBackUp;
   }

   /**
    * @param perguntasBackUp the perguntasBackUp to set
    */
   public void setPerguntasBackUp(ArrayList<Pergunta> perguntasBackUp)
   {
      this.perguntasBackUp = perguntasBackUp;
   }

   /**
    * @return the tecnicasAgrupadas
    */
   public ArrayList<Questionario> getTecnicasAgrupadas()
   {
      return tecnicasAgrupadas;
   }

   /**
    * @param tecnicasAgrupadas the tecnicasAgrupadas to set
    */
   public void setTecnicasAgrupadas(ArrayList<Questionario> tecnicasAgrupadas)
   {
      this.tecnicasAgrupadas = tecnicasAgrupadas;
   }

   /**
    * @return the apoAgrupada
    */
   public APO getApoAgrupada()
   {
      return apoAgrupada;
   }

   /**
    * @param apoAgrupada the apoAgrupada to set
    */
   public void setApoAgrupada(APO apoAgrupada)
   {
      this.apoAgrupada = apoAgrupada;
   }

   /**
    * @return the respondenteAgrupado
    */
   public int getRespondenteAgrupado()
   {
      return respondenteAgrupado;
   }

   /**
    * @param respondenteAgrupado the respondenteAgrupado to set
    */
   public void setRespondenteAgrupado(int respondenteAgrupado)
   {
      this.respondenteAgrupado = respondenteAgrupado;
   }

   /**
    * @return the tecnicaAgrupadaEscolhida
    */
   public int getTecnicaAgrupadaEscolhida()
   {
      return tecnicaAgrupadaEscolhida;
   }

   /**
    * @param tecnicaAgrupadaEscolhida the tecnicaAgrupadaEscolhida to set
    */
   public void setTecnicaAgrupadaEscolhida(int tecnicaAgrupadaEscolhida)
   {
      this.tecnicaAgrupadaEscolhida = tecnicaAgrupadaEscolhida;
   }

   /**
    * @return the perguntasAgrupadas
    */
   public ArrayList<Pergunta> getPerguntasAgrupadas()
   {
      return perguntasAgrupadas;
   }

   /**
    * @param perguntasAgrupadas the perguntasAgrupadas to set
    */
   public void setPerguntasAgrupadas(ArrayList<Pergunta> perguntasAgrupadas)
   {
      this.perguntasAgrupadas = perguntasAgrupadas;
   }

   /**
    * @return the conjuntosPerguntas
    */
   public ArrayList<PerguntaAgrupada> getConjuntosPerguntas()
   {
      return conjuntosPerguntas;
   }

   /**
    * @param conjuntosPerguntas the conjuntosPerguntas to set
    */
   public void setConjuntosPerguntas(ArrayList<PerguntaAgrupada> conjuntosPerguntas)
   {
      this.conjuntosPerguntas = conjuntosPerguntas;
   }

   /**
    * @return the conjuntoEscolhido
    */
   public PerguntaAgrupada getConjuntoEscolhido()
   {
      return conjuntoEscolhido;
   }

   /**
    * @param conjuntoEscolhido the conjuntoEscolhido to set
    */
   public void setConjuntoEscolhido(PerguntaAgrupada conjuntoEscolhido)
   {
      this.conjuntoEscolhido = conjuntoEscolhido;
   }

   /**
    * @return the categorias
    */
   public ArrayList<CategoriaDTO> getCategorias()
   {
      return categorias;
   }

   /**
    * @param categorias the categorias to set
    */
   public void setCategorias(ArrayList<CategoriaDTO> categorias)
   {
      this.categorias = categorias;
   }
}
