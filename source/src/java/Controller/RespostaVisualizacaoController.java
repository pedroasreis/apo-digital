/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.ConnectionDao;
import Dao.RespostaDao;
import Model.APO;
import Model.Pergunta;
import Model.Resposta;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pedro
 */
@ManagedBean (name = "RespostaVisualizacaoController")
@ViewScoped
public class RespostaVisualizacaoController {
    
    private Pergunta pergunta;
    private APO apo;
    private ArrayList<Resposta> respostas;
    
    private Connection conn;
    private ConnectionDao connFactory;
    private RespostaDao rDao;
    
    public RespostaVisualizacaoController() throws SQLException
    {
        SessionController sc = recuperarSessao();
        pergunta = sc.getPergunta();
        apo = sc.getApo();
        
        connFactory = new ConnectionDao();
        rDao = new RespostaDao();
        
        buscarRespostas();
    }
    
    private void buscarRespostas() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);

        rDao.setConn(conn);
        respostas = rDao.buscarPorPerguntaApo(pergunta, apo);
        
        conn.commit();
        conn.close();
    }
    
    private SessionController recuperarSessao() 
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        return (SessionController) session.getAttribute("SessionController");
    }

    /**
     * @return the pergunta
     */
    public Pergunta getPergunta() {
        return pergunta;
    }

    /**
     * @param pergunta the pergunta to set
     */
    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    /**
     * @return the apo
     */
    public APO getApo() {
        return apo;
    }

    /**
     * @param apo the apo to set
     */
    public void setApo(APO apo) {
        this.apo = apo;
    }

    /**
     * @return the respostas
     */
    public ArrayList<Resposta> getRespostas() {
        return respostas;
    }

    /**
     * @param respostas the respostas to set
     */
    public void setRespostas(ArrayList<Resposta> respostas) {
        this.respostas = respostas;
    }
    
}
