/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.CategoriaDao;
import Dao.ConnectionDao;
import Dao.PerguntaDao;
import Dao.QuestionarioDao;
import Model.APO;
import Model.Categoria;
import Model.Pergunta;
import Model.Questionario;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pedro
 */
@ManagedBean(name = "RespostaController")
@ViewScoped
public class RespostaController {

    private APO apo;
    private ArrayList<Pergunta> perguntas;
    private Pergunta pergunta;
    
    private Connection conn;
    private ConnectionDao connFactory;
    private QuestionarioDao qDao;
    private CategoriaDao cDao;
    private PerguntaDao pDao;

    public RespostaController() throws SQLException {
        apo = recuperarSessao().getApo();

        connFactory = new ConnectionDao();
        qDao = new QuestionarioDao();
        cDao = new CategoriaDao();
        pDao = new PerguntaDao();
        
        pergunta = new Pergunta();

        carregarRespostas();
    }

    private void carregarRespostas() throws SQLException {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);

        qDao.setConn(conn);
        cDao.setConn(conn);
        pDao.setConn(conn);

        /*
         * Buscando todos os procedimentos metodológicos da APO
         */
        apo.setMetodos(qDao.buscarPorApo(apo.getId()));

        /*
         * Buscando todos as categorias de cada procedimento metodológico
         */
        Iterator<Questionario> it = apo.getMetodos().iterator();
        while (it.hasNext()) 
        {
            Questionario q = it.next();
            q.setCategorias(cDao.buscarPorQuestionario(q));
            
            /*
             * Buscando todas as perguntas de cada categoria
             */
            Iterator<Categoria> itc = q.getCategorias().iterator();
            while(itc.hasNext())
            {
                Categoria c = itc.next();
                c.setPerguntas(pDao.buscarPorCategoria(c.getId()));
                //perguntas.addAll(c.getPerguntas());
            }
        }
        
        conn.commit();
        conn.close();
    }

    private SessionController recuperarSessao() 
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        return (SessionController) session.getAttribute("SessionController");
    }

    /**
     * @return the apo
     */
    public APO getApo() {
        return apo;
    }

    /**
     * @param apo the apo to set
     */
    public void setApo(APO apo) {
        this.apo = apo;
    }

    /**
     * @return the perguntas
     */
    public ArrayList<Pergunta> getPerguntas() {
        return perguntas;
    }

    /**
     * @param perguntas the perguntas to set
     */
    public void setPerguntas(ArrayList<Pergunta> perguntas) {
        this.perguntas = perguntas;
    }

    /**
     * @return the pergunta
     */
    public Pergunta getPergunta() {
        return pergunta;
    }

    /**
     * @param pergunta the pergunta to set
     */
    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }
}
