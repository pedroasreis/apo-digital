package Controller;

import Dao.ApoDao;
import Dao.ComodoDao;
import Dao.ConnectionDao;
import Dao.ModeloTopologicoDao;
import Model.APO;
import Model.Comodo;
import Model.ComodoPlanta;
import Model.ConexaoComodo;
import Model.ModeloTopologico;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Pedro
 */
@ManagedBean(name = "ModeloTopologicoController")
@ViewScoped
public class ModeloTopologicoController implements Serializable {

    private Connection conn;
    private ConnectionDao connFactory;
    private ComodoDao comodoDao;
    private ModeloTopologicoDao dao;
    private ApoDao apoDao;
    
    private ModeloTopologico modeloTopologico;
    private List<APO> apos;
    
    private String comodos;
    private String resultado;
    
    private ArrayList<Comodo> lista;

    public ModeloTopologicoController() {
        connFactory = new ConnectionDao();
        comodoDao = new ComodoDao();
        dao = new ModeloTopologicoDao();
        apoDao = new ApoDao();
        
        modeloTopologico = new ModeloTopologico();
        lista = new ArrayList<Comodo>();
        apos = new ArrayList<APO>();
        
        buscarComodos();
        buscarApos();
    }

    private void abrirConexao() throws SQLException {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
    }

    private void fecharConexao() {
        try {
            conn.commit();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void buscarApos(){
        try{
            abrirConexao();
            apoDao.setConn(conn);
            apos = apoDao.buscarTodos();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            fecharConexao();
        }
    }
    
    private void inserirModeloTopologico(){
        try{
            abrirConexao();
            dao.setConn(conn);
            modeloTopologico.setId(dao.inserirModeloTopologico(modeloTopologico));
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            fecharConexao();
        }
    }
    
    private void inserirComodoTopologico(ArrayList<ComodoPlanta> cps){
        try{
            abrirConexao();
            dao.setConn(conn);
            Iterator<ComodoPlanta> it = cps.iterator();
            while(it.hasNext()){
                ComodoPlanta cp = it.next();
                cp.setModeloTopologico(modeloTopologico);
                cp.setIdReal(dao.inserirComodoTopologico(cp));
                System.out.println("Id real gerado: " + cp.getIdReal());
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            fecharConexao();
        }
    }
    
    private void inserirConexoesTopologicas(ArrayList<ConexaoComodo> conexoes){
        try{
            abrirConexao();
            dao.setConn(conn);
            Iterator<ConexaoComodo> it = conexoes.iterator();
            while(it.hasNext()){
                ConexaoComodo cc = it.next();
                dao.inserirConexaoTopologica(cc.getComodo1(), cc.getComodo2());
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            fecharConexao();
        }
    }

    private void buscarComodos() {
        comodos = "";
        try {
            abrirConexao();
            comodoDao.setConn(conn);
            lista = comodoDao.buscarPorTipo(Comodo.UNIDADE);
            Iterator<Comodo> it = lista.iterator();
            while (it.hasNext()) {
                Comodo c = it.next();
                comodos += c.getNome() + ";";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fecharConexao();
        }
    }

    public void processarResultado() {
        System.out.println("Resultado Final: " + resultado);
        ArrayList<ConexaoComodo> cc = new ArrayList<ConexaoComodo>();
        ArrayList<ComodoPlanta> comodosPlanta = new ArrayList<ComodoPlanta>();
        Map<Integer, Integer> mapaId = new HashMap<Integer, Integer>();

        StringTokenizer level1 = new StringTokenizer(resultado, ";");
        while (level1.hasMoreElements()) {
            String aux = (String) level1.nextElement();

            StringTokenizer level2 = new StringTokenizer(aux, "-");
            while (level2.hasMoreElements()) {
                ComodoPlanta c1 = new ComodoPlanta();
                ComodoPlanta c2 = new ComodoPlanta();

                /*
                 * Nesse nível da string sempre teremos pelo menos um par de cômodos,
                 * portanto é seguro em cada iteração acessar dois tokens
                 */
                String com1 = (String) level2.nextElement();
                String com2 = (String) level2.nextElement();

                /*
                 * Quebrando primeira string e retirando nome e id do cômodo,
                 * lembrando que esse é um id criado na memória apenas identificando
                 * a unicidade de um cômodo para o contexto do mapa topológico criado
                 */
                criarComodoPlanta(c1, com1);
                relacionarComodoReal(c1);
                inserirComodoNaLista(comodosPlanta, c1);
                
                /*
                 * Fazendo a mesma operação acima para o segundo token
                 */
                criarComodoPlanta(c2, com2);
                relacionarComodoReal(c2);
                inserirComodoNaLista(comodosPlanta, c2);
                
                ConexaoComodo conexaoComodo = new ConexaoComodo(c1, c2);
                cc.add(conexaoComodo);
                
                System.out.println(c1.getNome() + "|" + c1.getComodo().getId() + " -> " + c2.getNome()
                        + "|" + c2.getComodo().getId());
            }
            //System.out.println(aux);
        }
        
        /*
         * Com as estruturas de dados todas prontas, salvar as informações
         * no banco de dados
         */
        inserirModeloTopologico();
        System.out.println("Número de cômodos diferentes: " + comodosPlanta.size());
        inserirComodoTopologico(comodosPlanta);
        Iterator<ComodoPlanta> it = comodosPlanta.iterator();
        while(it.hasNext()){
            ComodoPlanta cp = it.next();
            Integer i1 = cp.getId();
            Integer i2 = cp.getIdReal();
            System.out.println("No mapa: " + i1 + " -> " + i2);
            mapaId.put(i1, i2);
        }
        
        /*
         * Atualizar cômodos das conexões com os IDs gerados pela inserção
         */
        Iterator<ConexaoComodo> it2 = cc.iterator();
        while(it2.hasNext()){
            ConexaoComodo aux = it2.next();
            Integer i = aux.getComodo1().getId();
            aux.getComodo1().setIdReal(mapaId.get(i));
            
            Integer i2 = aux.getComodo2().getId();
            aux.getComodo2().setIdReal(mapaId.get(i2));
        }
        
        /*
         * Por fim armazenar no banco de dados as conexões entre os cômodos
         */
        inserirConexoesTopologicas(cc);
    }
    
    private void inserirComodoNaLista(ArrayList<ComodoPlanta> lista, ComodoPlanta cp){
        if(!lista.contains(cp)){
            lista.add(cp);
        }
    }

    private void criarComodoPlanta(ComodoPlanta c, String tokenizar) {
        StringTokenizer sc1 = new StringTokenizer(tokenizar, ",");
        c.setNome((String) sc1.nextElement());
        c.setId(Integer.valueOf((String) sc1.nextElement()));
    }
    
    private void relacionarComodoReal(ComodoPlanta c){
        c.getComodo().setNome(c.getNome());
        c.setComodo(lista.get(lista.indexOf(c.getComodo())));
    }

    /**
     * @return the comodos
     */
    public String getComodos() {
        return comodos;
    }

    /**
     * @param comodos the comodos to set
     */
    public void setComodos(String comodos) {
        this.comodos = comodos;
    }

    /**
     * @return the resultado
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    /**
     * @return the modeloTopologico
     */
    public ModeloTopologico getModeloTopologico() {
        return modeloTopologico;
    }

    /**
     * @param modeloTopologico the modeloTopologico to set
     */
    public void setModeloTopologico(ModeloTopologico modeloTopologico) {
        this.modeloTopologico = modeloTopologico;
    }

    /**
     * @return the apos
     */
    public List<APO> getApos() {
        return apos;
    }

    /**
     * @param apos the apos to set
     */
    public void setApos(List<APO> apos) {
        this.apos = apos;
    }
    
}
