/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Dao.ConnectionDao;
import Dao.RespostaDao;
import Model.Categoria;
import Model.Pergunta;
import Model.Resposta;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author pedro
 * Classe responsável por oferecer operações para a realização da aplicação de uma APO.
 */
@ManagedBean (name = "AvaliacaoController")
@ViewScoped
public class AvaliacaoController implements Serializable
{

    private ArrayList<Categoria> categorias;
    private Categoria categoria;
    private RespostaDao rDao;
    private Connection conn;
    private ConnectionDao connFactory;
    
    /*
     * Construtor da classe, inicializa os objetos do modelo e DAO.
     */
    public AvaliacaoController()
    {
        connFactory = new ConnectionDao();
        rDao = new RespostaDao();
        categorias = recuperarSessao().getCategorias();
        Iterator<Categoria> it = categorias.iterator();
        while(it.hasNext())
        {
            Categoria c = it.next();
            Iterator<Pergunta> i = c.getPerguntas().iterator();
            while(i.hasNext())
            {
                Pergunta p = i.next();
                if (p.getTipo() == Pergunta.INTERVALO_NUMERICO){
                    p.setQualificador(p.getQualificadores().get(0));
                }
            }
        }
        //System.out.println("Pergunta: " + categoria.getPerguntas().get(0));
    }

    /*
     * Método responsável por recuperar uma referência para o objeto "HttpSession" do usuário.
     */
    private SessionController recuperarSessao()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        return (SessionController) session.getAttribute("SessionController");
    }

    /*
     * Método responsável por inserir na base de dados as respostas dadas na realização da APO.
     */
    public void salvar() throws SQLException
    {
        FacesContext context = FacesContext.getCurrentInstance();
        conn = connFactory.getConnection();
        rDao.setConn(conn);
        conn.setAutoCommit(false);
        Iterator<Categoria> it = categorias.iterator();
        while(it.hasNext())
        {
            Categoria c = it.next();
            System.out.println("Categoria: " + c.getNome());
            Iterator<Pergunta> i = c.getPerguntas().iterator();
            while(i.hasNext())
            {
                Pergunta p = i.next();
                Resposta r = new Resposta();
                r.setPergunta(p);
                r.setTexto(p.getResposta());
                rDao.salvar(r);
                System.out.println("Resposta = " + p.getResposta() + "; Nota: " + p.getNota());
            }
        }
        conn.commit();
        conn.close();
        context.addMessage(null, new FacesMessage("Questionário respondido com sucesso!", ""));
    }

    /**
     * @return the categorias
     */
    public ArrayList<Categoria> getCategorias() {
        return categorias;
    }

    /**
     * @param categorias the categorias to set
     */
    public void setCategorias(ArrayList<Categoria> categorias) {
        this.categorias = categorias;
    }

    /**
     * @return the categoria
     */
    public Categoria getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

}
