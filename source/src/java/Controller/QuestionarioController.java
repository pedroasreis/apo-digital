/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.ConnectionDao;
import Dao.PerguntaDao;
import Dao.QuestionarioDao;
import Model.APO;
import Model.Questionario;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "QuestionarioController")
@ViewScoped
public class QuestionarioController implements Serializable {

    private Connection conn;
    private ConnectionDao connFactory;
    private Questionario questionario;
    private ArrayList<Questionario> questionarios;
    private QuestionarioDao dao;
    private PerguntaDao pDao;

    private String respondente;
    private int apoId;
    private boolean carregado;

    public QuestionarioController() throws SQLException {
        connFactory = new ConnectionDao();
        dao = new QuestionarioDao();
        pDao = new PerguntaDao();
        questionario = new Questionario();
        questionarios = new ArrayList<Questionario>();
    }

    public void carregarQuestionarios() {
        if(!carregado){
            try {
                abrirConexao();
                dao.setConn(conn);
                questionarios = dao.buscarPorApo(apoId);
                Iterator<Questionario> i = questionarios.iterator();
                while (i.hasNext()) {
                    Questionario q = i.next();
                    q.setNumCategorias(dao.contarCategorias(q.getId()));
                }
                carregado = true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                fecharConexao();
            }
        }
    }

    private void abrirConexao() throws SQLException {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
    }
    
    private void fecharConexao(){
        try{
            conn.commit();
            conn.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    private void verificarRespondente() {
        System.out.println("Respondente: " + respondente);
        if (respondente.equals("pesquisador")) {
            questionario.setRespondente(Questionario.PESQUISADOR);
        } else {
            questionario.setRespondente(Questionario.MORADOR);
        }
    }

    public void inserir() throws SQLException {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);

        /*
         * Verificação do tipo de respondente
         */
        verificarRespondente();

        questionario.setId(dao.salvar(questionario));
        
        /*
         * Salvando associação com APO correspondente
         */
        APO apo = new APO();
        apo.setId(apoId);
        dao.salvarQuestionarioAvaliacao(questionario, apo);
        
        conn.commit();
        conn.close();
        questionario = new Questionario();
        questionarios = new ArrayList<Questionario>();
        carregarQuestionarios();
    }

    public void editar() throws SQLException {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);

        verificarRespondente();

        dao.editar(questionario);
        conn.commit();
        conn.close();
        questionario = new Questionario();
        questionarios = new ArrayList<Questionario>();
        carregarQuestionarios();
    }

    public void remover() throws SQLException {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);
        dao.excluir(questionario);
        conn.commit();
        conn.close();
        questionario = new Questionario();
        questionarios = new ArrayList<Questionario>();
        carregarQuestionarios();
    }

    public void resetQuestionario() {
        questionario = new Questionario();
    }

    private SessionController recuperarSessao() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        return (SessionController) session.getAttribute("SessionController");
    }

    /**
     * @return the questionario
     */
    public Questionario getQuestionario() {
        return questionario;
    }

    /**
     * @param questionario the questionario to set
     */
    public void setQuestionario(Questionario questionario) {
        this.questionario = questionario;
    }

    /**
     * @return the questionarios
     */
    public ArrayList<Questionario> getQuestionarios() {
        return questionarios;
    }

    /**
     * @param questionarios the questionarios to set
     */
    public void setQuestionarios(ArrayList<Questionario> questionarios) {
        this.questionarios = questionarios;
    }

    /**
     * @return the respondente
     */
    public String getRespondente() {
        return respondente;
    }

    /**
     * @param respondente the respondente to set
     */
    public void setRespondente(String respondente) {
        this.respondente = respondente;
    }

    /**
     * @return the apoId
     */
    public int getApoId() {
        return apoId;
    }

    /**
     * @param apoId the apoId to set
     */
    public void setApoId(int apoId) {
        this.apoId = apoId;
    }

}
