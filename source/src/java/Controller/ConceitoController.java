/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Dao.ConceitoDao;
import Dao.ConnectionDao;
import Model.Conceito;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author pedro
 * Classe responsável por oferecer operações para o gerenciamento de Conceitos presentes nas APOs.
 */
@ManagedBean (name = "ConceitoController")
@ViewScoped
public class ConceitoController implements Serializable {

    private Conceito conceito;
    private List<Conceito> conceitos;
    private ConceitoDao dao;
    private Connection conn;
    private ConnectionDao connFactory;

   /*
     * Construtor da classe, inicializa os objetos do modelo e DAO.
     */
    public ConceitoController() throws SQLException{
        connFactory = new ConnectionDao();
        dao = new ConceitoDao();
        conceitos = new ArrayList<Conceito>();
        conceito = new Conceito();
    }

    /*
     * Método responsável por buscar da base de dados todos os conceitos registrados
     * e mantê-los em um array de objetos "Conceito".
     */
    public void carregarConceitos() throws SQLException{
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);
        conceitos = dao.buscarTodos();
        conn.commit();
        conn.close();
    }

    /*
     * Método responsável por inserir na base de dados um novo conceito criado pelo usuário.
     */
    public void inserir() throws SQLException{
        System.out.println("Inserindo novo conceito...");
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);
        dao.salvar(conceito);
        conn.commit();
        conn.close();
        resetConceito();
    }

    /*
     * Método responsável por editar um conceito já registrado na base de dados.
     */
    public void editar() throws SQLException{
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);
        dao.editar(conceito);
        conn.commit();
        conn.close();
        resetConceito();
    }

    /*
     * Método responsável por reinicializar o objeto do modelo "Conceito".
     */
    public void resetConceito(){
        conceito = new Conceito();
    }

    /**
     * @return the conceito
     */
    public Conceito getConceito() {
        return conceito;
    }

    /**
     * @param conceito the conceito to set
     */
    public void setConceito(Conceito conceito) {
        this.conceito = conceito;
    }

    /**
     * @return the conceitos
     */
    public List<Conceito> getConceitos() {
        return conceitos;
    }

    /**
     * @param conceitos the conceitos to set
     */
    public void setConceitos(List<Conceito> conceitos) {
        this.conceitos = conceitos;
    }

}
