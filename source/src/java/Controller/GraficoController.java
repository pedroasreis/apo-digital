/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.ConnectionDao;
import Dao.RespostaDao;
import Model.Pergunta;
import Model.Qualificador;
import Model.Resposta;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author pedro
 */

@ManagedBean (name = "GraficoController")
@ViewScoped
public class GraficoController {

    private RespostaDao dao;
    private Connection conn;
    private ConnectionDao connFactory;
    private GraficoRespostas grafico;

    public GraficoController() throws SQLException {
        dao = new RespostaDao();
        connFactory = new ConnectionDao();
        grafico = new GraficoRespostas();
        grafico.setPergunta(recuperarSessao().getPergunta());
        grafico.setQuant(new int[grafico.getPergunta().getQualificadores().size()]);
        carregarGrafico();
    }

    private void carregarGrafico() throws SQLException {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        dao.setConn(conn);
        grafico.setRespostas(dao.buscarPorPergunta(grafico.getPergunta()));
        for (int b = 0; b < grafico.getRespostas().size(); b++) {
            System.out.println("Resp: " + grafico.getRespostas().get(b).getTexto());
        }
        Iterator<Qualificador> it = grafico.getPergunta().getQualificadores().iterator();
        int i = 0;
        Integer ao = 5;
        Integer b = ao;
        b = b + 6;
        System.out.println("ao: " + ao);
        
        for (i = 0; i < grafico.getPergunta().getQualificadores().size(); i++) {
            for (int j = 0; j < grafico.getRespostas().size(); j++) {
                if (grafico.getRespostas().get(j).getTexto().equals(grafico.getPergunta().getQualificadores().get(i).getTexto())) {
                    grafico.getQuant()[i]++;
                }
                System.out.println(grafico.getRespostas().get(j).getTexto() + " | " + grafico.getPergunta().getQualificadores().get(i).getTexto());
            }
        }
        i = 0;
        while (it.hasNext()) {
            System.out.println("Quant: " +  grafico.getQuant()[i]);
            Qualificador q = it.next();
            grafico.getPieModel().set(q.getTexto(), grafico.getQuant()[i]);
            i++;
        }
        conn.commit();
        conn.close();
    }

    private SessionController recuperarSessao() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        return (SessionController) session.getAttribute("SessionController");
    }

    /**
     * @return the grafico
     */
    public GraficoRespostas getGrafico() {
        return grafico;
    }

    /**
     * @param grafico the grafico to set
     */
    public void setGrafico(GraficoRespostas grafico) {
        this.grafico = grafico;
    }

    public class GraficoRespostas {

        private Pergunta pergunta;
        private ArrayList<Resposta> respostas;
        private PieChartModel pieModel;
        private int quant[];

        public GraficoRespostas() {
            pieModel = new PieChartModel();
            /*pieModel.set("Brand 1", 540);
            pieModel.set("Brand 2", 325);
            pieModel.set("Brand 3", 702);
            pieModel.set("Brand 4", 421);*/
        }

        /**
         * @return the pergunta
         */
        public Pergunta getPergunta() {
            return pergunta;
        }

        /**
         * @param pergunta the pergunta to set
         */
        public void setPergunta(Pergunta pergunta) {
            this.pergunta = pergunta;
        }

        /**
         * @return the pieModel
         */
        public PieChartModel getPieModel() {
            return pieModel;
        }

        /**
         * @param pieModel the pieModel to set
         */
        public void setPieModel(PieChartModel pieModel) {
            this.pieModel = pieModel;
        }

        /**
         * @return the respostas
         */
        public ArrayList<Resposta> getRespostas() {
            return respostas;
        }

        /**
         * @param respostas the respostas to set
         */
        public void setRespostas(ArrayList<Resposta> respostas) {
            this.respostas = respostas;
        }

        /**
         * @return the quant
         */
        public int[] getQuant() {
            return quant;
        }

        /**
         * @param quant the quant to set
         */
        public void setQuant(int[] quant) {
            this.quant = quant;
        }
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }

    /**
     * @return the connFactory
     */
    public ConnectionDao getConnFactory() {
        return connFactory;
    }

    /**
     * @param connFactory the connFactory to set
     */
    public void setConnFactory(ConnectionDao connFactory) {
        this.connFactory = connFactory;
    }

}
