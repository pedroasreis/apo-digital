/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.ComodoDao;
import Dao.ConnectionDao;
import Dao.IconeDao;
import Model.Comodo;
import Model.Icone;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Pedro
 */
@ManagedBean (name = "ComodoController")
@ViewScoped
public class ComodoController implements Serializable
{
    
    private Comodo comodo;
    private ArrayList<Comodo> comodos;
    private ArrayList<Icone> icones;
    private Icone icone;
    
    private Connection conn;
    private ConnectionDao connFactory;
    private ComodoDao cDao;
    private IconeDao iDao;
    
    public ComodoController() throws SQLException
    {
        connFactory = new ConnectionDao();
        cDao = new ComodoDao();
        iDao = new IconeDao();
        
        comodo = new Comodo();
        carregarComodos();
        carregarIcones();
    }
    
    private void carregarComodos() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        cDao.setConn(conn);
        
        comodos = cDao.buscarTodos();
        
        conn.commit();
        conn.close();
    }
    
    private void carregarIcones() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        iDao.setConn(conn);
        
        icones = iDao.buscarTodos();
        
        conn.commit();
        conn.close();
    }
    
    public void associarIcone() throws SQLException
    {
        System.out.println("Associando ícone com cômodo...");
        
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        cDao.setConn(conn);
        
        comodo.setIcone(icone);
        System.out.println("Comodo: " + comodo.getId() + " - Icone: " + comodo.getIcone().getId());
        cDao.editar(comodo);
        
        conn.commit();
        conn.close();
    }
    
    public void reset()
    {
        comodo = new Comodo();
    }
    
    public void inserir() throws SQLException
    {
        System.out.println("Tipo de cômodo escolhido: " + comodo.getTipo());
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        cDao.setConn(conn);
        
        cDao.salvar(comodo);
        
        conn.commit();
        conn.close();
        
        carregarComodos();
    }
    
    public void editar() throws SQLException
    {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
        cDao.setConn(conn);
        
        cDao.editar(comodo);
        
        conn.commit();
        conn.close();
        
        carregarComodos();
    }

    /**
     * @return the comodo
     */
    public Comodo getComodo() {
        return comodo;
    }

    /**
     * @param comodo the comodo to set
     */
    public void setComodo(Comodo comodo) {
        this.comodo = comodo;
    }

    /**
     * @return the comodos
     */
    public ArrayList<Comodo> getComodos() {
        return comodos;
    }

    /**
     * @param comodos the comodos to set
     */
    public void setComodos(ArrayList<Comodo> comodos) {
        this.comodos = comodos;
    }

    /**
     * @return the icones
     */
    public ArrayList<Icone> getIcones() {
        return icones;
    }

    /**
     * @param icones the icones to set
     */
    public void setIcones(ArrayList<Icone> icones) {
        this.icones = icones;
    }

    /**
     * @return the icone
     */
    public Icone getIcone() {
        return icone;
    }

    /**
     * @param icone the icone to set
     */
    public void setIcone(Icone icone) {
        this.icone = icone;
    }
    
}
