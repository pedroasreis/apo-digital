package Controller;

import Dao.ApoDao;
import Dao.AtributoDao;
import Dao.CategoriaDao;
import Dao.ComodoDao;
import Dao.ConceitoDao;
import Dao.MoradorDao;
import Dao.PerguntaDao;
import Dao.QualificadorDao;
import Dao.QuestionarioDao;
import Dao.RespostaDao;
import Model.APO;
import Model.Atributo;
import Model.Categoria;
import Model.Comodo;
import Model.Conceito;
import Model.Morador;
import Model.Pergunta;
import Model.Qualificador;
import Model.Questionario;
import Model.Resposta;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Classe responsável por disponibilizar as perguntas da técnica Questionário a
 * serem respondidas pelos usuários do sistema
 *
 * @author Pedro
 */
@ManagedBean(name = "AplicacaoQuestionarioController")
@ViewScoped
public class AplicacaoQuestionarioController extends ControllerGeral {

    private PerguntaDao perguntaDao;
    private ConceitoDao conceitoDao;
    private ComodoDao comodoDao;
    private AtributoDao atributoDao;
    private QualificadorDao qualificadorDao;
    private CategoriaDao categoriaDao;
    private QuestionarioDao questionarioDao;
    private RespostaDao respostaDao;
    private MoradorDao moradorDao;
    private ApoDao apoDao;

    /*
     APO a ser respondida
     */
    private APO apo;
    private int apoId; //atributo usado apenas para captuar o id da APO na url via get
    private Morador morador;

    /*
     Lista de perguntas da seção Dados Pessoais
     */
    private ArrayList<Pergunta> dadosPessoais;

    /*
     Lista de perguntas da seção Edifício
     */
    private ArrayList<Pergunta> aspectosGeraisEdificio;
    private ArrayList<Pergunta> usoComumEdificio;
    private ArrayList<Pergunta> caracteristicasEdificio;

    /*
     Lista de cômodos da sessão Uso Comum
     */
    private ArrayList<Comodo> comodosUsoComum;
    private HashMap<Integer, Integer> mapaUsoComum; // chave -> id do cômodo, valor -> 0 - não foi escolhido, 1 - foi escolhido

    /*
     Lista de cômodos da sessão Unidade
     */
    private ArrayList<Comodo> comodosUnidade;

    /*
     Arrays correspondentes às pergunas da sessão Unidade
     */
    private ArrayList<Pergunta> unidadeGerais;
    /*
     Arrays de perguntas associado a cada cômodo avaliado
     */
    private ArrayList<Pergunta> salaTvEstar;
    private ArrayList<Pergunta> cozinha;
    private ArrayList<Pergunta> salaJantar;
    private ArrayList<Pergunta> dormitorios;
    private ArrayList<Pergunta> banheiros;
    private ArrayList<Pergunta> areaServico;
    private ArrayList<Pergunta> varanda;
    private ArrayList<Pergunta> salas;

    private Comodo sTvEstar;
    private Comodo coz;
    private Comodo sJantar;
    private Comodo dorm;
    private Comodo banh;
    private Comodo aServico;
    private Comodo var;
    private Comodo sals;

    /*
     Demais arrays de perguntas da sessão unidade
     */
    private ArrayList<Pergunta> analiseUso;
    private ArrayList<Pergunta> ambientais;
    private boolean carregado = false;
    /*
     Mapa entre id de qualificadores e seu texto
     */
    private HashMap<Integer, String> mapaIdQualificador;

    public AplicacaoQuestionarioController() {
        super();
        perguntaDao = new PerguntaDao();
        conceitoDao = new ConceitoDao();
        comodoDao = new ComodoDao();
        atributoDao = new AtributoDao();
        qualificadorDao = new QualificadorDao();
        categoriaDao = new CategoriaDao();
        questionarioDao = new QuestionarioDao();
        respostaDao = new RespostaDao();
        moradorDao = new MoradorDao();
        apoDao = new ApoDao();

        morador = new Morador();
        mapaUsoComum = new HashMap<Integer, Integer>();
    }

    /*
     Método chamado antes da renderização da view associada à este managed bean,
     é responsável por chamar os demais métodos que buscam as perguntas de cada
     seção e subseção do Questionário
     */
    public void carregarQuestionario() {
        apo = new APO();
        apo.setId(apoId);

        if (!carregado) {
            carregarPerguntas();
        }
        carregado = true;
    }

    private void carregarPerguntas() {
        Questionario q = new Questionario();
        mapaIdQualificador = new HashMap<Integer, String>();

        Categoria dPessoais = new Categoria();
        Categoria aspectosGerais = new Categoria();
        Categoria edf = new Categoria();
        Categoria caracteristicas = new Categoria();
        Categoria uComum = new Categoria();
        Categoria unidadeGeral = new Categoria();
        Categoria aUso = new Categoria();
        Categoria amb = new Categoria();

        sTvEstar = new Comodo();
        coz = new Comodo();
        sJantar = new Comodo();
        dorm = new Comodo();
        banh = new Comodo();
        aServico = new Comodo();
        var = new Comodo();
        sals = new Comodo();

        /*
         Buscando categorias e cômodos
         */
        try {
            abrirConexao();
            categoriaDao.setConn(conn);
            questionarioDao.setConn(conn);
            comodoDao.setConn(conn);
            apoDao.setConn(conn);

            apo = apoDao.buscarPorId(apo.getId());

            q = questionarioDao.buscarPorNomeApo(ConstantesAplicacaoAvaliacao.QUESTIONARIO, apo);

            /*
             Busca de categorias
             */
            dPessoais = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.DADOS_PESSOAIS, apo, q);
            aspectosGerais = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.ASPECTOS_GERAIS_EDIFICIO, apo, q);
            edf = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.EDIFICIO, apo, q);
            caracteristicas = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.CARACTERISTICAS_EDIFICIO, apo, q);
            uComum = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.USO_COMUM_EDIFICIO, apo, q);
            unidadeGeral = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.GERAIS_UNIDADE, apo, q);
            aUso = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.ANALISE_USO_UNIDADE, apo, q);
            amb = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.AMBIENTAL_UNIDADE, apo, q);

            /*
             Busca de cômodos
             */
            sTvEstar = comodoDao.buscarPorNome(ConstantesAplicacaoAvaliacao.SALA_TV_ESTAR);
            coz = comodoDao.buscarPorNome(ConstantesAplicacaoAvaliacao.COZINHA);
            sJantar = comodoDao.buscarPorNome(ConstantesAplicacaoAvaliacao.SALA_JANTAR);
            dorm = comodoDao.buscarPorNome(ConstantesAplicacaoAvaliacao.DORMITORIOS);
            banh = comodoDao.buscarPorNome(ConstantesAplicacaoAvaliacao.BANHEIROS);
            aServico = comodoDao.buscarPorNome(ConstantesAplicacaoAvaliacao.AREA_SERVICO);
            var = comodoDao.buscarPorNome(ConstantesAplicacaoAvaliacao.VARANDA);
            sals = comodoDao.buscarPorNome(ConstantesAplicacaoAvaliacao.SALAS);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fecharConexao();
        }
        dadosPessoais = carregarPerguntasCategoria(dPessoais);

        aspectosGeraisEdificio = carregarPerguntasCategoria(aspectosGerais);
        aspectosGeraisEdificio.addAll(carregarPerguntasCategoria(edf));

        caracteristicasEdificio = carregarPerguntasCategoria(caracteristicas);

        /*
         Carregando perguntas da seção Uso Comum
         */
        usoComumEdificio = carregarPerguntasCategoria(uComum);

        /*
         Carregando cômodos da seção Uso Comum
         */
        buscarComodosUsoComum();

        /*
         Carregando cômodos da seção Unidade
         */
        buscarComodosUnidade();

        /*
         Carregando perguntas da seção
         */
        unidadeGerais = carregarPerguntasCategoria(unidadeGeral);
        analiseUso = carregarPerguntasCategoria(aUso);
        ambientais = carregarPerguntasCategoria(amb);

        /*
         Carregando perguntas por cômodo
         */
        salaTvEstar = buscarPerguntasPorComodo(sTvEstar);
        cozinha = buscarPerguntasPorComodo(coz);
        salaJantar = buscarPerguntasPorComodo(sJantar);
        dormitorios = buscarPerguntasPorComodo(dorm);
        banheiros = buscarPerguntasPorComodo(banh);
        areaServico = buscarPerguntasPorComodo(aServico);
        varanda = buscarPerguntasPorComodo(var);
        salas = buscarPerguntasPorComodo(sals);
    }

    /*
     Método que retornará uma lista com todas as perguntas relacionadas ao cômodo passado
     por parâmetro
     */
    private ArrayList<Pergunta> buscarPerguntasPorComodo(Comodo c) {
        ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
        try {
            abrirConexao();
            perguntaDao.setConn(conn);
            conceitoDao.setConn(conn);
            qualificadorDao.setConn(conn);
            perguntas = perguntaDao.buscarPorComodoApo(c, apo);
            /*
             Buscando conceitos e qualificadores de cada pergunta
             */
            Iterator<Pergunta> it = perguntas.iterator();
            while (it.hasNext()) {
                Pergunta p = it.next();

                p.setQualificadores(qualificadorDao.buscarPorPergunta(p));
                /*
                 Povoando mapa de qualificadores
                 */
                Iterator<Qualificador> qualificadoresIt = p.getQualificadores().iterator();
                while (qualificadoresIt.hasNext()) {
                    Qualificador q = qualificadoresIt.next();
                    mapaIdQualificador.put(q.getId(), q.getTexto());
                }

                p.setConceitos(conceitoDao.buscarPorPergunta(p));

                /*
                 As perguntas da busca por cômodo sempre estarão apenas associadas à conceitos
                 */
                if (p.getTipo() == Pergunta.TEXTO) {
                    p.setSimples(true);
                } else {
                    p.setSoConceito(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fecharConexao();
        }
        return perguntas;
    }

    private void buscarComodosUsoComum() {
        try {
            abrirConexao();
            comodoDao.setConn(conn);
            comodosUsoComum = comodoDao.buscarPorTipo(Comodo.USO_COMUM);

            /*
             Criando o mapa de cômodos
             */
            mapaUsoComum = new HashMap<Integer, Integer>();
            Iterator<Comodo> it = comodosUsoComum.iterator();
            while (it.hasNext()) {
                Comodo c = it.next();
                mapaUsoComum.put(c.getId(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fecharConexao();
        }
    }

    private void buscarComodosUnidade() {
        try {
            abrirConexao();
            comodoDao.setConn(conn);
            comodosUnidade = comodoDao.buscarPorTipo(Comodo.UNIDADE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fecharConexao();
        }
    }

    private ArrayList<Pergunta> carregarPerguntasCategoria(Categoria categoria) {
        ArrayList<Pergunta> perguntas = new ArrayList<Pergunta>();
        try {
            abrirConexao();
            perguntaDao.setConn(conn);
            atributoDao.setConn(conn);
            conceitoDao.setConn(conn);
            comodoDao.setConn(conn);
            qualificadorDao.setConn(conn);

            /*
             Primeiramente deve-se buscar o id da categoria "Dados Pessoais" da APO
             escolhida para facilitar a busca das perguntas
             */
            //Categoria dPessoais = categoriaDao.buscarPorApoNome(ConstantesAplicacaoAvaliacao.DADOS_PESSOAIS, apo);
            /*
             Buscando as perguntas da categoria "Dados Pessoais"
             */
            perguntas = perguntaDao.buscarPorCategoria(categoria.getId());

            /*
             Buscando qualificadores, atributos, cômodos e conceitos de cada pergunta
             */
            Iterator<Pergunta> it = perguntas.iterator();
            while (it.hasNext()) {
                Pergunta p = it.next();

                p.setQualificadores(qualificadorDao.buscarPorPergunta(p));
                /*
                 Povoando mapa de qualificadores
                 */
                Iterator<Qualificador> qualificadoresIt = p.getQualificadores().iterator();
                while (qualificadoresIt.hasNext()) {
                    Qualificador q = qualificadoresIt.next();
                    mapaIdQualificador.put(q.getId(), q.getTexto());
                }

                p.setAtributos(atributoDao.buscarPorPergunta(p));
                p.setConceitos(conceitoDao.buscarPorPergunta(p));
                p.setComodos(comodoDao.buscarPorPergunta(p));

                /*
                 Todas as perguntas da categoria "Dados Pessoais são simples, isto é, não está associadas com cômodos, atributos ou
                 conceitos. Podendo apenas estar associada à qualificadores
                 */
                if (p.getConceitos().isEmpty() && p.getAtributos().isEmpty() && p.getComodos().isEmpty()) {
                    p.setSimples(true);
                } else if (!p.getConceitos().isEmpty() && !p.getComodos().isEmpty()) {
                    p.setConceitoComodo(true);
                    /*
                     Copiando cada cômodo da pergunta para a lista de cômodos de cada conceito
                     */
                    Iterator<Conceito> it2 = p.getConceitos().iterator();
                    while (it2.hasNext()) {
                        Conceito conc = it2.next();
                        Iterator<Comodo> it3 = p.getComodos().iterator();
                        while (it3.hasNext()) {
                            Comodo com = it3.next();
                            Comodo comodo = new Comodo();
                            comodo.setId(com.getId());
                            comodo.setTipo(com.getTipo());
                            comodo.setNome(com.getNome());
                            conc.getComodos().add(comodo);
                        }
                    }
                } else if (!p.getAtributos().isEmpty()) {
                    p.setSoAtributo(true);
                } else if (!p.getConceitos().isEmpty()) {
                    p.setSoConceito(true);
                } else if (p.getConceitos().isEmpty() && p.getAtributos().isEmpty() && !p.getComodos().isEmpty()) {
                    p.setSoComodo(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fecharConexao();
        }
        return perguntas;
    }

    /*
     Método que recebe o id de um cômodo e retorna um booleano informando se o cômodo
     foi escolhido ou não
     */
    public boolean comodoEscolhido(int id) {
        if (mapaUsoComum != null && mapaUsoComum.containsKey(id)) {
            if (mapaUsoComum.get(id) == 1) {
                return true;
            }
        }
        return false;
    }

    /*
     Método que recebe o id de um cômodo e altera seu estado no mapa
     */
    public void alterarEstadoComodo(int id) {
        System.out.println("Alterando estado do cômodo: " + id);
        if (mapaUsoComum.get(id) == 0) {
            mapaUsoComum.remove(id);
            mapaUsoComum.put(id, 1);
            System.out.println("Alterando estado para escolhido: " + mapaUsoComum.get(id));
        } else {
            System.out.println("Alterando para não escolhido");
            mapaUsoComum.remove(id);
            mapaUsoComum.put(id, 0);
        }
    }

    public void finalizarQuestionario() {
        try {
            abrirConexao();
            moradorDao.setConn(conn);

            /*
             Salvando morador
             */
            morador.setApo(apo);
            morador.setId(moradorDao.inserir(morador));

            /*
             Abaixo as chamadas ao método de inserção de respostas para cada seção do questionário.
             A primeira seção a ser salva é a "Dados Pessoais"
             */
            System.out.println("COMEÇOU DADOS PESSOAIS");
            salvarRespostas(dadosPessoais, morador, new Comodo());
            System.out.println("TERMINOU DADOS PESSOAIS");

            /*
             Perguntas da seção 'Edifício'
             */
            salvarRespostas(aspectosGeraisEdificio, morador, new Comodo());
            System.out.println("COMEÇANDO A SALVAR USO COMUM");
            salvarRespostas(usoComumEdificio, morador, new Comodo());
            System.out.println("TERMINOU DE SALVAR");
            salvarRespostas(caracteristicasEdificio, morador, new Comodo());

            /*
             Perguntas da seção 'Unidade', começando pelas perguntas da subseção 'Gerais'
             */
            salvarRespostas(unidadeGerais, morador, new Comodo());

            /*
             Cômodos da seção 'Unidade'
             */
            salvarRespostas(salaTvEstar, morador, sTvEstar);
            salvarRespostas(cozinha, morador, coz);
            salvarRespostas(salaJantar, morador, sJantar);
            salvarRespostas(dormitorios, morador, dorm);
            salvarRespostas(banheiros, morador, banh);
            salvarRespostas(areaServico, morador, aServico);
            salvarRespostas(varanda, morador, var);
            salvarRespostas(salas, morador, sals);

            /*
             Perguntas das subseções 'Análise de Uso' e 'Ambientais'
             */
            salvarRespostas(analiseUso, morador, new Comodo());
            salvarRespostas(ambientais, morador, new Comodo());

            /*
             Mensagem de sucesso da operação
             */
            addMessage("Respostas", "registradas com sucesso!");
            morador = new Morador();
        } catch (Exception e) {
            e.printStackTrace();
            addMessageError("Erro", "Falha na conexão com o banco de dados");
            abortarTransacao();
        } finally {
            fecharConexao();
        }
    }

    /*
     Método que recebe uma lista de perguntas um morador e um cômodo para salvar as respostas das perguntas.
     Lança a exceção ao invés de tratá-la, já que é apenas um método assistente, o principal deve tratar
     possíveis lançamentos de exceção, uma vez que o salvamento de reposta e o salvamento do morador estão
     ligadas e um não pode ocorrer sem o outro
     */
    private void salvarRespostas(ArrayList<Pergunta> perguntas, Morador morador, Comodo comodo) throws SQLException {
        respostaDao.setConn(conn);

        Iterator<Pergunta> it = perguntas.iterator();
        while (it.hasNext()) {
            Pergunta p = it.next();

            /*
             pergunta simples
             */
            if (p.isSimples()) {
                /*
                 Verificando o tipo da pergunta para gerar o texto da resposta,
                 caso a pergunta seja do tipo 'múltipla escolha', o método de
                 geração da resposta final deve ser chamado
                 */
                if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
                    p.setResposta(gerarTextoReposta(p.getIdQualificadores()));

                    /*
                     Limpando array de respostas
                     */
                    p.setIdQualificadores(new ArrayList<String>());
                } else if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
                    p.setResposta(mapaIdQualificador.get(p.getIdQualificador()));
                }

                Resposta r = new Resposta();
                /*
                 Apenas os quatro atributos abaixo serão definidos, os demais
                 continuarão com seu valor default que é 0, significando que 
                 a resposta não está associada com nenhuma dessas entidades
                 */
                r.setApo(apo);
                r.setMorador(morador);
                r.setPergunta(p);
                r.setTexto(p.getResposta());

                respostaDao.salvar(r);

                p.setResposta(null);
                p.setIdQualificador(null);
            } /*
             pergunta do tipo atributo, isto é, apenas com atributos associados
             */ else if (p.isSoAtributo()) {
                /*
                 Iterando na lista de atributos
                 */
                Iterator<Atributo> atributos = p.getAtributos().iterator();
                while (atributos.hasNext()) {
                    Atributo a = atributos.next();

                    if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
                        a.setResposta(gerarTextoReposta(a.getIdQualificadores()));
                        a.setRespostas(new ArrayList<String>());
                        a.setIdQualificadores(new ArrayList<String>());
                    } else if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
                        a.setResposta(mapaIdQualificador.get(a.getIdQualificador()));
                    }

                    Resposta r = new Resposta();
                    /*
                     Apenas os quatro atributos abaixo serão definidos, os demais
                     continuarão com seu valor default que é 0, significando que 
                     a resposta não está associada com nenhuma dessas entidades
                     */
                    r.setApo(apo);
                    r.setMorador(morador);
                    r.setPergunta(p);
                    r.setAtributo(a);
                    r.setTexto(a.getResposta());

                    respostaDao.salvar(r);
                    a.setResposta(null);
                    a.setIdQualificador(null);
                }
            } else if (p.isConceitoComodo()) {
                /*
                 Iterando na lista de conceitos
                 */
                Iterator<Conceito> conceitos = p.getConceitos().iterator();
                while (conceitos.hasNext()) {
                    Conceito c = conceitos.next();
                    /*
                     Iterando na lista de cômodos
                     */
                    Iterator<Comodo> comodos = c.getComodos().iterator();
                    while (comodos.hasNext()) {
                        Comodo cm = comodos.next();

                        if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
                            cm.setResposta(gerarTextoReposta(cm.getIdQualificadores()));
                            cm.setRespostas(new ArrayList<String>());
                            cm.setIdQualificadores(new ArrayList<String>());
                        } else if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
                            cm.setResposta(mapaIdQualificador.get(cm.getIdQualificador()));
                        }

                        Resposta r = new Resposta();
                        /*
                         Apenas os quatro atributos abaixo serão definidos, os demais
                         continuarão com seu valor default que é 0, significando que 
                         a resposta não está associada com nenhuma dessas entidades
                         */
                        r.setApo(apo);
                        r.setMorador(morador);
                        r.setPergunta(p);
                        r.setConceito(c);
                        r.setComodo(cm);
                        r.setTexto(cm.getResposta());

                        respostaDao.salvar(r);
                        cm.setResposta(null);
                        cm.setIdQualificador(null);
                    }
                }
            } /*
             Perguntas que só têm conceitos associados, são as perguntas da sessão unidade
             relacionadas a um cômodo, portanto, apenas para esse caso o terceiro parâmetro
             deste método, o cômodo, será utilizado
             */ else if (p.isSoConceito()) {
                /*
                 Iterando na lista de conceitos
                 */
                Iterator<Conceito> conceitos = p.getConceitos().iterator();
                while (conceitos.hasNext()) {
                    Conceito c = conceitos.next();

                    if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
                        c.setResposta(gerarTextoReposta(c.getIdQualificadores()));
                        c.setRespostas(new ArrayList<String>());
                        c.setIdQualificadores(new ArrayList<String>());
                    } else if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
                        c.setResposta(mapaIdQualificador.get(c.getIdQualificador()));
                    }

                    Resposta r = new Resposta();
                    /*
                     Apenas os quatro atributos abaixo serão definidos, os demais
                     continuarão com seu valor default que é 0, significando que 
                     a resposta não está associada com nenhuma dessas entidades
                     */
                    r.setApo(apo);
                    r.setMorador(morador);
                    r.setPergunta(p);
                    r.setConceito(c);
                    r.setComodo(comodo);
                    r.setTexto(c.getResposta());

                    respostaDao.salvar(r);
                    c.setResposta(null);
                    c.setIdQualificador(null);
                }
            } /*
             Caso seja uma pergunta apenas com cômodos associados, significa que é uma pergunta
             da categoria "Uso comum"
             */ else if (p.isSoComodo()) {
                Iterator<Comodo> comodos = p.getComodos().iterator();
                while (comodos.hasNext()) {
                    Comodo cm = comodos.next();

                    if(comodoEscolhido(cm.getId())){
                        if (p.getTipo() == Pergunta.MULTIPLA_ESCOLHA) {
                            cm.setResposta(gerarTextoReposta(cm.getIdQualificadores()));
                            cm.setRespostas(new ArrayList<String>());
                            cm.setIdQualificadores(new ArrayList<String>());
                        } else if (p.getTipo() == Pergunta.ESCOLHA_UNICA) {
                            cm.setResposta(mapaIdQualificador.get(cm.getIdQualificador()));
                        }

                        Resposta r = new Resposta();
                        /*
                         Apenas os quatro atributos abaixo serão definidos, os demais
                         continuarão com seu valor default que é 0, significando que 
                         a resposta não está associada com nenhuma dessas entidades
                         */
                        r.setApo(apo);
                        r.setMorador(morador);
                        r.setPergunta(p);
                        r.setComodo(cm);
                        r.setTexto(cm.getResposta());

                        respostaDao.salvar(r);
                        cm.setResposta(null);
                        cm.setIdQualificador(null);
                    }
                }
            }
        }

    }

    /*
     Método que recebe uma lista de respostas originadas de perguntas do tipo 2, de múltipla escolha,
     e monta apenas uma string contendo todas as escolhas do usuário
     */
    private String gerarTextoReposta(ArrayList<String> idOps) {
        String resposta = "";
        Iterator<String> it = idOps.iterator();
        while (it.hasNext()) {
            String r = it.next();
            resposta += mapaIdQualificador.get(Integer.decode(r)) + ";";
        }
        return resposta;
    }

    /**
     * @return the dadosPessoais
     */
    public ArrayList<Pergunta> getDadosPessoais() {
        return dadosPessoais;
    }

    /**
     * @param dadosPessoais the dadosPessoais to set
     */
    public void setDadosPessoais(ArrayList<Pergunta> dadosPessoais) {
        this.dadosPessoais = dadosPessoais;
    }

    /**
     * @return the aspectosGeraisEdificio
     */
    public ArrayList<Pergunta> getAspectosGeraisEdificio() {
        return aspectosGeraisEdificio;
    }

    /**
     * @param aspectosGeraisEdificio the aspectosGeraisEdificio to set
     */
    public void setAspectosGeraisEdificio(ArrayList<Pergunta> aspectosGeraisEdificio) {
        this.aspectosGeraisEdificio = aspectosGeraisEdificio;
    }

    /**
     * @return the usoComumEdificio
     */
    public ArrayList<Pergunta> getUsoComumEdificio() {
        return usoComumEdificio;
    }

    /**
     * @param usoComumEdificio the usoComumEdificio to set
     */
    public void setUsoComumEdificio(ArrayList<Pergunta> usoComumEdificio) {
        this.usoComumEdificio = usoComumEdificio;
    }

    /**
     * @return the caracteristicasEdificio
     */
    public ArrayList<Pergunta> getCaracteristicasEdificio() {
        return caracteristicasEdificio;
    }

    /**
     * @param caracteristicasEdificio the caracteristicasEdificio to set
     */
    public void setCaracteristicasEdificio(ArrayList<Pergunta> caracteristicasEdificio) {
        this.caracteristicasEdificio = caracteristicasEdificio;
    }

    /**
     * @return the apo
     */
    public APO getApo() {
        return apo;
    }

    /**
     * @param apo the apo to set
     */
    public void setApo(APO apo) {
        this.apo = apo;
    }

    /**
     * @return the apoId
     */
    public int getApoId() {
        return apoId;
    }

    /**
     * @param apoId the apoId to set
     */
    public void setApoId(int apoId) {
        this.apoId = apoId;
    }

    /**
     * @return the comodosUsoComum
     */
    public ArrayList<Comodo> getComodosUsoComum() {
        return comodosUsoComum;
    }

    /**
     * @param comodosUsoComum the comodosUsoComum to set
     */
    public void setComodosUsoComum(ArrayList<Comodo> comodosUsoComum) {
        this.comodosUsoComum = comodosUsoComum;
    }

    /**
     * @return the mapaUsoComum
     */
    public HashMap<Integer, Integer> getMapaUsoComum() {
        return mapaUsoComum;
    }

    /**
     * @param mapaUsoComum the mapaUsoComum to set
     */
    public void setMapaUsoComum(HashMap<Integer, Integer> mapaUsoComum) {
        this.mapaUsoComum = mapaUsoComum;
    }

    /**
     * @return the comodosUnidade
     */
    public ArrayList<Comodo> getComodosUnidade() {
        return comodosUnidade;
    }

    /**
     * @param comodosUnidade the comodosUnidade to set
     */
    public void setComodosUnidade(ArrayList<Comodo> comodosUnidade) {
        this.comodosUnidade = comodosUnidade;
    }

    /**
     * @return the unidadeGerais
     */
    public ArrayList<Pergunta> getUnidadeGerais() {
        return unidadeGerais;
    }

    /**
     * @param unidadeGerais the unidadeGerais to set
     */
    public void setUnidadeGerais(ArrayList<Pergunta> unidadeGerais) {
        this.unidadeGerais = unidadeGerais;
    }

    /**
     * @return the salaTvEstar
     */
    public ArrayList<Pergunta> getSalaTvEstar() {
        return salaTvEstar;
    }

    /**
     * @param salaTvEstar the salaTvEstar to set
     */
    public void setSalaTvEstar(ArrayList<Pergunta> salaTvEstar) {
        this.salaTvEstar = salaTvEstar;
    }

    /**
     * @return the cozinha
     */
    public ArrayList<Pergunta> getCozinha() {
        return cozinha;
    }

    /**
     * @param cozinha the cozinha to set
     */
    public void setCozinha(ArrayList<Pergunta> cozinha) {
        this.cozinha = cozinha;
    }

    /**
     * @return the salaJantar
     */
    public ArrayList<Pergunta> getSalaJantar() {
        return salaJantar;
    }

    /**
     * @param salaJantar the salaJantar to set
     */
    public void setSalaJantar(ArrayList<Pergunta> salaJantar) {
        this.salaJantar = salaJantar;
    }

    /**
     * @return the dormitorios
     */
    public ArrayList<Pergunta> getDormitorios() {
        return dormitorios;
    }

    /**
     * @param dormitorios the dormitorios to set
     */
    public void setDormitorios(ArrayList<Pergunta> dormitorios) {
        this.dormitorios = dormitorios;
    }

    /**
     * @return the banheiros
     */
    public ArrayList<Pergunta> getBanheiros() {
        return banheiros;
    }

    /**
     * @param banheiros the banheiros to set
     */
    public void setBanheiros(ArrayList<Pergunta> banheiros) {
        this.banheiros = banheiros;
    }

    /**
     * @return the areaServico
     */
    public ArrayList<Pergunta> getAreaServico() {
        return areaServico;
    }

    /**
     * @param areaServico the areaServico to set
     */
    public void setAreaServico(ArrayList<Pergunta> areaServico) {
        this.areaServico = areaServico;
    }

    /**
     * @return the analiseUso
     */
    public ArrayList<Pergunta> getAnaliseUso() {
        return analiseUso;
    }

    /**
     * @param analiseUso the analiseUso to set
     */
    public void setAnaliseUso(ArrayList<Pergunta> analiseUso) {
        this.analiseUso = analiseUso;
    }

    /**
     * @return the ambientais
     */
    public ArrayList<Pergunta> getAmbientais() {
        return ambientais;
    }

    /**
     * @param ambientais the ambientais to set
     */
    public void setAmbientais(ArrayList<Pergunta> ambientais) {
        this.ambientais = ambientais;
    }

    /**
     * @return the morador
     */
    public Morador getMorador() {
        return morador;
    }

    /**
     * @param morador the morador to set
     */
    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    /**
     * @return the varanda
     */
    public ArrayList<Pergunta> getVaranda() {
        return varanda;
    }

    /**
     * @param varanda the varanda to set
     */
    public void setVaranda(ArrayList<Pergunta> varanda) {
        this.varanda = varanda;
    }

    /**
     * @return the var
     */
    public Comodo getVar() {
        return var;
    }

    /**
     * @param var the var to set
     */
    public void setVar(Comodo var) {
        this.var = var;
    }

    /**
     * @return the salas
     */
    public ArrayList<Pergunta> getSalas() {
        return salas;
    }

    /**
     * @param salas the salas to set
     */
    public void setSalas(ArrayList<Pergunta> salas) {
        this.salas = salas;
    }

    /**
     * @return the sals
     */
    public Comodo getSals() {
        return sals;
    }

    /**
     * @param sals the sals to set
     */
    public void setSals(Comodo sals) {
        this.sals = sals;
    }
}
