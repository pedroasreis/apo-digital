package Controller;

import Dao.ConnectionDao;
import java.sql.Connection;
import java.sql.SQLException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Classe responsável por disponibilizar atributos e métodos comuns às demais
 * classes de controle do sistema
 *
 * @author Pedro
 */
public abstract class ControllerGeral {

    protected Connection conn;
    private ConnectionDao connFactory;

    public ControllerGeral() {
        connFactory = new ConnectionDao();
    }

    protected void abrirConexao() throws SQLException {
        conn = connFactory.getConnection();
        conn.setAutoCommit(false);
    }

    protected void fecharConexao() {
        try {
            conn.commit();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void abortarTransacao() {
        try {
            conn.rollback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addMessage(String mensagem1, String mensagem2) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem1, mensagem2);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void addMessageError(String mensagem1, String mensagem2) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem1, mensagem2);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
