package Controller;

import Dao.ApoDao;
import Dao.AtributoDao;
import Dao.CategoriaDao;
import Dao.ComodoDao;
import Dao.ConceitoDao;
import Dao.ConnectionDao;
import Dao.PerguntaDao;
import Dao.QualificadorDao;
import Dao.QuestionarioDao;
import Dao.RespostaDao;
import Model.APO;
import Model.Atributo;
import Model.Comodo;
import Model.Conceito;
import Model.Pergunta;
import Model.Qualificador;
import Model.Questionario;
import Model.Resposta;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.TextAnchor;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "FreeChartController")
@ViewScoped
public class FreeChartController implements Serializable
{

   private ApoDao apoDao;
   private PerguntaDao perguntaDao;
   private CategoriaDao categoriaDao;
   private RespostaDao respostaDao;
   private QuestionarioDao questionarioDao;
   private QualificadorDao qualificadorDao;
   private AtributoDao atributoDao;
   private ConceitoDao conceitoDao;
   private ComodoDao comodoDao;
   private Pergunta pergunta;
   private Set<Integer> idsPerguntas;
   private int respondente;
   private APO apo;
   private Questionario questionario;
   int count;
   private Document baltimorePdf;

   public FreeChartController() throws DocumentException, FileNotFoundException
   {
      apoDao = new ApoDao();
      perguntaDao = new PerguntaDao();
      categoriaDao = new CategoriaDao();
      respostaDao = new RespostaDao();
      qualificadorDao = new QualificadorDao();
      atributoDao = new AtributoDao();
      conceitoDao = new ConceitoDao();
      comodoDao = new ComodoDao();
      questionarioDao = new QuestionarioDao();

      pergunta = new Pergunta();
      apo = new APO();
      apo.setId(27);
      questionario = new Questionario();
      questionario.setId(63);

      idsPerguntas = new TreeSet<Integer>();

      count = 0;

      prepararPdf();
   }

   private void prepararPdf() throws DocumentException, FileNotFoundException
   {
      baltimorePdf = new Document();
      PdfWriter.getInstance(baltimorePdf, new FileOutputStream("graficos/Sucupira_Barra.pdf"));
      baltimorePdf.open();
   }

   public void gerarGraficos()
   {
      estruturarPerguntaResposta();
      baltimorePdf.close();
   }

   private Connection abrirConexao() throws SQLException
   {
      ConnectionDao connDao = new ConnectionDao();
      Connection conn = connDao.getConnection();
      conn.setAutoCommit(false);
      return conn;
   }

   private void fecharConexao(Connection conn)
   {
      try
      {
         conn.commit();
         conn.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   private void rollBack(Connection conn)
   {
      try
      {
         conn.rollback();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   private void estruturarPerguntaResposta()
   {
      Connection conn = null;
      try
      {
         conn = abrirConexao();
         perguntaDao.setConn(conn);
         qualificadorDao.setConn(conn);
         conceitoDao.setConn(conn);
         atributoDao.setConn(conn);
         comodoDao.setConn(conn);
         respostaDao.setConn(conn);
         categoriaDao.setConn(conn);

         /*
          * Todas as perguntas são carregadas
          */
         ArrayList<Pergunta> perguntas = perguntaDao.buscarPorApoTecnicaTipo(apo, questionario, Pergunta.ESCOLHA_UNICA);
         System.out.println("Perguntas encontradas: " + perguntas.size());

         /*
          * Array com todas as variações de cada pergunta
          */
         ArrayList<Pergunta> variacoes = null;

         /*
          * Para cada pergunta busca-se seus componentes e monta sua lista de variações.
          * os objetos serão sobrescitos a cada iteração do loop, evitando overflow
          * na memória.
          */
         for (Pergunta p : perguntas)
         {
            variacoes = new ArrayList<Pergunta>();

            p.setQualificadores(qualificadorDao.buscarPorPergunta(p));

            p.setConceitos(conceitoDao.buscarPorPergunta(p));
            p.setAtributos(atributoDao.buscarPorPergunta(p));
            p.setComodos(comodoDao.buscarPorPergunta(p));
            p.setCategoria(categoriaDao.buscarPorPergunta(p));

            if (p.getConceitos().isEmpty())
            {
               p.getConceitos().add(new Conceito());
            }
            if (p.getAtributos().isEmpty())
            {
               p.getAtributos().add(new Atributo());
            }
            if (p.getComodos().isEmpty())
            {
               p.getComodos().add(new Comodo());
            }

            /*
             * Criação das variações da pergunta
             */
            for (Conceito con : p.getConceitos())
            {
               for (Atributo atr : p.getAtributos())
               {
                  for (Comodo com : p.getComodos())
                  {
                     Pergunta pg = new Pergunta();
                     pg.setId(p.getId());
                     pg.setTexto(p.getTexto());
                     pg.setQualificadores(p.getQualificadores());
                     pg.setCategoria(p.getCategoria());

                     pg.setAtributo(new Atributo());
                     pg.getAtributo().setId(atr.getId());
                     pg.getAtributo().setNome(atr.getNome());
                     pg.getAtributo().setQualificadores(qualificadorDao.buscarPorAtributoOuConceito(pg.getId(), atr.getId()));

                     pg.setConceito(new Conceito());
                     pg.getConceito().setId(con.getId());
                     pg.getConceito().setNome(con.getNome());
                     pg.getConceito().setQualificadores(qualificadorDao.buscarPorAtributoOuConceito(pg.getId(), con.getId()));

                     pg.setComodo(com);

                     /*
                      * O algoritmo de verificação de variações garante que no
                      * mínimo 1 variação será encontrada, portanto o array de variações
                      * jamais estará vazio
                      */
                     variacoes.add(pg);
                  }
               }
            }

            /*
             * Buscando as respostas de cada variação, realizando a contagem e cálculo
             * do percentual de cada opção
             */
            for (Pergunta pg : variacoes)
            {
               pg.setResps(respostaDao.buscaCompleta(pg, apo));
               pg.setMapaRespostas(contarRespostasAgrupadas(pg));
               pg.setMapaPercentual(calcularPercentualRespostas(pg.getMapaRespostas()));

               /*
                * Gerar gráfico
                */
               System.out.println("Gerando gráfico... " + pg.getId());
               gerarTesteStack(pg);
               //gerarBarra(pg);
            }
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         if (conn != null)
         {
            fecharConexao(conn);
         }
      }
   }

   private Map<String, Integer> contarRespostasAgrupadas(Pergunta p)
   {
      Map<String, Integer> mapa = new HashMap<String, Integer>();
      ArrayList<Qualificador> qualificadores = null;

      if (!p.getConceito().getQualificadores().isEmpty())
      {
         qualificadores = p.getConceito().getQualificadores();
      }
      else if (!p.getAtributo().getQualificadores().isEmpty())
      {
         qualificadores = p.getAtributo().getQualificadores();
      }
      else
      {
         qualificadores = p.getQualificadores();
      }
      p.setQualificadores(qualificadores);

      /*
       Primeiramente define as chaves do mapa, que são os textos dos qualificadores
       da pergunta
       */
      for (Qualificador q : qualificadores)
      {
         mapa.put(q.getTexto(), 0);
      }

      /*
       Percorre a lista de respostas da pergunta acessando o contador de cada tipo
       de resposta e aumentando 1 na ocorrência dessa
       */
      Iterator<Resposta> it = p.getResps().iterator();
      while (it.hasNext())
      {
         Resposta r = it.next();
         /*
          Remove qualquer ocorrência de ';' do texto das respostas
          */
         if (r.getTexto() != null || r.getTexto().equals("") || r.getTexto().equals(" "))
         {
            r.setTexto(r.getTexto().replace(";", ""));

            /*
             Teste verificando se há uma ',' no final do texto da resposta
             */
            if (r.getTexto().length() >= 1 && r.getTexto().charAt(r.getTexto().length() - 1) == ',')
            {
               r.setTexto(r.getTexto().replace(",", ""));
            }

            if (mapa.containsKey(r.getTexto()))
            {
               mapa.put(r.getTexto(), mapa.get(r.getTexto()) + 1);
            }
         }
         else
         {
            it.remove();
         }
      }

      return mapa;
   }

   private Map<String, Double> calcularPercentualRespostas(Map<String, Integer> mapa)
   {
      Map<String, Double> mapaPercentual = new HashMap<String, Double>();
      Double total = new Double(0.0);

      /*
       Calculando a quantidade total de respostas dadas
       */
      for (Map.Entry<String, Integer> entrada : mapa.entrySet())
      {
         total += ((double) entrada.getValue());
      }

      /*
       Fazendo o cálculo percentual e criando o mapa com esses valores
       */
      for (Map.Entry<String, Integer> entrada : mapa.entrySet())
      {
         Double res = new Double(0.0);
         res = (((double) entrada.getValue()) * 100.0) / ((double) total);
         //System.out.println("Percentual: " + res);
         mapaPercentual.put(entrada.getKey(), res);
      }

      return mapaPercentual;
   }

   public void gerarTeste(Pergunta pergunta) throws IOException, BadElementException, DocumentException
   {
      DefaultPieDataset dataset = new DefaultPieDataset();
      /*dataset.setValue("IPhone 5s", new Double(20));
       dataset.setValue("SamSung G", new Double(20));
       dataset.setValue("MotoX", new Double(40));
       dataset.setValue("Nokia Lumia", new Double(10));*/
      DecimalFormat df = new DecimalFormat("#,###.0");
      ArrayList<String> keys = new ArrayList<String>();
      int i = 0;
      /*for (Map.Entry<String, Double> entrada : pergunta.getMapaPercentual().entrySet())
       {
       dataset.setValue(entrada.getKey() + " " + df.format(entrada.getValue()) + "%", entrada.getValue());
       keys.add(pergunta.getQualificadores().get(i).getTexto() + " " + df.format(entrada.getValue()) + "%");
       //System.out.println(pergunta.getQualificadores().get(i).getTexto() + " " + df.format(entrada.getValue()) + "%" + " -> " + entrada.getKey() + " " + df.format(entrada.getValue()) + "%");
       i++;
       }*/

      for (Qualificador q : pergunta.getQualificadores())
      {
         dataset.setValue(q.getTexto() + " " + df.format(pergunta.getMapaPercentual().get(q.getTexto())) + "%", pergunta.getMapaPercentual().get(q.getTexto()));
         keys.add(q.getTexto() + " " + df.format(pergunta.getMapaPercentual().get(q.getTexto())) + "%");
         i++;
      }
      System.out.println("#############################");

      String titulo = pergunta.getTexto();
      if (pergunta.getConceito().getId() > 0)
      {
         titulo += " - " + pergunta.getConceito().getNome();
      }
      if (pergunta.getAtributo().getId() > 0)
      {
         titulo += " - " + pergunta.getAtributo().getNome();
      }
      if (pergunta.getComodo().getId() > 0)
      {
         titulo += " - " + pergunta.getComodo().getNome();
      }
      titulo += " (" + pergunta.getResps().size() + ")";
      JFreeChart chart = ChartFactory.createPieChart(
              titulo, // chart title
              dataset, // data
              true, // include legend
              true,
              false);

      PiePlot plot = (PiePlot) chart.getPlot();
      if (i == 5)
      {
         plot.setSectionPaint(keys.get(0), new Color(51, 153, 0));
         plot.setSectionPaint(keys.get(1), new Color(0, 204, 0));
         plot.setSectionPaint(keys.get(2), new Color(255, 255, 0));
         plot.setSectionPaint(keys.get(3), new Color(255, 153, 0));
         plot.setSectionPaint(keys.get(4), new Color(255, 0, 0));
      }

      int width = 640; /* Width of the image */
      int height = 480; /* Height of the image */
      String imagem = pergunta.getCategoria().getId() + "_" + count + ".jpeg";
      System.out.println("Gerando gráfico: " + imagem + " ...");
      File pieChart = new File("graficos/" + imagem);
      count++;
      ChartUtilities.saveChartAsJPEG(pieChart, chart, width, height);

      Image img = Image.getInstance("graficos/" + imagem);
      img.setAlignment(Image.ALIGN_CENTER);
      img.scalePercent(75.0f);
      baltimorePdf.add(img);
      img = null;
   }
   
   public void gerarTesteStack(Pergunta pergunta) throws IOException, BadElementException, DocumentException
   {
      DefaultCategoryDataset dataset = new DefaultCategoryDataset();
      /*dataset.setValue("IPhone 5s", new Double(20));
       dataset.setValue("SamSung G", new Double(20));
       dataset.setValue("MotoX", new Double(40));
       dataset.setValue("Nokia Lumia", new Double(10));*/
      DecimalFormat df = new DecimalFormat("#,###.0");
      ArrayList<String> keys = new ArrayList<String>();
      int i = 0;
      /*for (Map.Entry<String, Double> entrada : pergunta.getMapaPercentual().entrySet())
       {
       dataset.setValue(entrada.getKey() + " " + df.format(entrada.getValue()) + "%", entrada.getValue());
       keys.add(pergunta.getQualificadores().get(i).getTexto() + " " + df.format(entrada.getValue()) + "%");
       //System.out.println(pergunta.getQualificadores().get(i).getTexto() + " " + df.format(entrada.getValue()) + "%" + " -> " + entrada.getKey() + " " + df.format(entrada.getValue()) + "%");
       i++;
       }*/

      for (Qualificador q : pergunta.getQualificadores())
      {
         double val = pergunta.getMapaPercentual().get(q.getTexto());
         val = val * 10;
         val = Math.round(val);
         val = val / 10;
         dataset.setValue(val, q.getTexto(), "C1");
         keys.add(q.getTexto() + " " + df.format(pergunta.getMapaPercentual().get(q.getTexto())) + "%");
         i++;
      }
      System.out.println("#############################");

      String titulo = pergunta.getTexto();
      if (pergunta.getConceito().getId() > 0)
      {
         titulo += " - " + pergunta.getConceito().getNome();
      }
      if (pergunta.getAtributo().getId() > 0)
      {
         titulo += " - " + pergunta.getAtributo().getNome();
      }
      if (pergunta.getComodo().getId() > 0)
      {
         titulo += " - " + pergunta.getComodo().getNome();
      }
      titulo += " (" + pergunta.getResps().size() + ")";
      JFreeChart chart = ChartFactory.createStackedBarChart3D(
              titulo, // chart title
              "Category",
              "Value",
              dataset, // data
              PlotOrientation.HORIZONTAL,
              true, // include legend
              true,
              false);
      CategoryPlot plot = (CategoryPlot) chart.getPlot();
      BarRenderer renderer = (BarRenderer) plot.getRenderer();
      
      renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));
        renderer.setBaseNegativeItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));

      /*PiePlot plot = (PiePlot) chart.getPlot();
      if (i == 5)
      {
         plot.setSectionPaint(keys.get(0), new Color(51, 153, 0));
         plot.setSectionPaint(keys.get(1), new Color(0, 204, 0));
         plot.setSectionPaint(keys.get(2), new Color(255, 255, 0));
         plot.setSectionPaint(keys.get(3), new Color(255, 153, 0));
         plot.setSectionPaint(keys.get(4), new Color(255, 0, 0));
      }*/

      int width = 1000; /* Width of the image */
      int height = 250; /* Height of the image */
      String imagem = pergunta.getCategoria().getId() + "_" + count + ".jpeg";
      System.out.println("Gerando gráfico: " + imagem + " ...");
      File pieChart = new File("graficos/" + imagem);
      count++;
      ChartUtilities.saveChartAsJPEG(pieChart, chart, width, height);

      Image img = Image.getInstance("graficos/" + imagem);
      img.setAlignment(Image.ALIGN_CENTER);
      img.scalePercent(75.0f);
      baltimorePdf.add(img);
      img = null;
   }
   
   public void gerarTesteStackArray(ArrayList<Pergunta> perguntas) throws IOException, BadElementException, DocumentException
   {
      DefaultCategoryDataset dataset = new DefaultCategoryDataset();
      /*dataset.setValue("IPhone 5s", new Double(20));
       dataset.setValue("SamSung G", new Double(20));
       dataset.setValue("MotoX", new Double(40));
       dataset.setValue("Nokia Lumia", new Double(10));*/
      DecimalFormat df = new DecimalFormat("#,###.0");
      ArrayList<String> keys = new ArrayList<String>();
      int i = 0;
      /*for (Map.Entry<String, Double> entrada : pergunta.getMapaPercentual().entrySet())
       {
       dataset.setValue(entrada.getKey() + " " + df.format(entrada.getValue()) + "%", entrada.getValue());
       keys.add(pergunta.getQualificadores().get(i).getTexto() + " " + df.format(entrada.getValue()) + "%");
       //System.out.println(pergunta.getQualificadores().get(i).getTexto() + " " + df.format(entrada.getValue()) + "%" + " -> " + entrada.getKey() + " " + df.format(entrada.getValue()) + "%");
       i++;
       }*/

      for (Qualificador q : pergunta.getQualificadores())
      {
         double val = pergunta.getMapaPercentual().get(q.getTexto());
         val = val * 10;
         val = Math.round(val);
         val = val / 10;
         dataset.setValue(val, q.getTexto(), "C1");
         keys.add(q.getTexto() + " " + df.format(pergunta.getMapaPercentual().get(q.getTexto())) + "%");
         i++;
      }
      System.out.println("#############################");

      String titulo = pergunta.getTexto();
      if (pergunta.getConceito().getId() > 0)
      {
         titulo += " - " + pergunta.getConceito().getNome();
      }
      if (pergunta.getAtributo().getId() > 0)
      {
         titulo += " - " + pergunta.getAtributo().getNome();
      }
      if (pergunta.getComodo().getId() > 0)
      {
         titulo += " - " + pergunta.getComodo().getNome();
      }
      titulo += " (" + pergunta.getResps().size() + ")";
      JFreeChart chart = ChartFactory.createStackedBarChart3D(
              titulo, // chart title
              "Category",
              "Value",
              dataset, // data
              PlotOrientation.HORIZONTAL,
              true, // include legend
              true,
              false);
      CategoryPlot plot = (CategoryPlot) chart.getPlot();
      BarRenderer renderer = (BarRenderer) plot.getRenderer();
      
      renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(
                new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));
        renderer.setBaseNegativeItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.CENTER));

      /*PiePlot plot = (PiePlot) chart.getPlot();
      if (i == 5)
      {
         plot.setSectionPaint(keys.get(0), new Color(51, 153, 0));
         plot.setSectionPaint(keys.get(1), new Color(0, 204, 0));
         plot.setSectionPaint(keys.get(2), new Color(255, 255, 0));
         plot.setSectionPaint(keys.get(3), new Color(255, 153, 0));
         plot.setSectionPaint(keys.get(4), new Color(255, 0, 0));
      }*/

      int width = 1000; /* Width of the image */
      int height = 250; /* Height of the image */
      String imagem = pergunta.getCategoria().getId() + "_" + count + ".jpeg";
      System.out.println("Gerando gráfico: " + imagem + " ...");
      File pieChart = new File("graficos/" + imagem);
      count++;
      ChartUtilities.saveChartAsJPEG(pieChart, chart, width, height);

      Image img = Image.getInstance("graficos/" + imagem);
      img.setAlignment(Image.ALIGN_CENTER);
      img.scalePercent(75.0f);
      baltimorePdf.add(img);
      img = null;
   }

   public void gerarBarra(Pergunta pergunta) throws IOException
   {
      final String fiat = "FIAT";
      final String audi = "AUDI";
      final String ford = "FORD";
      final String speed = "Speed";
      final String millage = "Millage";
      final String userrating = "User Rating";
      final String safety = "safety";
      final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

      /*dataset.addValue( 1.0 , fiat , speed );
       dataset.addValue( 3.0 , fiat , userrating );
       dataset.addValue( 5.0 , fiat , millage );
       dataset.addValue( 5.0 , fiat , safety );

       dataset.addValue( 5.0 , audi , speed );
       dataset.addValue( 6.0 , audi , userrating );
       dataset.addValue( 10.0 , audi , millage );
       dataset.addValue( 4.0 , audi , safety );

       dataset.addValue( 4.0 , ford , speed );
       dataset.addValue( 2.0 , ford , userrating );
       dataset.addValue( 3.0 , ford , millage );
       dataset.addValue( 6.0 , ford , safety );*/

      DecimalFormat df = new DecimalFormat("#,###.00");
      for (Map.Entry<String, Double> entrada : pergunta.getMapaPercentual().entrySet())
      {
         dataset.setValue(entrada.getValue(), entrada.getKey() + " " + df.format(entrada.getValue()) + "%", speed);
      }

      JFreeChart barChart = ChartFactory.createStackedBarChart(
              "CAR USAGE STATIStICS",
              "Category", "Score",
              dataset, PlotOrientation.HORIZONTAL,
              true, true, false);

      int width = 400; /* Width of the image */
      int height = 400; /* Height of the image */
      //File BarChart = new File( "BarChart.jpeg" ); 
      String imagem = pergunta.getCategoria().getId() + "_" + count + ".jpeg";
      //System.out.println("Gerando gráfico: " + imagem + " ...");
      File barChartF = new File("graficos/" + imagem);
      count++;
      ChartUtilities.saveChartAsJPEG(barChartF, barChart, width, height);
   }
   
}
