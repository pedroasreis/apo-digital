package Controller;

import Dao.ConnectionDao;
import Dao.QualificadorDao;
import Dao.RespostaDao;
import Model.Pergunta;
import Model.Qualificador;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "ConsultasCruzadasController")
@ViewScoped
public class ConsultasCruzadasController implements Serializable
{

   private CartesianChartModel grafico;
   private Connection conn;
   private ConnectionDao connFactory;
   private RespostaDao respostaDao;
   private QualificadorDao qualificadorDao;
   
   private int numQualificadores;
   private boolean renderizar = false;
   
   private String nome;

   public ConsultasCruzadasController()
   {
      connFactory = new ConnectionDao();
      
      respostaDao = new RespostaDao();
      qualificadorDao = new QualificadorDao();
      grafico = new CartesianChartModel();
   }

   public void abrirConexao() throws SQLException
   {
      conn = connFactory.getConnection();
      conn.setAutoCommit(false);
   }

   public void fecharConexao()
   {
      try
      {
         conn.commit();
         conn.close();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   public void aluguelSatisfacaoAnteriorBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(826);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.aluguelSatisfacaoBairroAnteriorBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Satisfação bairro anterior de moradores de casa alugada");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void aluguelSatisfacaoAtualBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(824);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.aluguelSatisfacaoBairroAtualBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Satisfação bairro atual de moradores de casa alugada");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void aluguelSatisfacaoAnteriorSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1716);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.aluguelSatisfacaoBairroAnteriorSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Satisfação bairro anterior de moradores de casa alugada");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void aluguelSatisfacaoAtualSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1714);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.aluguelSatisfacaoBairroAtualSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Satisfação bairro atual de moradores de casa alugada");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void satisfacaoLocalizacaoNaCidadeInclusaoBairroAnteriorBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(860);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(860, 134);
         ArrayList<String> respostas = respostaDao.satisfeitosLocalizacaoInclusaoBairroAnteriorBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Inclusão na cidade do bairro anterior dos moradores "
                 + "satisfeitos com a localização do bairro atual na cidade");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void satisfacaoLocalizacaoNaCidadeInclusaoBairroAnteriorSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1718);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(1718, 134);
         ArrayList<String> respostas = respostaDao.satisfeitosLocalizacaoInclusaoBairroAnteriorSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Inclusão na cidade do bairro anterior dos moradores "
                 + "satisfeitos com a localização do bairro atual na cidade");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void satisfacaoLocalizacaoNaCidadeInclusaoBairroAtualBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(859);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(859, 134);
         ArrayList<String> respostas = respostaDao.satisfeitosLocalizacaoInclusaoBairroAtualBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Inclusão na cidade do bairro atual dos moradores "
                 + "satisfeitos com a localização do bairro atual na cidade");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void satisfacaoLocalizacaoNaCidadeInclusaoBairroAtualSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1717);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(1717, 134);
         ArrayList<String> respostas = respostaDao.satisfeitosLocalizacaoInclusaoBairroAtualSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Inclusão na cidade do bairro atual dos moradores "
                 + "satisfeitos com a localização do bairro atual na cidade");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void rendaSentimentoAnteriorBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(826);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.rendaSentimentoAnteriorBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Sentimento de moradores em relação ao bairro anterior "
                 + "e que tenham renda familiar de 1 a 3 salários mínimos ");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void rendaSentimentoAtualBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(824);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.rendaSentimentoAtualBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Sentimento de moradores em relação ao bairro atual "
                 + "e que tenham renda familiar de 1 a 3 salários mínimos ");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void rendaSentimentoAnteriorSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1716);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.rendaSentimentoAnteriorSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Sentimento de moradores em relação ao bairro anterior "
                 + "e que tenham renda familiar de 1 a 3 salários mínimos ");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void rendaSentimentoAtualSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1714);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.rendaSentimentoAtualSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Sentimento de moradores em relação ao bairro atual "
                 + "e que tenham renda familiar de 1 a 3 salários mínimos ");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void posicaoFemininaBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(815);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.posicaoFemininaBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Posição no grupo familiar dos entrevistados do sexo feminino");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void posicaoMasculinaBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(815);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.posicaoMasculinaBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Posição no grupo familiar dos entrevistados do sexo masculino");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void posicaoFemininaSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1705);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.posicaoFemininaSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Posição no grupo familiar dos entrevistados do sexo feminino");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void posicaoMasculinaSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1705);
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.posicaoMasculinaSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Posição no grupo familiar dos entrevistados do sexo masculino");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void reformaPrivacidadeVizinhos()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1727);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(1727, 109);
         ArrayList<String> respostas = respostaDao.reformaPrivacidadeVizinhos();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Nível de privacidade entre os vizinhos para os entrevistados "
                 + "que realizaram reforma na residência");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void reformaPrivacidadeMoradores()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1727);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(1727, 110);
         ArrayList<String> respostas = respostaDao.reformaPrivacidadeMoradores();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Nível de privacidade entre os moradores da casa para os entrevistados "
                 + "que realizaram reforma na residência");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void reformaAparencia()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1727);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(1727, 102);
         ArrayList<String> respostas = respostaDao.reformaAparencia();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Aparência externa da casa para os entrevistados "
                 + "que realizaram reforma na residência");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void reformaNivelConvivencia()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1721);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.reformaNivelConvivencia();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Nível de convivência entre os vizinhos para os entrevistados "
                 + "que realizaram reforma na residência");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void faixaEtariaInsatisfeitosEquipamentosColetivosBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(813);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.faixaEtariaInsatisfeitosEquipamentosColetivosBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Faixa etária dos moradores insatisfeitos com a "
                 + "quantidade de equipamentos de uso coletivo no bairro");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void faixaEtariaInsatisfeitosEquipamentosColetivosSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1703);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.faixaEtariaInsatisfeitosEquipamentosColetivosSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Faixa etária dos moradores insatisfeitos com a "
                 + "quantidade de equipamentos de uso coletivo no bairro");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void maiorDistanciaProximidadeEquipamentos()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(1727, 99);
         ArrayList<String> respostas = respostaDao.maiorDistanciaProximidadeEquipamentos();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Moradores que acham maiores distâncias até equipamentos "
                 + "de uso coletivo um aspecto negativo de morar em casas e suas impressões da proximidade desses equipamentos e "
                 + "serviços gerais");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void satTransColMeioTransMaisUtilizadoBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(857);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.satTransColMeioTransMaisUtilizadoBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Moradores satisfeitos com o transporte coletivo e "
                 + "o meio de transporte que mais utilizam");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void satTransColMeioTransMaisUtilizadoSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);

         Pergunta p = new Pergunta();
         p.setId(1749);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.satTransColMeioTransMaisUtilizadoSucupira();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Moradores satisfeitos com o transporte coletivo e "
                 + "o meio de transporte que mais utilizam");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void reformaAparenciaBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorAtributoOuConceito(835, 102);
         ArrayList<String> respostas = respostaDao.reformaAparenciaBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Moradores que fizeram reforma na residência e suas "
                 + "opiniões sobre a aparência externa de suas residências");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void reformaNivelConvivenciaBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);
         Pergunta p = new Pergunta();
         p.setId(829);
         
         ArrayList<Qualificador> qualificadores = qualificadorDao.buscarPorPergunta(p);
         ArrayList<String> respostas = respostaDao.reformaNivelConvivenciaBaltimore();
         numQualificadores = qualificadores.size();
         
         System.out.println("Num qualificadores: " + numQualificadores + " - Num de respostas: " + respostas.size());

         gerarGrafico(qualificadores, respostas, "Baltimore - Moradores que fizeram reforma na residência e suas "
                 + "opiniões sobre o nível de convivência com seus vizinhos");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void satisfeitosLocalizacaoTrabalhoSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);
         
         ArrayList<Qualificador> qualificadores = new ArrayList<Qualificador>();
         
         ArrayList<String> respostas = respostaDao.satisfeitosLocalizacaoTrabalhoSucupira();
         
         TreeSet<String> conjuntoQualificadores = new TreeSet<String>();
         conjuntoQualificadores.addAll(respostas);
         
         Iterator<String> it = conjuntoQualificadores.iterator();
         while(it.hasNext()){
            Qualificador q = new Qualificador();
            q.setTexto(it.next());
            qualificadores.add(q);
         }
         numQualificadores = qualificadores.size();

         gerarGrafico(qualificadores, respostas, "Sucupira - Tempo do deslocamento até o trabalho dos "
                 + "moradores satisfeitos com a localização do trabalho em relação às suas residências");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void bairroAnteriorBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);
         
         ArrayList<Qualificador> qualificadores = new ArrayList<Qualificador>();
         
         ArrayList<String> respostas = respostaDao.bairroAnteriorBaltimore();
         
         TreeSet<String> conjuntoQualificadores = new TreeSet<String>();
         conjuntoQualificadores.addAll(respostas);
         
         Iterator<String> it = conjuntoQualificadores.iterator();
         while(it.hasNext()){
            Qualificador q = new Qualificador();
            q.setTexto(it.next());
            qualificadores.add(q);
         }
         numQualificadores = qualificadores.size();

         gerarGrafico(qualificadores, respostas, "Baltimore - Bairro anterior");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void bairroAnteriorSucupira()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);
         
         ArrayList<Qualificador> qualificadores = new ArrayList<Qualificador>();
         
         ArrayList<String> respostas = respostaDao.bairroAnteriorSucupira();
         
         TreeSet<String> conjuntoQualificadores = new TreeSet<String>();
         conjuntoQualificadores.addAll(respostas);
         
         Iterator<String> it = conjuntoQualificadores.iterator();
         while(it.hasNext()){
            Qualificador q = new Qualificador();
            q.setTexto(it.next());
            qualificadores.add(q);
         }
         numQualificadores = qualificadores.size();

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Bairro anterior");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }
   
   public void satisfeitosLocalizacaoTrabalhoBaltimore()
   {
      try
      {
         abrirConexao();
         qualificadorDao.setConn(conn);
         respostaDao.setConn(conn);
         
         ArrayList<Qualificador> qualificadores = new ArrayList<Qualificador>();
         
         ArrayList<String> respostas = respostaDao.satisfeitosLocalizacaoTrabalhoBaltimore();
         
         TreeSet<String> conjuntoQualificadores = new TreeSet<String>();
         conjuntoQualificadores.addAll(respostas);
         
         Iterator<String> it = conjuntoQualificadores.iterator();
         while(it.hasNext()){
            Qualificador q = new Qualificador();
            q.setTexto(it.next());
            qualificadores.add(q);
         }
         numQualificadores = qualificadores.size();

         gerarGrafico(qualificadores, respostas, "Jardim Sucupira - Bairro anterior");
         renderizar = true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         fecharConexao();
      }
   }

   private void gerarGrafico(ArrayList<Qualificador> qualificadores, ArrayList<String> respostas, String titulo)
   {
      grafico = new CartesianChartModel();
      nome = titulo;

      HashMap<String, Integer> mapa = new HashMap<String, Integer>();
      for (Qualificador q : qualificadores)
      {
         mapa.put(q.getTexto(), 0);
      }
      for (String r : respostas)
      {
         if (mapa.containsKey(r))
         {
            mapa.put(r, mapa.get(r) + 1);
         }
      }

      double total = (double) respostas.size();
      for (Qualificador q : qualificadores)
      {
         ChartSeries cs = new ChartSeries(q.getTexto());
         //System.out.println("Texto: " + q.getTexto() + " - Valor: " + (((double) mapa.get(q.getTexto()) * 100.0) / total));
         cs.set(total + "", (((double) mapa.get(q.getTexto()) * 100.0) / total));

         grafico.addSeries(cs);
      }
   }

   /**
    * @return the grafico
    */
   public CartesianChartModel getGrafico()
   {
      return grafico;
   }

   /**
    * @param grafico the grafico to set
    */
   public void setGrafico(CartesianChartModel grafico)
   {
      this.grafico = grafico;
   }

   /**
    * @return the numQualificadores
    */
   public int getNumQualificadores()
   {
      return numQualificadores;
   }

   /**
    * @param numQualificadores the numQualificadores to set
    */
   public void setNumQualificadores(int numQualificadores)
   {
      this.numQualificadores = numQualificadores;
   }

   /**
    * @return the renderizar
    */
   public boolean isRenderizar()
   {
      return renderizar;
   }

   /**
    * @param renderizar the renderizar to set
    */
   public void setRenderizar(boolean renderizar)
   {
      this.renderizar = renderizar;
   }

   /**
    * @return the nome
    */
   public String getNome()
   {
      return nome;
   }

   /**
    * @param nome the nome to set
    */
   public void setNome(String nome)
   {
      this.nome = nome;
   }
}
