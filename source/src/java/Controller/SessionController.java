/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.APO;
import Model.Categoria;
import Model.Pergunta;
import Model.Questionario;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author pedro
 */
@ManagedBean(name = "SessionController")
@SessionScoped
public class SessionController implements Serializable {

    private Questionario questionario;
    private Questionario questionarioCategoria;
    private ArrayList<Categoria> categorias;
    private Pergunta pergunta;
    private APO apo;

    public SessionController() {
        questionario = new Questionario();
        questionarioCategoria = new Questionario();
    }

    /**
     * @return the questionario
     */
    public Questionario getQuestionario() {
        return questionario;
    }

    /**
     * @param questionario the questionario to set
     */
    public void setQuestionario(Questionario questionario) {
        this.questionario = questionario;
    }

    /**
     * @return the questionarioCategoria
     */
    public Questionario getQuestionarioCategoria() {
        return questionarioCategoria;
    }

    /**
     * @param questionarioCategoria the questionarioCategoria to set
     */
    public void setQuestionarioCategoria(Questionario questionarioCategoria) {
        this.questionarioCategoria = questionarioCategoria;
    }

    /**
     * @return the categorias
     */
    public ArrayList<Categoria> getCategorias() {
        return categorias;
    }

    /**
     * @param categorias the categorias to set
     */
    public void setCategorias(ArrayList<Categoria> categorias) {
        this.categorias = categorias;
    }

    /**
     * @return the pergunta
     */
    public Pergunta getPergunta() {
        return pergunta;
    }

    /**
     * @param pergunta the pergunta to set
     */
    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    /**
     * @return the apo
     */
    public APO getApo() {
        return apo;
    }

    /**
     * @param apo the apo to set
     */
    public void setApo(APO apo) {
        this.apo = apo;
    }
}
