package Migracao;

import Model.AtividadePlanta;
import Model.Morador;
import Model.Planta;
import Model.PlantaConexao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
 * Classe responsável pelas operações no sqlite3 e operações de leitura de arquivo.
 */
public class DBOperations {

    private DBConnection dbconn = new DBConnection();
    private Connection conn;
    private String sgbd;
    private DBHandler db;

    public DBOperations(String sgbd) throws SQLException, IOException {
        db = new DBHandler(sgbd);
        this.sgbd = sgbd;
        selecionarConexao();
    }

    public BufferedReader ReadFile(String nome) throws FileNotFoundException,
            IOException {
        File textfile = new File(nome);
        BufferedReader br = new BufferedReader(new FileReader(textfile));
        return br;
    }

    public void selecionarConexao() throws SQLException {
        if(conn != null){
            conn.close();
        }
        if (sgbd.equals("mysql")) {
            conn = dbconn.getConnectionMySql();
        } else {
            conn = dbconn.getConnectionSqlite();
        }
    }

    /*
     * Método que captura as respostas do banco de dados do tablet e transfere para o servidor.
     * Além disso, também transfere os moradores criados para o banco de dados principal.
     * Novas funcionalidades adicionadas ao método: capturar planta criada pelo usuário e informação 
     * sobre conexão entre os cômodos da planta; capturar informações sobre as atividades realizadas
     * no apartamento.
     */
    public void capturarRespostasTablet(ArrayList<Integer> ids) throws SQLException, IOException {
        Map<Integer, Integer> mapaIdMorador = new HashMap<Integer, Integer>();
        mapaIdMorador.put(0, 0);
        Map<Integer, Integer> mapaIdPlanta = new HashMap<Integer, Integer>();
        mapaIdPlanta.put(0, 0);
        String cmd;
        db.setIds(ids);
        db.gerarCondicoes();

        ArrayList<Resposta> respostas = db.getResposta();
        ArrayList<Morador> moradores = db.getMoradores();
        ArrayList<Planta> planta = db.getPlanta();
        ArrayList<PlantaConexao> plantaConexao = db.getPlantaConexao();
        //ArrayList<AtividadeComodo> atividadeComodo = db.getAtividadeComodo();
        ArrayList<AtividadePlanta> atividadePlanta = db.getAtividadePlanta();

        sgbd = "mysql";
        selecionarConexao();

        //Statement st = conn.createStatement();
        //Statement st3 = conn.createStatement();
        Statement st4 = conn.createStatement();
        Statement st5 = conn.createStatement();
        //Statement st6 = conn.createStatement();

        /*
         * Salvando moradores. Pequena alteração realizada: primeiro salva-se o morador
         * e recupera-se o id retornado pela inserção, após isso, uma entrada em um map
         * será feita com a chave sendo o id do morador advindo do banco de dados
         * sqlite e o valor sendo o id real no banco de dados do servidor
         */
        Iterator<Morador> it2 = moradores.iterator();
        while (it2.hasNext()) {
            Morador m = it2.next();
            cmd = "insert into morador values(" + "null" + ", " + "?" + ", null, " + "?" + ", "
                    + "null, null, null, null, null, null, null, null)";
            PreparedStatement st2 = conn.prepareStatement(cmd, Statement.RETURN_GENERATED_KEYS);
            st2.setInt(1, m.getEdificioId());
            st2.setString(2, m.getNumApartamento());
            
            System.out.println(cmd);
            st2.executeUpdate();
            ResultSet rs2 = st2.getGeneratedKeys();
            int id = 0;
            if (rs2.next()) {
                id = rs2.getInt(1);
            }
            /*
             * Preenchendo mapa de moradores, com chave sendo id do db sqlite e 
             * valor o id gerado pela inserção no db do servidor
             */
            Integer idLite = m.getId();
            Integer idServer = id;
            mapaIdMorador.put(idLite, idServer);
            st2.close();
        }

        /*
         * Salvando planta, utilizando mesmo princípio de mapa de id do db sqlite
         * e id gerado pela inserçao no servidor
         */
        Iterator<Planta> it3 = planta.iterator();
        while (it3.hasNext()) {
            Planta p = it3.next();
            cmd = "insert into planta values(" + "null" + ", " + "?" + ", " + "?" + ", "
                    + "?" + ");";
            PreparedStatement st3 = conn.prepareStatement(cmd, Statement.RETURN_GENERATED_KEYS);
            st3.setInt(1, p.getComodo().getId());
            st3.setInt(2, mapaIdMorador.get(p.getMorador().getId()));
            st3.setString(3, p.getIdentificador());

            st3.executeUpdate();
            ResultSet rs3 = st3.getGeneratedKeys();

            int idPlanta = 0;
            if (rs3.next()) {
                idPlanta = rs3.getInt(1);
            }
            System.out.println(cmd);
            /*
             * Preenchendo mapa de IDs da planta
             */
            Integer idPlantaLite = p.getId();
            Integer idPlantaServer = idPlanta;
            mapaIdPlanta.put(idPlantaLite, idPlantaServer);

            st3.close();
        }

        /*
         * Salvando respostas com alteração no id do morador, associando o id real
         * do banco de dados do servidor. E também alteração no id da instância do
         * cômodo (planta)
         */
        Iterator<Resposta> it = respostas.iterator();
        while (it.hasNext()) {
            Resposta r = it.next();
            cmd = "insert into resposta values(" + "null" + ", " + "?" + ", " + "?" + ", "
                    + "?" + ", " + "?" + ", " + "?" + ", " + "?" + ", "
                    + "?" + ", " + "?" + ");";
            PreparedStatement st = conn.prepareStatement(cmd);
            Integer idMorador = r.getMoradorId();
            st.setInt(1, mapaIdMorador.get(idMorador));
            st.setInt(2, r.getPerguntaId());
            st.setString(3, r.getTexto());
            st.setInt(4, r.getApoId());
            st.setInt(5, r.getAmbienteId());
            st.setInt(6, r.getConceitoId());
            st.setInt(7, mapaIdPlanta.get(r.getPlantaId()));
            st.setInt(8, r.getAtributoId());

            System.out.println(cmd);
            st.executeUpdate();
            st.close();
        }

        /*
         * Salvando planta conexão
         */
        Iterator<PlantaConexao> it4 = plantaConexao.iterator();
        while (it4.hasNext()) {
            PlantaConexao pc = it4.next();
            cmd = "insert into planta_conexao values(" + mapaIdPlanta.get(pc.getPlanta1().getId()) + ", " + mapaIdPlanta.get(pc.getPlanta2().getId()) + ");";
            st4.executeUpdate(cmd);
            System.out.println(cmd);
        }
        st4.close();

        /*
         * Salvando atividade_planta
         */
        Iterator<AtividadePlanta> it5 = atividadePlanta.iterator();
        System.out.println("Salvando atividade_planta...");
        System.out.println("Número de registros a serem salvos: " + atividadePlanta.size());
        while (it5.hasNext()) {
            AtividadePlanta ac = it5.next();
            cmd = "insert into atividade_planta values(" + ac.getAtividade().getId() + ", " + mapaIdPlanta.get(ac.getPlanta().getId())
                    + ", " + mapaIdMorador.get(ac.getMorador().getId()) + ");";

            st5.executeUpdate(cmd);
            System.out.println(cmd);
        }
        st5.close();

        conn.commit();
        conn.close();
    }

    public void processaArquivo(String nomeArquivo) {
        int i = 1;
        boolean terminado = false;
        BufferedReader br = null;
        try {
            br = ReadFile(nomeArquivo);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        sgbd = "sqlite3";
        try {
            selecionarConexao();
            conn.setAutoCommit(false);
        } catch (SQLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        while (!terminado) {
            try {
                Statement st = conn.createStatement();
                String cmd;
                while ((cmd = br.readLine()) != null) {
                    st.executeUpdate(cmd);
                    System.out.println(cmd + " -> " + i);
                    i++;
                }
                terminado = true;
            } catch (SQLException e) {
                e.printStackTrace();
                //System.out.println("Exception... -> " + i);
                i++;
            } catch (IOException e) {
                if (i == 1) {
                    e.printStackTrace();
                }
            }
        }
        try {
            conn.commit();
            conn.close();
            br.close();
        } catch (SQLException e) {
            System.out.println("Erro ao fechar conexão...");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Erro ao fechar arquivo...");
            e.printStackTrace();
        }
    }

    public ArrayList<APOM> listarApos() throws SQLException {
        System.out.println("Listando APOs...");
        ArrayList<APOM> apos = new ArrayList<APOM>();
        Statement st = conn.createStatement();
        String cmd = "select * from avaliacao";
        ResultSet rs = st.executeQuery(cmd);
        while (rs.next()) {
            APOM a = new APOM();
            preencherApo(rs, a);
            System.out.println(a.getNome());
            apos.add(a);
        }
        return apos;
    }

    public void preencherApo(ResultSet rs, APOM a) throws SQLException {
        a.setId(rs.getInt("id"));
        a.setNome(rs.getString("nome"));
        a.setTexto(rs.getString("texto"));
    }

    public String getSgbd() {
        return sgbd;
    }

    public void setSgbd(String sgbd) throws SQLException {
        this.sgbd = sgbd;
        selecionarConexao();
    }

    public void closeConnection() throws SQLException {
        conn.close();
    }
}
