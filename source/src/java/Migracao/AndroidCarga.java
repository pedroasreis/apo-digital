package Migracao;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/*
 * Classe responsável pelas operações de transferência das informações do banco 
 * de dados presente no servidor para o dispositivo android (tablet) e vice-versa.
 */
@ManagedBean(name = "AndroidCarga")
@ViewScoped
public class AndroidCarga implements Serializable {

    private DBOperations sql;
    private DBHandler db;
    private ArrayList<APOM> apos;
    private List<String> aposSelecionadas;
    private Map<String, String> apoBoxes;
    private boolean boxes = false;
    private boolean buttons = true;
    private boolean buttonCarregar = false;
    private boolean buttonDescarregar = false;
    
    public AndroidCarga(String sgbd) throws SQLException, IOException {
        db = new DBHandler(sgbd);
        aposSelecionadas = new ArrayList<String>();
    }
    
    public AndroidCarga() throws IOException{
        db = new DBHandler();
    }

    public void buscarDataBase() throws IOException {
        String cmd = "adb pull /mnt/sdcard/Android/data/com.UFU.APO/files/apo C:\\sqlite\\apo";
        System.out.println(cmd);
        Runtime.getRuntime().exec(cmd);
    }

    private void criarDump(ArrayList<Integer> ids) throws SQLException, IOException {
        db.setIds(ids);
        db.buscarRegistros();
        db.getDb().close();
    }
    
    /*
     * Método auxiliar, que é chamado por um botão secreto da página "migracao.xhtml".
     * Sua função é criar as tabelas no database "apo" (sqlite).
     */
    public void inicializarDbAndroid() throws SQLException, IOException {
        DBOperations sq = new DBOperations("sqlite");
        sq.processaArquivo("C:\\apo_deletar.txt");
        sq.closeConnection();
        devolverDataBase();
    }

    private void povoarDbAndroid() throws SQLException, IOException {
        DBOperations sq = new DBOperations("sqlite");
        /*
         * o método abaixo processa o arquivo "apodb.txt" que contém os comandos
         * de inserção de registros
         */
        sq.processaArquivo("apodb.txt");
    }

    /*
     * Comando para transferência de arquivo para um dispositivo android: 
     * adb push C:\db\apo /mnt/sdcard/Android/data/com.UFU.APO/files
     */
    private void devolverDataBase() throws IOException {
        String cmd = "adb push C:\\sqlite\\apo /mnt/sdcard/Android/data/com.UFU.APO/files";
        System.out.println(cmd);
        Runtime.getRuntime().exec(cmd);
    }
    
    public ArrayList<Integer> prepararIds(){
        Iterator<String> it = aposSelecionadas.iterator();
        ArrayList<Integer> ids = new ArrayList<Integer>();
        while(it.hasNext()){
            String apo = it.next();
            ids.add(Integer.valueOf(apo));
            //System.out.println(apo);
        }
        return ids;
    }
    
    public void carregarAndroid() throws IOException, SQLException, InterruptedException {
        ArrayList<Integer> ids = prepararIds();
        carregarDataAndroid(ids);
        boxes = false;
        buttonCarregar = false;
        buttons = true;
        aposSelecionadas = new ArrayList<String>();
    }

    public void carregarDataAndroid(ArrayList<Integer> ids) throws IOException, SQLException, InterruptedException {
        db.trocarPlataforma("mysql");
        buscarDataBase(); // busca arquivo apo.db do dispositivo android para servidor
        Thread.sleep(5000);
        criarDump(ids); // busca todos os registros do banco de dados no servidor e salva no arquivo apodb.txt
        povoarDbAndroid(); // processa o arquivo apodb.txt inserindo os registros nele contido no arquivo apo.db
        devolverDataBase(); // devolve o arquivo apo.db já povoado para o dispositivo android
    }
    
    public void buscarAposServidor() throws SQLException, IOException {
        sql = new DBOperations("mysql");
        buscarApos();
        boxes = true;
        buttons = false;
        buttonCarregar = true;
    }
    
    public void buscarAposTablet() throws SQLException, IOException {
        sql = new DBOperations("sqlite");
        buscarDataBase();
        buscarApos();
        boxes = true;
        buttons = false;
        buttonDescarregar = true;
    }

    public void buscarApos() throws SQLException, IOException {
        setApos(sql.listarApos());
        setApoBoxes(new HashMap<String, String>());
        Iterator<APOM> it = apos.iterator();
        while(it.hasNext()){
            APOM apo = it.next();
            getApoBoxes().put(apo.getNome(), "" + apo.getId());
        }
    }
    
    public void descarregarAndroid() throws IOException, SQLException{
        ArrayList<Integer> ids = prepararIds();
        descarregarDataAndroid(ids);
        boxes = false;
        buttonDescarregar = false;
        buttons = true;
        aposSelecionadas = new ArrayList<String>();
    }

    public void descarregarDataAndroid(ArrayList<Integer> ids) throws IOException, SQLException {
        //db.setIds(ids);
        sql = new DBOperations("sqlite");
        buscarDataBase();
        sql.capturarRespostasTablet(ids);
        devolverDataBase();
    }

    /*
     * método responsável pela inicialização de um banco de dados apo no
     * dispositivo android conectado, isto é, criação de todas as tabelas
     * necessárias.
     */
    public void criarDbAndroid() throws IOException, SQLException, InterruptedException {
        String cmd;
        File f = new File("C:\\sqlite\\apo");
        boolean existe = f.exists();
        if (existe) {
            f.delete();
            /*cmd = "del C:\\sqlite\\apo";
             Runtime.getRuntime().exec(cmd);*/
            Thread.sleep(10000);
        }
        cmd = "C:\\sqlite\\sqlite3 apo";
        Runtime.getRuntime().exec(cmd); // cria o banco de dados apo.db
        DBConnection dbc = new DBConnection();
        Connection conn = dbc.getConnectionSqlite();
        conn.close();
        sql.processaArquivo("apo.txt");
        devolverDataBase();
    }

    /**
     * @return the apos
     */
    public ArrayList<APOM> getApos() {
        return apos;
    }

    /**
     * @param apos the apos to set
     */
    public void setApos(ArrayList<APOM> apos) {
        this.apos = apos;
    }

    /**
     * @return the aposSelecionadas
     */
    public List<String> getAposSelecionadas() {
        return aposSelecionadas;
    }

    /**
     * @param aposSelecionadas the aposSelecionadas to set
     */
    public void setAposSelecionadas(List<String> aposSelecionadas) {
        this.aposSelecionadas = aposSelecionadas;
    }

    /**
     * @return the apoBoxes
     */
    public Map<String, String> getApoBoxes() {
        return apoBoxes;
    }

    /**
     * @param apoBoxes the apoBoxes to set
     */
    public void setApoBoxes(Map<String, String> apoBoxes) {
        this.apoBoxes = apoBoxes;
    }

    /**
     * @return the boxes
     */
    public boolean isBoxes() {
        return boxes;
    }

    /**
     * @param boxes the boxes to set
     */
    public void setBoxes(boolean boxes) {
        this.boxes = boxes;
    }

    /**
     * @return the buttons
     */
    public boolean isButtons() {
        return buttons;
    }

    /**
     * @param buttons the buttons to set
     */
    public void setButtons(boolean buttons) {
        this.buttons = buttons;
    }

    /**
     * @return the buttonCarregar
     */
    public boolean isButtonCarregar() {
        return buttonCarregar;
    }

    /**
     * @param buttonCarregar the buttonCarregar to set
     */
    public void setButtonCarregar(boolean buttonCarregar) {
        this.buttonCarregar = buttonCarregar;
    }

    /**
     * @return the buttonDescarregar
     */
    public boolean isButtonDescarregar() {
        return buttonDescarregar;
    }

    /**
     * @param buttonDescarregar the buttonDescarregar to set
     */
    public void setButtonDescarregar(boolean buttonDescarregar) {
        this.buttonDescarregar = buttonDescarregar;
    }

}
