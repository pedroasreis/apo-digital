package Migracao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    public Connection getConnectionMySql() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/apo";
        String username = "root";
        String password = "root";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException e) {
            System.out.println("ClassNotFoundException na classe de conexao com o banco de dados");
        } catch (InstantiationException e) {
            System.out.println("InstantiationException na classe de conexao com o banco de dados");
        } catch (IllegalAccessException e) {
            System.out.println("IllegalAccessException na classe de conexao com o banco de dados");
        }
        Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

    public Connection getConnectionSqlite() {
        Connection connection = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\apo");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
