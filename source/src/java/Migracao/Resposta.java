package Migracao;

public class Resposta {

    private int id;
    private int moradorId;
    private int perguntaId;
    private int apoId;
    private String texto;
    private int ambienteId;
    private int conceitoId;
    private int plantaId;
    private int atributoId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMoradorId() {
        return moradorId;
    }

    public void setMoradorId(int moradorId) {
        this.moradorId = moradorId;
    }

    public int getPerguntaId() {
        return perguntaId;
    }

    public void setPerguntaId(int perguntaId) {
        this.perguntaId = perguntaId;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     * @return the ambienteId
     */
    public int getAmbienteId() {
        return ambienteId;
    }

    /**
     * @param ambienteId the ambienteId to set
     */
    public void setAmbienteId(int ambienteId) {
        this.ambienteId = ambienteId;
    }

    /**
     * @return the conceitoId
     */
    public int getConceitoId() {
        return conceitoId;
    }

    /**
     * @param conceitoId the conceitoId to set
     */
    public void setConceitoId(int conceitoId) {
        this.conceitoId = conceitoId;
    }

    /**
     * @return the apoId
     */
    public int getApoId() {
        return apoId;
    }

    /**
     * @param apoId the apoId to set
     */
    public void setApoId(int apoId) {
        this.apoId = apoId;
    }

    /**
     * @return the plantaId
     */
    public int getPlantaId() {
        return plantaId;
    }

    /**
     * @param plantaId the plantaId to set
     */
    public void setPlantaId(int plantaId) {
        this.plantaId = plantaId;
    }

    /**
     * @return the atributoId
     */
    public int getAtributoId() {
        return atributoId;
    }

    /**
     * @param atributoId the atributoId to set
     */
    public void setAtributoId(int atributoId) {
        this.atributoId = atributoId;
    }
}
