package Migracao;

import Model.AtividadeComodo;
import Model.AtividadePlanta;
import Model.Morador;
import Model.Planta;
import Model.PlantaConexao;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

public class DBHandler {

    private ArrayList<String> tabelas;
    private Connection db;
    private DBConnection dbc = new DBConnection();
    private ArrayList<Integer> ids;
    private String condicoes;

    public DBHandler(String sgbd) throws SQLException, IOException {
        if (sgbd.equals("mysql")) {
            db = dbc.getConnectionMySql();
        } else if (sgbd.equals("sqlite")) {
            db = dbc.getConnectionSqlite();
        }
        tabelas = new ArrayList<String>();
        buscarTabelas();
    }

    public DBHandler() throws IOException {
        tabelas = new ArrayList<String>();
        buscarTabelas();
    }

    public void trocarPlataforma(String sgbd) throws SQLException {
        if (sgbd.equals("mysql")) {
            db = dbc.getConnectionMySql();
        } else if (sgbd.equals("sqlite")) {
            db = dbc.getConnectionSqlite();
        }
    }

    private void buscarTabelas() throws IOException {
        File textfile = new File("C:\\tabelas.txt");

        BufferedReader br = new BufferedReader(new FileReader(textfile));
        String cmd;
        while ((cmd = br.readLine()) != null) {
            tabelas.add(cmd);
        }
    }

    public void buscarRegistros() throws SQLException, IOException {
        /*
         * Apagando conteúdo do arquivo 
         */
        File f = new File("apodb.txt");
        FileOutputStream fw = new FileOutputStream("apodb.txt");
        fw.write((new String()).getBytes());
        fw.close();
        
        gerarCondicoes();
        //getAdministrador();
        getAvaliacao();
        getAtividade();
        getAtributo();
        getCategoria();
        getCategoriaQuestionario();
        getComodo();
        getConceito();
        getConceitoCategoria();
        getConceitoPergunta();
        getEdificio();
        getIcone();
        getMorador();
        getPergunta();
        getPerguntaAtributo();
        getPerguntaCategoria();
        getPerguntaComodo();
        getPerguntaQualificador();
        getQualificador();
        getQuestionario();
        getQuestionarioAvaliacao();
        getSuperAtividade();
        getModeloTopologico();
        getComodoTopologico();
        getConexaoTopologica();
        //getResposta();
        //getUsuario();
    }

    /*
     * Método que gera a string que representa as restrições de apo que devem estar presentes nas consultas de todas as tabelas.
     */
    public void gerarCondicoes() {
        StringBuilder sb = new StringBuilder();
        int size = ids.size();
        int i = 1;
        Iterator<Integer> it = ids.iterator();
        while (it.hasNext()) {
            Integer id = it.next();
            if (i < size) {
                sb.append("a.id = " + id + " or ");
            } else {
                sb.append("a.id = " + id);
            }
            i++;
        }
        condicoes = sb.toString();
        System.out.println(condicoes);
    }

    /*
     * select * from avaliacao a where a.id = ? or a.id = ? ... or a.id = ?;
     */
    public void getAvaliacao() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select * from avaliacao a where(" + condicoes + ");";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into avaliacao "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "', '" + rs.getString(3) + "', " + rs.getInt(4) + ", '" +
                    rs.getString(5) + "', '" + rs.getString(6) + "', " + rs.getInt(7) + ", " + rs.getInt(8) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }
    
    public void getAtributo() throws SQLException, IOException {
        String sql = "select * from atributo;";
        Statement st = db.createStatement();
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        
        while(rs.next()){
            String cmd = "insert into atributo values(" + rs.getInt("id") + ", '" + rs.getString("nome") + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        
        fw.close();
    }
    
    public void getPerguntaAtributo() throws SQLException, IOException {
        String sql = "select * from pergunta_atributo;";
        Statement st = db.createStatement();
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        
        while(rs.next()){
            String cmd = "insert into pergunta_atributo values(" + rs.getInt(1) + ", '" + rs.getInt(2) + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        
        fw.close();
    }

    /*
     * select distinct ct.id, ct.nome, ct.texto from categoria ct, categoria_questionario c, questionario_avaliacao q, avaliacao a where 
     * (a.id = ? or a.id = ? ... or a.id = ?) and a.id = q.avaliacao_id and q.questionario_id = c.questionario_id and c.categoria_id = ct.id;
     */
    public void getCategoria() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct ct.id, ct.nome, ct.texto from categoria ct, categoria_questionario c, questionario_avaliacao q, avaliacao a where"
                + "(" + condicoes + ") and a.id = q.avaliacao_id and q.questionario_id = c.questionario_id and c.categoria_id = ct.id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into categoria "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "', '" + rs.getString(3) + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct c.categoria_id, c.questionario_id from categoria_questionario c, questionario_avaliacao q, avaliacao a where 
     * (a.id = ? or a.id = ? ... or a.id = ?) and a.id = q.avaliacao_id and q.questionario_id = c.questionario_id;
     */
    public void getCategoriaQuestionario() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct c.categoria_id, c.questionario_id from categoria_questionario c, questionario_avaliacao q, avaliacao a where ("
                + condicoes + ") and a.id = q.avaliacao_id and q.questionario_id = c.questionario_id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into categoria_questionario "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct c.id, c.nome from conceito c, conceito_categoria cc, categoria_questionario cq, questionario_avaliacao qa, avaliacao a where 
     * (a.id = ? or a.id = ? ... or a.id = ?) and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and 
     * cq.categoria_id = cc.categoria_id and cc.conceito_id = c.id;
     */
    public void getConceito() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct c.id, c.nome from conceito c, categoria_questionario cq, pergunta_categoria pc, questionario_avaliacao qa, conceito_pergunta cp, avaliacao a where("
                + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and cq.categoria_id = pc.categoria_id "
                + "and pc.pergunta_id = cp.pergunta_id and "
                + "cp.conceito_id = c.id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into conceito "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct cc.conceito_id, cc.categoria_id from conceito_categoria cc, categoria_questionario cq, questionario_avaliacao qa, avaliacao a 
     * where (a.id = ? or a.id = ? ... or a.id = ?) and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and 
     * cq.categoria_id = cc.categoria_id;
     */
    public void getConceitoCategoria() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct cc.conceito_id, cc.categoria_id from conceito_categoria cc, categoria_questionario cq, questionario_avaliacao qa, avaliacao a"
                + " where (" + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and cq.categoria_id = cc.categoria_id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into conceito_categoria "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct cp.conceito_id, cp.pergunta_id from conceito_pergunta cp, pergunta_categoria pc, categoria_questionario cq, 
     * questionario_avaliacao qa, avaliacao a where where (a.id = ? or a.id = ? ... or a.id = ?) 
     * and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and cq.categoria_id = pc.categoria_id and 
     * pc.pergunta_id = cp.pergunta_id;
     */
    public void getConceitoPergunta() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct cp.conceito_id, cp.pergunta_id from conceito_pergunta cp, pergunta_categoria pc, categoria_questionario cq,"
                + " questionario_avaliacao qa, avaliacao a where (" + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id "
                + "and cq.categoria_id = pc.categoria_id and pc.pergunta_id = cp.pergunta_id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into conceito_pergunta "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select e.id, e.nome, e.endereco from edifico e, avaliacao a where (a.id = ? or a.id = ? ... or a.id = ?) and a.id_edificio = e.id;
     */
    public void getEdificio() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select e.id, e.nome, e.endereco from edificio e, avaliacao a where (" + condicoes + ") and a.id_edificio = e.id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into edificio "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "', '" + rs.getString(3) + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct m.id, m.edificio_id, m.perfil_familiar, m.num_apartamento, m.sexo, m.grau_escolaridade, m.posicao_grupo_familiar,
     * m.num_pessoas_apartamento, m.renda_familiar, m.situacao_ap, m.ano_mudanca_ap, m.trabalhadoes_ap from morador m, edificio e, avaliacao a 
     * where (a.id = ? or a.id = ? ... or a.id = ?) and a.id_edificio = e.id and e.id = m.edificio_id;
     */
    public void getMorador() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct m.id, m.edificio_id, m.perfil_familiar, m.num_apartamento, m.sexo, m.grau_escolaridade, m.posicao_grupo_familiar, "
                + "m.num_pessoas_apartamento, m.renda_familiar, m.situacao_ap, m.ano_mudanca_ap, m.trabalhadores_ap from morador m, edificio e, avaliacao a "
                + "where (" + condicoes + ") and a.id_edificio = e.id and e.id = m.edificio_id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into morador "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ", '" + rs.getString(3) + "', " + rs.getInt(4) + ", '"
                    + rs.getByte(5) + "', '" + rs.getString(6) + "', '" + rs.getString(7) + "', " + rs.getInt(8)
                    + ", " + rs.getDouble(9) + ", '" + rs.getString(10) + "', " + rs.getInt(11) + ", '" + rs.getString(12) + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct p.id, p.texto, p.comentario, p.tipo from pergunta p, pergunta_categoria pc, categoria_questionario cq, 
     * questionario_avaliacao qa, avaliacao a where (a.id = ? or a.id = ? ... or a.id = ?) 
     * and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and cq.categoria_id = pc.categoria_id and pc.pergunta_id = p.id;
     */
    public void getPergunta() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct p.id, p.texto, p.comentario, p.tipo, p.icone_id from pergunta p, pergunta_categoria pc, categoria_questionario cq, "
                + "questionario_avaliacao qa, avaliacao a where (" + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id "
                + "and cq.categoria_id = pc.categoria_id and pc.pergunta_id = p.id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into pergunta "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "', '" + rs.getString(3) + "', '" + rs.getString(4) + "', " + rs.getInt(5) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct pc.pergunta_id, pc.categoria_id from pergunta_categoria pc, categoria_questionario cq, questionario_avaliacao qa, avaliacao a
     * where (a.id = ? or a.id = ? ... or a.id = ?) and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and 
     * cq.categoria_id = pc.categoria_id;
     */
    public void getPerguntaCategoria() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct pc.pergunta_id, pc.categoria_id from pergunta_categoria pc, categoria_questionario cq, questionario_avaliacao qa, avaliacao a "
                + "where (" + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and cq.categoria_id = pc.categoria_id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into pergunta_categoria "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct pq.pergunta_id, pq.qualificador_id from pergunta_qualificador pq, pergunta_categoria pc, categoria_questionario cq, 
     * questionario_avaliacao qa, avaliacao a where (a.id = ? or a.id = ? ... or a.id = ?) 
     * and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and cq.categoria_id = pc.categoria_id and 
     * pc.pergunta_id = pq.pergunta_id;
     */
    public void getPerguntaQualificador() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct pq.pergunta_id, pq.qualificador_id from pergunta_qualificador pq, pergunta_categoria pc, categoria_questionario cq, "
                + "questionario_avaliacao qa, avaliacao a where (" + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id "
                + "and cq.categoria_id = pc.categoria_id and pc.pergunta_id = pq.pergunta_id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into pergunta_qualificador "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct q.id, q.texto, q.inicio, q.fim from qualificador q, pergunta_qualificador pq, pergunta_categoria pc, 
     * categoria_questionario cq, questionario_avaliacao qa, avaliacao a where (a.id = ? or a.id = ? ... or a.id = ?) 
     * and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and cq.categoria_id = pc.categoria_id and 
     * pc.pergunta_id = pq.pergunta_id and pq.qualificador_id = q.id;
     */
    public void getQualificador() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct q.id, q.texto, q.inicio, q.fim, q.icone_id from qualificador q, pergunta_qualificador pq, pergunta_categoria pc, "
                + "categoria_questionario cq, questionario_avaliacao qa, avaliacao a where (" + condicoes + ") "
                + "and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id and cq.categoria_id = pc.categoria_id and "
                + "pc.pergunta_id = pq.pergunta_id and pq.qualificador_id = q.id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into qualificador "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "', " + rs.getInt(3) + ", " + rs.getInt(4) + ", " + rs.getInt(5) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct q.id, q.nome, q.texto from questionario q, questionario_avaliacao, avaliacao a where (a.id = ? or a.id = ? ... or a.id = ?) and 
     * a.id = avaliacao_id and avaliacao_id = q.id;
     */
    public void getQuestionario() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct q.id, q.nome, q.texto, q.respondente from questionario q, questionario_avaliacao, avaliacao a where (" + condicoes + ") "
                + "and a.id = avaliacao_id and questionario_id = q.id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into questionario "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "', '" + rs.getString(3) + "'," + rs.getInt(4) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }
    
    public void getSuperAtividade() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select * from super_atividade;";
        
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while(rs.next()){
            String cmd = "insert into super_atividade values(" + rs.getInt(1) + ", '" + rs.getString(2) + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }
    
    public void getAtividade() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select * from atividade;";
        
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while(rs.next()){
            String cmd = "insert into atividade values(" + rs.getInt(1) + ", '" + rs.getString(2) + "', " + rs.getInt(3) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*
     * select distinct questionario_id, avaliacao_id from questionario_avaliacao, avaliacao a where (a.id = ? or a.id = ? ... or a.id = ?) and 
     * a.id = avaliacao_id;
     */
    public void getQuestionarioAvaliacao() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct questionario_id, avaliacao_id from questionario_avaliacao, avaliacao a where (" + condicoes + ") "
                + "and a.id = avaliacao_id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into questionario_avaliacao "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    public ArrayList<Resposta> getResposta() throws SQLException, IOException {
        ArrayList<Resposta> respostas = new ArrayList<Resposta>();
        db = dbc.getConnectionSqlite();
        Statement st = db.createStatement();
        String sql = "select r.id, r.morador_id, r.pergunta_id, r.texto, r.apo_id, r.ambiente_id, r.conceito_id, r.instancia_comodo, "
                + "r.atributo_id from resposta r, pergunta_categoria pc, categoria_questionario cq, "
                + "questionario_avaliacao qa, avaliacao a where (" + condicoes + ") and a.id = qa.avaliacao_id and "
                + "qa.questionario_id = cq.questionario_id and cq.categoria_id = pc.categoria_id and pc.pergunta_id = r.pergunta_id; ";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            Resposta r = new Resposta();
            r.setId(rs.getInt(1));
            r.setMoradorId(rs.getInt(2));
            r.setPerguntaId(rs.getInt(3));
            r.setTexto(rs.getString(4));
            r.setApoId(rs.getInt(5));
            r.setAmbienteId(rs.getInt(6));
            r.setConceitoId(rs.getInt(7));
            r.setPlantaId(rs.getInt(8));
            r.setAtributoId(rs.getInt(9));
            respostas.add(r);
        }
        
        db.close();
        return respostas;
    }

    public ArrayList<Morador> getMoradores() throws SQLException {
        ArrayList<Morador> moradores = new ArrayList<Morador>();
        db = dbc.getConnectionSqlite();
        Statement st = db.createStatement();
        String sql = "select distinct m.id, m.edificio_id, m.num_apartamento from morador m," 
                + " avaliacao a where (" + condicoes + ") and a.id = m.edificio_id";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            Morador m = new Morador();
            m.setId(rs.getInt(1));
            m.setEdificioId(rs.getInt(2));
            m.setNumApartamento(rs.getString(3));
            moradores.add(m);
        }
        
        db.close();
        return moradores;
    }
    
    public ArrayList<Planta> getPlanta() throws SQLException {
        ArrayList<Planta> planta = new ArrayList<Planta>();
        db = dbc.getConnectionSqlite();
        Statement st = db.createStatement();
        String sql = "select distinct p.id, p.id_comodo, p.id_morador, p.identificador from morador m, planta p," 
                + " avaliacao a where (" + condicoes + ") and a.id = m.edificio_id and m.id = p.id_morador";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            Planta p = new Planta(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4));
            planta.add(p);
        }
        
        db.close();
        return planta;
    }
    
    public ArrayList<PlantaConexao> getPlantaConexao() throws SQLException {
        ArrayList<PlantaConexao> planta = new ArrayList<PlantaConexao>();
        db = dbc.getConnectionSqlite();
        Statement st = db.createStatement();
        String sql = "select distinct pc.id_comodo1, pc.id_comodo2 from morador m, planta p," 
                + " avaliacao a, conexao_planta pc where (" + condicoes + ") and a.id = m.edificio_id and m.id = p.id_morador"
                + " and (p.id = pc.id_comodo1 or p.id = pc.id_comodo2)";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            PlantaConexao pc = new PlantaConexao(rs.getInt(1), rs.getInt(2));
            planta.add(pc);
        }
        
        db.close();
        return planta;
    }
    
    public void getModeloTopologico() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct mt.id, mt.apo_id, mt.descricao from modelo_topologico mt, avaliacao a where ("
                + condicoes + ") and a.id = mt.apo_id";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into modelo_topologico "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) +  ", '" + rs.getString(3) + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }
    
    public void getComodoTopologico() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct ct.id, ct.comodo_id, ct.modelo_topologico_id from comodo_topologico ct, "
                + "modelo_topologico mt, avaliacao a where ("
                + condicoes + ") and a.id = mt.apo_id and ct.modelo_topologico_id = mt.id";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into comodo_topologico "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) +  ", " + rs.getInt(3) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }
    
    public void getConexaoTopologica() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct ct.comodo_topologico1, ct.comodo_topologico2 "
                + "from conexao_topologica ct, comodo_topologico cmt, modelo_topologico mt, "
                + "avaliacao a where ("
                + condicoes + ") and a.id = mt.apo_id and ct.comodo_topologico1 = cmt.id and "
                + "cmt.modelo_topologico_id = mt.id";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into conexao_topologica "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }
    
    public ArrayList<AtividadePlanta> getAtividadePlanta() throws SQLException {
        ArrayList<AtividadePlanta> ap = new ArrayList<AtividadePlanta>();
        
        db = dbc.getConnectionSqlite();
        Statement st = db.createStatement();
        String sql = "select * from atividade_planta ap, avaliacao a, morador m where (" + condicoes + ") and "
                + "a.id = m.edificio_id and ap.id_morador = m.id;";
        
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            AtividadePlanta a = new AtividadePlanta(rs.getInt(1), rs.getInt(2), rs.getInt(3));
            ap.add(a);
        }
        
        db.close();
        return ap;
    }
    
    public ArrayList<AtividadeComodo> getAtividadeComodo() throws SQLException{
        ArrayList<AtividadeComodo> atividades = new ArrayList<AtividadeComodo>();
        
        db = dbc.getConnectionSqlite();
        Statement st = db.createStatement();
        
        String sql = "select ac.id_morador, ac.id_planta, ac.id_atividade from atividade_comodo ac, avaliacao a, "
                + "morador m where (" + condicoes + ") and a.id = m.edificio_id and m.id = ac.id_morador";
        
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            AtividadeComodo ac = new AtividadeComodo(rs.getInt(1), rs.getInt(2), rs.getInt(3));
            atividades.add(ac);
        }
        
        st.close();
        return atividades;
    }

    private void getIcone() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct i.id, i.titulo from pergunta p, pergunta_categoria pc, categoria_questionario cq, icone i, "
                + "pergunta_comodo pcm, comodo c, questionario_avaliacao qa, avaliacao a where (" + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id "
                + "and cq.categoria_id = pc.categoria_id and pc.pergunta_id = p.id and p.id = pcm.pergunta_id and pcm.comodo_id = "
                + "c.id and c.icone_id = i.id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into icone "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "');";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    private void getPerguntaComodo() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct pcm.pergunta_id, pcm.comodo_id from pergunta p, pergunta_categoria pc, categoria_questionario cq, pergunta_comodo pcm, "
                + "questionario_avaliacao qa, avaliacao a where (" + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id "
                + "and cq.categoria_id = pc.categoria_id and pc.pergunta_id = p.id and p.id = pcm.pergunta_id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into pergunta_comodo "
                    + "values(" + rs.getInt(1) + ", " + rs.getInt(2) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    private void getComodo() throws SQLException, IOException {
        Statement st = db.createStatement();
        String sql = "select distinct cmd.id, cmd.nome, cmd.icone_id, cmd.tipo from pergunta p, pergunta_categoria pc, categoria_questionario cq, comodo cmd, pergunta_comodo pcm, "
                + "questionario_avaliacao qa, avaliacao a where (" + condicoes + ") and a.id = qa.avaliacao_id and qa.questionario_id = cq.questionario_id "
                + "and cq.categoria_id = pc.categoria_id and pc.pergunta_id = p.id and p.id = pcm.pergunta_id and pcm.comodo_id = cmd.id;";
        System.out.println(sql);
        ResultSet rs = st.executeQuery(sql);
        FileWriter fw = new FileWriter("apodb.txt", true);
        while (rs.next()) {
            String cmd = "insert into comodo "
                    + "values(" + rs.getInt(1) + ", '" + rs.getString(2) + "', " + rs.getInt(3) + ", " + rs.getInt(4) + ");";
            fw.write(cmd);
            fw.write("\n");
        }
        fw.close();
    }

    /*public void getUsuario() throws SQLException, IOException{
     Statement st = db.createStatement();
     String sql = "select * from " + tabelas.get(14) + ";";
     ResultSet rs = st.executeQuery(sql);
     FileWriter fw = new FileWriter("apodb.txt", true);
     while(rs.next()){
     String cmd = new String("insert into " + tabelas.get(14) + " " +
     "values(" + rs.getInt(1) + ");");
     fw.write(cmd);
     fw.write("\n");
     }
     fw.close();
     }*/
    public ArrayList<Integer> getIds() {
        return ids;
    }

    public void setIds(ArrayList<Integer> ids) {
        this.ids = ids;
    }

    public String getCondicoes() {
        return condicoes;
    }

    public void setCondicoes(String condicoes) {
        this.condicoes = condicoes;
    }

    public Connection getDb() {
        return db;
    }

    public void setDb(Connection db) {
        this.db = db;
    }
}
