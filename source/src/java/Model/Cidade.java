package Model;

import java.io.Serializable;

/**
 *
 * @author Pedro
 */
public class Cidade implements Serializable {
    
    private int id;
    private String nome;

    @Override
    public boolean equals(Object obj) {
        return(id == ((Cidade)obj).getId());
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
