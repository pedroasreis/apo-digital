/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Pedro
 */
public class PlantaConexao {
    
    private Planta planta1;
    private Planta planta2;
    
    public PlantaConexao(){
        planta1 = new Planta();
        planta2 = new Planta();
    }
    
    public PlantaConexao(int idCom1, int idCom2){
        planta1 = new Planta();
        planta2 = new Planta();
        
        planta1.setId(idCom1);
        planta2.setId(idCom2);
    }

    /**
     * @return the planta1
     */
    public Planta getPlanta1() {
        return planta1;
    }

    /**
     * @param planta1 the planta1 to set
     */
    public void setPlanta1(Planta planta1) {
        this.planta1 = planta1;
    }

    /**
     * @return the planta2
     */
    public Planta getPlanta2() {
        return planta2;
    }

    /**
     * @param planta2 the planta2 to set
     */
    public void setPlanta2(Planta planta2) {
        this.planta2 = planta2;
    }
    
    
    
}
