/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Pedro
 */
public class MoradorDataModel extends ListDataModel<Morador> implements SelectableDataModel<Morador> {

    public MoradorDataModel() {
    }

    public MoradorDataModel(List<Morador> data) {
        super(data);
    }

    @Override
    public Morador getRowData(String rowKey) {
        List<Morador> moradores = (List<Morador>)getWrappedData();

        for (Morador morador : moradores) {
            if((morador.getId() + "").equals(rowKey)) {
                return morador;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Morador morador) {
        return morador.getId() + "";
    }
    
}