/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Pedro
 */
public class AtividadePlanta {
    
    private Atividade atividade;
    private Planta planta;
    private Morador morador;
    
    public AtividadePlanta(int idAtividade, int idPlanta, int idMorador){
        atividade = new Atividade();
        planta = new Planta();
        morador = new Morador();
        
        atividade.setId(idAtividade);
        planta.setId(idPlanta);
        morador.setId(idMorador);
    }
    
    public AtividadePlanta(){
        
    }

    /**
     * @return the atividade
     */
    public Atividade getAtividade() {
        return atividade;
    }

    /**
     * @param atividade the atividade to set
     */
    public void setAtividade(Atividade atividade) {
        this.atividade = atividade;
    }

    /**
     * @return the planta
     */
    public Planta getPlanta() {
        return planta;
    }

    /**
     * @param planta the planta to set
     */
    public void setPlanta(Planta planta) {
        this.planta = planta;
    }

    /**
     * @return the morador
     */
    public Morador getMorador() {
        return morador;
    }

    /**
     * @param morador the morador to set
     */
    public void setMorador(Morador morador) {
        this.morador = morador;
    }
    
}
