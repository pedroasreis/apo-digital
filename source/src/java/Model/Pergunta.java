/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
//import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author pedro
 */
public class Pergunta implements Serializable {

    private int id;
    private String texto;
    private String comentario;
    private int tipo;
    private ArrayList<Comodo> comodos;
    private Icone icone;
    
    private int escalaDeCores;
    
    private ArrayList<Qualificador> qualificadores;
    private ArrayList<Conceito> conceitos;
    private Qualificador qualificador;
    
    private ArrayList<Atributo> atributos;

    private String resposta;
    private ArrayList<String> respostas;
    private int nota;
    
    private ArrayList<Resposta> resps;
    
    private Categoria categoria;
    
    private Resposta resp;
    
    private Comodo comodo;
    private Conceito conceito;
    private Atributo atributo;
    
    private Pergunta perguntaOriginal;
    
    /*
     * Array com uma mesma pergunta, diferenciando-se entre elas por suas instâncias
     * de conceitos, atributos e cômodos
     */
    private ArrayList<Pergunta> perguntasRelacionadas;
    
    /*
     * Estrutura de dados para a utilização do gráfico circular
     */
    private PieChartModel pieModel;
    
    /*
     * Estrutura para gráfico de barras
     */
    private CartesianChartModel categoryModel;
    
    /*
    Modelo para gráfico de barras empilhado para perguntas de
    múltipla escolha
    */
    private CartesianChartModel cartesianModel;
    
    /*
    Gráfico de barras em bloco
    */
    //private HorizontalBarChartModel horizontalBarChartModel;
    
    private String respondente;
    
    private ArrayList<String> idQualificadores;
    private Integer idQualificador;
    
    private int ordem;
    
    /*
    Set necessário para a criação dos gráficos agrupados
    */
    private Set<String> conjuntoQualificadores;
    
    /*
    Mapa utilizado na contagem das resposta de uma pergunta para a exibição de
    gráficos agrupados
    */
    private Map<String, Integer> mapaRespostas;
    
    /*
    Mapa percentual
    */
    private Map<String, Double> mapaPercentual;
    
    private String label;
    private String textoAgrupado;
    
    /*
    Flags que ajudarão a determinar como a pergunta e suas estruturas devem ser apresentadas na tela
    */
    private boolean simples; //pergunta simples, sem nenhuma estrutura associada, a não ser possíveis qualificadores
    private boolean soAtributo; //pergunta associada apenas com uma lista de atributos
    private boolean conceitoComodo; //pergunta associada tanto com uma lista de conceitos como com uma lista de cômodos
    private boolean soConceito; // pergunta associada apenas com conceitos
    private boolean soComodo; // pergunta associada apenas à cômodos

    public static final int TEXTO = 1;
    public static final int MULTIPLA_ESCOLHA = 2;
    public static final int ESCOLHA_UNICA = 3;
    public static final int INTERVALO_NUMERICO = 4;
    
    /*
    Altura do gráfico
    */
    public static final int ALTURA_GRAFICO = 40;
    
    private int alturaGraf;

    public Pergunta(){
        qualificadores = new ArrayList<Qualificador>();
        respostas = new ArrayList<String>();
        conceitos = new ArrayList<Conceito>();
        comodos = new ArrayList<Comodo>();
        atributos = new ArrayList<Atributo>();
        idQualificadores = new ArrayList<String>();
        
        icone = new Icone();
        resp = new Resposta();
        resps = new ArrayList<Resposta>();
        pieModel = new PieChartModel();
        categoryModel = new CartesianChartModel();
        ChartSeries cs1 = new ChartSeries("Inicialização");
        cs1.set("Respostas", 50);
        categoryModel.addSeries(cs1);
        
        ChartSeries cs2 = new ChartSeries("Inicialização 2");
        cs2.set("Respostas", 40);
        categoryModel.addSeries(cs2);
        
        comodo = new Comodo();
        conceito = new Conceito();
        atributo = new Atributo();
        
        perguntasRelacionadas = new ArrayList<Pergunta>();
        
        conjuntoQualificadores = new HashSet<String>();
    }
    
    public Pergunta(Pergunta p){
        perguntaOriginal = p;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     * @return the tipo
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the comentario
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * @param comentario the comentario to set
     */
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    /**
     * @return the qualificador
     */
    public ArrayList<Qualificador> getQualificadores() {
        return qualificadores;
    }

    /**
     * @param qualificador the qualificador to set
     */
    public void setQualificadores(ArrayList<Qualificador> qualificadores) {
        this.qualificadores = qualificadores;
    }

    /**
     * @return the resposta
     */
    public String getResposta() {
        return resposta;
    }

    /**
     * @param resposta the resposta to set
     */
    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    /**
     * @return the respostas
     */
    public ArrayList<String> getRespostas() {
        return respostas;
    }

    /**
     * @param respostas the respostas to set
     */
    public void setRespostas(ArrayList<String> respostas) {
        this.respostas = respostas;
    }

    /**
     * @return the nota
     */
    public int getNota() {
        return nota;
    }

    /**
     * @param nota the nota to set
     */
    public void setNota(int nota) {
        this.nota = nota;
    }

    /**
     * @return the qualificador
     */
    public Qualificador getQualificador() {
        return qualificador;
    }

    /**
     * @param qualificador the qualificador to set
     */
    public void setQualificador(Qualificador qualificador) {
        this.qualificador = qualificador;
    }

    /**
     * @return the conceitos
     */
    public ArrayList<Conceito> getConceitos() {
        return conceitos;
    }

    /**
     * @param conceitos the conceitos to set
     */
    public void setConceitos(ArrayList<Conceito> conceitos) {
        this.conceitos = conceitos;
    }

    /**
     * @return the comodos
     */
    public ArrayList<Comodo> getComodos() {
        return comodos;
    }

    /**
     * @param comodos the comodos to set
     */
    public void setComodos(ArrayList<Comodo> comodos) {
        this.comodos = comodos;
    }

    /**
     * @return the icone
     */
    public Icone getIcone() {
        return icone;
    }

    /**
     * @param icone the icone to set
     */
    public void setIcone(Icone icone) {
        this.icone = icone;
    }

    /**
     * @return the atributos
     */
    public ArrayList<Atributo> getAtributos() {
        return atributos;
    }

    /**
     * @param atributos the atributos to set
     */
    public void setAtributos(ArrayList<Atributo> atributos) {
        this.atributos = atributos;
    }

    /**
     * @return the resp
     */
    public Resposta getResp() {
        return resp;
    }

    /**
     * @param resp the resp to set
     */
    public void setResp(Resposta resp) {
        this.resp = resp;
    }

    /**
     * @return the categoria
     */
    public Categoria getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the resps
     */
    public ArrayList<Resposta> getResps() {
        return resps;
    }

    /**
     * @param resps the resps to set
     */
    public void setResps(ArrayList<Resposta> resps) {
        this.resps = resps;
    }

    /**
     * @return the pieModel
     */
    public PieChartModel getPieModel() {
        return pieModel;
    }

    /**
     * @param pieModel the pieModel to set
     */
    public void setPieModel(PieChartModel pieModel) {
        this.pieModel = pieModel;
    }

    /**
     * @return the comodo
     */
    public Comodo getComodo() {
        return comodo;
    }

    /**
     * @param comodo the comodo to set
     */
    public void setComodo(Comodo comodo) {
        this.comodo = comodo;
    }

    /**
     * @return the conceito
     */
    public Conceito getConceito() {
        return conceito;
    }

    /**
     * @param conceito the conceito to set
     */
    public void setConceito(Conceito conceito) {
        this.conceito = conceito;
    }

    /**
     * @return the atributo
     */
    public Atributo getAtributo() {
        return atributo;
    }

    /**
     * @param atributo the atributo to set
     */
    public void setAtributo(Atributo atributo) {
        this.atributo = atributo;
    }

    /**
     * @return the perguntaOriginal
     */
    public Pergunta getPerguntaOriginal() {
        return perguntaOriginal;
    }

    /**
     * @param perguntaOriginal the perguntaOriginal to set
     */
    public void setPerguntaOriginal(Pergunta perguntaOriginal) {
        this.perguntaOriginal = perguntaOriginal;
    }

    /**
     * @return the perguntasRelacionadas
     */
    public ArrayList<Pergunta> getPerguntasRelacionadas() {
        return perguntasRelacionadas;
    }

    /**
     * @param perguntasRelacionadas the perguntasRelacionadas to set
     */
    public void setPerguntasRelacionadas(ArrayList<Pergunta> perguntasRelacionadas) {
        this.perguntasRelacionadas = perguntasRelacionadas;
    }

    /**
     * @return the respondente
     */
    public String getRespondente() {
        return respondente;
    }

    /**
     * @param respondente the respondente to set
     */
    public void setRespondente(String respondente) {
        this.respondente = respondente;
    }

    /**
     * @return the categoryModel
     */
    public CartesianChartModel getCategoryModel() {
        return categoryModel;
    }

    /**
     * @param categoryModel the categoryModel to set
     */
    public void setCategoryModel(CartesianChartModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    /**
     * @return the simples
     */
    public boolean isSimples() {
        return simples;
    }

    /**
     * @param simples the simples to set
     */
    public void setSimples(boolean simples) {
        this.simples = simples;
    }

    /**
     * @return the soAtributo
     */
    public boolean isSoAtributo() {
        return soAtributo;
    }

    /**
     * @param soAtributo the soAtributo to set
     */
    public void setSoAtributo(boolean soAtributo) {
        this.soAtributo = soAtributo;
    }

    /**
     * @return the conceitoComodo
     */
    public boolean isConceitoComodo() {
        return conceitoComodo;
    }

    /**
     * @param conceitoComodo the conceitoComodo to set
     */
    public void setConceitoComodo(boolean conceitoComodo) {
        this.conceitoComodo = conceitoComodo;
    }

    /**
     * @return the soConceito
     */
    public boolean isSoConceito() {
        return soConceito;
    }

    /**
     * @param soConceito the soConceito to set
     */
    public void setSoConceito(boolean soConceito) {
        this.soConceito = soConceito;
    }

    /**
     * @return the idQualificadores
     */
    public ArrayList<String> getIdQualificadores() {
        return idQualificadores;
    }

    /**
     * @param idQualificadores the idQualificadores to set
     */
    public void setIdQualificadores(ArrayList<String> idQualificadores) {
        this.idQualificadores = idQualificadores;
    }

    /**
     * @return the idQualificador
     */
    public Integer getIdQualificador() {
        return idQualificador;
    }

    /**
     * @param idQualificador the idQualificador to set
     */
    public void setIdQualificador(Integer idQualificador) {
        this.idQualificador = idQualificador;
    }

    /**
     * @return the soComodo
     */
    public boolean isSoComodo() {
        return soComodo;
    }

    /**
     * @param soComodo the soComodo to set
     */
    public void setSoComodo(boolean soComodo) {
        this.soComodo = soComodo;
    }

    /**
     * @return the ordem
     */
    public int getOrdem() {
        return ordem;
    }

    /**
     * @param ordem the ordem to set
     */
    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

    /**
     * @return the cartesianModel
     */
    public CartesianChartModel getCartesianModel() {
        return cartesianModel;
    }

    /**
     * @param cartesianModel the cartesianModel to set
     */
    public void setCartesianModel(CartesianChartModel cartesianModel) {
        this.cartesianModel = cartesianModel;
    }

    /**
     * @return the conjuntoQualificadores
     */
    public Set<String> getConjuntoQualificadores() {
        return conjuntoQualificadores;
    }

    /**
     * @param conjuntoQualificadores the conjuntoQualificadores to set
     */
    public void setConjuntoQualificadores(Set<String> conjuntoQualificadores) {
        this.conjuntoQualificadores = conjuntoQualificadores;
    }

    /**
     * @return the mapaRespostas
     */
    public Map<String, Integer> getMapaRespostas() {
        return mapaRespostas;
    }

    /**
     * @param mapaRespostas the mapaRespostas to set
     */
    public void setMapaRespostas(Map<String, Integer> mapaRespostas) {
        this.mapaRespostas = mapaRespostas;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the textoAgrupado
     */
    public String getTextoAgrupado() {
        return textoAgrupado;
    }

    /**
     * @param textoAgrupado the textoAgrupado to set
     */
    public void setTextoAgrupado(String textoAgrupado) {
        this.textoAgrupado = textoAgrupado;
    }

    /**
     * @return the mapaPercentual
     */
    public Map<String, Double> getMapaPercentual() {
        return mapaPercentual;
    }

    /**
     * @param mapaPercentual the mapaPercentual to set
     */
    public void setMapaPercentual(Map<String, Double> mapaPercentual) {
        this.mapaPercentual = mapaPercentual;
    }

    /**
     * @return the alturaGraf
     */
    public int getAlturaGraf() {
        return alturaGraf;
    }

    /**
     * @param alturaGraf the alturaGraf to set
     */
    public void setAlturaGraf(int alturaGraf) {
        this.alturaGraf = alturaGraf;
    }

   /**
    * @return the escalaDeCores
    */
   public int getEscalaDeCores()
   {
      return escalaDeCores;
   }

   /**
    * @param escalaDeCores the escalaDeCores to set
    */
   public void setEscalaDeCores(int escalaDeCores)
   {
      this.escalaDeCores = escalaDeCores;
   }

}
