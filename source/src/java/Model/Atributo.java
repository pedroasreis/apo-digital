package Model;

import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class Atributo
{

   private int id;
   private String nome;
   /*
    Um atributo pode ter uma lista de respostas ou uma única resposta dependendo
    do tipo da pergunta com a qual o atributo está associado
    */
   private String resposta;
   private ArrayList<String> respostas;
   private Integer idQualificador;
   private ArrayList<String> idQualificadores;
   
   private ArrayList<Qualificador> qualificadores;

   public Atributo()
   {
      respostas = new ArrayList<String>();
      idQualificadores = new ArrayList<String>();
      qualificadores = new ArrayList<Qualificador>();
   }

   public Atributo(int id, String nome)
   {
      this.id = id;
      this.nome = nome;
   }

   @Override
   public boolean equals(Object obj)
   {
      Atributo a = (Atributo) obj;
      if (id == a.getId())
      {
         return true;
      }

      return false;
   }

   /**
    * @return the id
    */
   public int getId()
   {
      return id;
   }

   /**
    * @param id the id to set
    */
   public void setId(int id)
   {
      this.id = id;
   }

   /**
    * @return the nome
    */
   public String getNome()
   {
      return nome;
   }

   /**
    * @param nome the nome to set
    */
   public void setNome(String nome)
   {
      this.nome = nome;
   }

   /**
    * @return the resposta
    */
   public String getResposta()
   {
      return resposta;
   }

   /**
    * @param resposta the resposta to set
    */
   public void setResposta(String resposta)
   {
      this.resposta = resposta;
   }

   /**
    * @return the respostas
    */
   public ArrayList<String> getRespostas()
   {
      return respostas;
   }

   /**
    * @param respostas the respostas to set
    */
   public void setRespostas(ArrayList<String> respostas)
   {
      this.respostas = respostas;
   }

   /**
    * @return the idQualificador
    */
   public Integer getIdQualificador()
   {
      return idQualificador;
   }

   /**
    * @param idQualificador the idQualificador to set
    */
   public void setIdQualificador(Integer idQualificador)
   {
      this.idQualificador = idQualificador;
   }

   /**
    * @return the idQualificadores
    */
   public ArrayList<String> getIdQualificadores()
   {
      return idQualificadores;
   }

   /**
    * @param idQualificadores the idQualificadores to set
    */
   public void setIdQualificadores(ArrayList<String> idQualificadores)
   {
      this.idQualificadores = idQualificadores;
   }

   /**
    * @return the qualificadores
    */
   public ArrayList<Qualificador> getQualificadores()
   {
      return qualificadores;
   }

   /**
    * @param qualificadores the qualificadores to set
    */
   public void setQualificadores(ArrayList<Qualificador> qualificadores)
   {
      this.qualificadores = qualificadores;
   }
}
