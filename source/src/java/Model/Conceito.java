package Model;

import java.util.ArrayList;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author pedro
 */
public class Conceito implements Converter
{

   private int id;
   private String nome;
   private int ordem;
   private String resposta;
   private ArrayList<String> respostas;
   private Integer idQualificador;
   private ArrayList<String> idQualificadores;
   /*
    Lista de atributos necessária para o aninhamento: pergunta -> conceitos -> atributos
    */
   private ArrayList<Atributo> atributos;
   /*
    Lista de cômodos necessário para o aninhamento: pergunta -> conceitos -> cômodos
    */
   private ArrayList<Comodo> comodos;
   
   private ArrayList<Qualificador> qualificadores;

   public Conceito()
   {
      atributos = new ArrayList<Atributo>();
      comodos = new ArrayList<Comodo>();
      respostas = new ArrayList<String>();
      qualificadores = new ArrayList<Qualificador>();
      idQualificadores = new ArrayList<String>();
   }

   /**
    * @return the id
    */
   public int getId()
   {
      return id;
   }

   /**
    * @param id the id to set
    */
   public void setId(int id)
   {
      this.id = id;
   }

   /**
    * @return the nome
    */
   public String getNome()
   {
      return nome;
   }

   /**
    * @param nome the nome to set
    */
   public void setNome(String nome)
   {
      this.nome = nome;
   }

   @Override
   public Object getAsObject(FacesContext context, UIComponent component, String value)
   {
      if (value.trim().equals(""))
      {
         return null;
      }
      else
      {
         try
         {
            String[] partes = value.split(":");
            int id = Integer.parseInt(partes[0]);
            String nome = partes[1];


            Conceito c = new Conceito();
            c.setId(id);
            c.setNome(nome);
            return c;

         }
         catch (NumberFormatException exception)
         {
            return new Conceito();
         }
      }
   }

   @Override
   public String getAsString(FacesContext context, UIComponent component, Object value)
   {
      return String.valueOf(value);
   }

   @Override
   public String toString()
   {
      return nome;
   }

   /**
    * @return the atributos
    */
   public ArrayList<Atributo> getAtributos()
   {
      return atributos;
   }

   /**
    * @param atributos the atributos to set
    */
   public void setAtributos(ArrayList<Atributo> atributos)
   {
      this.atributos = atributos;
   }

   /**
    * @return the comodos
    */
   public ArrayList<Comodo> getComodos()
   {
      return comodos;
   }

   /**
    * @param comodos the comodos to set
    */
   public void setComodos(ArrayList<Comodo> comodos)
   {
      this.comodos = comodos;
   }

   /**
    * @return the resposta
    */
   public String getResposta()
   {
      return resposta;
   }

   /**
    * @param resposta the resposta to set
    */
   public void setResposta(String resposta)
   {
      this.resposta = resposta;
   }

   /**
    * @return the respostas
    */
   public ArrayList<String> getRespostas()
   {
      return respostas;
   }

   /**
    * @param respostas the respostas to set
    */
   public void setRespostas(ArrayList<String> respostas)
   {
      this.respostas = respostas;
   }

   /**
    * @return the idQualificador
    */
   public Integer getIdQualificador()
   {
      return idQualificador;
   }

   /**
    * @param idQualificador the idQualificador to set
    */
   public void setIdQualificador(Integer idQualificador)
   {
      this.idQualificador = idQualificador;
   }

   /**
    * @return the idQualificadores
    */
   public ArrayList<String> getIdQualificadores()
   {
      return idQualificadores;
   }

   /**
    * @param idQualificadores the idQualificadores to set
    */
   public void setIdQualificadores(ArrayList<String> idQualificadores)
   {
      this.idQualificadores = idQualificadores;
   }

   /**
    * @return the ordem
    */
   public int getOrdem()
   {
      return ordem;
   }

   /**
    * @param ordem the ordem to set
    */
   public void setOrdem(int ordem)
   {
      this.ordem = ordem;
   }

   /**
    * @return the qualificadores
    */
   public ArrayList<Qualificador> getQualificadores()
   {
      return qualificadores;
   }

   /**
    * @param qualificadores the qualificadores to set
    */
   public void setQualificadores(ArrayList<Qualificador> qualificadores)
   {
      this.qualificadores = qualificadores;
   }
}
