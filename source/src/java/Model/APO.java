/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class APO {

    private int id;
    private String nome;
    private String texto;
    private String cidade;
    private String estado;
    private int idCidade;
    private int idEstado;
    
    private ArrayList<Questionario> metodos;
    private ArrayList<Morador> moradores;
    private ArrayList<Pergunta> perguntas;
    
    public APO(){
        metodos = new ArrayList<Questionario>();
        moradores = new ArrayList<Morador>();
        perguntas = new ArrayList<Pergunta>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public ArrayList<Questionario> getMetodos() {
        return metodos;
    }

    public void setMetodos(ArrayList<Questionario> metodos) {
        this.metodos = metodos;
    }

    /**
     * @return the moradores
     */
    public ArrayList<Morador> getMoradores() {
        return moradores;
    }

    /**
     * @param moradores the moradores to set
     */
    public void setMoradores(ArrayList<Morador> moradores) {
        this.moradores = moradores;
    }

    /**
     * @return the perguntas
     */
    public ArrayList<Pergunta> getPerguntas() {
        return perguntas;
    }

    /**
     * @param perguntas the perguntas to set
     */
    public void setPerguntas(ArrayList<Pergunta> perguntas) {
        this.perguntas = perguntas;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the idCidade
     */
    public int getIdCidade() {
        return idCidade;
    }

    /**
     * @param idCidade the idCidade to set
     */
    public void setIdCidade(int idCidade) {
        this.idCidade = idCidade;
    }

    /**
     * @return the idEstado
     */
    public int getIdEstado() {
        return idEstado;
    }

    /**
     * @param idEstado the idEstado to set
     */
    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }
    
}
