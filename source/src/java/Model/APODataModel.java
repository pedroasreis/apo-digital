/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Pedro
 */
public class APODataModel extends ListDataModel<APO> implements SelectableDataModel<APO> {

    public APODataModel() {
    }

    public APODataModel(List<APO> data) {
        super(data);
    }

    @Override
    public APO getRowData(String rowKey) {
        List<APO> apos = (List<APO>)getWrappedData();

        for (APO apo : apos) {
            if((apo.getId() + "").equals(rowKey)) {
                return apo;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(APO apo) {
        return apo.getId() + "";
    }
    
}
