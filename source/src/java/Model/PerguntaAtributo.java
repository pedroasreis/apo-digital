/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Pedro
 */
public class PerguntaAtributo {
    
    private Pergunta pergunta;
    private Atributo atributo;
    
    public PerguntaAtributo(){
        pergunta = new Pergunta();
        atributo = new Atributo();
    }
    
    public PerguntaAtributo(int idPergunta, int idAtributo){
        pergunta = new Pergunta();
        atributo = new Atributo();
        
        pergunta.setId(idPergunta);
        atributo.setId(idAtributo);
    }

    /**
     * @return the pergunta
     */
    public Pergunta getPergunta() {
        return pergunta;
    }

    /**
     * @param pergunta the pergunta to set
     */
    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    /**
     * @return the atributo
     */
    public Atributo getAtributo() {
        return atributo;
    }

    /**
     * @param atributo the atributo to set
     */
    public void setAtributo(Atributo atributo) {
        this.atributo = atributo;
    }
    
}
