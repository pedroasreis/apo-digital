package Model;

import java.io.Serializable;

/**
 *
 * @author Pedro
 */
public class ConexaoComodo implements Serializable{
    
    private ComodoPlanta comodo1;
    private ComodoPlanta comodo2;
    
    public ConexaoComodo(ComodoPlanta comodo1, ComodoPlanta comodo2){
        this.comodo1 = comodo1;
        this.comodo2 = comodo2;
    }
    
    public ConexaoComodo(){
        
    }

    /**
     * @return the comodo1
     */
    public ComodoPlanta getComodo1() {
        return comodo1;
    }

    /**
     * @param comodo1 the comodo1 to set
     */
    public void setComodo1(ComodoPlanta comodo1) {
        this.comodo1 = comodo1;
    }

    /**
     * @return the comodo2
     */
    public ComodoPlanta getComodo2() {
        return comodo2;
    }

    /**
     * @param comodo2 the comodo2 to set
     */
    public void setComodo2(ComodoPlanta comodo2) {
        this.comodo2 = comodo2;
    }
    
}
