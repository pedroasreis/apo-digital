/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Pedro
 */
public class Planta {
    
    private int id;
    private Comodo comodo;
    private Morador morador;
    private String identificador;
    
    public Planta(){
        comodo = new Comodo();
        morador = new Morador();
    }
    
    public Planta(int id, int idComodo, int idMorador, String identificador){
        comodo = new Comodo();
        morador = new Morador();
        this.id = id;
        comodo.setId(idComodo);
        morador.setId(idMorador);
        this.identificador = identificador;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the comodo
     */
    public Comodo getComodo() {
        return comodo;
    }

    /**
     * @param comodo the comodo to set
     */
    public void setComodo(Comodo comodo) {
        this.comodo = comodo;
    }

    /**
     * @return the morador
     */
    public Morador getMorador() {
        return morador;
    }

    /**
     * @param morador the morador to set
     */
    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    /**
     * @return the identificador
     */
    public String getIdentificador() {
        return identificador;
    }

    /**
     * @param identificador the identificador to set
     */
    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }
    
}
