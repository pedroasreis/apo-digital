/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.io.Serializable;

/**
 *
 * @author pedro
 */
public class Resposta implements Serializable {

    private int id;
    private String texto;
    private Pergunta pergunta;
    private APO apo;
    
    private Comodo comodo;
    private Comodo comodoInstancia;
    private Conceito conceito;
    private Atributo atributo;
    private Morador morador;

    public Resposta(){
        //pergunta = new Pergunta();
        apo = new APO();
        comodo = new Comodo();
        conceito = new Conceito();
        atributo = new Atributo();
        comodoInstancia = new Comodo();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     * @return the pergunta
     */
    public Pergunta getPergunta() {
        return pergunta;
    }

    /**
     * @param pergunta the pergunta to set
     */
    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    /**
     * @return the apo
     */
    public APO getApo() {
        return apo;
    }

    /**
     * @param apo the apo to set
     */
    public void setApo(APO apo) {
        this.apo = apo;
    }

    /**
     * @return the comodo
     */
    public Comodo getComodo() {
        return comodo;
    }

    /**
     * @param comodo the comodo to set
     */
    public void setComodo(Comodo comodo) {
        this.comodo = comodo;
    }

    /**
     * @return the conceito
     */
    public Conceito getConceito() {
        return conceito;
    }

    /**
     * @param conceito the conceito to set
     */
    public void setConceito(Conceito conceito) {
        this.conceito = conceito;
    }

    /**
     * @return the atributo
     */
    public Atributo getAtributo() {
        return atributo;
    }

    /**
     * @param atributo the atributo to set
     */
    public void setAtributo(Atributo atributo) {
        this.atributo = atributo;
    }

    /**
     * @return the morador
     */
    public Morador getMorador() {
        return morador;
    }

    /**
     * @param morador the morador to set
     */
    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    /**
     * @return the comodoInstancia
     */
    public Comodo getComodoInstancia() {
        return comodoInstancia;
    }

    /**
     * @param comodoInstancia the comodoInstancia to set
     */
    public void setComodoInstancia(Comodo comodoInstancia) {
        this.comodoInstancia = comodoInstancia;
    }

}
