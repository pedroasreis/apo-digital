package Model;

import java.io.Serializable;
import java.util.ArrayList;
import org.primefaces.model.chart.CartesianChartModel;

/**
 * Classe responsável por encapsular uma lista de perguntas com os mesmos
 * qualificadores
 * @author Pedro
 */
public class PerguntaAgrupada implements Serializable {
    
    private ArrayList<Pergunta> perguntas;
    private CartesianChartModel barraAgrupada; // estrutura usada para a exibição do gráfico de barras
    private String label; // apenas um label para identificação do conjunto
    
    private int larguraGrafico;
    
    private int quantQualificadores;
    
    public static final int LARGURA = 150;
    public static final int ALTURA = 50;
    
    public PerguntaAgrupada(){
        perguntas = new ArrayList<Pergunta>();
        barraAgrupada = new CartesianChartModel();
    }

    /**
     * @return the perguntas
     */
    public ArrayList<Pergunta> getPerguntas() {
        return perguntas;
    }

    /**
     * @param perguntas the perguntas to set
     */
    public void setPerguntas(ArrayList<Pergunta> perguntas) {
        this.perguntas = perguntas;
    }

    /**
     * @return the barraAgrupada
     */
    public CartesianChartModel getBarraAgrupada() {
        return barraAgrupada;
    }

    /**
     * @param barraAgrupada the barraAgrupada to set
     */
    public void setBarraAgrupada(CartesianChartModel barraAgrupada) {
        this.barraAgrupada = barraAgrupada;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the larguraGrafico
     */
    public int getLarguraGrafico() {
        return larguraGrafico;
    }

    /**
     * @param larguraGrafico the larguraGrafico to set
     */
    public void setLarguraGrafico(int larguraGrafico) {
        this.larguraGrafico = larguraGrafico;
    }

    /**
     * @return the nQualificadores
     */
    public int getQuantQualificadores() {
        return quantQualificadores;
    }

    /**
     * @param nQualificadores the nQualificadores to set
     */
    public void setQuantQualificadores(int quantQualificadores) {
        this.quantQualificadores = quantQualificadores;
    }
    
    
    
}
