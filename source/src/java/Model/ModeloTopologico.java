package Model;

import java.io.Serializable;

/**
 *
 * @author Pedro
 */
public class ModeloTopologico implements Serializable {
    
    private int id;
    private String descricao;
    private APO apo;
    
    public ModeloTopologico(){
        apo = new APO();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the apo
     */
    public APO getApo() {
        return apo;
    }

    /**
     * @param apo the apo to set
     */
    public void setApo(APO apo) {
        this.apo = apo;
    }
    
}
