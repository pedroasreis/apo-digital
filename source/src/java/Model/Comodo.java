/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class Comodo {
    
    private int id;
    private String nome;
    private Icone icone;
    private Integer tipo;
    
    private String resposta;
    private ArrayList<String> respostas;
    
    private Integer idQualificador;
    private ArrayList<String> idQualificadores;
    
    /*
     * Tipos de cômodos
     */
    public static final int EDIFICIO = 1;
    public static final int USO_COMUM = 2;
    public static final int UNIDADE = 3;
    
    public Comodo() {
        icone = new Icone();
        respostas = new ArrayList<String>();
        idQualificadores = new ArrayList<String>();
    }

    @Override
    public boolean equals(Object obj) {
        return (nome.equals(((Comodo)obj).getNome()));
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the icone
     */
    public Icone getIcone() {
        return icone;
    }

    /**
     * @param icone the icone to set
     */
    public void setIcone(Icone icone) {
        this.icone = icone;
    }

    /**
     * @return the tipo
     */
    public Integer getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the resposta
     */
    public String getResposta() {
        return resposta;
    }

    /**
     * @param resposta the resposta to set
     */
    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    /**
     * @return the respostas
     */
    public ArrayList<String> getRespostas() {
        return respostas;
    }

    /**
     * @param respostas the respostas to set
     */
    public void setRespostas(ArrayList<String> respostas) {
        this.respostas = respostas;
    }

    /**
     * @return the idQualificador
     */
    public Integer getIdQualificador() {
        return idQualificador;
    }

    /**
     * @param idQualificador the idQualificador to set
     */
    public void setIdQualificador(Integer idQualificador) {
        this.idQualificador = idQualificador;
    }

    /**
     * @return the idQualificadores
     */
    public ArrayList<String> getIdQualificadores() {
        return idQualificadores;
    }

    /**
     * @param idQualificadores the idQualificadores to set
     */
    public void setIdQualificadores(ArrayList<String> idQualificadores) {
        this.idQualificadores = idQualificadores;
    }
    
}
