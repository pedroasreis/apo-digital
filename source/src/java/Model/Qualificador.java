/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

/**
 *
 * @author pedro
 */
public class Qualificador {

    private int id;
    private String texto;
    private int inicio;
    private int fim;
    private int iconeId;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * @param texto the texto to set
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     * @return the inicio
     */
    public int getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    /**
     * @return the fim
     */
    public int getFim() {
        return fim;
    }

    /**
     * @param fim the fim to set
     */
    public void setFim(int fim) {
        this.fim = fim;
    }

    /**
     * @return the iconeId
     */
    public int getIconeId() {
        return iconeId;
    }

    /**
     * @param iconeId the iconeId to set
     */
    public void setIconeId(int iconeId) {
        this.iconeId = iconeId;
    }

}
