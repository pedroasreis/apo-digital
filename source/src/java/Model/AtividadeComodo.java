/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Pedro
 */
public class AtividadeComodo {
    
    private Morador morador;
    private Planta planta;
    private Atividade atividade;
    
    public AtividadeComodo(){
        morador = new Morador();
        planta = new Planta();
        atividade = new Atividade();
    }
    
    public AtividadeComodo(int idMorador, int idPlanta, int idAtividade){
        morador = new Morador();
        planta = new Planta();
        atividade = new Atividade();
        
        morador.setId(idMorador);
        planta.setId(idPlanta);
        atividade.setId(idAtividade);
    }

    /**
     * @return the morador
     */
    public Morador getMorador() {
        return morador;
    }

    /**
     * @param morador the morador to set
     */
    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    /**
     * @return the planta
     */
    public Planta getPlanta() {
        return planta;
    }

    /**
     * @param planta the planta to set
     */
    public void setPlanta(Planta planta) {
        this.planta = planta;
    }

    /**
     * @return the atividade
     */
    public Atividade getAtividade() {
        return atividade;
    }

    /**
     * @param atividade the atividade to set
     */
    public void setAtividade(Atividade atividade) {
        this.atividade = atividade;
    }
    
}
