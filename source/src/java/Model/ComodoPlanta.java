package Model;

import java.io.Serializable;

/**
 * Classe utilizada para tratar os cômodos criados pelo pesquisador
 * na criação do modelo topológico via web
 * @author Pedro
 */
public class ComodoPlanta implements Serializable{
    
    private int id;
    private String nome;
    
    private Comodo comodo;
    private ModeloTopologico modeloTopologico;
    
    private int idReal;
    
    public ComodoPlanta(int id, String nome){
        this.id = id;
        this.nome = nome;
        
        comodo = new Comodo();
        modeloTopologico = new ModeloTopologico();
    }
    
    public ComodoPlanta(){
        comodo = new Comodo();
    }

    @Override
    public boolean equals(Object obj) {
        return (id == ((ComodoPlanta)obj).getId());
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the comodo
     */
    public Comodo getComodo() {
        return comodo;
    }

    /**
     * @param comodo the comodo to set
     */
    public void setComodo(Comodo comodo) {
        this.comodo = comodo;
    }

    /**
     * @return the modeloTopologico
     */
    public ModeloTopologico getModeloTopologico() {
        return modeloTopologico;
    }

    /**
     * @param modeloTopologico the modeloTopologico to set
     */
    public void setModeloTopologico(ModeloTopologico modeloTopologico) {
        this.modeloTopologico = modeloTopologico;
    }

    /**
     * @return the idReal
     */
    public int getIdReal() {
        return idReal;
    }

    /**
     * @param idReal the idReal to set
     */
    public void setIdReal(int idReal) {
        this.idReal = idReal;
    }
    
}
