/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class Morador {
    
    private int id;
    private int edificioId;
    private String numApartamento;
    
    private String numero;
    private String bloco;
    
    private ArrayList<Pergunta> perguntas;
    private ArrayList<Resposta> respostas;
    
    private APO apo;
    
    public Morador(){
        apo = new APO();
        perguntas = new ArrayList<Pergunta>();
        respostas = new ArrayList<Resposta>();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the edificioId
     */
    public int getEdificioId() {
        return edificioId;
    }

    /**
     * @param edificioId the edificioId to set
     */
    public void setEdificioId(int edificioId) {
        this.edificioId = edificioId;
    }

    /**
     * @return the numApartamento
     */
    public String getNumApartamento() {
        return numApartamento;
    }

    /**
     * @param numApartamento the numApartamento to set
     */
    public void setNumApartamento(String numApartamento) {
        this.numApartamento = numApartamento;
    }

    /**
     * @return the apo
     */
    public APO getApo() {
        return apo;
    }

    /**
     * @param apo the apo to set
     */
    public void setApo(APO apo) {
        this.apo = apo;
    }

    /**
     * @return the perguntas
     */
    public ArrayList<Pergunta> getPerguntas() {
        return perguntas;
    }

    /**
     * @param perguntas the perguntas to set
     */
    public void setPerguntas(ArrayList<Pergunta> perguntas) {
        this.perguntas = perguntas;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the bloco
     */
    public String getBloco() {
        return bloco;
    }

    /**
     * @param bloco the bloco to set
     */
    public void setBloco(String bloco) {
        this.bloco = bloco;
    }

    /**
     * @return the respostas
     */
    public ArrayList<Resposta> getRespostas() {
        return respostas;
    }

    /**
     * @param respostas the respostas to set
     */
    public void setRespostas(ArrayList<Resposta> respostas) {
        this.respostas = respostas;
    }
    
}
