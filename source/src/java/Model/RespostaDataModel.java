/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Pedro
 */
public class RespostaDataModel extends ListDataModel<Resposta> implements SelectableDataModel<Resposta> {

    public RespostaDataModel() {
    }

    public RespostaDataModel(List<Resposta> data) {
        super(data);
    }

    @Override
    public Resposta getRowData(String rowKey) {
        List<Resposta> respostas = (List<Resposta>)getWrappedData();

        for (Resposta resposta : respostas) {
            if((resposta.getId() + "").equals(rowKey)) {
                return resposta;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Resposta resposta) {
        return resposta.getId() + "";
    }
    
}
