/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class Categoria
{

   private int id;
   private String texto;
   private String nome;
   private ArrayList<Pergunta> perguntas;
   private ArrayList<Conceito> conceitos;
   private Questionario questionario;

   public Categoria()
   {
      perguntas = new ArrayList<Pergunta>();
      conceitos = new ArrayList<Conceito>();
      questionario = new Questionario();
   }

   @Override
   public boolean equals(Object obj)
   {
      return (id == ((Categoria) obj).getId());
   }

   @Override
   public int hashCode()
   {
      int hash = 7;
      hash = 59 * hash + this.id;
      return hash;
   }

   /**
    * @return the id
    */
   public int getId()
   {
      return id;
   }

   /**
    * @param id the id to set
    */
   public void setId(int id)
   {
      this.id = id;
   }

   /**
    * @return the texto
    */
   public String getTexto()
   {
      return texto;
   }

   /**
    * @param texto the texto to set
    */
   public void setTexto(String texto)
   {
      this.texto = texto;
   }

   /**
    * @return the nome
    */
   public String getNome()
   {
      return nome;
   }

   /**
    * @param nome the nome to set
    */
   public void setNome(String nome)
   {
      this.nome = nome;
   }

   /**
    * @return the perguntas
    */
   public ArrayList<Pergunta> getPerguntas()
   {
      return perguntas;
   }

   /**
    * @param perguntas the perguntas to set
    */
   public void setPerguntas(ArrayList<Pergunta> perguntas)
   {
      this.perguntas = perguntas;
   }

   /**
    * @return the conceitos
    */
   public ArrayList<Conceito> getConceitos()
   {
      return conceitos;
   }

   /**
    * @param conceitos the conceitos to set
    */
   public void setConceitos(ArrayList<Conceito> conceitos)
   {
      this.conceitos = conceitos;
   }

   /**
    * @return the questionario
    */
   public Questionario getQuestionario()
   {
      return questionario;
   }

   /**
    * @param questionario the questionario to set
    */
   public void setQuestionario(Questionario questionario)
   {
      this.questionario = questionario;
   }
}
