/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Pedro
 */
public class Atividade {
    
    private int id;
    private String nome;
    private SuperAtividade superAtividade;
    
    public Atividade(){
        superAtividade = new SuperAtividade();
    }
    
    public Atividade(int id, String nome, int idSuperAtividade){
        superAtividade = new SuperAtividade();
        this.id = id;
        this.nome = nome;
        superAtividade.setId(idSuperAtividade);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the superAtividade
     */
    public SuperAtividade getSuperAtividade() {
        return superAtividade;
    }

    /**
     * @param superAtividade the superAtividade to set
     */
    public void setSuperAtividade(SuperAtividade superAtividade) {
        this.superAtividade = superAtividade;
    }
    
}
