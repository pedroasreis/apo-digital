create table planta(
	id integer autoincrement,
	id_comodo integer,
	id_morador integer,
	identificador varchar(45),
	primary_key(id),
	foreign key id_comodo references comodo(id),
	foreign key id_morador references morador(id)
);

create table planta_conexao(
	id_comodo1 integer,
	id_comodo2 integer
	primary_key(id_comodo1, id_comodo2),
	foreign key id_comodo1 references planta(id),
	foreign key id_comodo2 references planta(id)
);

alter table resposta add instancia_comodo integer;

/* A��es a serem tomadas para consumar as modifica��es acima */

/* Alterar subsistema de migra��o de dados (pendente) */
/* Alterar banco de dados principal e seu script (ok) */
/* Modificar aplica��o Unity (ok) */
